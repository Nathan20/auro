const electron = require('electron');
const {ipcRenderer} = electron;
const { dialog } = require('electron').remote;
const fs = require('fs');
const Store = require('./modules/store.js');

const pathtemplate = document.querySelector('#pathtemplate');
const pathablage = document.querySelector('#pathablage');

// Instantiate Store class
let store = new Store({
  // We'll call our data file 'user-preferences'
  configName: 'user-preferences',
  defaults: {
    // 800x600 is the default size of our window
    windowBounds: { width: 800, height: 600 },
  }
});

document.querySelector('#Submit').addEventListener("click",  submitForm);
document.querySelector('#Cancel').addEventListener("click",  cancelForm);


document.addEventListener('DOMContentLoaded', function() {

	pathtemplate.value = store.get('templatepath');

	pathablage.value = store.get('datapath');



}, false);
	


function submitForm(e){
	e.preventDefault();
	store.set('templatepath', pathtemplate.value);
	store.set('datapath', pathablage.value );
	
	ipcRenderer.send('opt:close', 'none');
	console.log("Send opt:close");
}
function cancelForm(e){
	e.preventDefault();
	ipcRenderer.send('opt:close', 'none');
	console.log("Send opt:close");
}