const electron = require('electron');
const { ipcRenderer } = electron;
let DM = new Object();

document.addEventListener('DOMContentLoaded', function () {
    ipcRenderer.send('workerWin:loaded', "0");
}, false);

ipcRenderer.on('calcData:start', function (e,msg) {
    DM = msg;
    console.log(DM);
    ipcRenderer.send('calcData:done', DM);
});
function calcData() {
	let zahlungseingangDM = DM.zeDM;
	let rechnungenDM = DM.rechnungenDM;
	let vertragDM = DM.vertragDM;
	let projekteDM = DM.projekteDM;
	let positionenDM = DM.positionenDM;
	let objekteDM = DM.objekteDM;

	let rechnungen = rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
	let vertraege = vertragDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
	let objekte = objekteDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
	// --------------------
	let summe_zahlungseingang = 0;
	if (typeof (rechnungen) != 'undefined' && rechnungen != 'undefined' && rechnungen.length > 0) {
		let len = rechnungen.length
		for (re = 0; re < len; re++) {
			summe_zahlungseingang = 0;
			if (typeof (zahlungseingangDM) != 'undefined' && zahlungseingangDM != 'undefined') {
				let zahlungseingang = zahlungseingangDM.filter(el => parseInt(el.rechnung) == parseInt(rechnungen[re].id));
				if (typeof (zahlungseingang) !== 'undefined' && zahlungseingang.length > 0) {
					for (let i = 0; i < zahlungseingang.length; i++) {
						summe_zahlungseingang += zahlungseingang[i].betrag;
					}
				}
				rechnungen[re].zahlungseingang = summe_zahlungseingang;

			}
			let rechnungbetrag = 0;
			let positionen = positionenDM.filter(el => el.rechnung == rechnungen[re].id);
			for (po = 0; po < positionen.length; po++) {
				rechnungbetrag += positionen[po].betrag;
			}
			rechnungen[re].betrag = rechnungbetrag;
		}
	}
	if (vertraege.length > 0) {
		for (ve = 0; ve < vertraege.length; ve++) {
			let projekt = projekteDM.find(el => parseInt(el.id) == parseInt(vertraege[ve].projekt));
			let offenerbetrag = 0, vertragbetrag = 0, totaloffen = 0, bezahlt = 0, totalbezahlt = 0, totalverschickt = 0;
			let positionen = positionenDM.filter(re => re.vertrag == vertraege[ve].id && re.status !== CONSTANTS.TRASH && re.status !== CONSTANTS.ARCHIV);
			positionen.sort((a, b) => {
				let numA = a.rechnung;
				let numB = b.rechnung;
				return numA - numB;
			});
			let aktrecnr = 0, recNew = false, aktbezahlt = 0;

			for (po = 0; po < positionen.length; po++) {
				vertragbetrag += positionen[po].betrag;
				if (aktrecnr == positionen[po].rechnung) {
					recNew = false;
				} else {
					recNew = true;
					aktrecnr = positionen[po].rechnung;
				}
				bezahlt = 0;
				offenerbetrag = positionen[po].betrag;
				verschickt = 0;
				let rechzupos = rechnungenDM.find(el => el.id == positionen[po].rechnung);
				if (typeof rechzupos != 'undefined') {
					switch (vertraege[ve].typ) {
						case CONSTANTS.GUTSCHRIFT:
							// Gutschriften
							if (typeof rechzupos.zahlungseingang !== 'undefined') {
								aktbezahlt = rechzupos.zahlungseingang;
							} else {
								aktbezahlt = 0;
							}
							bezahlt = aktbezahlt;
							offenerbetrag = positionen[po].betrag - aktbezahlt;
							verschickt = offenerbetrag;
							aktbezahlt = 0;
							break;
						case CONSTANTS.RESERVIERUNG:
							if (typeof rechzupos.zahlungseingang !== 'undefined') {
								bezahlt += rechzupos.zahlungseingang;
							}

							break;
						default:
							// Alles andere
							if (recNew == true) {
								if (typeof rechzupos.zahlungseingang !== 'undefined') {
									aktbezahlt = rechzupos.zahlungseingang;
								} else {
									aktbezahlt = 0;
								}
							}

							if (aktbezahlt != 0) {
								if (offenerbetrag >= 0) {
									if (offenerbetrag > aktbezahlt) {
										bezahlt = aktbezahlt;
										offenerbetrag = positionen[po].betrag - aktbezahlt;
										verschickt = offenerbetrag;
										aktbezahlt = 0;
									}
									if (offenerbetrag <= aktbezahlt) {
										bezahlt = offenerbetrag;
										verschickt = 0;
										aktbezahlt = aktbezahlt - offenerbetrag;
										offenerbetrag = 0;
									}
								} else {
									// Wenn der offene Betrag negativ ist handelt es sich um
									// einen Abzug. Dann wird das auf den aktuellen Betrag 
									// angerechnet
									bezahlt = offenerbetrag;
									aktbezahlt = aktbezahlt - offenerbetrag;
									offenerbetrag = 0;

								}
							} else {
								if (rechzupos.status == CONSTANTS.VERSCHICKT) {
									verschickt = offenerbetrag;
								}
							}
					}
				}

				totalbezahlt += bezahlt;
				totaloffen += offenerbetrag;
				totalverschickt += verschickt;

				positionen[po].offenerbetrag = offenerbetrag;
				positionen[po].bezahlt = bezahlt;
				positionen[po].verschickt = verschickt;
				if (typeof projekt == 'undefined' || projekt.bearbeiter == user) {
					//datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
				}
			}
			vertraege[ve].betrag = vertragbetrag;
			vertraege[ve].offenerbetrag = totaloffen;
			vertraege[ve].bezahlt = totalbezahlt;
			vertraege[ve].verschickt = totalverschickt;

			//	datenmodell.setIdOnly('Vertraege', vertraege[ve], vertraege[ve].id);

		}
	}

	// --------------------
	if (objekte.length > 0) {
		for (let i = 0; i < objekte.length; i++) {
			let projekt = projekteDM.find(el => parseInt(el.id) == parseInt(objekte[i].projekt));
			let kaufpreis = 0, whghaben = 0, rechoffen = 0, whgsoll = 0, mehrsoll = 0, mehrhaben = 0, gutschriften = 0, gutschriftenhaben = 0, reservierungbezahlt = 0, habengesamt = 0;
			vertraege = vertragDM.filter(el => parseInt(el.objekt) == parseInt(objekte[i].id) && el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
			if (typeof vertraege !== "undefined" && vertraege.length > 0) {
				for (let j = 0; j < vertraege.length; j++) {
					switch (parseInt(vertraege[j].typ)) {
						case CONSTANTS.KAUFVERTRAG:
							// Kaufvertrag
							kaufpreis += vertraege[j].betrag;
							whgsoll += vertraege[j].offenerbetrag;
							whghaben += vertraege[j].bezahlt;
							rechoffen += vertraege[j].verschickt;

							break;
						case CONSTANTS.MEHRKOSTEN:
							// Mehrkosten
							mehrsoll += vertraege[j].offenerbetrag;
							mehrhaben += vertraege[j].bezahlt;
							//rechoffen += vertraege[j].verschickt;
							break;
						case CONSTANTS.GUTSCHRIFT:
							// Mehrkosten
							gutschriften += vertraege[j].offenerbetrag;
							gutschriftenhaben += vertraege[j].bezahlt;
							break;
						case CONSTANTS.RESERVIERUNG:
							reservierungbezahlt += vertraege[j].bezahlt;
							break;
						default:
					}
				}
			} else {
				objekte[i].kunde = "";
			}
			if (kaufpreis == 0) {
				kaufpreis = objekte[i].betrag;
				whgsoll = objekte[i].betrag;
			}

			habengesamt = whghaben + mehrhaben - gutschriftenhaben;
			objekte[i].kaufpreis = kaufpreis;
			objekte[i].whgsoll = whgsoll;
			objekte[i].whghaben = whghaben;
			objekte[i].rechoffen = rechoffen;
			objekte[i].mehrsoll = mehrsoll;
			objekte[i].mehrhaben = mehrhaben;
			objekte[i].gutschriften = gutschriften;
			objekte[i].gutschriftenhaben = gutschriftenhaben;
			objekte[i].reservierungbezahlt = reservierungbezahlt;
			objekte[i].habengesamt = habengesamt;
			//datenmodell.setIdOnly('Objekte', objekte[i], objekte[i].id);

		}


	}
	// --------------------
	if (projekteDM.length > 0) {
		for (pr = 0; pr < projekteDM.length; pr++) {
			let kaufpreis = 0, whgsoll = 0, mehrsoll = 0, whghaben = 0, mehrhaben = 0, gutschriften = 0, gutschriftenhaben = 0, habengesamt = 0, rechoffen = 0;
			let objekte = objekteDM.filter(el => parseInt(el.projekt) == parseInt(projekteDM[pr].id));
			if (typeof objekte !== "undefined" && objekte.length > 0) {
				for (let i = 0; i < objekte.length; i++) {
					if (objekte[i].status !== CONSTANTS.TRASH && objekte[i].status !== CONSTANTS.ARCHIV) {
						kaufpreis += objekte[i].kaufpreis;
						whgsoll += objekte[i].whgsoll;
						whghaben += objekte[i].whghaben;
						rechoffen += objekte[i].rechoffen;
						mehrsoll += objekte[i].mehrsoll;
						mehrhaben += objekte[i].mehrhaben;
						gutschriften += objekte[i].gutschriften;
						gutschriftenhaben += objekte[i].gutschriftenhaben;
						habengesamt += objekte[i].habengesamt;
					}
				}
			}
			projekteDM[pr].kaufpreis = kaufpreis;
			projekteDM[pr].whgsoll = whgsoll;
			projekteDM[pr].whghaben = whghaben;
			projekteDM[pr].mehrsoll = mehrsoll;
			projekteDM[pr].mehrhaben = mehrhaben;
			projekteDM[pr].habengesamt = habengesamt;
			projekteDM[pr].rechoffen = rechoffen;
			projekteDM[pr].gutschriften = gutschriften;
			projekteDM[pr].gutschriftenhaben = gutschriftenhaben;
			//datenmodell.setIdOnly('Projekte', projekteDM[pr], projekteDM[pr].id);
		}
	}
}