const electron = require('electron');
const { ipcRenderer, remote } = electron;
const { dialog } = require('electron').remote;
const fs = require('fs');
//const Math = require('Math');
const path = require('path');

const Store = require('./modules/store.js');
const CONSTANTS = require('./modules/constants');
const base = require('./modules/base.js');
const { ById, convPreisField, formatBetrag, formatProzent, convProzentField } = base;
//
//
// uses: https://mindmup.github.io/editable-table/
//

document.addEventListener('DOMContentLoaded', function () {
    ipcRenderer.send('TemplateMABVWindow:loaded', "0");
}, false);
ipcRenderer.on('TemplateMABVWindow:start', function (e, datensatz) {
    // ----------- POSMABV --------------
    ById('datatyp').innerHTML = datensatz.name;

    ById('dpSelectBtn').innerHTML = datensatz.name;
    // console.log(datensatz);
    switch (datensatz.name) {
        case CONSTANTS.POSMABV:
            showTemplateMABV(datensatz.positionen);
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
            break;
        case CONSTANTS.POSRESERVIERUNG:
            showTemplateReservierung(datensatz.positionen);
            break;
        case CONSTANTS.POSTYP3:
            showTemplateTyp3(datensatz.positionen);
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();

            break;
        default:
            showTemplateGeneric(datensatz);
            showVorlagen(datensatz.vorlagen);


            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
            ById('tblVorlagen').removeEventListener('click', clickHandler);
            ById('tblVorlagen').addEventListener('click', clickHandler);


            // $('#tblVorlagen').editableTableWidget().numericInputExample().find('td:first').focus();

            break;

    }
    if (typeof datensatz.bezeichnung !== 'undefined') {
        ById('inpBezeichnung').value = datensatz.bezeichnung;
    } else {
        ById('inpBezeichnung').value = "";
    }
    if (typeof datensatz.name !== 'undefined') {
        ById('inpName').value = datensatz.name;
    } else {
        ById('inpName').value = "";
    }
    if (typeof datensatz.rechtyp !== 'undefined') {
        ById('inpRechTyp').value = datensatz.rechtyp;
    } else {
        ById('inpRechTyp').value = "";
    }
    if (typeof datensatz.zeile !== 'undefined') {
        ById('inpZeile').value = datensatz.zeile;
    } else {
        ById('inpZeile').value = "";
    }


    // -------------------------
});

function clickHandler(event){
    if (event.target.innerHTML == "Delete") {
        console.log("delete");
    }
    if (event.target.innerHTML == "Add") {
        parenttd = event.target.parentNode;
        found = parenttd.previousSibling;
        let text = "", pfad = "";
        while (found != null) {
            if (typeof found.classList != 'undefined') {
                if (found.classList.contains("vor-text")) {
                    trs = found.childNodes;
                    trs.forEach(tr => {
                        if (tr.nodeName == "INPUT") {
                            text = tr.value;
                        }
                    });
                }

                if (found.classList.contains("vor-pfad")) {
                    pfad = found.innerHTML;
                }
            }
            found = found.previousSibling;
        }
        if (text != "" && pfad != "") {
            let data = new Object();
            data.text = text;
            data.pfad = pfad;
            if (typeof datensatz.vorlagen != 'undefined') {
                datensatz.vorlagen.push(data)
            } else {
                let ds = new Array();
                ds.push(data);
                datensatz.vorlagen = ds;
            }

        }
        showVorlagen(datensatz.vorlagen);


    }
    if (event.target.classList.contains('vor-pfad')) {
        event.stopPropagation();
       // console.log(event.target );
        let dirname = path.dirname(event.target.innerHTML);
        let options = {
            title: "Vorlage auswählen",
            defaultPath: dirname,
            properties: ['openFile'],
            filters: [
                { name: 'docx', extensions: ['docx'] },
                { name: 'All Files', extensions: ['*'] }
            ]
        };
        let selectedPaths = dialog.showOpenDialogSync(options);
        if (typeof selectedPaths !== 'undefined') {
            let sp = selectedPaths[0];
            if (typeof remote.process.env["PORTABLE_EXECUTABLE_FILE"] != 'undefined') {
                let relativename = path.relative(remote.process.env["PORTABLE_EXECUTABLE_FILE"], path.dirname(sp));
                sp = path.join(relativename, path.basename(sp));
            }
            event.target.innerHTML = sp;
        }


    }
}
ipcRenderer.on('TemplateMABVWindow:selectdata', function (e, items) {
    dpSelectFill(items);
    dpInit();
});
ipcRenderer.on('TemplateMABVWindow:updateselectdata', function (e, items) {
    dpSelectFill(items);
});
ById('btnTemplateBodyCancel').addEventListener('click', function () {
    ipcRenderer.send('TemplateMABVWindow:close', "0");
});
ById('btnTemplateBodyOk').addEventListener('click', function () {
    getDatafromTable();
});
ById('btnTemplateBodyAddLine').addEventListener('click', function () {
    let msg = new Object();
    msg.typ = ById('datatyp').innerHTML;
    ipcRenderer.send('TemplateMABVWindow:addLine', msg);
});
ById('btnTemplateNew').addEventListener('click', function () {
    ipcRenderer.send('TemplateMABVWindow:TemplateNew', "0");
});
ById('btnTemplateDel').addEventListener('click', function () {
    ipcRenderer.send('TemplateMABVWindow:TemplateDel', ById('dpSelectBtn').innerHTML);
});
function showTemplateMABV(positionen) {
    ById('fldcheckbox').classList.add("is-hidden");
    ById('btnTemplateDel').classList.add("is-hidden");
    ById('mainTable').classList.remove("is-hidden");
    ById('fldBezeichnung').classList.remove("is-hidden");
    ById('idVorlagen').classList.add("is-hidden");
    ById('fldBetrag').classList.add("is-hidden");
    ById('btnTemplateBodyAddLine').classList.remove("is-hidden");
    ById('tblTemplateBody').innerHTML = "";
    ById('fldRechTyp').classList.add("is-hidden");
    ById('fldZeile').classList.add("is-hidden");

    ById('tblHeadRow').innerHTML = "<th style='width:5em'>Position</th><th>Beschreibung</th><th>Wert</th>";
    positionen.sort((a, b) => {
        let numA = parseInt(a.nr);
        let numB = parseInt(b.nr);
        return numA - numB;
    });

    for (i = 0; i < positionen.length; i++) {
        let tr = document.createElement('tr');
        let str = "";
        str += "<td class='pos-nr'>" + positionen[i].nr + "</td>";
        str += "<td class='pos-text'>" + positionen[i].text + "</td>";
        str += "<td class='pos-anteil'>" + formatProzent(positionen[i].anteil / 100) + "</td>";
        tr.innerHTML = str;
        ById('tblTemplateBody').appendChild(tr);
    }
}
function showTemplateReservierung(positionen) {
    ById('fldcheckbox').classList.add("is-hidden");
    ById('btnTemplateDel').classList.add("is-hidden");
    ById('fldBezeichnung').classList.add("is-hidden");
    ById('mainTable').classList.add("is-hidden");
    ById('fldBetrag').classList.remove("is-hidden");
    ById('idVorlagen').classList.add("is-hidden");
    ById('btnTemplateBodyAddLine').classList.add("is-hidden");
    ById('inpBetrag').value = formatBetrag(Math.abs(positionen[0].betrag));
    ById('fldRechTyp').classList.add("is-hidden");
    ById('fldZeile').classList.add("is-hidden");

}
function showTemplateTyp3(positionen) {
    ById('fldcheckbox').classList.add("is-hidden");
    ById('btnTemplateDel').classList.add("is-hidden");
    ById('mainTable').classList.remove("is-hidden");
    ById('fldBezeichnung').classList.remove("is-hidden");
    ById('fldBetrag').classList.add("is-hidden");
    ById('idVorlagen').classList.add("is-hidden");
    ById('fldRechTyp').classList.add("is-hidden");
    ById('fldZeile').classList.add("is-hidden");
    ById('btnTemplateBodyAddLine').classList.remove("is-hidden");
    ById('tblTemplateBody').innerHTML = "";

    ById('tblHeadRow').innerHTML = "<th style='width:5em'>Position</th><th style='width:70%'>Beschreibung</th><th class='has-text-right' style='width:20%'>Wert</th>";
    positionen.sort((a, b) => {
        let numA = parseInt(a.nr);
        let numB = parseInt(b.nr);
        return numA - numB;
    });

    for (i = 0; i < positionen.length; i++) {
        let tr = document.createElement('tr');
        let str = "";
        str += "<td class='pos-nr'>" + positionen[i].nr + "</td>";
        str += "<td class='pos-text'>" + positionen[i].text + "</td>";
        str += "<td class='pos-betrag has-text-right'>" + formatBetrag(positionen[i].betrag) + "</td>";
        tr.innerHTML = str;
        ById('tblTemplateBody').appendChild(tr);
    }
}
function showVorlagen(datensatz) {
    ById('tblVorlagen').innerHTML = "";
    ById('tblVorlagenHeadRow').innerHTML = "<th class='has-text-left'>Beschreibung</th><th class='has-text-left' style='width:70%'  >Pfad</th><th  style='width:2em'></th>";
    let zeile, td;
    if (typeof datensatz != 'undefined' && datensatz.length > 0) {
        datensatz.forEach(data => {
            zeile = document.createElement('tr');
            ById('tblVorlagen').appendChild(zeile);


            td = document.createElement('td');
            td.classList.add("vor-text");
            td.innerHTML = "<input class='input' type='text' value='" + data.text + "'> ";
            zeile.appendChild(td);

            td = document.createElement('td');
            td.classList.add("vor-pfad");
            td.innerHTML = data.pfad;
            zeile.appendChild(td);

            td = document.createElement('td');
            td.classList.add("vor-btn");
            td.innerHTML = "<a class='button is-small is-warning' href='#'>Delete</a> ";
            zeile.appendChild(td);
        });
    }
    // -----------------

    zeile = document.createElement('tr');
    ById('tblVorlagen').appendChild(zeile);

    td = document.createElement('td');
    td.classList.add("vor-text");
    td.innerHTML = "<input class='input' type='text' placeholder='Neue Wordvorlage anlegen'> ";
    zeile.appendChild(td);

    td = document.createElement('td');
    td.classList.add("vor-pfad");
    td.innerHTML = "Vorlage auswählen - hier klicken";
    zeile.appendChild(td);

    td = document.createElement('td');
    td.classList.add("vor-btn");
    td.innerHTML = "<a class='button is-small is-primary' href='#'>Add</a> ";
    zeile.appendChild(td);

}

function showTemplateGeneric(datensatz) {
    ById('fldcheckbox').classList.remove("is-hidden");
    ById('btnTemplateDel').classList.remove("is-hidden");
    ById('mainTable').classList.remove("is-hidden");
    ById('fldBezeichnung').classList.remove("is-hidden");
    ById('fldBetrag').classList.add("is-hidden");
    ById('idVorlagen').classList.remove("is-hidden");
    ById('fldRechTyp').classList.remove("is-hidden");
    ById('fldZeile').classList.remove("is-hidden");
    ById('btnTemplateBodyAddLine').classList.remove("is-hidden");
    ById('tblTemplateBody').innerHTML = "";
    ById('tblHeadRow').innerHTML = "<th class='has-text-right' style='width:5em'>Position</th><th class='has-text-left' style='width:70%' >Beschreibung</th><th class='has-text-right' style='width:12%'>Faktor</th><th class='has-text-right' style='width:12%'>Anteil</th><th class='has-text-right' style='width:20%'>Betrag</th><th class='has-text-right' style='width:12%'>MwSt</th>";
    if (datensatz.aktiv == true) {
        ById('fldaktiv').checked = true;
    }
    if (datensatz.bezug == true) {
        ById('fldbezug').checked = true;
    }
    datensatz.positionen.sort((a, b) => {
        let numA = parseInt(a.nr);
        let numB = parseInt(b.nr);
        return numA - numB;
    });

    for (i = 0; i < datensatz.positionen.length; i++) {
        let tr = document.createElement('tr');
        let str = "";
        str += "<td class='pos-nr'>" + datensatz.positionen[i].nr + "</td>";
        str += "<td class='pos-text'>" + datensatz.positionen[i].text + "</td>";
       let faktor = typeof datensatz.positionen[i].faktor != 'undefined' ? datensatz.positionen[i].faktor:1;
        str += "<td class='pos-faktor has-text-right'> " + faktor + "</td>";
        str += "<td class='pos-anteil has-text-right'>" + formatProzent(datensatz.positionen[i].anteil / 100) + "</td>";
        str += "<td class='pos-betrag has-text-right'>" + formatBetrag(datensatz.positionen[i].betrag) + "</td>";
        str += "<td class='pos-mwst has-text-right'>" + formatProzent(datensatz.positionen[i].mwst / 100) + "</td>";
        tr.innerHTML = str;
        ById('tblTemplateBody').appendChild(tr);
    }
}
function getDatafromTable() {
    let msg = new Object();
    msg.typ = ById('datatyp').innerHTML;
    let daten = new Array();
    let vorlagen = new Array();
    let trs;

    switch (msg.typ) {
        case CONSTANTS.POSMABV:
            msg.bezeichnung = ById('inpBezeichnung').value;
            msg.daten = daten;
            trs = ById('tblTemplateBody').childNodes;
            trs.forEach(tr => {
                let datensatz = new Object();
                let tds = tr.childNodes;
                tds.forEach(td => {
                    if (td.classList.contains('pos-nr')) {
                        datensatz.nr = td.innerHTML;
                    }
                    if (td.classList.contains('pos-text')) {
                        datensatz.text = td.innerHTML;
                    }
                    if (td.classList.contains('pos-anteil')) {
                        datensatz.anteil = convProzentField(td.innerHTML);
                    }
                    if (td.classList.contains('pos-faktor')) {
                        datensatz.faktor = td.innerHTML;
                    }
                });
                if (datensatz.nr != "") {
                    daten.push(datensatz);
                }
                //console.log(tr);
            });
            break;
        case CONSTANTS.POSRESERVIERUNG:
            msg.betrag = convPreisField(ById('inpBetrag').value);
            break;
        case CONSTANTS.POSTYP3:
            msg.bezeichnung = ById('inpBezeichnung').value;
            msg.daten = daten;
            trs = ById('tblTemplateBody').childNodes;
            trs.forEach(tr => {
                let datensatz = new Object();
                let tds = tr.childNodes;
                tds.forEach(td => {
                    if (td.classList.contains('pos-nr')) {
                        datensatz.nr = td.innerHTML;
                    }
                    if (td.classList.contains('pos-text')) {
                        datensatz.text = td.innerHTML;
                    }
                    if (td.classList.contains('pos-betrag')) {
                        datensatz.betrag = convPreisField(td.innerHTML);
                    }
                });
                if (datensatz.nr != "") {
                    daten.push(datensatz);
                }
                //console.log(tr);
            });
            break;
        default:
            msg.name = ById('inpName').value;
            msg.bezeichnung = ById('inpBezeichnung').value;
            msg.rechtyp = ById('inpRechTyp').value;
            msg.zeile = ById('inpZeile').value;
            msg.daten = daten;
            msg.vorlagen = vorlagen;
            msg.aktiv = ById('fldaktiv').checked;
            msg.bezug = ById('fldbezug').checked;
            trs = ById('tblTemplateBody').childNodes;
            trs.forEach(tr => {
                let datensatz = new Object();
                let tds = tr.childNodes;
                tds.forEach(td => {
                    if (td.classList.contains('pos-nr')) {
                        datensatz.nr = td.innerHTML;
                    }
                    if (td.classList.contains('pos-text')) {
                        datensatz.text = td.innerHTML;
                    }
                    if (td.classList.contains('pos-anteil')) {
                        datensatz.anteil = convProzentField(td.innerHTML);
                    }
                    if (td.classList.contains('pos-faktor')) {
                        datensatz.faktor = parseFloat(td.innerHTML.replace(",", "."));
                    }
                    if (td.classList.contains('pos-mwst')) {
                        datensatz.mwst = convProzentField(td.innerHTML);
                    }
                    if (td.classList.contains('pos-betrag')) {
                        datensatz.betrag = convPreisField(td.innerHTML);
                    }
                });
                if (datensatz.nr != "") {
                    daten.push(datensatz);
                }
                //console.log(tr);
            });

            // die wordvorlagen auslesen
            trs = ById('tblVorlagen').childNodes;
            trs.forEach(tr => {
                let datensatz = new Object();
                let tds = tr.childNodes;
                datensatz.text = "";
                tds.forEach(td => {
                    if (td.nodeName == "TD") {
                        if (td.classList.contains('vor-text')) {
                            cns = td.childNodes;
                            cns.forEach(cn => {
                                if (cn.nodeName == "INPUT") {
                                    datensatz.text = cn.value;
                                }
                            });
                        }
                        if (td.classList.contains('vor-pfad')) {
                            datensatz.pfad = td.innerHTML;
                        }

                    }
                });
                if (datensatz.text != "") {
                    vorlagen.push(datensatz);
                }

            });
            msg.vorlagen = vorlagen;

    }

    ipcRenderer.send('TemplateMABVWindow:update', msg);

}
function dpSelectFill(items) {
    ById('dpSelectContent').innerHTML = "";
    for (i = 0; i < items.length; i++) {
        el1 = document.createElement('div');
        el1.className = 'dropdown-item';
        el1.id = items[i].name;
        el1.innerHTML = items[i].name;
        ById('dpSelectContent').appendChild(el1);
    }
    ById('dpSelect').addEventListener('click', function (event) {
        //console.log(event.target.id, event.target.classList);
        if (event.target.classList.contains('dropdown-item')) {
            ById('dpSelectBtn').innerHTML = event.target.id;
            ipcRenderer.send('TemplateMABVWindow:mode', event.target.id);
        }
    });


}
function dpInit() {
    // Dropdowns

    var $dropdowns = getAll('.dropdown:not(.is-hoverable)');

    if ($dropdowns.length > 0) {
        $dropdowns.forEach(function ($el) {



            $el.addEventListener('click', function (event) {
                event.stopPropagation();
                $el.classList.toggle('is-active');
            });
        });

        document.addEventListener('click', function (event) {
            closeDropdowns();
        });
    }

    function closeDropdowns() {
        $dropdowns.forEach(function ($el) {
            $el.classList.remove('is-active');
        });
    }

    // Close dropdowns if ESC pressed
    document.addEventListener('keydown', function (event) {
        var e = event || window.event;
        if (e.keyCode === 27) {
            closeDropdowns();
        }
    });

    // Functions

    function getAll(selector) {
        return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
    }


};
