const electron = require('electron');
const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');
const path = require('path');

// http://javascript-ninja.fr/docxgenjs/examples/demo.html
// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
exports.replaceErrors = function (key, value) {
  if (value instanceof Error) {
    return Object.getOwnPropertyNames(value).reduce(function (error, key) {
      error[key] = value[key];
      return error;
    }, {});
  }
  return value;
};

exports.errorHandler = function (error) {
  console.log(JSON.stringify({ error: error }, replaceErrors));

  if (error.properties && error.properties.errors instanceof Array) {
    const errorMessages = error.properties.errors.map(function (error) {
      return error.properties.explanation;
    }).join('\n');
    console.log('errorMessages', errorMessages);
    // errorMessages is a humanly readable message looking like this :
    // 'The tag beginning with "foobar" is unopened'
  }
  throw error;
};

exports.genBrief = function (filename) {
  //console.log(objbrief);
  // Load the docx file as a binary
  // Pfad der Datei ist in __dirname
  // var content = fs.readFileSync(path.resolve(__dirname, 'invoice.docx'), 'binary');
  //const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  let store = new Store({
    // We'll call our data file 'user-preferences'
    configName: 'user-preferences',
    defaults: {}
  });

  let userDataPath = store.get('templatepath');
  let wordtemplate = store.get('wordtemplate');


  try {
    if (typeof wordtemplate != 'undefined' && wordtemplate != 'undefined' && fs.existsSync(wordtemplate)) {
      //console.log('Found file');
      var content = fs.readFileSync(wordtemplate, 'binary');
    } else {
      var content = fs.readFileSync(path.resolve(userDataPath, 'Vorlage_Rechnung.docx'), 'binary')
    }
  } catch (error) {
    errorHandler(error);
    return;
  }

  var zip = new PizZip(content);
  var doc;
  try {
    doc = new Docxtemplater(zip);
  } catch (error) {
    // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
    errorHandler(error);
    return;

  }

  // set the templateVariables
  doc.setData({
    vorname: objbrief.vorname,
    name: objbrief.name,
    phone: objbrief.telefon,
    strasse: objbrief.strasse,
    plz: objbrief.plz,
    ort: objbrief.ort,
    rechnr: objbrief.rechnr,
    rechdat: objbrief.rechdat,
    betreff: objbrief.betreff,
    positionen: objbrief.positionen,
    betrag: objbrief.betrag

  });


  try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render();
  } catch (error) {
    // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    errorHandler(error);
    return;

  }

  var buf = doc.getZip()
    .generate({ type: 'nodebuffer' });

  // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
  fs.writeFileSync(filename, buf);
};


exports.createBrief = function (filename, wordtemplate, data) {
  let store = new Store({
    // We'll call our data file 'user-preferences'
    configName: 'user-preferences',
    defaults: {}
  });



  // let wordtemplate = store.get(template);

  if (!fs.existsSync(wordtemplate)) {
    let options = {
      buttons: ["Ok"],
      message: "Vorlage nicht vorhanden!",
      title: "Fehler",
      detail: "Die Vorlage für diesen Brief ist nicht vorhanden.\n Bitte mit Rechtsklick auf den Button eine Vorlage auswählen.",
      type: "error"
    };
    dialog.showMessageBoxSync(options);
    return;
  }
  try {
    var content = fs.readFileSync(wordtemplate, 'binary');
  } catch (error) {
    errorHandler(error);
    return;
  }

  var zip = new PizZip(content);
  var doc;
  try {
    doc = new Docxtemplater(zip, {
      linebreaks: true

    });
  } catch (error) {
    // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
    errorHandler(error);
    return;
  }

  poslist = data.positionen;
  if (typeof poslist != 'undefined') {
    poslist.forEach(function (pos) {
      pos.text= pos.text.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
      var lines = pos.text.split("\n");
      var pre = "<w:p><w:r><w:t>";
      var post = "</w:t></w:r></w:p>";
      var lineBreak = "<w:br/>";
      pos.text = pre + lines.join(lineBreak) + post;
      pos.bmgBetrag = pos.bmgbetrag;

    })
  }

  // set the templateVariables
  doc.setData({
    //Kundendaten uas Vertrag  & Rechnung
    RECanrede: checkValue(data.rechanrede),
    RECvorname: checkValue(data.rechvorname),
    RECname: checkValue(data.rechname),
    RECtelefon: checkValue(data.rechtelefon),
    RECstrasse: checkValue(data.rechstrasse),
    RECplz: checkValue(data.rechplz),
    RECort: checkValue(data.rechort),
    RECemail: checkValue(data.rechemail),
    RECund: data.rechund,
    RECanredezeile1: data.anredezeile1,
    RECanredezeile2: data.anredezeile2,
    RECanrede2: checkValue(data.rechanrede2),
    RECvorname2: checkValue(data.rechvorname2),
    RECname2: checkValue(data.rechname2),
    RECtelefon2: checkValue(data.rechtelefon2),
    RECstrasse2: checkValue(data.rechstrasse2),
    RECplz2: checkValue(data.rechplz2),
    RECort2: checkValue(data.rechort2),
    RECemail2: checkValue(data.rechemail2),
    RECtelefon3: checkValue(data.rechtelefon3),
    //Rechnungsdaten
    rechnr: checkValue(data.rechnr),
    rechdat: checkValue(data.rechdat),
    faelligkeit: checkValue(data.faelligdat),
    leistungszeitraum: checkValue(data.leistungszeitraum),
    m1dat: checkValue(data.m1dat),
    m2dat: checkValue(data.m2dat),
    m3dat: checkValue(data.m3dat),
    mahnstufe: checkValue(data.mahnstufe),
    kostenstelle: checkValue(data.kostenstelle),
    objnr: checkValue(data.objektnr),
    projektnr: checkValue(data.objektnr),
    prj: checkValue(data.txtprj),
    projekt: checkValue(data.txtprj),
    beschreibung: checkValue(data.txtbeschreibung),
    anschrift: checkValue(data.txtbeschreibung),
    objekt: checkValue(data.txtobj),
    kunde: checkValue(data.txtkunde),
    kaeufer: checkValue(data.txtkunde),
    mieter: checkValue(data.txtkunde),

    betrag: checkValue(data.betrag),
    brutto: checkValue(data.betrag),
    netto: checkValue(data.netto),
    mwst: checkValue(data.mwst),
    betreff: checkValue(data.betreff),

    //Rechnungspositionen: rechpos, text, beschreibung, betrag
    positionen_alt: data.positionen,
    positionen: poslist,

    //Daten zum Käufer
    OBJanrede: checkValue(data.kanrede),
    OBJvorname: checkValue(data.kvorname),
    OBJname: checkValue(data.kname),
    OBJtelefon: checkValue(data.ktelefon),
    OBJstrasse: checkValue(data.kstrasse),
    OBJplz: checkValue(data.kplz),
    OBJort: checkValue(data.kort),
    OBJemail: checkValue(data.kemail),
    OBJund: checkValue(data.kund),
    OBJanrede2: checkValue(data.kanrede2),
    OBJvorname2: checkValue(data.kvorname2),
    OBJname2: checkValue(data.kname2),
    OBJtelefon2: checkValue(data.ktelefon2),
    OBJstrasse2: checkValue(data.kstrasse2),
    OBJplz2: checkValue(data.kplz2),
    OBJort2: checkValue(data.kort2),
    OBJemail2: checkValue(data.kemail2),
    OBJtelefon3: checkValue(data.ktelefon3),

    //Ratenliste: text, anteil, betrag, offenerbetrag, bezahlt
    //gestellt, rechnr
    alleraten: data.alleraten,
    //Summen zu den Raten
    ratenoffen: data.totaloffen,
    ratenbetrag: data.totalbetrag,
    ratenbezahlt: data.totalbezahlt,
    ratengestellt: data.totalgestellt,

    //Vertragsdaten
    urkundenr: checkValue(data.urkundenr),
    urkundedatum: checkValue(data.urkundedatum),
    notariat: checkValue(data.notariat),
    fertig1datum: checkValue(data.fertig1datum),
    fertig2datum: checkValue(data.fertig2datum),
    uebergabedatum: checkValue(data.uebergabedatum),
    vertragsnr: checkValue(data.vertragsnr),
    vertragbetrag: checkValue(data.vertragbetrag),

    //Daten zur Einheit und zum Projekt
    einheit: checkValue(data.einheit),
    einheitnr: checkValue(data.objnummer),
    kaufpreis: checkValue(data.kaufpreis),
    prjname: checkValue(data.prjname),
    prjbeschreibung: checkValue(data.prjbeschreibung),
    bauleiter: checkValue(data.bauleiter),
    prjnummer: checkValue(data.prjnummer),
    prjbautraeger: checkValue(data.bautraeger),
    bank: checkValue(data.bank),
    ibana: checkValue(data.ibana),
    ibane: checkValue(data.ibane),
    bic: checkValue(data.bic),

    //Allgemein
    datum: data.datum,

    //Projektdaten
    prjpos: data.prjpos,
    possum: data.posSum,

    dar: data.darlehen,
    vertragsgegenstand: data.vertragsgegenstand,

    //Architektenverträge
    vertraege: data.vertraege,
    verSum: data.vertraegeSum,
    vermwst: data.vertraegmwst,

  });

  try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render();
  } catch (error) {
    // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    errorHandler(error);
    return;

  }
  var buf = doc.getZip()
    .generate({ type: 'nodebuffer' });

  // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
  fs.writeFileSync(filename, buf);
};


function errorHandler(message) {
  let options = {
    buttons: ["Ok"],
    message: message.name,
    title: "Fehlermeldung",
    detail: message.stack,
    icon: "./assets/icons/png/bandage.png"
  };
  dialog.showMessageBoxSync(options);

}
function checkValue(el) {
  if (typeof el != 'undefined' && el != "") {
    return el;
  } else {
    return "";
  }
}
