$.fn.provtableinput = function () {
	'use strict';
	var element = $(this),
		dataRows = element.find('tbody tr');
		

	element.find('td').on('change', function (evt) {
		var cell = $(this),
			column = cell.index();
			ById('foundinfo').innerHTML="";
		if (cell[0].classList.contains('uneditable')) {
			return false;
		}
		if (cell[0].classList.contains("pro-ma")) {
			let kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.mitarbeiter == true);
			let kun = kunden.filter(el => {
				let result = 0;
				if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(cell[0].innerHTML.toLowerCase()) >= 0 ? 1 : 0; }
				if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(cell[0].innerHTML.toLowerCase()) >= 0 ? 1 : 0; }
				if (typeof el.vorname != 'undefined' && typeof el.name != 'undefined') { result += String(el.vorname + " " + el.name).toLowerCase().indexOf(cell[0].innerHTML.toLowerCase()) >= 0 ? 1 : 0; }
				return result;
			});
			if (kun.length == 1) {
				cell[0].innerHTML = kun[0].vorname + " " + kun[0].name;
			}
		}
	}).on('validate', function (evt, value) {
		var cell = $(this),
			column = cell.index();
		if (cell[0].classList.contains("pro-anteil")) {
			if (isNaN(parseInt(value))) {
				return false;
			}
		}
		if (cell[0].classList.contains("pro-betrag")) {
			if (isNaN(parseInt(value))) {
				return false;
			}
		}
		if (cell[0].classList.contains("pro-ma")) {
			ById('foundinfo').innerHTML="";

			let kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.mitarbeiter == true);
			let kun = kunden.filter(el => {
				let result = 0;
				if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(value.toLowerCase()) >= 0 ? 1 : 0; }
				if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(value.toLowerCase()) >= 0 ? 1 : 0; }
				if (typeof el.vorname != 'undefined' && typeof el.name != 'undefined') { 
					result += String(el.vorname + " " + el.name).toLowerCase().indexOf(value.toLowerCase()) >= 0 ? 1 : 0; }
				return result;
			});

			if (kun.length == 1) {
				value = kun[0].vorname + " " + kun[0].name;
				ById('foundinfo').innerHTML="Gefunden: "+ value;
			}

		}

		return;
	});
	return this;
};
