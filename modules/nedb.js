const { app } = require('electron');
const Datastore = require('nedb');

function dbFactory(fileName) {
    return new Datastore({
        filename: `${process.env.NODE_ENV === 'dev' ? '.' : app.getPath('userData')}/data/${fileName}`,
        timestampData: true,
        autoload: true
    });
}


const db = {
    kunden: dbFactory('kunden.db'),
    projekte: dbFactory('projekte.db')
};

module.exports = db;

