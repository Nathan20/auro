
const path = require('path');
const fs = require('fs');

exports.ById = function (id) {
  return window.document.getElementById(id);
};

exports.formatBetrag = function (number) {
  return Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number)
};
exports.formatProzent = function (number) {
  return Intl.NumberFormat('de-DE', { style: 'percent', minimumFractionDigits: 2 }).format(number)
};

exports.formatProzent = function (number) {
  return Intl.NumberFormat('de-DE', { style: 'percent', minimumFractionDigits: 2 }).format(number)
};

exports.formatNumber = function (number) {
  return Intl.NumberFormat('de-DE', { style: 'decimal', minimumFractionDigits: 2 }).format(number)
};

exports.convProzentField = function(string) {
  let temp1 = string.replace(".", "");
  temp1 = temp1.replace("%", "");
  return parseFloat(temp1.replace(",", "."))
};


exports.convPreisField = function (string) {
  let temp1 = string.replace(".", "");
  return parseFloat(temp1.replace(",", "."))
};

exports.stripString = function (string) {
  let temp1 = string.replace("/", "");
  temp1 = temp1.replace("\\", "");
  return temp1;
};

exports.removeSpace = function (string){
  //string.split('').map(x => console.log((x.charCodeAt(0))))
  let temp1 = string.replace(/\xa0/g, ' ');
  temp1 = temp1.replace(/\u202f/g, ' ');
  temp1 = temp1.replace(" €", "");
  temp1 = temp1.replace(" %", "");
  return temp1;


}

exports.formatIBAN = function (string) {
  let newstring = "";
  let count = 0;
  for (let c of string) {
    if (c != " ") {
      newstring = newstring + c;
      count++
      if (count % 4 == 0) {
        newstring = newstring + " ";
      }
    }
  }

  return newstring;
}
exports.formatDatum = function (string) {
  if (typeof string != 'undefined' && string != 'undefined') {
    if (string == 'h') {
      return getDatum(0);
    } else {
      let d = string.split('.');
      if (d.length == 3) {
        if (parseInt(d[0]) < 10) {
          d[0] = "0" + parseInt(d[0]);
        }
        if (parseInt(d[1]) < 10) {
          d[1] = "0" + parseInt(d[1]);
        }
        if (d[2] < 1900) {
          d[2] = parseInt(d[2]) + 2000;
        }
        return d[0] + "." + d[1] + "." + d[2];
      } else {
        return string;
      }
    }
  }
  return "";
}
exports.formatDateObject = function (string) {
  let str = "";
  let jetzt = new Date(string);
  let da = jetzt.getDate();
  let mo = jetzt.getMonth() + 1;
  let ye = jetzt.getFullYear();
  let ho = jetzt.getHours();
  let mi = jetzt.getMinutes();
  let se = jetzt.getSeconds();
  if (se < 10) { se = "0" + se; }
  if (mi < 10) { mi = "0" + mi; }
  if (ho < 10) { ho = "0" + ho; }
  if (da < 10) { da = "0" + da; }
  if (mo < 10) { mo = "0" + mo; }
  str = da + "." + mo + "." + ye ;
  return str;
}
exports.formatDateTime = function (string) {
  let str = "";
  let jetzt = new Date(string);
	let da = jetzt.getDate();
	let mo = jetzt.getMonth() + 1;
	let ye = jetzt.getFullYear();
	let ho = jetzt.getHours();
	let mi = jetzt.getMinutes();
	let se = jetzt.getSeconds();
	if (se < 10) { se = "0" + se; }
	if (mi < 10) { mi = "0" + mi; }
	if (ho < 10) { ho = "0" + ho; }
	if (da < 10) { da = "0" + da; }
	if (mo < 10) { mo = "0" + mo; }
	str = da + "." + mo + "." + ye + " " + ho + ":" + mi + ":" + se;
	return str;
}

exports.string2Date = function (el) {
  let year = 0, month = 0, day = 0;
  let d = el.split('.');
  if (d.length == 3) {
    day = d[0];
    month = d[1]-1;
    year = d[2];
    let date = new Date(year, month, day, 12, 00);

    return date;
  } else {
    let date = new Date(2100, 19, 1);
    return date;

  }
}

exports.setValueofId = function (Id, value) {
  let str = "";
  (typeof value !== 'undefined' && value !== "undefined") ? str = value : str = "";
  ById(Id).value = str;
  return str;
}

exports.strdef = function (value) {
  let str = "";
  (typeof value !== 'undefined' && value !== "undefined") ? str = value : str = "";
  return str;
}

exports.toggleHidden = function () {
  item = event.target.getAttribute("href").replace('#', '');
  window.document.getElementById(item).classList.toggle('is-hidden');
}

exports.write2logfile = function (string, flag) {
  let userDataPath = (electron.app || electron.remote.app).getPath('userData');
  let filePath = path.join(userDataPath, "ito_out.log");
  fs.writeFileSync(filePath, logfileDatum() + " " + string + "\n", { 'flag': flag });
}

function logfileDatum() {
  let jetzt = new Date();
  let str = "";
  let da = jetzt.getDate();
  let mo = jetzt.getMonth() + 1;
  let ye = jetzt.getFullYear();
  let ho = jetzt.getHours();
  let mi = jetzt.getMinutes();
  let se = jetzt.getSeconds();
  if (se < 10) { se = "0" + se; }
  if (mi < 10) { mi = "0" + mi; }
  if (ho < 10) { ho = "0" + ho; }
  if (da < 10) { da = "0" + da; }
  if (mo < 10) { mo = "0" + mo; }
  str = da + "." + mo + "." + ye + " " + ho + ":" + mi + ":" + se;
  return str;
}



exports.getDatumFile = function getDatumFile() {
  let jetzt = new Date();
  let str = "";
  let da = jetzt.getDate();
  let mo = jetzt.getMonth() + 1;
  let ye = jetzt.getFullYear();
  if (da < 10) { da = "0" + da; }
  if (mo < 10) { mo = "0" + mo; }
  str = ye + "" + mo + "" + da;
  return str;
}
exports.getYear = function getYear(){
   return new Date().getFullYear();
}

exports.checkYear = function checkYear(datum, yearcnt){
		if (yearcnt <1){
				return true;
		}
		let aktjahr = getYear();
		let year = string2Date(datum).getFullYear();
		let lastyear = parseInt(aktjahr)-1;
		if( yearcnt & (1<<0) && aktjahr == year){
			return true;
		}
		if(yearcnt & (1<<1) && year == lastyear   ){
			return true;
		}
		return false;
		
}
exports.getDatum = function getDatum(days) {

  let jetzt = new Date();

  jetzt = addDays(jetzt, days);


  let str = "";
  let da = jetzt.getDate();
  let mo = jetzt.getMonth() + 1;
  let ye = jetzt.getFullYear();
  let ho = jetzt.getHours();
  let mi = jetzt.getMinutes();
  let se = jetzt.getSeconds();
  if (da < 10) { da = "0" + da; }
  if (mo < 10) { mo = "0" + mo; }
  str = da + "." + mo + "." + ye;
  return str;
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
exports.openFileWithDefaultApp = function (file) {
  /^win/.test(process.platform) ?
    require("child_process").exec('start "" "' + file + '"') :
    require("child_process").spawn(getCommandLine(), [file],
      { detached: true, stdio: 'ignore' }).unref();
}

exports.getCommandLine = function () {
  switch (process.platform) {
    case 'darwin':
      return 'open';
    default:
      return 'xdg-open';
  }
}

exports.roundToTwo = function (num)  {
  return +(Math.round(num + "e+2")  + "e-2");
}
