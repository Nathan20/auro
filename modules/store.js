const electron = require('electron');
const path = require('path');
const fs = require('fs');
const mysql = require("mysql");
var zip = require("node-native-zip");
let CONSTANTS = require('./constants');
const { type } = require('os');
//const stream = require('stream');

class Store {
  constructor(opts) {
    if (typeof opts.filename != 'undefined' && opts.filename != 'undefined' && opts.filename != "") {
      this.path = opts.filename;
    } else {
      const userDataPath = (electron.app || electron.remote.app).getPath('userData');
      let ext = 'json';
      if (typeof opts.ext != 'undefined') {
        ext = opts.ext;
      }
      if (typeof opts.path === 'undefined' || opts.path === 'undefined') {
        this.path = path.join(userDataPath, opts.configName + "." + ext);
      } else {
        this.path = path.join(opts.path, opts.configName + "." + ext);
      }
    }
    this.data = parseDataFile(this.path, opts.defaults);
    this.lock = path.join(path.dirname(this.path), path.basename(this.path, '.json') + ".lck");
  }
  get(key) {
    this.data = parseDataFile(this.path, "");

    if (typeof this.data[key] !== 'undefined') {
      return this.data[key];
    } else {
      return;
    }
  }
  set(key, val) {
    this.data[key] = val;
    this.data["DMVersion"] += 1;
    fs.writeFileSync(this.path, JSON.stringify(this.data));
  }
  getDM() {
    return this.data;
  }
  setIdOnly(key, val, id) {
    let idx = this.data[key].findIndex(el => el.id == id);
    this.data[key][idx] = val;
    this.data["DMVersion"] += 1;
    fs.writeFileSync(this.path, JSON.stringify(this.data));

  }
  async setAsync(key, val, user) {
    let dmversion = this.data["DMVersion"];
    let status = "";
    await checkFileNotExist(this.lock, 30000).then(msg => {
      fs.writeFileSync(this.lock, user);

      this.data = parseDataFile(this.path, "");
      if (this.data["DMVersion"] != dmversion) {
        // File hat sich geändert seit dem letzten einladen!
        // 
      }
      this.data[key] = val;
      this.data["DMVersion"] += 1;
      fs.writeFileSync(this.path, JSON.stringify(this.data));
      fs.unlinkSync(this.lock);
      status = "ok";

    }).catch(msg => {
      status = "Timeout";
    });
    return status;
  }



  async setNewAsync(key, val, user) {
    let dmversion = this.data["DMVersion"];
    let status = "";
    await checkFileNotExist(this.lock, 30000).then(msg => {
      fs.writeFileSync(this.lock, user);
      status = "ok";

      this.data = parseDataFile(this.path, "");
      if (this.data["DMVersion"] != dmversion) {
        // File hat sich geändert seit dem letzten einladen!
        status = "DMchanged"
      }
      let newid = 0;
      let newelementidx = 0;
      let cLen = this.data[key].length;
      for (let cnt = 0; cnt < cLen; cnt++) {
        if (typeof this.data[key][cnt].id == 'undefined' || this.data[key][cnt].id == '-1') {
          newelementidx = cnt;
        }
        else {
          if (newid <= parseFloat(this.data[key][cnt].id)) {
            newid = parseFloat(this.data[key][cnt].id) + 1;
          }
        }
      }
      val.id = newid;
      this.data[key].push(val);
      this.data["DMVersion"] += 1;
      fs.writeFileSync(this.path, JSON.stringify(this.data));
      fs.unlinkSync(this.lock);
    }).catch(msg => {
      status = "Timeout";
    });
    return status;
  }
  async setIDAsync(key, val, id, user) {
    let dmversion = this.data["DMVersion"];
    let status = "";
    await checkFileNotExist(this.lock, 30000).then(msg => {
      fs.writeFileSync(this.lock, user);
      this.data = parseDataFile(this.path, "");
      if (this.data["DMVersion"] != dmversion) {
        // File hat sich geändert seit dem letzten einladen!
      }
      let item = this.data[key].find(el => el.id == id);
      Object.assign(item, val);
      this.data["DMVersion"] += 1;
      fs.writeFileSync(this.path, JSON.stringify(this.data));
      fs.unlinkSync(this.lock);
      status = "ok";
    }).catch(msg => {
      status = msg;
    });
    return status;
  }



  setIdNew(key, val) {
    this.data[key].push(val);
    fs.writeFileSync(this.path, JSON.stringify(this.data));
  }
  reload() {
    this.data = parseDataFile(this.path, "");
    return;
  }
  getLock(user) {
    let message = "ok";
    if (fs.existsSync(this.lock)) {
      message = fs.readFileSync(this.lock, 'utf8');
    } else {
      fs.writeFileSync(this.lock, user);
    }
    return message;
  }
  releaseLock() {
    if (fs.existsSync(this.lock)) {
      fs.unlinkSync(this.lock);
    }
  }
  makebackup() {
    let ext = path.extname(this.path);
    let pos = this.path.lastIndexOf(ext);
    let zippath = this.path.substring(0, pos);
    let zipname = path.basename(zippath);
    const zlib = require('zlib');
    let dmversion = this.data['DMVersion'];

    let regex = new RegExp(path.basename(zippath) + '-back-' + dmversion + '.json$');
    let status = checkfile(path.dirname(zippath), regex);
    //console.log(status, regex);
    if (status == 0) {
      // Wenn Status == 0 gibt es noch  kein Backup mit dieser DMVersion
      // Dann wird keine neue Version angelegt
      let nameSBackup = zippath + "-back-" + dmversion + ".json";
      fs.writeFileSync(nameSBackup, JSON.stringify(this.data));

      // let zipme = new Promise((resolve, reject) => {
      //   let fileContents = fs.createReadStream(nameSBackup);
      //   let writeStream = fs.createWriteStream(nameSBackup + ".gz");
      //   const zip = zlib.createGzip();
      //   fileContents.pipe(zip).pipe(writeStream).on('finish', (err) => {
      //     if (err) return reject(err);
      //     else resolve();
      //   })
      // })
      // zipme.then(
      //   function (value) {/* console.log('success', value); */ },
      //   function (error) { console.log('error', error); }

      // );
    }
    // console.log(zipfullpath, nameSBackup);
    regex = new RegExp(zipname + '-back-\\d*.json$');
    let numberofFiles = CONSTANTS.KEEPBACKUPS + 1;
    while (numberofFiles > CONSTANTS.KEEPBACKUPS) {
      numberofFiles = removeOldestFile(path.dirname(zippath), regex, CONSTANTS.KEEPBACKUPS - 1);
    }

    regex = new RegExp(zipname + '-back-\\d*.json.gz$');

    numberofFiles = (CONSTANTS.KEEPBACKUPS + 1) * 2;
    while (numberofFiles > 2 * CONSTANTS.KEEPBACKUPS) {
      // console.log(numberofFiles);
      numberofFiles = removeOldestFile(path.dirname(zippath), regex, CONSTANTS.KEEPBACKUPS * 2 - 1);
    }

  }




  loadnew(filename) {
    if (typeof filename != 'undefined') {
      this.path = filename;
    }
    //console.log(this.path);
    this.data = parseDataFile(this.path, "");
    return;
  }
}

function parseDataFile(filePath, defaults) {
  // We'll try/catch it in case the file doesn't exist yet, which will be the case on the first application run.
  // fs.readFileSync will return a JSON string which we then parse into a Javascript object
  try {
    return JSON.parse(fs.readFileSync(filePath));

  } catch (error) {
    // if there was some kind of error, return the passed in defaults instead.
    return defaults;
  }
}

// expose the class
module.exports = Store;

function timeStamp() {
  let jetzt = new Date();
  let str = "";
  let da = jetzt.getDate();
  let mo = jetzt.getMonth() + 1;
  let ye = jetzt.getFullYear();
  let ho = jetzt.getHours();
  let mi = jetzt.getMinutes();
  let se = jetzt.getSeconds();
  if (se < 10) { se = "0" + se; }
  if (mi < 10) { mi = "0" + mi; }
  if (ho < 10) { ho = "0" + ho; }
  if (da < 10) { da = "0" + da; }
  if (mo < 10) { mo = "0" + mo; }
  str = ye + mo + da + ho + mi + se;
  return str;
}
function checkfile(dir, regexp) {
  var fs = require("fs"), path = require('path'), files = fs.readdirSync(dir);
  let status = 0;
  for (i = 0; i < files.length; i++) {
    match = regexp.test(files[i]);
    if (match != false) {
      status = 1;
    }
  }
  return status;
}
function removeOldestFile(dir, regexp, minAnzahl) {
  // console.log("RemoveOldest ", dir, regexp, minAnzahl);
  var fs = require("fs"),
    path = require('path'),
    oldest = null,
    files = fs.readdirSync(dir),
    one_matched = 0;

  for (i = 0; i < files.length; i++) {

    if (regexp.test(files[i]) == false) {
      // console.log("File not matched", one_matched, files[i]);

      continue
    }
    else {
      one_matched = one_matched + 1;
      // console.log("File ", one_matched, files[i]);

      if (one_matched == 1) {
        oldest = files[i];
        continue
      }
    }

    f1_time = fs.statSync(path.join(dir, files[i])).mtime.getTime()
    f2_time = fs.statSync(path.join(dir, oldest)).mtime.getTime()
    if (f1_time < f2_time)
      oldest = files[i];
  }

  if (oldest != null && minAnzahl < one_matched - 1) {
    fs.unlinkSync(path.join(dir, oldest));
    one_matched = one_matched - 1;
    //console.log("remove ", path.join(dir, oldest));
  }
  return one_matched;
}

function makeInt(number) {
  return (parseInt(number) != parseInt(number) ? 0 : parseInt(number));
}
function makeFloat(number) {
  return (parseFloat(number) != parseFloat(number) ? 0 : parseFloat(number));
}

async function checkFileNotExist(path, timeout = 2000) {
  let totalTime = 0;
  let checkTime = timeout / 10;

  return await new Promise((resolve, reject) => {
    const timer = setInterval(function () {

      totalTime += checkTime;
      let fileExists = fs.existsSync(path);
      if (!fileExists) {
        resolve(fileExists);
      }
      if (totalTime >= timeout) {
        clearInterval(timer);
        message = "timeout";
        if (fs.existsSync(path)) {
          message = fs.readFileSync(path, 'utf8');
        }
        reject(message);
      }
    }, checkTime);
  });
}
