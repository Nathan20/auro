const electron = require('electron');
const path = require('path');
const fs = require('fs');
const mysql = require("mysql");
var zip = require("node-native-zip");
let CONSTANTS = require('./constants');
let tbl = require('./tbldefinition');
const { type } = require('os');
const { resourceLimits } = require('worker_threads');
const { app } = require('electron');

class Ablage {
  constructor(opts) {
    // console.log("Constructor: ", opts.sql);
    this.sql = opts.sql;
    if (typeof this.sql != 'undefined') {
      this.connection = mysql.createConnection({
        host: this.sql.host,
        user: this.sql.user,
        password: this.sql.password,
        database: this.sql.database,
        port: this.sql.port
      });
    }
  }
  close() {
    return new Promise((resolve, reject) => {
      this.connection.end(err => {
        if (err)
          return reject(err);
        resolve();
      });
    });
  }
  check() {
    return new Promise((resolve, reject) => {
      this.connection.query("show tables;", (err, rows) => {
        if (err)
          return reject(err);
        resolve(rows);
      });
    });
  }
  checktables() {
    //console.log('Check Tables');
    checkTable(this.connection, "rechnung", tbl.tbldefs['rechnung']);
    checkTable(this.connection, "vertrag", tbl.tbldefs['vertrag']);
    checkTable(this.connection, "projekt", tbl.tbldefs['projekt']);
    checkTable(this.connection, "kunde", tbl.tbldefs['kunde']);
    checkTable(this.connection, "objekt", tbl.tbldefs['objekt']);
    checkTable(this.connection, "benutzer", tbl.tbldefs['benutzer']);
    checkTable(this.connection, "buchung", tbl.tbldefs['buchung']);
    checkTable(this.connection, "kommentar", tbl.tbldefs['kommentar']);
    checkTable(this.connection, "provision", tbl.tbldefs['provision']);
    checkTable(this.connection, "ztemplate", tbl.tbldefs['ztemplate']);
    checkTable(this.connection, "positionen", tbl.tbldefs['positionen']);
    checkTable(this.connection, "vorlagen", tbl.tbldefs['vorlagen']);
    checkTable(this.connection, "vorlagenrech", tbl.tbldefs['vorlagenrech']);
    checkTable(this.connection, "zahlungseingang", tbl.tbldefs['zahlungseingang']);
    checkTable(this.connection, "positionenver", tbl.tbldefs['positionenver']);
    checkTable(this.connection, "konfig", tbl.tbldefs['konfig']);
    return;
  }
  get(tblname) {
    return new Promise((resolve, reject) => {
      //console.log(tblname);
      let resQuery = new Array();
      let str = "select * from `" + tblname + "`";
      this.connection.query(str, function (err, rows, fields) {
        if (err) {
          // console.error("error :" + err.stack);
          return reject(err);
        }

        let id = 0;
        rows.forEach(row => {
          let single = new Object();


          Object.keys(row).forEach(key => {
            single[key] = row[key];
            if (key == 'id') { id = row[key]; }
          });
          resQuery.push(single);
        });
        return resolve(resQuery);
      });


    });
  }
  set(tblname, data) {
    let definition = tbl.tbldefs[tblname];
    data.forEach(dataset => {
      //console.log(dataset);
      let str, strteil2 = "";

      str = "INSERT INTO `" + tblname + "` set ";
      let first = true;
      let id = 0;
      Object.keys(dataset).forEach(key => {
        if (key == "id") { id = dataset[key] };
        if (typeof dataset[key] != 'object' | typeof definition[key] != 'undefined') {
          if (first == true) {
            first = false;
          } else {
            strteil2 += ", ";
          }
          strteil2 += this.makeStringSet(definition[key], key, dataset[key]);
        } else {
          // Unterobjekte auch ablegen
          // key = vorlagen --> ztvorlagen
          // key = positionen --> ztpositionen
          this.setUnterobjekte(dataset[key], key, tblname, id);
        }
      });

      str += strteil2 + "ON DUPLICATE KEY UPDATE " + strteil2 + ";";
      // console.log(str);
      this.connection.query(str, function (err, rows, fields) {
        if (err) {
          console.error("error :" + err.stack);
          return;
        }
      });
    });
  }
  makeStringSet(def, key, data) {
    let wert = 0;
    // console.log(def, key, data);
    if (def.search("int") != -1 || def.search("float") != -1) {
      if (data == null || data == "" || isNaN(data)) {
        wert = 0;
      } else {
        wert = data;
      }

    } else {
      if (data == null || data == "") {
        wert = "";
      } else {
        wert = data;
      }
    }
    if (def.search("tinyint(1)") != -1) {
      if (data == "true" || data == true) {
        wert = 1;
      } else {
        wert = 0;
      }
    }

    return key + " = " + mysql.escape(wert) + " ";

  }
  setkonfigliste(typ, data) {
    console.log(data);
    //let defztpos = tbl.tbldefs[tblname];
    let delstr = "DELETE from konfig where typ = " + mysql.escape(typ) + ";";
    // console.log(delstr);
    this.connection.query(delstr, function (err, rows, fields) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });
    let ustr = "";
    ustr = "INSERT INTO konfig set typ = " + mysql.escape(typ) + " ";

    data.forEach(dataset => {
      ustr += ", wert =" + mysql.escape(dataset);
    });

    console.log(ustr);
    // this.connection.query(ustr, function (err, rows, fields) {
    //   if (err) {
    //     console.error("error :" + err.stack);
    //     return;
    //   }
    // });



  }

  setUnterobjekte(positionen, tblname, reftblname, id) {
    let defztpos = tbl.tbldefs[tblname];
    let delstr = "DELETE from " + tblname + " where " + reftblname + " = " + mysql.escape(id) + ";";
    // console.log(delstr);
    this.connection.query(delstr, function (err, rows, fields) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });
    positionen.forEach(position => {
      //console.log(dataset);
      let ustr = "";
      ustr = "INSERT INTO " + tblname + " set " + reftblname + " = " + id + " ";

      // console.log(position);

      Object.keys(position).forEach(poskey => {
        //  console.log(defztpos[poskey], poskey, position[poskey]);
        if (poskey != reftblname) {
          if (typeof position[poskey] != 'object' | typeof position[poskey] != 'undefined') {
            ustr += ", " + this.makeStringSet(defztpos[poskey], poskey, position[poskey]);
          }
        }
      });
      //console.log(ustr);
      this.connection.query(ustr, function (err, rows, fields) {
        if (err) {
          console.error("error :" + err.stack);
          return;
        }
      });
    });

  }
  updateDataFields(tblname, fields, filter) {
    //console.log(wheres);
    let first = true;
    let str = "update `" + tblname + "` ";
    Object.keys(fields).forEach(el => {
      if (first == true) { first = false; str += " set "; } else { str += " , "; }
      str += el + " = " + mysql.escape(fields[el]);
    });
    first = true;
    Object.keys(filter).forEach(el => {
      if (first == true) { first = false; str += " WHERE "; } else { str += " AND "; }
      str += el + " = " + mysql.escape(filter[el]);
    });

    str += ";"
    //console.log(str);
    this.connection.query(str, function (err, rows, f) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });

  }
  insertDataSet(tblname, item) {
    // console.log(tblname, item);
    let definition = tbl.tbldefs[tblname];
    let first = true;
    let str = "insert into `" + tblname + "` ";
    Object.keys(item).forEach(el => {
      if (first == true) { first = false; str += " set "; } else { str += " , "; }
      str += this.makeStringSet(definition[el], el, item[el]);
    });

    str += ";"
    // console.log(str);
    this.connection.query(str, function (err, rows, f) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });

  }
  getmaxID(tblname) {
    return new Promise((resolve, reject) => {
      //console.log(tblname);
      let str = "select max(id) from `" + tblname + "`";
      let id = 0;

      this.connection.query(str, function (err, rows, fields) {
        if (err) {
          // console.error("error :" + err.stack);
          return reject(err);
        }

        rows.forEach(row => {
          Object.keys(row).forEach(key => {
            if (key == 'max(id)') { id = row[key]; }
          });
        });
        return resolve(id + 1);
      });


    });
  }

  deleteDataSetID(tblname, id) {
    let str = "delete from `" + tblname + "` where";
    str += " id  = " + id;

    str += ";"
    // console.log(str);
    this.connection.query(str, function (err, rows, f) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });

  }
  setIdOnly(tblname, dataset) {
    let definition = tbl.tbldefs[tblname];
    //console.log(tblname, dataset["id"] );
    let updatestr = "";
    updatestr = "UPDATE  `" + tblname + "` set ";
    let first = true;
    let wert = 0, id = 0;
    let strset = "";
    Object.keys(dataset).forEach(key => {
      // console.log(key, dataset[key]);
      if (key == "id") { id = dataset[key] };
      if (typeof definition[key] != 'undefined') {
        if (first == true) {
          first = false;
        } else {
          strset += ", ";
        }
        strset += this.makeStringSet(definition[key], key, dataset[key]);
      } else {
        this.setUnterobjekte(dataset[key], key, tblname, id);
      }
    });

    updatestr += strset + "  where id = " + dataset["id"] + ";";
    // console.log(updatestr);
    this.connection.query(updatestr, function (err, rows, fields) {
      if (err) {
        console.error("error :" + err.stack);
        return;
      }
    });

  }
}

// expose the class
module.exports = Ablage;


function checkAttDef(connection, tblName, tbldef, rows) {
  rows.forEach(row => {
    let str = "";
    //console.log(row);
    if (typeof tbldef[row.Field] != 'undefined') {
      //  console.log("Feld " + row.Field + ": " + row.Type + "(" + table[row.Field] + ")");
      if (row.Type == tbldef[row.Field]) {
        //console.log("Feld " + row.Field + ": " + row.Type + " wie definiert");
      } else {
        //console.log("Feld " + row.Field + ": " + row.Type + " CONFLICT " + tbldef[row.Field]);

        // ALTER TABLE dbo.Employee Add Address varchar(500) NOT NULL;
        str = "Alter table `" + tblName + "` modify " + row.Field + " " + tbldef[row.Field] + ";"
        //console.log(str);
        connection.query(str, function (err, rows, fields) {
          if (err) {
            console.error("error :" + err.stack);
            return;
          }
        });

      }

    } else {
      // console.log("Feld " + row.Field + ": " + row.Type + " nicht definiert");

      // ALTER TABLE dbo.Employee Add Address varchar(500) NOT NULL;
      let str = "Alter table `" + tblName + "` add " + row.Field + " " + table[row.Field] + ";"
      // console.log(str);
      connection.query(str, function (err, rows, fields) {
        if (err) {
          console.error("error :" + err.stack);
          return;
        }
      });
    }


  });
}
function checkAtt(connection, tblName, tbldef, rows) {
  let found = false;
  Object.keys(tbldef).forEach(key => {
    //console.log(key);
    found = false;
    rows.forEach(row => {
      if (row.Field == key) {
        found = true;
      }
    });
    if (found == false) {
      //console.log("Das Feld " + key + " ist noch nicht angelegt.")
      let str = "Alter table `" + tblName + "` add " + key + " " + tbldef[key] + ";"
      // console.log(str);
      connection.query(str, function (err, rows, fields) {
        if (err) {
          console.error("error :" + err.stack);
          return;
        }
      });
    }

  });
}
function checkTable(connection, tblName, tbldef) {
  // check Datenbank
  connection.query("desc " + tblName, function (err, rows, fields) {
    if (err) {
      //console.error("error :" + err.stack);
      if (err.stack.search("ER_NO_SUCH_TABLE") != -1) {
        addTblNew(connection, tblName, tbldef);
        connection.query("desc " + tblName, function (err, rows, fields) {
          if (err) {
            console.error("error :" + err.stack);
            return;
          }
        });

      } else {
        console.error("error :" + err.stack);
        return;
      }
    } else {
      checkAtt(connection, tblName, tbldef, rows);
      checkAttDef(connection, tblName, tbldef, rows);
    }
  });
}
function addTblNew(connection, tblName, tbldef) {
  let pk = ",primary key(id) ";
  if (tblName == 'positionen' |
    tblName == 'vorlagenrech' |
    tblName == 'vorlagen') {
    pk = "";
  }
  let str = " CREATE TABLE `" + tblName + "` (";
  let start = 0;
  Object.keys(tbldef).forEach(key => {
    if (start == 0) {
      start = 1;
    } else {
      str += ",";
    }
    // console.log(key, " Wert ", table[key]);
    str += "`" + key + "` " + tbldef[key];
  });
  str += pk + " ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
  //  console.log(str);
  connection.query(str, function (err, rows, fields) {
    if (err) {
      console.error("error :" + err.stack);
      return;
    }
  });

}