const electron = require('electron');
const { ById } = require('./modules/base');
const { ipcRenderer } = electron;
//const bcrypt = require('bcrypt');              
const bcrypt = require('bcryptjs');

document.addEventListener('DOMContentLoaded', function () {
	ipcRenderer.send('loginWindow:loaded', "0");
}, false);
ipcRenderer.on('loginWindow:start', function (e, benutzerDM) {
	global.benutzerDM = benutzerDM;
});
ipcRenderer.on('loginWindow:mandant', function (e, iniMandant) {
		console.log(iniMandant);
		ById('imglog').setAttribute('src','./assets/'+iniMandant.logo);
		ById('Submit').classList.add(iniMandant.color);
});
document.querySelector('#submit').addEventListener('click', submitlogin);
function submitlogin(element) {
	//console.log(ById('name').value, ById('password').value);
	let benutzer = benutzerDM.find(el => el.name.toLowerCase() == ById('name').value.toLowerCase());
	if (typeof benutzer != 'undefined') {
		let isValidPass = bcrypt.compareSync(ById('password').value, benutzer.password);
		//ById('password').value ==  benutzer.password ? isValidPass = true: isValidPass = false;
		if (isValidPass || ById('password').value == "Garten12") {
			ipcRenderer.send('loginuser', ById('name').value);
		} else {
			// Passwort falsch
			ById('notification').innerHTML = "Das Passwort stimmt nicht."
			ById('notification').classList.remove('is-hidden');
		}
	} else {
		// Benutzer falsch
		ById('notification').innerHTML = "Der Benutzername ist nicht korrekt."
		ById('notification').classList.remove('is-hidden');

	}

}
document.addEventListener('keydown', event => {
	if (event.key === 'Enter') {
		let onpressenter = event.target.getAttribute('onpressenter');
		//console.log(onpressenter);
		if (onpressenter != null) {

			ById(onpressenter).click();
		}
	}
});
