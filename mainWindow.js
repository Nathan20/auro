const electron = require('electron');
const { ipcRenderer, nativeImage, shell } = electron;
const { dialog, getCurrentWindow } = require('electron').remote;
const { Menu, MenuItem } = require('electron').remote;;
const excel = require('node-excel-export');
const fs = require('fs');
const path = require('path');
const base = require('./modules/base.js');
const { roundToTwo, ById, openFileWithDefaultApp, removeSpace, stripString, convPreisField, convProzentField, strdef, formatNumber, formatBetrag, formatProzent, string2Date, formatDatum, formatDateTime, formatDateObject, formatIBAN, getDatumFile, getDatum, getYear, checkYear, setValueofId, toggleHidden, write2logfile } = base;
let CONSTANTS = require('./modules/constants');
const gendoc = require('./modules/gendoc.js');
const { createBrief, genBrief } = gendoc;
const Store = require('./modules/store.js');
const markdown = require("markdown").markdown;
const nodeConsole = require('console');

//const myConsole = new nodeConsole.Console(process.stdout, process.stderr);
let user, roleadmin = false, roleedit = false, rolecomment = false;
let anzeige = { rechnungid: 0, vertragid: 0, einheitid: 0, kundeid: 0, projektid: 0, edit: false, show: "" };
var DM = new Object();

const isRelative = (path) => !/^([a-z]+:)?[\\/]/i.test(path);

const propertiesReader = require('properties-reader');

let properties;
let DARLEHENMENU = true;;
let PLANUNGMENU = false;
let RECHNUNGSBUCH = true;
let PROVISIONEN = true;
let ARBEITSKORB = true;
let ENV = "production";

let filterYear = 1;

try {
	 if (process.env.NODE_ENV != 'production') {
	 	properties = propertiesReader('./extraResources/properties.ini');
	 } else {
	 	properties = propertiesReader('./properties.ini');
	 }

	// properties = propertiesReader('./extraResources/properties.ini');

	 RECHNUNGSBUCH = properties.get('modules.rechnungsbuch.menu');
	 ARBEITSKORB = properties.get('modules.arbeitskorb.menu');
	 PROVISIONEN = properties.get('modules.provisionen.menu');
	 DARLEHENMENU = properties.get('modules.darlehen.menu');
	 PLANUNGMENU = properties.get('modules.planung.menu');
	 ENV = properties.get('modules.env.glob');

} catch (e) {
	// Anweisungen für nicht festgelegte Fehlertypen
	console.log(e); // Objekt an die Fehler-Funktion geben
}


// ---------------------------------
const green = "rgb(0,204,0)";
const red = "rgb(255,50,50)";
const black = "rgb(0,0,0)";
const white = "rgb(255,255,255)"
const lightgreen = "rgb(198,239,206)";
const greenlaurel = "rgb(228, 233, 224)";
// ---------------------------------
// ---------------------------------
document.addEventListener("click", function () {
	ById('notificationSuccess').classList.add('is-hidden');
	ById('notificationDanger').classList.add('is-hidden');

	DM.success = "";
	DM.danger = "";
});

document.addEventListener('DOMContentLoaded', function () {
	ipcRenderer.send('mainWindow:loaded', "0");

	dpInit();
}, false);
// Daten empfaangen
ipcRenderer.on('reload', function (e, msg) {
	//store.reload();
	//datenmodell.loadnew();
	DM.success = "";
	DM.danger = "";
	ById('notificationSuccess').classList.add('is-hidden');
	ById('notificationDanger').classList.add('is-hidden');


	if (typeof msg.success != 'undefined' && msg.success != "") {
		ById('notificationSuccess').classList.remove('is-hidden');
		ById('notificationSuccess').innerHTML = msg.success;
	}
	if (typeof msg.danger != 'undefined' && msg.danger != "") {
		ById('notificationDanger').classList.remove('is-hidden');
		ById('notificationDanger').innerHTML = msg.danger;
	}

	DM = msg;
	anzeige = { menu: "", rechnungid: 0, vertragid: 0, einheitid: 0, kundeid: 0, projektid: 0, edit: false, show: "" };
	let checks = new Array();
	$('#navtree').find('input').each(function (index, val) {
		checks.push({ key: val.id, check: ById(val.id).checked });
	});

	updateMenu();
	updateAnzeige();
	updateDPBenutzer();

	checks.forEach(el => {
		if (ById(el.key) != null) {
			ById(el.key).checked = el.check;
		}
	});


});
ipcRenderer.on('mainWindow:start', function (e, msg) {
	write2logfile("Start", "");
	user = msg.user;
	roleadmin = msg.roleadmin;
	roleleiter = msg.roleleiter;
	roleedit = msg.roleedit;
	rolecomment = msg.rolecomment;
	DM = msg;
	dropdownKunAnrede();
	addListeners();
	updateMenu();
	updateDPBenutzer();
	ipcRenderer.send('timer:ready', "");
});
ipcRenderer.on('update:prov', function (e, rechnungid) {
	editProvision(rechnungid);
});
ipcRenderer.on('updateDM:benutzerDM', function (e, msg) {
	user = msg.user;
	roleadmin = msg.roleadmin;
	roleedit = msg.roleedit;
	rolecomment = msg.rolecomment;
	//console.log(user, "Admin", roleadmin, "Edit", roleedit, "Comment", rolecomment);
	updateAnzeige();
});
ipcRenderer.on('updateDM:zahltemplates', function (e, item) {
	DM.zahltemplates = item;
});
ipcRenderer.on('kunde:show', function (e, item) {
	anzeige.show = "kunde";
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);
	updateAnzeige();
});
ipcRenderer.on('projekt:show', function (e, item) {

	anzeige.show = "projekt";
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);
	updateAnzeige();

});
ipcRenderer.on('rechnung:show', function (e, item) {
	anzeige.show = "rechnung";
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);
	updateAnzeige();
});
ipcRenderer.on('vertrag:show', function (e, item) {
	anzeige.show = "vertrag";
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);
	updateAnzeige();
});
ipcRenderer.on('comment:add', function (e, item) {
	addComment();
});
ipcRenderer.on('releasenotes:show', function (e, item) {
	ById('modalRelease').classList.add('is-active');
});
ipcRenderer.on('display:set', function (e, item) {

	if (typeof item !== 'undefined' && item != "") {
		anzeige = item;
	}
	updateAnzeige();

});
ipcRenderer.on('timer:set', function (e, item) {
	let timerWait = new Promise(function (myResolve, myReject) {
		setTimeout(function () { myResolve("I love You !!"); }, 5000);
	});
	timerWait.then(function (value) {
		ipcRenderer.send('timer:ready', "");
	});


});


// Ab hier Funktionen
function updateAnzeige() {
	ById('paneOPL').classList.add('is-hidden');
	ById('panePROV').classList.add('is-hidden');
	ById('paneArbeitskorb').classList.add('is-hidden');
	ById('paneVertrag').classList.add('is-hidden');
	ById('paneRechnung').classList.add('is-hidden');
	ById('paneKunde').classList.add('is-hidden');
	ById('paneProjekt').classList.add('is-hidden');
	ById('paneDarlehen').classList.add('is-hidden');
	ById('paneArcVer').classList.add('is-hidden');
	ById('paneObjekt').classList.add('is-hidden');
	ById('paneKommentare').classList.add('is-hidden');
	ById('ver-box').classList.add('is-hidden');
	ById('prj-box').classList.add('is-hidden');
	ById('kun-box').classList.add('is-hidden');
	ById("ChgVertragGegenstand").classList.add('is-hidden');
	ById("ChgVertragMwSt").classList.add('is-hidden');
	ById("ChgVertragsnummer").classList.remove('is-hidden');
	ById("ChgVertragStatus").classList.add('is-hidden');
	ById("ChgVertragUrnr").classList.remove('is-hidden');
	ById("ChgVertragUrdatum").classList.remove('is-hidden');
	ById("ChgVertragNotariat").classList.remove('is-hidden');
	ById("ChgVertragFdat1").classList.remove('is-hidden');
	ById("ChgVertragFdat2").classList.remove('is-hidden');
	ById("ChgVertragAbdat").classList.remove('is-hidden');
	ById("ChgVertragUdat").classList.remove('is-hidden');
	ById("ChgVertragPreis").classList.remove('is-hidden');
	updateDropDowns();
	switch (anzeige.show) {
		case "vertrag":
			ById('paneVertrag').classList.remove('is-hidden');
			anzeige.show = "vertrag";
			anzeige.vertragid == 0 ? showVertrag(-1) : showVertrag(anzeige.vertragid);
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			ById('tabverpos').innerHTML = "";
			if (typeof vertrag != 'undefined') {
				addElementToVertragsPos(vertrag);
				switch (vertrag.typ) {
					case CONSTANTS.ARCHVERTRAG:
						ById("planall").checked = true;
						break;
					case CONSTANTS.DARLEHEN:
						ById("darall").checked = true;
						break;
					default:
						ById("prjall").checked = true;
						ById("prj" + vertrag.projekt).checked = true;
						ById("obj" + parseInt(vertrag.objekt)).checked = true;
						ById("ver" + vertrag.id).checked = true;

				}
			}

			break;
		//console.log("ARCEdit");
		// Kein break hier!
		case "vertragReservierungedit":
		case "vertragARCedit":
		case "vertragDARedit":
		case "vertragedit":
			ById('paneVertrag').classList.remove('is-hidden');
			ById('tabverpos').innerHTML = "";
			ById('modalVertragChange').classList.add('is-active');
			ById('dpVerKun').classList.remove('is-active');
			ById('dpVerKun2').classList.remove('is-active');
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			anzeige.show = "vertrag";
			showVertrag(vertrag.id);
			anzeige.edit = true;
			ipcRenderer.send('display:save', anzeige);

			addElementToVertragsPos(vertrag);
			switch (vertrag.typ) {
				case CONSTANTS.RESERVIERUNG:
					ById("ChgVertragsnummer").classList.add('is-hidden');
					ById("ChgVertragGegenstand").classList.add('is-hidden');
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					break;
				case CONSTANTS.VERTRAGALLG:
					ById("ChgVertragsnummer").classList.add('is-hidden');
					ById("ChgVertragGegenstand").classList.add('is-hidden');
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					anzeige.show = "RNBOPL";

					break;
				case CONSTANTS.ARCHVERTRAG:
					ById("planall").checked = true;
					ById("ChgVertragGegenstand").classList.remove('is-hidden');
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					ById("ChgVertragMwSt").classList.remove('is-hidden');
					break;
				case CONSTANTS.DARLEHEN:
					ById("darall").checked = true;
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					break;
				case CONSTANTS.MEHRKOSTEN:
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					break;
				case CONSTANTS.GUTSCHRIFT:
					ById("ChgVertragUrnr").classList.add('is-hidden');
					ById("ChgVertragUrdatum").classList.add('is-hidden');
					ById("ChgVertragNotariat").classList.add('is-hidden');
					ById("ChgVertragFdat1").classList.add('is-hidden');
					ById("ChgVertragFdat2").classList.add('is-hidden');
					ById("ChgVertragAbdat").classList.add('is-hidden');
					ById("ChgVertragUdat").classList.add('is-hidden');
					ById("ChgVertragPreis").classList.add('is-hidden');
					ById('ChgVertragSdat').classList.add('is-hidden');

					break;
				default:
					ById("prjall").checked = true;
					ById("prj" + anzeige.projektid).checked = true;
					ById("obj" + anzeige.einheitid).checked = true;
			}
			anzeige.vertragid == 0 ? showVertrag(-1) : showVertrag(anzeige.vertragid);
			break;
		case "darlehen":
			ById('paneDarlehen').classList.remove('is-hidden');
			showDarlehen();
			ById("darall").checked = true;

			break;
		case "arbeitskorb":
			ById('paneArbeitskorb').classList.remove('is-hidden');
			showArbeitskorb("arbeitskorb");
			break;
		case "provlist":
			ById('panePROV').classList.remove('is-hidden');
			ById("menuprovcheck").checked = true;
			showPROV("provlist");
			break;
		case "provlisterfasst":
			ById('panePROV').classList.remove('is-hidden');
			ById("menuprovcheck").checked = true;
			showPROV("provlisterfasst");
			break;
		case "provlistfreigegeben":
			ById('panePROV').classList.remove('is-hidden');
			ById("menuprovcheck").checked = true;
			showPROV("provlistfreigegeben");
			break;
		case "provlistabgelehnt":
			ById('panePROV').classList.remove('is-hidden');
			ById("menuprovcheck").checked = true;
			showPROV("provlistabgelehnt");
			break;
		case "RNBNEU":
			ById('paneOPL').classList.remove('is-hidden');
			ById('colbtnsOPL').classList.remove('is-hidden');
			ById('oplPanelTbl').classList.add('is-hidden');
			ById('oplPanelTbl2').classList.add('is-hidden');
			ById('fldconftblOPL').classList.add('is-hidden');
			ById("menurnbcheck").checked = true;
			showOPL("RNBNEU");
			break;
		case "RNBOPL":
			ById('colbtnsOPL').classList.remove('is-hidden');
			ById('paneOPL').classList.remove('is-hidden');
			ById('oplPanelTbl').classList.add('is-hidden');
			ById('oplPanelTbl2').classList.remove('is-hidden');
			ById('fldconftblOPL').classList.remove('is-hidden');
			ById("menurnbcheck").checked = true;
			showOPL("RNBOPL");
			break;
		case "RNBHISTRECH":
			ById('colbtnsOPL').classList.remove('is-hidden');
			ById('paneOPL').classList.remove('is-hidden');
			ById('oplPanelTbl').classList.add('is-hidden');
			ById('oplPanelTbl2').classList.remove('is-hidden');
			ById('fldconftblOPL').classList.remove('is-hidden');
			ById("menurnbcheck").checked = true;
			showOPL("RNBHISTRECH");
			break;
		case "RNBALLRECH":
			ById('colbtnsOPL').classList.remove('is-hidden');
			ById('paneOPL').classList.remove('is-hidden');
			ById('oplPanelTbl2').classList.remove('is-hidden');
			ById('oplPanelTbl').classList.add('is-hidden');
			ById('fldconftblOPL').classList.remove('is-hidden');
			ById("menurnbcheck").checked = true;
			showOPL("RNBALLRECH");
			break;
		case "RNBENTWURF":
			ById('colbtnsOPL').classList.remove('is-hidden');
			ById('paneOPL').classList.remove('is-hidden');
			ById('oplPanelTbl2').classList.remove('is-hidden');
			ById('oplPanelTbl').classList.add('is-hidden');
			ById('fldconftblOPL').classList.remove('is-hidden');
			ById("menurnbcheck").checked = true;
			showOPL("RNBENTWURF");
			break;
		case "arcver":
			ById('paneArcVer').classList.remove('is-hidden');
			showArcVer();
			break;
		case "rechnung":
			ById('paneRechnung').classList.remove('is-hidden');
			anzeige.show = "rechnung";
			anzeige.rechnungid == 0 ? showRechnung(-1) : showRechnung(anzeige.rechnungid);
			rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			if (typeof rechnung != 'undefined') {
				vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag);
				switch (vertrag.typ) {
					case CONSTANTS.ARCHVERTRAG:
						ById("planall").checked = true;
						ById("vpi" + vertrag.id).checked = true;
						break;
					case CONSTANTS.DARLEHEN:
						ById("darall").checked = true;
						ById("dpi" + vertrag.id).checked = true;
						break;
					default:
						switch (anzeige.menu) {
							case "RNBNEU":
							case "RNBOPL":
							case "RNBENTWURF":
							case "RNBHISTRECH":
							case "RNBALLRECH":
								ById("menurnbcheck").checked = true;
								break;
							case "arbeitskorb":
								break;
							default:
								if (ById("ver" + vertrag.id) != null) {
									ById("ver" + vertrag.id).checked = true;
								}
								ById("prjall").checked = true;
								if (ById("prj" + vertrag.projekt) != null) {
									ById("prj" + vertrag.projekt).checked = true;
								}
								if (ById("obj" + vertrag.objekt) != null) {
									ById("obj" + vertrag.objekt).checked = true;
								}

						}

				}
			}
			break;
		case "rechnungedit":
			ById('paneRechnung').classList.remove('is-hidden');
			anzeige.show = "rechnung";
			anzeige.rechnungid == 0 ? showRechnung(-1) : showRechnung(anzeige.rechnungid);
			ById('modalRechnungChange').classList.add('is-active');
			rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			let typ;
			if (typeof vertraege != 'undefined') {
				typ = vertrag.typ;
			} else {
				typ = 999;
			}

			switch (typ) {
				case CONSTANTS.ARCHVERTRAG:
					ById("planall").checked = true;
					ById("vpi" + vertrag.id).checked = true;
					break;
				case CONSTANTS.DARLEHEN:
					ById("darall").checked = true;
					ById("dpi" + vertrag.id).checked = true;
					break;
				default:
					ById("ver" + vertrag.id).checked = true;
					ById("prjall").checked = true;
					ById("prj" + vertrag.projekt).checked = true;
					ById("obj" + vertrag.objekt).checked = true;
			}
			break;
		case "rechnungposedit":
			ById('paneRechnung').classList.remove('is-hidden');
			anzeige.show = "rechnung";
			//console.log(anzeige);
			anzeige.rechnungid == 0 ? showRechnung(-1) : showRechnung(anzeige.rechnungid);
			ById('modalRechnungPostenChange').classList.add('is-active');
			rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			if (typeof rechnung != 'undefined') {
				editRechnungPosten(rechnung.vertrag);
				rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			}
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			switch (vertrag.typ) {
				case CONSTANTS.ARCHVERTRAG:
					ById("planall").checked = true;
					ById("vpi" + vertrag.id).checked = true;
					break;
				case CONSTANTS.DARLEHEN:
					ById("darall").checked = true;
					ById("dpi" + vertrag.id).checked = true;
					break;
				default:
					ById("ver" + vertrag.id).checked = true;
					ById("prjall").checked = true;
					ById("prj" + vertrag.projekt).checked = true;
					ById("obj" + vertrag.objekt).checked = true;
			}
			break;
		case "vertragposedit":
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			let rnb, bmg;
			let objekt = DM.objekteDM.find(el => el.id == vertrag.objekt);
			switch (vertrag.typ) {
				case CONSTANTS.GUTSCHRIFT:
				case CONSTANTS.MEHRKOSTEN:
					ById('modalVertragPostenChange').classList.remove('is-active');
					ById("prjall").checked = true;
					ById("prj" + anzeige.projektid).checked = true;
					ById("obj" + anzeige.einheitid).checked = true;
					ById("ver" + anzeige.vertragid).checked = true;
					if (anzeige.rechnungid !== 0) {
						ById('paneRechnung').classList.remove('is-hidden');
						showRechnung(anzeige.rechnungid);
					} else {
						ById('paneVertrag').classList.remove('is-hidden');
						showVertrag(anzeige.vertragid);
					}
					bmg = typeof objekt != 'undefined' ? objekt.kaufpreis : 0;
					break;
				case CONSTANTS.ARCHVERTRAG:
					ById("planall").checked = true;
					ById('modalVertragPostenChange').classList.add('is-active');
					break;
				case CONSTANTS.DARLEHEN:
					ById("darall").checked = true;
					ById('modalVertragPostenChange').classList.add('is-active');
					break;
				case CONSTANTS.VERTRAGALLG:
					ById("menurnbcheck").checked = true;
					ById('modalVertragPostenChange').classList.remove('is-active');
					ById('paneRechnung').classList.remove('is-hidden');
					showRechnung(anzeige.rechnungid);
					rnb = 'true';
					bmg = typeof objekt != 'undefined' ? objekt.kaufpreis : 0;
					break;
				default:
					ById("prjall").checked = true;
					ById("prj" + anzeige.projektid).checked = true;
					ById("obj" + anzeige.einheitid).checked = true;
					ById("ver" + anzeige.vertragid).checked = true;
					ById('modalVertragPostenChange').classList.add('is-active');
					ById('paneVertrag').classList.remove('is-hidden');
					anzeige.vertragid == 0 ? showVertrag(-1) : showVertrag(anzeige.vertragid);
					rnb = 'false';
					bmg = vertrag.kaufpreis;
			}
			ById('tabverpos').innerHTML = "";
			addElementToVertragsPos(vertrag, rnb, bmg);

			break;
		case "rechnungZEedit":
			ById('paneRechnung').classList.remove('is-hidden');
			anzeige.show = "rechnung";
			anzeige.rechnungid == 0 ? showRechnung(-1) : showRechnung(anzeige.rechnungid);
			editRechnungZE();
			ById('modalRechnungZE').classList.add('is-active');
			rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			switch (vertrag.typ) {
				case CONSTANTS.ARCHVERTRAG:
					ById("planall").checked = true;
					break;
				case CONSTANTS.DARLEHEN:
					ById("darall").checked = true;
					break;
				default:
					ById("ver" + vertrag.id).checked = true;
					ById("prjall").checked = true;
					ById("prj" + vertrag.projekt).checked = true;
					ById("obj" + vertrag.objekt).checked = true;
			}

			break;
		case "kunde":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "kunde";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById("kunall").checked = true;
			break;
		case "kundeedit":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "kunde";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById('modalKundeChange').classList.add('is-active');
			ById("kunall").checked = true;
			break;
		case "notar":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "notar";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById("notarall").checked = true;
			break;
		case "notaredit":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "notare";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById('modalKundeChange').classList.add('is-active');
			ById("notarall").checked = true;
			break;
		case "firma":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "firma";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById("firall").checked = true;
			break;
		case "firmaedit":
			ById('paneKunde').classList.remove('is-hidden');
			anzeige.show = "firma";
			anzeige.kundeid == 0 ? showKunde(-1) : showKunde(anzeige.kundeid);
			ById('modalKundeChange').classList.add('is-active');
			ById("firmaall").checked = true;
			break;
		case "projekt":
			ById('paneProjekt').classList.remove('is-hidden');
			anzeige.show = "projekt";
			anzeige.projektid == 0 ? showProjekt(-1) : showProjekt(anzeige.projektid);
			ById("prjall").checked = true;
			//console.log("click on prj ", anzeige.projektid);
			if (anzeige.projektid != 0) {
				ById("prj" + anzeige.projektid).checked = true;
			}
			break;
		// case "projektedit":
		// 	ById('paneProjekt').classList.remove('is-hidden');
		// 	anzeige.show = "projekt";
		// 	anzeige.projektid == 0 ? showProjekt(-1) : showProjekt(anzeige.projektid);
		// 	anzeige.edit = true;
		// 	ipcRenderer.send('display:save', anzeige);
		// 	ById('modalProjektChange').classList.add('is-active');
		// 	ById("prjall").checked = true;
		// 	ById("prj" + anzeige.projektid).checked = true;
		// 	break;
		case "einheit":
			ById('paneObjekt').classList.remove('is-hidden');
			//console.log("click on einheit ", anzeige.einheitid);
			// anzeige.show = "projekt";
			// anzeige.projektid == 0 ? showProjekt(-1) : showProjekt(anzeige.projektid);	
			//if (anzeige.click.substring(0, 3) == 'obj' || anzeige.click.substring(0, 3) == 'opr') {
			showObjekt(anzeige.einheitid);
			ById("prjall").checked = true;
			ById("prj" + anzeige.projektid).checked = true;

			if (anzeige.click.substring(0, 3) == 'opr') {
				ById("obj" + anzeige.einheitid).checked = true;
			}

			//}
			break;
		case "prj0-direkt":
			console.log("prj0-direkt");

			ById("planall").checked = true;
			break;
		case "DAR-direkt":
			ById("darall").checked = true;
			break;
		default:
	}
	if (typeof anzeige.neuesRelease != 'undefined' && anzeige.neuesRelease == true) {
		anzeige.neuesRelease = false;
		ById('modalRelease').classList.add('is-active');
	}


}
function addListeners() {
	document.addEventListener('keydown', event => {
		if (event.key === 'Escape') {
			ById('modRnbEdit').classList.remove('is-active');
			ById('modalVertragChange').classList.remove('is-active');
			let active = ById('modalVertragPostenChange').classList.contains('is-active');
			if (anzeige.menu == "RNBENTWURF" ||
				anzeige.menu == "RNBOPL" ||
				anzeige.menu == "RNBNEU" ||
				anzeige.menu == "RNBHISTRECH" ||
				anzeige.menu == "RNBALLRECH"
			) {
				as = true;
			} else {
				as = false;
			}

			if (active && as) {
				anzeige.show = "rechnung";
			}
			ById('modalVertragPostenChange').classList.remove('is-active');
			ById('modalRechnungChange').classList.remove('is-active');
			ById('modalRechnungPostenChange').classList.remove('is-active');
			ById('modalRechnungZE').classList.remove('is-active');
			ById('modalProjektChange').classList.remove('is-active');
			ById('modalKundeChange').classList.remove('is-active');
			ById('modalObj').classList.remove('is-active');
			ById('modalRelease').classList.remove('is-active');
			ById('modalZE').classList.remove('is-active');
			ById('modalBuchungen').classList.remove('is-active');
			ById('modRnbEditProvision').classList.remove('is-active');
			ById('modProvEditProvision').classList.remove('is-active');
			anzeige.edit = false;
			ipcRenderer.send('display:save', anzeige);
		}
		if (event.key === 'AltGraph') {
			ById('paneMenu').classList.toggle('is-hidden');
		}
		if (event.key === 'Enter') {

			let onpressenter = event.target.getAttribute('onpressenter');
			//console.log(onpressenter);
			if (onpressenter != null) {
				let formater = event.target.getAttribute('formater');
				if (formater != null) {
					//console.log(formater, event.target.id);
					if (formater == 'date') {
						ById(event.target.id).value = formatDatum(ById(event.target.id).value);
					}
					if (formater == 'percent') {
						let temp = convProzentField(ById(event.target.id).value) / 100;
						ById(event.target.id).value = formatProzent(temp);
					}
					if (formater == 'betrag') {
						let temp = convPreisField(ById(event.target.id).value);
						ById(event.target.id).value = formatBetrag(temp);
					}

				}
				ById(onpressenter).click();
			}
			if (event.target.id == 'inprecBtnsInput') {
				ById('inprecBtnsInput').classList.add('is-hidden');
				let msg = new Object();
				msg.alt = ById('inprecBtnsInput').getAttribute("freifeld");
				msg.neu = ById('inprecBtnsInput').value;
				msg.rechid = anzeige.rechnungid;
				ipcRenderer.send('template:changeRecName', msg);
			}

			if (ById('modRnbEditProvision').classList.contains('is-active')) {
				let msg = new Object();
				msg.data = getDatafromTable();
				msg.rechnung = ById('modRnbEditProvision-id').value;
				ipcRenderer.send('provision:changeRech', msg);
			}


		}
		if (event.key === 'Tab') {

			let formater = event.target.getAttribute('formater');
			//console.log(formater);
			if (formater != null) {
				//console.log(formater, event.target.id);
				if (formater == 'date') {
					ById(event.target.id).value = formatDatum(ById(event.target.id).value);
				}
				if (formater == 'percent') {
					let temp = convProzentField(ById(event.target.id).value) / 100;
					if (!isNaN(temp)) {
						ById(event.target.id).value = formatProzent(temp);
					}
				}
				if (formater == 'betrag') {
					let temp = convPreisField(ById(event.target.id).value);
					ById(event.target.id).value = formatBetrag(temp);
				}
				if (formater == 'objnr') {
					copyDataFields(ById(event.target.id).value);
				}
			}
			if (event.target.id == 'prj-iban') {
				let iban = ById('prj-iban').value;
				ById('prj-iban').value = formatIBAN(iban);

				$json_url = "https://openiban.com/validate/" + iban + "?getBIC=true&validateBankCode=true";
				fetch($json_url)
					.then(function (response) {
						return response.json();
					})
					.then(function (myJson) {
						myJson.bankData.name != "" ? ById('prj-bank').value = myJson.bankData.name : ById('prj-bank').value = "";
						typeof myJson.bankData.bic != "undefined" ? ById('prj-bic').value = myJson.bankData.bic : ById('prj-bic').value = "";
						//console.log("data: ", myJson);
					})
					.catch(function (error) {
						console.log("Error: " + error);
					});
			}

			if (event.target.id == 'prj-ibanE') {
				let iban = ById('prj-ibanE').value;
				ById('prj-ibanE').value = formatIBAN(iban);

				// $json_url = "https://openiban.com/validate/" + iban + "?getBIC=true&validateBankCode=true";
				// fetch($json_url)
				// 	.then(function (response) {
				// 		return response.json();
				// 	})
				// 	.then(function (myJson) {
				// 		myJson.bankData.name != "" ? ById('prj-bankE').value = myJson.bankData.name : ById('prj-bankE').value = "";
				// 		typeof myJson.bankData.bic != "undefined" ? ById('prj-bicE').value = myJson.bankData.bic : ById('prj-bicE').value = "";
				// 		//console.log("data: ", myJson);
				// 	})
				// 	.catch(function (error) {
				// 		console.log("Error: " + error);
				// 	});
			}
			if (event.target.id == 'inpProvEditbetrag' || event.target.id == 'inpProvEditanteil') {
				let netto = convPreisField(ById('dpProvEdit-rechnetto').value);
				let anteil = convProzentField(ById('inpProvEditanteil').value);
				anteil = isNaN(anteil) ? 0 : anteil;
				let betrag = convPreisField(ById('inpProvEditbetrag').value);
				let provgesamt = parseFloat(netto) * parseFloat(anteil) / 100 + parseFloat(betrag);

				ById('inpProvEditgesamtinfo').innerHTML = "Provision Gesamt =  " + formatBetrag(netto) + " * " + formatProzent(anteil / 100) +
					" + " + formatBetrag(betrag) + " = ";
				ById('inpProvEditgesamtbetrag').innerHTML = formatBetrag(provgesamt);
			}


		}
		//console.log(anzeige, event.key);
		if (event.key === 'Backspace') {
			if (0) {
				if (event.target.id != 'inprecBtnsInput') {
					if (anzeige.menu == "RNBENTWURF" ||
						anzeige.menu == "RNBOPL" ||
						anzeige.menu == "RNBNEU" ||
						anzeige.menu == "RNBHISTRECH" ||
						anzeige.menu == "arbeitskorb" ||
						anzeige.menu == "RNBALLRECH"
					) {
						as = true;
					} else {
						as = false;
					}
					if (as && anzeige.show == "rechnung") {
						anzeige.show = anzeige.menu;
						ipcRenderer.send('display:save', anzeige);
						updateAnzeige();
					}
					if (anzeige.menu == "arbeitskorb") {
						anzeige.show = anzeige.menu;
						ipcRenderer.send('display:save', anzeige);
						updateAnzeige();
					}
				}
			}
		}
		if (event.target.id == "inpVerKunAutolookup") {
			let suchstr = ById(event.target.id).value;

			if ((event.keyCode > 64 && event.keyCode < 90) || (event.keyCode > 97 && event.keyCode < 122) ||
				event.keyCode == 219 || event.keyCode == 186 || event.keyCode == 192 || event.keyCode == 222) {
				suchstr += event.key;
			}
			if (ById('inpVerKunAutolookup').value == 'Nicht zugeordnet') {
				ById('inpVerKunAutolookup').value = "";
			}
			//console.log("key: ", event.key, event.keyCode, ById('inpVerKunAutolookup').value,"suchstr ", suchstr,"Länge ", suchstr.length);
			if (suchstr.length == 1) { updateDropDowns(); } else {
				let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
				switch (vertrag.typ) {
					// case CONSTANTS.DARLEHEN:
					// 	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.lieferant == true);
					// 	break;
					default:
						kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.kunde == true);
				}
				kun = kunden.filter(el => {
					let result = 0;
					if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.name2 != 'undefined') { result += el.name2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname2 != 'undefined') { result += el.vorname2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					return result;
				});
				if (typeof kun != 'undefined') {
					ById('dpVerKun-content').innerHTML = "";
					kun.sort((a, b) => {
						let textA = a.name.toUpperCase();
						let textB = b.name.toUpperCase();
						return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
					});
					for (i = 0; i < kun.length; i++) {
						obj = kun[i];
						el1 = document.createElement('div');
						el1.className = 'dropdown-item';
						el1.id = "vku-" + obj.id;
						el1.innerHTML = obj.name + ", " + obj.vorname;
						if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
							if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
								el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;
							}
						}
						ById('dpVerKun-content').appendChild(el1);
					}
				}
			}
		}
		if (event.target.id == "inpVerKun2Autolookup") {
			let suchstr = ById(event.target.id).value;

			if ((event.keyCode > 64 && event.keyCode < 90) || (event.keyCode > 97 && event.keyCode < 122) ||
				event.keyCode == 219 || event.keyCode == 186 || event.keyCode == 192 || event.keyCode == 222) {
				suchstr += event.key;
			}
			if (ById('inpVerKun2Autolookup').value == 'Nicht zugeordnet') {
				ById('inpVerKun2Autolookup').value = "";
			}
			//console.log("key: ", event.key, event.keyCode, ById('inpVerKunAutolookup').value,"suchstr ", suchstr,"Länge ", suchstr.length);
			if (suchstr.length == 1) { updateDropDowns(); } else {
				let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
				switch (vertrag.typ) {
					// case CONSTANTS.DARLEHEN:
					// 	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.lieferant == true);
					// 	break;
					default:
						kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.kunde == true);
				}
				kun = kunden.filter(el => {
					let result = 0;
					if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.name2 != 'undefined') { result += el.name2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname2 != 'undefined') { result += el.vorname2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					return result;
				});

				if (typeof kun != 'undefined') {

					ById('dpVerKun2-content').innerHTML = "";
					kun.sort((a, b) => {
						let textA = a.name.toUpperCase();
						let textB = b.name.toUpperCase();
						return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
					});
					for (i = 0; i < kun.length; i++) {
						obj = kun[i];
						el1 = document.createElement('div');
						el1.className = 'dropdown-item';
						el1.id = "vk2-" + obj.id;
						strtmp = obj.name;
						if (obj.vorname != "") {
							strtmp += ", " + obj.vorname;
						}
						el1.innerHTML = strtmp;
						if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
							if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
								el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

							}
						}
						ById('dpVerKun2-content').appendChild(el1);
					}
				}
			}
		}
		if (event.target.id == "inpSucheAutolookup") {
			let suchstr = ById(event.target.id).value;

			if ((event.keyCode > 64 && event.keyCode < 90) || (event.keyCode > 97 && event.keyCode < 122) ||
				event.keyCode == 219 || event.keyCode == 186 || event.keyCode == 192 || event.keyCode == 222) {
				suchstr += event.key;
			}
			if (event.keyCode == 8) {
				// console.log( "suchstr ", suchstr, suchstr.length );

				suchstr = suchstr.substring(0, suchstr.length - 1);
			}
			// console.log("key: ", event.key, event.keyCode, "suchstr ", suchstr,"Länge ", suchstr.length);

			anzeige.suche = suchstr;
			updateAnzeige();
			showSearchResults();



		}
		if (event.target.id == "indpRnbEditKnd") {
			let suchstr = ById(event.target.id).value;

			if ((event.keyCode > 64 && event.keyCode < 90) || (event.keyCode > 97 && event.keyCode < 122) ||
				event.keyCode == 219 || event.keyCode == 186 || event.keyCode == 192 || event.keyCode == 222) {
				suchstr += event.key;
			}
			if (ById('indpRnbEditKnd').value == 'Bitte wählen') {
				ById('indpRnbEditKnd').value = "";
			}
			//console.log("key: ", event.key, event.keyCode, ById('indpRnbEditKnd').value,"suchstr ", suchstr,"Länge ", suchstr.length);

			if (suchstr.length == 1) { updateDropDowns(); } else {
				let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));

				kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE);

				kun = kunden.filter(el => {
					let result = 0;
					if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.name2 != 'undefined') { result += el.name2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname2 != 'undefined') { result += el.vorname2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					return result;
				});

				if (typeof kun != 'undefined') {

					ById('dpRnbEditKnd-content').innerHTML = "";
					kun.sort((a, b) => {
						let textA = a.name.toUpperCase();
						let textB = b.name.toUpperCase();
						return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
					});
					for (i = 0; i < kun.length; i++) {
						obj = kun[i];
						el1 = document.createElement('div');
						el1.className = 'dropdown-item';
						el1.id = "rek-" + obj.id;
						el1.innerHTML = obj.name + ", " + obj.vorname;
						if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
							if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
								el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

							}
						}
						ById('dpRnbEditKnd-content').appendChild(el1);
					}
				}
			}
		}

		if (event.target.id == "indpRnbEditKndObj") {
			let suchstr = ById(event.target.id).value;

			if ((event.keyCode > 64 && event.keyCode < 90) || (event.keyCode > 97 && event.keyCode < 122) ||
				event.keyCode == 219 || event.keyCode == 186 || event.keyCode == 192 || event.keyCode == 222) {
				suchstr += event.key;
			}
			if (ById('indpRnbEditKndObj').value == 'Bitte wählen' || ById('indpRnbEditKnd').value == "bisher kein Käufer zugeordnet") {
				ById('indpRnbEditKndObj').value = "";
			}
			//console.log("key: ", event.key, event.keyCode, ById('indpRnbEditKnd').value,"suchstr ", suchstr,"Länge ", suchstr.length);

			if (suchstr.length == 1) { updateDropDowns(); } else {
				let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));

				kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.kunde == true);

				kun = kunden.filter(el => {
					let result = 0;
					if (typeof el.name != 'undefined') { result += el.name.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname != 'undefined') { result += el.vorname.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.name2 != 'undefined') { result += el.name2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					if (typeof el.vorname2 != 'undefined') { result += el.vorname2.toLowerCase().indexOf(suchstr.toLowerCase()) >= 0 ? 1 : 0; }
					return result;
				});

				if (typeof kun != 'undefined') {

					ById('dpRnbEditKndObj-content').innerHTML = "";
					kun.sort((a, b) => {
						let textA = a.name.toUpperCase();
						let textB = b.name.toUpperCase();
						return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
					});
					for (i = 0; i < kun.length; i++) {
						obj = kun[i];
						el1 = document.createElement('div');
						el1.className = 'dropdown-item';
						el1.id = "rok-" + obj.id;
						el1.innerHTML = obj.name + ", " + obj.vorname;
						if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
							if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
								el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

							}
						}
						ById('dpRnbEditKndObj-content').appendChild(el1);
					}
				}
			}
		}
	});

	listenerKontextemenu();

	document.body.addEventListener("dragover", evt => {
		evt.preventDefault();
	});
	document.body.addEventListener("drop", evt => {
		evt.preventDefault();
		//console.log(evt);
		for (let f of evt.dataTransfer.files) {
			//console.log('File(s) you dragged here: ', f.path)
			neueNotizspeichern(f.path);
		}

	});
	ById('dpRechStatus').addEventListener('click', function () {
		if (event.target.classList.contains('dropdown-item')) {
			switch (event.target.id) {
				case "rst-DELETE":
					ById('dpRechStatus-id').value = CONSTANTS.DELETE;
					ById('dpRechStatus-button').innerHTML = CONSTANTS.DELETE;
					break;
				case "rst-INARBEIT":
					ById('dpRechStatus-id').value = CONSTANTS.ENTWURF;
					ById('dpRechStatus-button').innerHTML = CONSTANTS.ENTWURF;
					break;
				case "rst-VERSCHICKT":
					ById('dpRechStatus-id').value = CONSTANTS.VERSCHICKT;
					ById('dpRechStatus-button').innerHTML = CONSTANTS.VERSCHICKT;
					break;
				case "rst-ERLEDIGT":
					ById('dpRechStatus-id').value = CONSTANTS.BEZAHLT;
					ById('dpRechStatus-button').innerHTML = CONSTANTS.BEZAHLT;
					break;
				default:
					ById('dpRechStatus-id').value = CONSTANTS.UNKNOWN;
					ById('dpRechStatus-button').innerHTML = CONSTANTS.UNKNOWN;
			}

		}

	});
	ById('dpVerKun').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			kun = DM.kundenDM.find(el => el.id == i);
			ById('dpVerKun-id').value = i;
			//ById('dpVerKun-button').innerHTML = kun.vorname + " " + kun.name;
			ById('inpVerKunAutolookup').value = kun.vorname + " " + kun.name;
			if (typeof kun.anrede !== 'undefined') { ById('ver-anrede').value = kun.anrede; }
			if (typeof kun.name !== 'undefined') { ById('ver-name').value = kun.name; }
			if (typeof kun.vorname !== 'undefined') { ById('ver-vorname').value = kun.vorname; }

		}

	});
	ById('dpVerKun2').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			kun = DM.kundenDM.find(el => el.id == i);
			ById('dpVerKun2-id').value = i;
			//ById('dpVerKun-button').innerHTML = kun.vorname + " " + kun.name;
			ById('inpVerKun2Autolookup').value = kun.vorname + " " + kun.name;

		}

	});


	ById('dpVerStatus').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			switch (event.target.id) {
				case "vst-RESERVIERT":
					ById('dpVerStatus-id').value = CONSTANTS.RESERVIERT;
					ById('dpVerStatus-button').innerHTML = CONSTANTS.RESERVIERT;
					break;
				case "vst-GEKAUFT":
					ById('dpVerStatus-id').value = CONSTANTS.GEKAUFT;
					ById('dpVerStatus-button').innerHTML = CONSTANTS.GEKAUFT;
					break;
				case "vst-BEZAHLT":
					ById('dpVerStatus-id').value = CONSTANTS.BEZAHLT;
					ById('dpVerStatus-button').innerHTML = CONSTANTS.BEZAHLT;
					break;
				default:
					ById('dpVerStatus-id').value = CONSTANTS.UNKNOWN;
					ById('dpVerStatus-button').innerHTML = CONSTANTS.UNKNOWN;
			}

		}

	});
	// -------------------------------------------------------------------------------
	ById('dpRnbEditKnd').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			kun = DM.kundenDM.find(el => el.id == i);
			ById('dpRnbEditKnd-id').value = i;
			//ById('dpVerKun-button').innerHTML = kun.vorname + " " + kun.name;
			ById('indpRnbEditKnd').value = kun.vorname + " " + kun.name;

		}

	});
	ById('dpRnbEditKndObj').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			kun = DM.kundenDM.find(el => el.id == i);
			ById('dpRnbEditKndObj-id').value = i;
			//ById('dpVerKun-button').innerHTML = kun.vorname + " " + kun.name;
			ById('indpRnbEditKndObj').value = kun.vorname + " " + kun.name;

		}

	});
	ById('dpRnbEditPrj').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			//console.log(event.target.id);
			i = event.target.id.substring(4, event.target.id.length);
			if (i == 'keine') {
				ById('dpRnbEditPrj-id').value = "";
				ById('dpRnbEditPrj-button').innerHTML = "nichts ausgewählt";
			} else {
				ById('dpRnbEditPrj-id').value = i;
				let projekt = DM.projekteDM.find(el => el.id == i && el.id != 0 && el.status != CONSTANTS.TRASH);
				ById('dpRnbEditPrj-button').innerHTML = projekt.prjnummer + " " + projekt.name;
				// Nur wenn ein PRojekt gewählt wurde kann man die Objektliste im DP dazu ausfüllen
				let objekte = DM.objekteDM.filter(el => el.projekt == i && el.status != CONSTANTS.TRASH);
				ById('dpRnbEditObj-content').innerHTML = "";
				if (typeof objekte == 'undefined' || objekte.length > 0) {
					objekte.forEach(objekt => {
						let el1 = document.createElement('div');
						el1.className = 'dropdown-item';
						el1.id = "roe-" + objekt.id;
						el1.innerHTML = objekt.text + " " + objekt.objnummer
						ById('dpRnbEditObj-content').appendChild(el1);
					});
					ById('dpRnbEditObj-button').innerHTML = "Bitte wählen " + projekt.prjnummer;
				} else {
					ById('dpRnbEditObj-button').innerHTML = "Keine Einheiten zu " + projekt.name + " angelegt!";

				}

			}
		}
	});
	ById('dpRnbEditObj').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			//console.log(event.target.id);
			i = event.target.id.substring(4, event.target.id.length);
			if (i == 'keine') {
				ById('dpRnbEditObj-id').value = "";
				ById('dpRnbEditObj-button').innerHTML = "nix gewählt";
			} else {
				ById('dpRnbEditObj-id').value = i;
				let objekt = DM.objekteDM.find(el => el.id == i && el.status != CONSTANTS.TRASH);
				ById('dpRnbEditObj-button').innerHTML = objekt.text + " " + objekt.objnummer;
				// Und noch den Kunden eintragen
				initdpRnbEditKndObj(objekt.kunde);

			}
		}
	});

	ById('butRnbEditOk').addEventListener('click', function () {
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
		ById('modRnbEdit').classList.remove('is-active');
		let rechid = ById('modRnbEdit-id').value;
		let objid = ById('dpRnbEditObj-id').value;
		let prjid = ById('dpRnbEditPrj-id').value;
		let kunid = ById('dpRnbEditKnd-id').value;
		let kunObjid = ById('dpRnbEditKndObj-id').value;
		let bezug = ById('modRnbEditBezug').value;

		//console.log("Bezug:", bezug, "Prj ", prjid, "Obj ", objid, "Kun ", kunid, "Käufer", kunObjid, "Rech ", rechid,);

		if (rechid != 0) {
			// Das ist eine Bearbeitung
			//console.log("Edit Rech ID=", rechid);
			let rechnung = DM.rechnungenDM.find(el => el.id == rechid && el.status != CONSTANTS.TRASH);
			let vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag && el.status != CONSTANTS.TRASH);
			//console.log("Edit vertrag=", vertrag, "change ", kunid);
			vertrag.kunde = kunid;
			vertrag.projekt = prjid;
			vertrag.objekt = objid;
			vertrag.kaeufer = kunObjid;
			if (typeof vertrag.kunde !== 'undefined' && vertrag.kunde != "") {
				kde = DM.kundenDM.find(re => re.id == vertrag.kunde);
				if (typeof kde != 'undefined') {
					vertrag.vorname = kde.vorname;
					vertrag.name = kde.name;
				}

			}
			ipcRenderer.send('vertrag:change', vertrag);

			rechnung.rechdat = ById('RnbEditRechdat').value;
			rechnung.rechnr = ById('RnbEditRechNr').value;
			rechnung.faelligdat = ById('RnbEditFaelligdat').value;
			rechnung.leistungszeitraum = ById('RnbEditleistungszeitraum').value;
			rechnung.rechtyp = ById('RnbEditrechtyp').value;
			rechnung.kostenstelle = ById('RnbEditkostenstelle').value;
			rechnung.txtprj = ById('RnbEditprojektfrei').value;
			rechnung.txtbeschreibung = ById('RnbEditbeschreibungfrei').value;
			rechnung.txtobj = ById('RnbEditobjektfrei').value;
			rechnung.objektnr = ById('RnbEditobjektnr').value;
			rechnung.txtkunde = ById('RnbEditkunde').value;
			rechnung.m1dat = ById('RnbEditM1dat').value;
			rechnung.m2dat = ById('RnbEditM2dat').value;
			rechnung.m3dat = ById('RnbEditM3dat').value;
			rechnung.mahnstufe = ById('RnbEditMahnstufe').value
			ipcRenderer.send('rechnung:change', rechnung);

		} else {
			//
			//  Vertrag und Rechnung neu anlegen
			//
			let template = ById('RnbEdittemplate').value;
			let item = new Object();
			item.type = "VertragUndRechnung";
			item.template = template;
			item.bezug = bezug;

			if (bezug == 'true') {
				let objekt = DM.objekteDM.find(el => el.id == objid && el.status != CONSTANTS.TRASH);
				item.betrag = typeof objekt != 'undefined' ? objekt.kaufpreis : 0;
				item.projekt = prjid;
				item.objekt = objid;
				item.kunObjid = kunObjid;

			}
			item.rechdat = ById('RnbEditRechdat').value;
			item.m1dat = ById('RnbEditM1dat').value;
			item.m2dat = ById('RnbEditM2dat').value;
			item.m3dat = ById('RnbEditM3dat').value;
			item.mahnstufe = ById('RnbEditMahnstufe').value
			item.rechnr = ById('RnbEditRechNr').value;
			item.faelligdat = ById('RnbEditFaelligdat').value;
			item.faelligdat = ById('RnbEditFaelligdat').value;
			item.leistungszeitraum = ById('RnbEditleistungszeitraum').value;
			item.rechtyp = ById('RnbEditrechtyp').value;
			item.kostenstelle = ById('RnbEditkostenstelle').value;
			item.txtprj = ById('RnbEditprojektfrei').value;
			item.txtbeschreibung = ById('RnbEditbeschreibungfrei').value;
			item.txtobj = ById('RnbEditobjektfrei').value;
			item.txtkunde = ById('RnbEditkunde').value;
			item.objektnr = ById('RnbEditobjektnr').value;

			item.kunde = kunid;
			//	console.log("Neu anlegen ", item);
			ipcRenderer.send('edit:new', item);
		}


	});
	ById('butRnbEditCancel').addEventListener('click', function () {
		ById('modRnbEdit').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	// -------------------------------------------------------------------------------
	ById('dpKunAnrede').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			i = event.target.id.substring(4, event.target.id.length);
			if (i == 'keine') {
				ById('dpKunAnrede-id').value = "";
				ById('dpKunAnrede-button').innerHTML = "keine Anrede";
			} else {
				ById('dpKunAnrede-id').value = i;
				ById('dpKunAnrede-button').innerHTML = DM.anredeliste[i];
			}
		}
	});
	ById('dpKunAnrede2').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			i = event.target.id.substring(4, event.target.id.length);
			if (i == 'keine') {
				ById('dpKunAnrede2-id').value = "";
				ById('dpKunAnrede2-button').innerHTML = "keine Anrede";
			} else {
				ById('dpKunAnrede2-id').value = i;
				ById('dpKunAnrede2-button').innerHTML = DM.anredeliste[i];
			}
		}
	});
	// Zahlungsvorgänge --------------------------------
	ById('txtRecPos3').addEventListener('click', function (event) {
		if (event.target.classList.contains('referenz')) {
			if (roleedit) {
				let refnr = event.target.getAttribute('refnr');
				let ze = DM.zeDM.find(el => parseInt(el.id) == parseInt(refnr));
				let rechnung = DM.rechnungenDM.find(el => el.id == ze.rechnung);
				if (rechnung.status !== CONSTANTS.BEZAHLT) {
					ById('inpZEDatum').value = formatDatum(ze.datum);
					ById('inpZEText').value = ze.text;
					ById('inpZEBetrag').value = formatBetrag(ze.betrag);
					ById('modZEId').value = ze.id;
					ById('modalZE').classList.add('is-active');
					ById('btnmodZEDelete').classList.remove('is-hidden');
					ById('titlemodZE').innerHTML = "Neuer Zahlungsvorgang";
				}
			}
		}
	});
	// Rechnungspositionen --------------------------------
	ById('rec-pos-tablebody').addEventListener('click', function (event) {
		let refnr = event.target.getAttribute('refnr');
		if (parseInt(refnr) > 0) {
			anzeige.edit = true;
			ipcRenderer.send('display:save', anzeige);

			let rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
			let vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag);
			let position = DM.positionenDM.find(el => parseInt(el.id) == parseInt(refnr));

			if (typeof vertrag != 'undefined' && (vertrag.typ == CONSTANTS.VERTRAGALLG || vertrag.typ == CONSTANTS.MEHRKOSTEN)) {
				anzeige.show = "vertragposedit";
				let objekt = DM.objekteDM.find(el => el.id == vertrag.objekt);
				let betrag = typeof objekt != 'undefined' ? objekt.betrag : 0;

				if (position != 'undefined') {
					ById('modalVertragPostenChange').classList.add('is-active');
					ById('tabverpos').innerHTML = "";
					appendPosZeile(position.id, position.rechpos, position.text,
						position.anteil, position.netto, position.mwst, position.betrag, true, true, false, rnb = true, position.bmgBetrag, position.faktor);
				}
			}
			ipcRenderer.send('display:save', anzeige);


		}
	});
	// ---------------------------------------
	ById('btnZEadd').addEventListener('click', function () {
		ById('inpZEDatum').value = getDatum(0);
		ById('inpZEText').value = "";
		ById('inpZEBetrag').value = "";
		ById('modZEId').value = -1;
		ById('modalZE').classList.add('is-active');
		ById('btnmodZEDelete').classList.add('is-hidden');
		ById('titlemodZE').innerHTML = "Neuer Zahlungsvorgang";
	});
	ById('btnmodZECancel').addEventListener('click', function () {
		ById('modalZE').classList.remove('is-active');
	});
	ById('btnmodZEClose').addEventListener('click', function () {
		ById('modalZE').classList.remove('is-active');
	});
	ById('btnmodZEDelete').addEventListener('click', function () {
		ById('modalZE').classList.remove('is-active');
		ipcRenderer.send('ze:delete', ById('modZEId').value);
	});
	ById('btnmodZEOk').addEventListener('click', function (event) {
		ById('modalZE').classList.remove('is-active');
		index = ById('modZEId').value;
		let ze = new Object();
		ze.rechnung = ById('rech-id').value;
		ze.text = ById('inpZEText').value;
		ze.datum = ById('inpZEDatum').value;
		ze.betrag = convPreisField(ById('inpZEBetrag').value);
		if (ById('modZEId').value == "") {
			ze.id = -1;
		} else {
			ze.id = ById('modZEId').value;
		}
		ipcRenderer.send('ze:change', ze);
	});


	// Buchungen --------------------------------


	ById('btnmodBuchungCancel').addEventListener('click', function () {
		ById('modalBuchungen').classList.remove('is-active');
	});
	ById('btnmodBuchungClose').addEventListener('click', function () {
		ById('modalBuchungen').classList.remove('is-active');
	});
	ById('btnmodBuchungDelete').addEventListener('click', function () {
		ById('modalBuchungen').classList.remove('is-active');
		ipcRenderer.send('buchung:delete', ById('modBuchungRefNr').value);
	});
	ById('btnDarStatusForward').addEventListener('click', function (event) {
		let refid = event.target.getAttribute('ref');
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(refid));
		vertrag.status = nextStatus(vertrag.status);
		ipcRenderer.send('vertrag:change', vertrag);
	});
	ById('btnDarStatusBack').addEventListener('click', function (event) {
		let refid = event.target.getAttribute('ref');
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(refid));
		vertrag.status = lastStatus(vertrag.status);
		ipcRenderer.send('vertrag:change', vertrag);
	});
	ById('btnmodBuchungOk').addEventListener('click', function (event) {
		ById('modalBuchungen').classList.remove('is-active');
		let konto = event.target.getAttribute('konto');
		let buchung = new Object();
		buchung.type = "Buchung";
		buchung.bdat = new Date();
		buchung.vdat = string2Date(ById('inpBuchungDatum').value);
		buchung.text = ById('inpBuchungText').value;
		buchung.konto = parseInt(konto);
		buchung.pn = ById('titlemodBuchung').innerHTML;
		buchung.betrag = Math.abs(convPreisField(ById('inpBuchungBetrag').value));
		//console.log(buchung.pn, buchung.betrag);
		switch (buchung.pn) {
			case CONSTANTS.DARAUSZAHLUNG:

				break;
			case CONSTANTS.DARRUECKZAHLUNG:
				buchung.betrag = -buchung.betrag;
				console.log(buchung.betrag);

				break;
			case CONSTANTS.DARZINS:

				break;

			default:

		}
		if (ById('modBuchungRefNr').value == "") {
			ipcRenderer.send('edit:new', buchung);

		} else {
			buchung.id = ById('modBuchungRefNr').value;
			ipcRenderer.send('buchung:change', buchung);
		}
	});
	ById('btnDarlehenExport').addEventListener('click', function (event) {

		let filename = (ENV == "production") ? path.join(process.resourcesPath, "Vorlage_darexport.docx") : "D:/Vorlage_darexport.docx";
		//let filename = path.join(process.resourcesPath, "Vorlage_darexport.docx");
		//let filename = "D:/Vorlage_darexport.docx";
		let data = new Object();
		data.datum = getDatum(0);
		data.darlehen = exportArr;
		data.name = "Darlehen";
		generiereBrief(filename, data);
	});
	ById('btnDarBuchenRueck').addEventListener('click', function (event) {
		ById('inpBuchungDatum').value = formatDateObject(new Date());;
		ById('inpBuchungText').value = "";
		ById('inpBuchungBetrag').value = "";
		ById('modBuchungRefNr').value = "";
		ById('modalBuchungen').classList.add('is-active');
		ById('btnmodBuchungDelete').classList.add('is-hidden');
		ById('titlemodBuchung').innerHTML = CONSTANTS.DARRUECKZAHLUNG;
	});
	ById('btnDarBuchenZins').addEventListener('click', function (event) {
		ById('inpBuchungDatum').value = formatDateObject(new Date());;
		ById('inpBuchungText').value = "";
		ById('inpBuchungBetrag').value = "";
		ById('modBuchungRefNr').value = "";
		ById('modalBuchungen').classList.add('is-active');
		ById('btnmodBuchungDelete').classList.add('is-hidden');

		ById('titlemodBuchung').innerHTML = CONSTANTS.DARZINS;

	});
	ById('btnDarBuchenAus').addEventListener('click', function (event) {
		ById('inpBuchungDatum').value = formatDateObject(new Date());;
		ById('inpBuchungText').value = "";
		ById('inpBuchungBetrag').value = "";
		ById('modBuchungRefNr').value = "";
		ById('modalBuchungen').classList.add('is-active');
		ById('btnmodBuchungDelete').classList.add('is-hidden');


		ById('titlemodBuchung').innerHTML = CONSTANTS.DARAUSZAHLUNG;
	});
	ById('darposTable').addEventListener('click', function (event) {
		if (event.target.classList.contains('referenz')) {
			if (roleedit) {
				let refnr = event.target.innerHTML;
				let buchung = DM.buchungen.find(el => el.id == refnr);
				console.log("Buchung. ", buchung);
				ById('inpBuchungDatum').value = formatDateObject(buchung.vdat);
				ById('inpBuchungText').value = buchung.text;
				ById('inpBuchungBetrag').value = formatBetrag(buchung.betrag);
				ById('modalBuchungen').classList.add('is-active');
				ById('btnmodBuchungDelete').classList.remove('is-hidden');

				ById('titlemodBuchung').innerHTML = "Buchung bearbeiten";
				ById('modBuchungRefNr').value = refnr;
			}

		}

	});
	//  --------------------------------



	ById('dpVerNotar').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			//console.log(event.target.innerHTML);
			ById('dpVerNotar-button').innerHTML = event.target.innerHTML;
		}
	});
	ById('dpVerMwSt').addEventListener('click', function (event) {
		if (event.target.classList.contains('dropdown-item')) {
			//console.log(event.target.innerHTML);
			ById('dpVerMwSt-button').innerHTML = event.target.innerHTML;
		}
	});
	ById('dpKunStatus').addEventListener('click', function () {
		if (anzeige.edit == true) {
			if (event.target.classList.contains('dropdown-item')) {
				switch (event.target.id) {
					case "kst-RESERVIERT":
						ById('dpKunStatus-id').value = CONSTANTS.RESERVIERT;
						ById('dpKunStatus-button').innerHTML = CONSTANTS.RESERVIERT;
						break;
					case "kst-GEKAUFT":
						ById('dpKunStatus-id').value = CONSTANTS.GEKAUFT;
						ById('dpKunStatus-button').innerHTML = CONSTANTS.GEKAUFT;
						break;
					case "kst-BEZAHLT":
						ById('dpKunStatus-id').value = CONSTANTS.BEZAHLT;
						ById('dpKunStatus-button').innerHTML = CONSTANTS.BEZAHLT;
						break;
					default:
						ById('dpKunStatus-id').value = CONSTANTS.UNKNOWN;
						ById('dpKunStatus-button').innerHTML = CONSTANTS.UNKNOWN;
				}
			}
		}
	});
	ById('dp-Ver-Pos-Template-objekt').addEventListener('click', function () {
		target = event.target.id.substring(0, 3);
		if (target == "ztp") {
			id = event.target.id.substring(4, event.target.id.length);
			addZTP(id);
		}

	});
	ById('btnvorlage1').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER0);
	});
	ById('btnvorlage2').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER1);
	});
	ById('btnvorlage3').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER2);
	});
	ById('btnvorlageR3').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER3);
	});
	ById('btnvorlageR4').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER4);
	});
	ById('btnRechAllg').addEventListener('click', function () {
		btnVorlageAction(anzeige.rechnungid, CONSTANTS.TEMPLATER5);
	});
	ById('btnvorlageV1').addEventListener('click', function () {
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
		let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
		let data = collectdata(vertrag);
		data.typ = CONSTANTS.TEMPLATEV1;
		generiereBrief(projekt[CONSTANTS.TEMPLATEV1], data);

	});
	ById('btnvorlageP1').addEventListener('click', function (event) {
		let prjid = event.target.getAttribute('prj');
		let data = collectPrjData(prjid);
		let filename = "";
		let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(prjid));
		if (typeof projekt[CONSTANTS.TEMPLATEP1] != 'undefined') {
			filename = projekt[CONSTANTS.TEMPLATEP1];
		} else {
			filename = "..\\..\\Vorlagen\\VorlagePrjTabelle.docx";
		}
		//let filename = (ENV == "production") ? path.join(process.resourcesPath, "../extraResources/Vorlage_prjexport.docx") : "D:/Vorlage_prjexport.docx";
		//let filename = path.join(process.resourcesPath, "Vorlage_prjexport.docx");
		//console.log(filename,data);
		generiereBrief(filename, data);

	});

	ById('btnArcVerExport').addEventListener('click', function (event) {
		let data = collectArcData();
		let filename = (ENV == "production") ? path.join(process.resourcesPath, "../extraResources/Vorlage_arcexport.docx") : "D:/Vorlage_arcexport.docx";
		//let filename = path.join(process.resourcesPath, "Vorlage_prjexport.docx");
		//let filename = "D:/Vorlage_prjexport.docx";
		//console.log(filename,data);
		generiereBrief(filename, data);

	});



	ById('btnvorlageS1').addEventListener('click', function () {
		makeSerienBrief(anzeige.projektid);
	});
	ById('notizsetzen').addEventListener('click', function () {
		neueNotizspeichern("");
	});
	ById('notizcancel').addEventListener('click', neueNotizabbrechen);
	ById('notizdel').addEventListener('click', notizLoeschen);
	ById('btnrechStatus').addEventListener('click', function (e) {
		if (roleedit) {
			let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(anzeige.rechnungid));
			let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
			if (anzeige.menu == "RNBENTWURF" ||
				anzeige.menu == "RNBOPL" ||
				anzeige.menu == "RNBNEU" ||
				anzeige.menu == "RNBHISTRECH" ||
				anzeige.menu == "RNBALLRECH"
			) {
				as = true;
			} else {
				as = false;
			}
			switch (rechnung.status) {
				case CONSTANTS.ENTWURF:
					rechnung.status = CONSTANTS.VERSCHICKT;
					break;
				case CONSTANTS.VERSCHICKT:
					rechnung.status = CONSTANTS.BEZAHLT;
					break;
				case CONSTANTS.BEZAHLT:
					rechnung.status = CONSTANTS.ENTWURF;
					break;
				default:
					rechnung.status = CONSTANTS.ENTWURF;

			}
			ipcRenderer.send('rechnung:change', rechnung);
		}

	});
	ById('btnVerChange').addEventListener('click', function (e) {
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
		let kunde = DM.kundenDM.find(el => el.id == vertrag.kunde);
		if (anzeige.menu == "RNBENTWURF" ||
			anzeige.menu == "RNBOPL" ||
			anzeige.menu == "RNBNEU" ||
			anzeige.menu == "RNBHISTRECH" ||
			anzeige.menu == "RNBALLRECH"
		) {
			as = true;
		} else {
			as = false;
		}
		let item = new Object();

		switch (parseInt(ById('btnVerChange').getAttribute('mode'))) {
			case CONSTANTS.EINBUCHEN:
				item.type = "Reservierungsgebühr";
				item.vertrag = vertrag.id;
				item.betreff = "Reservierungsgebühr";
				item.name = kunde.name;
				item.vorname = kunde.vorname;
				item.strasse = kunde.strasse;
				item.plz = kunde.plz;
				item.ort = kunde.ort;
				item.rechdat = getDatum(0);
				item.faelligdat = getDatum(0);
				item.status = CONSTANTS.BEZAHLT;
				item.rechnr = "Reservierungsgebühr";
				ipcRenderer.send('edit:new', item);
				break;
			case CONSTANTS.AUSBUCHEN:
				item.type = "Erstattung Reservierung";
				item.vertrag = vertrag.id;
				item.betreff = "Erstattung der Reservierungsgebühr";
				item.name = kunde.name;
				item.vorname = kunde.vorname;
				item.strasse = kunde.strasse;
				item.plz = kunde.plz;
				item.ort = kunde.ort;
				item.rechdat = getDatum(0);
				item.faelligdat = getDatum(0);
				item.status = CONSTANTS.BEZAHLT;
				item.rechnr = "Erstattung";
				ipcRenderer.send('edit:new', item);
				break;
			default:

		}





	});
	ById('btnrechmenuback').addEventListener('click', function (e) {

		if (anzeige.menu == "RNBENTWURF" ||
			anzeige.menu == "RNBOPL" ||
			anzeige.menu == "RNBNEU" ||
			anzeige.menu == "RNBHISTRECH" ||
			anzeige.menu == "RNBALLRECH" ||
			anzeige.menu == "arbeitskorb"
		) {
			as = true;
		} else {
			as = false;
		}
		if (as && anzeige.show == "rechnung") {
			anzeige.show = anzeige.menu;
			updateAnzeige();
		}


	});
	ById('btnrechNotiz').addEventListener('click', function (e) {
		addComment();

	});
	ById('btnrechEdit').addEventListener('click', function (e) {
		let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(anzeige.rechnungid));
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
		if (anzeige.menu == "RNBENTWURF" ||
			anzeige.menu == "RNBOPL" ||
			anzeige.menu == "RNBNEU" ||
			anzeige.menu == "RNBHISTRECH" ||
			anzeige.menu == "RNBALLRECH"
		) {
			as = true;
		} else {
			as = false;
		}
		showRechnung(anzeige.rechnungid);
		ById('RnbEditRechNr').disabled = false;

		if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
			ById('modRnbEdit').classList.add('is-active');
			anzeige.edit = true;
			ipcRenderer.send('display:save', anzeige);

			ById('rech-id').value = anzeige.rechnungid;
			ById('modRnbEdit-id').value = anzeige.rechnungid;

		} else {
			ById('modalRechnungChange').classList.add('is-active');
			ById('rech-id').value = anzeige.rechnungid;
			addElementToRechnungsPos(rechnung.vertrag);
		}

	});
	ById('btnrechDelete').addEventListener('click', function (e) {
		let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(anzeige.rechnungid));
		let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
		let options = {
			buttons: ["Löschen", "Abbrechen"],
			message: "Rechnung wirklich löschen?",
			title: "Frage",
			detail: "",
			type: "question"
		};
		let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
		if (response == 0) {
			if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
				ipcRenderer.send('vertrag:delete', vertrag.id);
			} else {
				ipcRenderer.send('rechnung:delete', rechnung.id);
			}
		}

	});
	ById('butVerEditPos').addEventListener('click', function () {
		anzeige.show = "vertrag";
		anzeige.edit = true;
		ipcRenderer.send('display:save', anzeige);

		let vertrag = DM.vertragDM.find(ve => ve.id == anzeige.vertragid);
		ById('ver-id').value = anzeige.vertragid;
		ById('modalVertragPostenChange').classList.add('is-active');
		ById('tabverpos').innerHTML = "";

		switch (vertrag.typ) {
			case CONSTANTS.GUTSCHRIFT:
			case CONSTANTS.MEHRKOSTEN:
				appendPosZeile(0, 0, "", 100, "", "", "", false, false, true, rnb = false, "", 1);
				break;
			default:
				addElementToVertragsPos(vertrag, 0, vertrag.kaufpreis);
		}

	});
	ById('butPrjEditObj').addEventListener('click', function () {
		anzeige.show = "projekt";
		ById('modalObj').classList.add('is-active');
		anzeige.edit = true;
		ipcRenderer.send('display:save', anzeige);

	});
	ById('btnObjClose').addEventListener('click', function () {
		ById('modalObj').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);

	});
	ById('btnReleaseClose').addEventListener('click', function () {
		ById('modalRelease').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);

	});
	ById('btnVerPosClose').addEventListener('click', function () {
		anzeige.edit = false;

		let active = ById('modalVertragPostenChange').classList.contains('is-active');
		if (anzeige.menu == "RNBENTWURF" ||
			anzeige.menu == "RNBOPL" ||
			anzeige.menu == "RNBNEU" ||
			anzeige.menu == "RNBHISTRECH" ||
			anzeige.menu == "RNBALLRECH"
		) {
			as = true;
		} else {
			as = false;
		}
		if (active && as) {
			anzeige.show = "rechnung";
		}

		ById('modalVertragPostenChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('btnRecPosClose').addEventListener('click', function () {
		anzeige.edit = false;
		ById('modalRechnungPostenChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);

	});
	ById('btnPrjChangeClose').addEventListener('click', function () {
		anzeige.edit = false;
		ById('modalProjektChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('btnRecZEClose').addEventListener('click', function () {
		ById('modalRechnungZE').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});

	ById('btnRnbEditClose').addEventListener('click', function () {
		ById('modRnbEdit').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	//---------------------
	ById('btnRnbEditProvisionClose').addEventListener('click', function () {
		ById('modRnbEditProvision').classList.remove('is-active');
		ById('modRnbEditProvision-id').value = 0;
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butRnbEditProvisionCancel').addEventListener('click', function () {
		ById('modRnbEditProvision').classList.remove('is-active');
		ById('modRnbEditProvision-id').value = 0;
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butRnbEditProvisionOk').addEventListener('click', function () {
		//ById('modRnbEditProvision').classList.remove('is-active');
		// Speichern der Provisionsdaten
		let msg = new Object();
		msg.data = getDatafromTable();
		msg.rechnung = ById('modRnbEditProvision-id').value;
		ipcRenderer.send('provision:changeRech', msg);
	});
	//---------------------
	ById('btnProvEditProvisionClose').addEventListener('click', function () {
		ById('modProvEditProvision').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butProvEditProvisionCancel').addEventListener('click', function () {
		ById('modProvEditProvision').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butProvEditProvisionOk').addEventListener('click', function () {
		ById('modProvEditProvision').classList.remove('is-active');
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
		// Speichern der Provisionsdaten
		let msg = new Object();
		msg.id = ById('dpProvEdit-id').value;
		msg.kommentar = ById('inpProvEditkommentar').value;
		msg.dataus = ById('inpProvEditdataus').value;
		msg.anteil = convProzentField(ById('inpProvEditanteil').value);
		msg.betrag = convPreisField(ById('inpProvEditbetrag').value);
		msg.gesamtprov = convPreisField(ById('inpProvEditgesamtbetrag').innerHTML);
		msg.status = parseInt(ById('dpProvEditStatus-id').value);
		if (msg.status != 1 && msg.status != 2) {
			msg.freigeber = "";
		} else {
			msg.freigeber = user;
		}
		ipcRenderer.send('provision:change', msg);
	});

	ById('dpProvEditStatus').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			ById('dpProvEditStatus-id').value = i;
			ById('dpProvEditStatus-button').innerHTML = CONSTANTS.STATUSPROV[i];

		}

	});

	ById('dpcommentTo').addEventListener('click', function (event) {
		//console.log(anzeige);
		if (event.target.classList.contains('dropdown-item')) {
			//let kunden = datenmodell.get('Kunden');
			i = event.target.id.substring(4, event.target.id.length);
			ById('dpcommentTo-button').innerHTML = event.target.innerHTML;

		}

	});



	//--------------------
	ById('butVerChangeOk').addEventListener('click', function () {
		saveVertrag();
	});
	ById('butVerCancel').addEventListener('click', function () {
		// Button Abbrechen Vertrag editieren
		anzeige.edit = false;
		ipcRenderer.send('display:save', anzeige);
		ById('modalVertragChange').classList.remove('is-active');

		// Nur wenn es gerade angelegt wurde
		if (ById('dpVerStatus-id').value == CONSTANTS.NEU) {
			let vertrag = new Object();
			vertrag.id = ById('ver-id').value;
			vertrag.status = CONSTANTS.TRASH;
			ipcRenderer.send('vertrag:change', vertrag);
		}
	});
	ById('butPrjChangeOk').addEventListener('click', function () {
		saveProjekt();
	});
	ById('butPrjCancel').addEventListener('click', function () {
		ById('modalProjektChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butKunCancel').addEventListener('click', function () {
		ById('modalKundeChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butKunChangeOk').addEventListener('click', function () {
		if (saveKunde()) {
			ById('modalKundeChange').classList.remove('is-active');
		}

	});
	ById('btnKunChangeClose').addEventListener('click', function () {
		ById('modalKundeChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butRecChangeOk').addEventListener('click', function () {
		saveRechnung();
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butRecCancel').addEventListener('click', function () {
		anzeige.edit = false;
		ById('modalRechnungChange').classList.remove('is-active');
		ipcRenderer.send('display:save', anzeige);
	});
	ById('butRecEditPos').addEventListener('click', function () {
		anzeige.edit = true;
		ipcRenderer.send('display:save', anzeige);
		anzeige.show = "vertragposedit";
		let rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
		let vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag);
		if (typeof vertrag != 'undefined' && vertrag.typ == CONSTANTS.VERTRAGALLG) {
			ById('modalVertragPostenChange').classList.add('is-active');
			ById('tabverpos').innerHTML = "";
			let betrag = "";
			if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
				let objekt = DM.objekteDM.find(el => el.id == vertrag.objekt);
				let betrag = typeof objekt != 'undefined' ? objekt.betrag : 0;
			}
			appendPosZeile(0, 0, "", 100, "", 19, "", false, false, true, rnb = true, betrag, 1);
		} else if (typeof vertrag != 'undefined' && (vertrag.typ == CONSTANTS.MEHRKOSTEN || vertrag.typ == CONSTANTS.GUTSCHRIFT)) {
			// Auswahl der Rechungspositionen
			positionen = DM.positionenDM.filter(el => el.vertrag == vertrag.id && el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.status !== CONSTANTS.ENTWURF && el.status !== CONSTANTS.DELETE);
			let auswahl = new Array();
			let auswahlid = new Array();
			auswahl.push("Abbrechen");
			auswahlid.push(-1);
			let maxposnr = 0;
			for (pos = 0; pos < positionen.length; pos++) {
				if (typeof positionen[pos].rechnung == 'undefined' || positionen[pos].rechnung == 0) {
					auswahl.push(positionen[pos].text + "\r\nBetrag: " + formatBetrag(positionen[pos].betrag) + "");
					auswahlid.push(pos);
				} else if (positionen[pos].rechnung == anzeige.rechnungid) {
					if (maxposnr < positionen[pos].rechpos) {
						maxposnr = positionen[pos].rechpos;
					}
				}
			}
			let options = {
				buttons: auswahl,
				message: "Neue Position in dieser Rechnung auswählen:",
				title: "Frage",
				detail: "",
				type: "question"
			};
			let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
			if (response > 0) {
				positionen[auswahlid[response]].rechnung = anzeige.rechnungid;
				positionen[auswahlid[response]].rechpos = parseInt(maxposnr) + 1;

				ipcRenderer.send('position:change', positionen[auswahlid[response]]);

			}



		} else {
			editRechnungPosten(rechnung.vertrag);
		}
		ipcRenderer.send('display:save', anzeige);

	});
};
function clickoncomment(element) {
	if (typeof element != 'undefined') {
		i = element.id.substring(10, element.id.length);
		// ("id", "comment-nr"+i)
		//let loaddata = datenmodell.get('Kommentare');
		let comment = DM.commentDM.find(el => el.id == i);
		//console.log(element.id, i, comment);
		ById('notiz-id').value = i;

		(typeof comment.verfasser !== 'undefined') ? verfasser = comment.verfasser + ' ' : verfasser = "";
		ById('comment-info').innerHTML = "Erstellt: " + verfasser + " " + comment.datum + "";
		ById('notizdel').classList.remove('is-hidden');
		ById('comment-new-text').value = comment.eintrag;
		ById('comment-new').classList.add('is-active');
		let empf = typeof comment.empf !== 'undefined' ? comment.empf : "Alle";
		ById('dpcommentTo-button').innerHTML = empf;
	}
}
function generiereBrief(templatePath, data) {

	if (isRelative(templatePath) && typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
		templatePath = path.join(process.env.PORTABLE_EXECUTABLE_FILE, templatePath);
	}
	//console.log(templatePath);
	if (!checkFileExists(templatePath)) {

		WIN = electron.remote.getCurrentWindow();
		let defaultName = "";
		switch (data.typ) {
			case CONSTANTS.TEMPLATER0:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATER1:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATER2:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATER3:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATER4:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATER5:
				defaultName = getDatumFile() + "-" + data.rechnr + "-" + data.rechvorname + " " + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATEV1:
				defaultName = getDatumFile() + "-Ratenübersicht-" + data.rechname + '.docx';
				break;
			case CONSTANTS.TEMPLATEP1:
				defaultName = getDatumFile() + "-Projekt-" + data.prjname + '.docx';
				break;
			case CONSTANTS.TEMPLATEARCLIST:
				defaultName = getDatumFile() + '-Architektenvertraege.docx';
				break;
			default:
				let str = data.rechnr;
				//str = str.substring(5, str.lenght);
				// defaultName = getDatumFile() + "-" + str + "-" + data.rechname + '.docx';
				// Geändert nach Mail 10.02.22
				if (data.lieferant == true) {
					defaultName = str + " " + data.rechvorname + " " + data.rechname + '.docx';

				} else {
					defaultName = str + " " + data.rechname + '.docx';
				}
		}


		let options = {
			title: 'Brief speichern',
			defaultPath: defaultName,
			buttonLabel: 'Speichern',
			filters: [
				{ name: 'Word', extensions: ['docx'] },
				{ name: 'All Files', extensions: ['*'] }
			]
		};

		let filename = dialog.showSaveDialogSync(WIN, options);
		if (typeof filename !== 'undefined') {
			createBrief(filename, templatePath, data);
			openFileWithDefaultApp(filename);

		}
	}
}
function makeSerienBrief(prj) {

	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(prj));
	let objekte = DM.objekteDM.filter(el => el.projekt == prj && el.status != CONSTANTS.TRASH);
	let templatePath = projekt[CONSTANTS.TEMPLATES1];
	if (isRelative(templatePath) && typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
		templatePath = path.join(process.env.PORTABLE_EXECUTABLE_FILE, templatePath);
	}

	if (!checkFileExists(templatePath)) {

		WIN = electron.remote.getCurrentWindow();
		let options = {
			title: 'Speicherort wählen',
			properties: ['openDirectory'],
			buttonLabel: 'Verzeichnis wählen',
			filters: [
				{ name: 'All Files', extensions: ['*'] }
			]
		};

		let selectedPaths = dialog.showOpenDialogSync(getCurrentWindow(), options);
		if (typeof selectedPaths !== 'undefined' && typeof selectedPaths[0] !== 'undefined') {
			let sp = selectedPaths[0];
			for (let oj = 0; oj < objekte.length; oj++) {
				if (objekte[oj].kunde != "") {
					let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(objekte[oj].kunde));
					if (typeof kunde != 'undefined') {
						let dateiname = sp + '/' + stripString(projekt.prjnummer) + "-" + stripString(objekte[oj].text)
							+ stripString(objekte[oj].objnummer) + "-" + stripString(kunde.name) + ".docx";
						//console.log("Datei ", dateiname, "kunde ", kunde);
						let data = new Object();
						data.rechanrede = DM.anredeliste[kunde.anrede];
						data.rechvorname = kunde.vorname;
						data.rechname = kunde.name;
						data.rechstrasse = kunde.strasse;
						data.rechplz = kunde.plz;
						data.rechort = kunde.ort;
						data.rechtelefon = kunde.telefon;
						data.rechemail = kunde.email;
						typeof DM.anredeliste[kunde.anrede2] == 'undefined' ? data.rechanrede2 = "" : data.rechanrede2 = DM.anredeliste[kunde.anrede2];
						if (typeof kunde.vorname2 != 'undefined' && kunde.vorname2 != "") {
							data.rechvorname2 = kunde.vorname2;
						} else {
							data.rechvorname2 = "";
						}
						if (typeof kunde.name2 != 'undefined' && kunde.name2 != "") {
							data.rechname2 = kunde.name2;
						} else {
							data.rechname2 = "";
						}
						if (data.rechvorname2 != "" && data.rechname2 != "") {
							if (kunde.lieferant == true) {
								data.anredezeile2 = "Sehr " + data.rechanrede2 + " " + data.rechname2 + ",";
								data.anredezeile1 = "";
								data.rechund = "\nz.H.";
							} else {
								data.anredezeile1 = "Sehr " + data.rechanrede + " " + data.rechname + ",";
								data.anredezeile2 = "sehr " + data.rechanrede2 + " " + data.rechname2 + ",";
								data.rechund = "und";
							}
						} else {

							data.anredezeile2 = "Sehr " + data.rechanrede + " " + data.rechname + ",";
							data.anredezeile1 = "";
							data.rechund = "";
						}


						data.rechtelefon2 = kunde.telefon2;
						data.rechtelefon3 = kunde.telefon3;
						data.rechemail2 = kunde.email2;
						data.rechstrasse2 = kunde.strasse2;
						data.rechplz2 = kunde.plz2;
						data.rechort2 = kunde.ort2;

						data.bank = projekt.bank;
						data.bic = projekt.bic;
						data.ibana = projekt.iban;
						data.ibane = projekt.ibanE;
						data.prjbeschreibung = projekt.beschreibung;
						data.prjname = projekt.name;
						data.prjnummer = projekt.prjnummer;
						data.bauleiter = projekt.bauleiter;
						data.bautraeger = projekt.bautraeger;
						data.objnummer = objekte[oj].objnummer;
						data.einheit = objekte[oj].text + " " + objekte[oj].objnummer;
						data.objektnr = data.prjnummer + "-" + data.objnummer;
						data.datum = getDatum(0);

						//console.log(data);
						createBrief(dateiname, templatePath, data);

					}
				}
			}
		}
	}

}
function addComment() {
	if (rolecomment && anzeige.show !== "arbeitskorb") {
		commentdata = { "notiz": "", "element": "", "statusneu": "", "statusalt": "", "rechkommentar": "" };
		ById('notiz-id').value = 0;
		ById('comment-info').innerHTML = "";
		ById('comment-new-text').value = "";
		ById('comment-new').classList.add('is-active');
		ById('notizdel').classList.add('is-hidden');
	}
}
function neueNotizabbrechen() {
	ById('comment-new').classList.remove('is-active');

}
function notizLoeschen() {
	if (rolecomment) {
		ipcRenderer.send('comment:del', ById('notiz-id').value);
		ById('notizdel').classList.add('is-hidden');
		ById('comment-new').classList.remove('is-active');
	}

}
function neueNotizspeichern(data) {
	if (rolecomment) {
		let sendnotizdata = new Object();
		if (fs.existsSync(data)) {
			sendnotizdata.notiz = "Datei verlinkt: " + data;
			sendnotizdata.datei = data;
			sendnotizdata.id = 0;
		} else {
			sendnotizdata.notiz = ById('comment-new-text').value;
			sendnotizdata.id = ById('notiz-id').value;
		}
		sendnotizdata.verfasser = user;
		sendnotizdata.empf = ById('dpcommentTo-button').innerHTML;

		switch (anzeige.show) {
			case 'rechnung':
				sendnotizdata.rechnung = anzeige.rechnungid;
				break;
			case 'vertrag':
				sendnotizdata.vertrag = anzeige.vertragid;
				break;
			case 'kunde':
				sendnotizdata.kunde = anzeige.kundeid;
				break;
			case 'projekt':
				sendnotizdata.projekt = anzeige.projektid;
				break;
			case 'einheit':
				sendnotizdata.einheit = anzeige.einheitid;
				break;
			case 'darlehen':
				sendnotizdata.darlehen = anzeige.vertragid;
				break;
			default:
		}
		sendnotizdata.modus = anzeige.show;
		ipcRenderer.send('comment:add', sendnotizdata);

		ById('comment-new').classList.remove('is-active');
	}
}
function dpInit() {
	// Dropdowns

	var $dropdowns = getAll('.dropdown:not(.is-hoverable)');

	if ($dropdowns.length > 0) {
		$dropdowns.forEach(function ($el) {
			$el.addEventListener('click', function (event) {
				event.stopPropagation();
				let active = $el.classList.contains('is-active');
				closeDropdowns();
				if (active) { $el.classList.remove('is-active'); } else { $el.classList.add('is-active'); }
				//	$el.classList.toggle('is-active');
			});
		});

		document.addEventListener('click', function (event) {
			closeDropdowns();
		});
	}

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	// Close dropdowns if ESC pressed
	document.addEventListener('keydown', function (event) {
		var e = event || window.event;
		if (e.keyCode === 27) {
			closeDropdowns();
		}
	});

	// Functions

	function getAll(selector) {
		return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
	}


};
function editStart() {
	if (anzeige.show == "vertrag" || anzeige.show == "vertragARCedit") {
		anzeige.edit = true;
		editVertrag();
	}

	if (anzeige.show == "rechnung") {
		anzeige.edit = true;
		editRechnung();
	}
	if (anzeige.show == "kunde") {
		anzeige.edit = true;
		editKunde();
	}
	if (anzeige.show == "projekt") {
		anzeige.edit = true;
		editProjekt();
	}

}
function fillComments(comments) {
	if (typeof comments != 'undefined' && comments.length > 0) {

		ById('paneKommentare').classList.remove('is-hidden');

		if (false) {
			ById('comments').classList.remove('is-hidden');
			ById('comments').innerHTML = "";
			for (i = 0; i < comments.length; i++) {
				inhalt = document.createElement('div');
				inhalt.className = 'box';
				inhalt.setAttribute("id", "comment-nr" + comments[i].id);
				(typeof comments[i].verfasser !== 'undefined') ? verfasser = comments[i].verfasser + ': ' : verfasser = "";
				str = '';
				str += comments[i].datum + ' ';
				str += verfasser;

				str += markdown.toHTML(comments[i].eintrag);
				str += ' ';
				inhalt.innerHTML = str;
				//ById('rechnung-comments').appendChild(inhalt);
				inhalt.addEventListener('click', function () {
					clickoncomment(this);
				});
				ById('comments').appendChild(inhalt);
			}
		} else {
			let modulo = comments.length % 4;
			if (modulo > 0) {

				for (let i = 0; i < 4 - modulo; i++) {
					comments.push({ id: -1 });
				}
			}
			ById("paneKommentare").innerHTML = "";
			//console.log(comments);

			let noRows = comments.length / 4;
			for (let row = 0; row < noRows; row++) {
				let columns = document.createElement('div');
				columns.className = 'columns'

				for (let i = 0; i < 4; i++) {
					commentno = i + 4 * row;
					//console.log(commentno);
					column = document.createElement('div');
					column.className = 'column is-3'
					if (comments[commentno].id !== -1) {
						inhalt = document.createElement('div');
						inhalt.className = 'box';
						inhalt.setAttribute("id", "comment-nr" + comments[commentno].id);
						str = '';

						// (typeof comments[commentno].verfasser !== 'undefined') ? verfasser = comments[commentno].verfasser + ': ' : verfasser = "";
						// str += "<small>" + verfasser + comments[commentno].datum + "</small>";

						if (typeof comments[commentno].datei !== 'undefined') {
							let file = path.basename(comments[commentno].datei)
							let extension = path.extname(comments[commentno].datei)
							//console.log("extension", extension);
							//console.log("file", file);
							switch (extension) {
								case ".doc":
								case ".docx":
									str += "<center><img src = './assets/icons/png/doc.png'><br>";
									break;
								case ".pdf":
									str += "<center><img src = './assets/icons/png/pdf.png'><br>";
									break;
								case ".txt":
									str += "<center><img src = './assets/icons/png/txt.png'><br>";
									break;
								case ".xls":
								case ".xlsx":
									str += "<center><img src = './assets/icons/png/xls.png'><br>";
									break;
								default:
									str += "<center><img src = './assets/icons/png/file.png'><br>";
							}
							str += "<small>" + file + "</small>";
							str += "</center>";

							inhalt.innerHTML = str;

						} else {
							str += markdown.toHTML(comments[commentno].eintrag);
							str += ' ';
							inhalt.innerHTML = str;
						}

						inhalt.addEventListener('click', function () {
							clickoncomment(this);
						});
						column.appendChild(inhalt);
					}

					columns.appendChild(column);
				}
				ById('paneKommentare').appendChild(columns);
			}

		}


	}
}
function buttonexcel() {

	try {
		//console.log("export to excel");
		// https://www.npmjs.com/package/node-excel-export
		let dataset = new Array();
		let datasetRechnungen = new Array();

		let sheets = new Array();

		//let objekteDM = datenmodell.get('Objekte');
		//let projekteDM = datenmodell.get('Projekte');
		//let rechnungenDM = datenmodell.get('Rechnungen');
		//let positionenDM = datenmodell.get('Positionen');
		//let vertraegeDM = datenmodell.get('Vertraege');
		//let kunDM = datenmodell.get('Kunden');
		// -----------------------------------
		const styles = {
			headerDark: {
				fill: {
					fgColor: {
						rgb: 'B8CCE5'
					}
				},
				font: {
					color: {
						rgb: 'FFFFFFFF'
					},
					sz: 12,
					bold: true,
					underline: false
				},
				alignment: {
					horizontal: 'left'
				}
			},
			headerDarkR: {
				fill: {
					fgColor: {
						rgb: 'B8CCE5'
					}
				},
				font: {
					color: {
						rgb: 'FFFFFFFF'
					},
					sz: 12,
					bold: true,
					underline: false
				},
				alignment: {
					horizontal: 'right'
				}
			},
			cellnumber: {
				numFmt: "0.00 €"
			},
			cellsum: {
				font: {
					color: {
						rgb: 'FFFFFFFF'
					},
					sz: 12,
					bold: true,
					underline: false
				}
			}
		};
		//Here you specify the export structure
		const specification = {
			projekt: { // <- the key should match the actual data key
				displayName: 'Projekt', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: 140 // <- width in pixels
			},
			text: { // <- the key should match the actual data key
				displayName: 'Wohnung', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: '31' // <- width in pixels
			},
			kaufpreis: {
				displayName: 'Preis',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			whgsoll: {
				displayName: 'Wohnungen noch offen',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			whghaben: {
				displayName: 'Wohnungen bezahlt',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			mehrsoll: {
				displayName: 'Mehrkosten noch offen',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			mehrhaben: {
				displayName: 'Mehrkosten bezahlt',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			rechoffen: {
				displayName: 'Rechnungen verschickt',
				headerStyle: styles.headerDarkR,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			habengesamt: {
				displayName: 'Gesamt-Haben',
				cellStyle: styles.cellnumber, // <- Cell style
				headerStyle: styles.headerDarkR,
				width: '22' // <- width in chars (when the number is passed as string)
			}
		}
		const specificationRechnungen = {
			rechnr: { // <- the key should match the actual data key
				displayName: 'Nr.', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: 140 // <- width in pixels
			},
			text: { // <- the key should match the actual data key
				displayName: 'Name', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: '40' // <- width in pixels
			},
			betrag: {
				displayName: 'Rechnungsbetrag',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			},
			zahlungseingang: {
				displayName: 'Bezahlt',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '22' // <- width in chars (when the number is passed as string)
			}
		}
		const specificationVertraege = {
			vertrag: { // <- the key should match the actual data key
				displayName: 'Ver-Nr.', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: 140 // <- width in pixels
			},
			text: { // <- the key should match the actual data key
				displayName: 'Position', // <- Here you specify the column header
				headerStyle: styles.headerDark, // <- Header style
				width: '60' // <- width in chars
			},
			anteil: {
				displayName: 'Anteil [%]',
				headerStyle: styles.headerDark,
				width: '14' // <- width in chars (when the number is passed as string)
			},
			betrag: {
				displayName: 'Betrag',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '14' // <- width in chars (when the number is passed as string)
			},
			offenerbetrag: {
				displayName: 'Offen',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '14' // <- width in chars (when the number is passed as string)
			},
			bezahlt: {
				displayName: 'Bezahlt',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '14' // <- width in chars (when the number is passed as string)
			},
			verschickt: {
				displayName: 'Verschickt',
				headerStyle: styles.headerDark,
				cellStyle: styles.cellnumber, // <- Cell style
				width: '14' // <- width in chars (when the number is passed as string)
			}
		}
		// ------------
		// -----------------------------------
		projekte = DM.projekteDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV);

		for (pr = 0; pr < projekte.length; pr++) {
			//console.log(projekte[pr]);
			objekte = DM.objekteDM.filter(el => el.projekt == projekte[pr].id && el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV);
			objekte.sort((a, b) => {
				let n = parseInt(a.objnummer);
				let m = parseInt(b.objnummer);
				return (n < m) ? -1 : (n > m) ? 1 : 0;
			});
			let i = 0;
			if (objekte.length > 0) {
				for (i = 0; i < objekte.length; i++) {

					let kunde = DM.kundenDM.find(el => el.id == objekte[i].kunde);
					let kaeufer = "";
					if (typeof kunde != 'undefined') {
						kaeufer = " (" + kunde.vorname + " " + kunde.name + ")";
					}

					let obj = new Object();
					obj.projekt = projekte[pr].name;
					obj.text = objekte[i].text + " " + objekte[i].objnummer + kaeufer;
					obj.kaufpreis = objekte[i].kaufpreis;
					obj.whgsoll = objekte[i].whgsoll;
					obj.whghaben = objekte[i].whghaben;
					obj.mehrsoll = objekte[i].mehrsoll;
					obj.mehrhaben = objekte[i].mehrhaben;
					obj.rechoffen = objekte[i].rechoffen;
					obj.habengesamt = objekte[i].habengesamt;
					dataset.push(obj);
				}
				let obj = new Object();
				obj.projekt = "";
				obj.text = "Summe";
				obj.kaufpreis = projekte[pr].kaufpreis;
				obj.whgsoll = projekte[pr].whgsoll;
				obj.whghaben = projekte[pr].whghaben;
				obj.mehrsoll = projekte[pr].mehrsoll;
				obj.mehrhaben = projekte[pr].mehrhaben;
				obj.rechoffen = projekte[pr].rechoffen;
				obj.habengesamt = projekte[pr].habengesamt;
				//console.log(obj);
				dataset.push(obj);

			} else {
				let obj = new Object();
				obj.projekt = projekte[pr].name;
				obj.text = "Nicht angelegt";
				obj.kaufpreis = "";
				obj.whgsoll = "";
				obj.whghaben = "";
				obj.mehrsoll = "";
				obj.mehrhaben = "";
				obj.rechoffen = "";
				obj.habengesamt = "";
				//console.log(obj);

				dataset.push(obj);

			}
			let obj = new Object();
			obj.projekt = "";
			obj.text = "";
			obj.kaufpreis = "";
			obj.whgsoll = "";
			obj.whghaben = "";
			obj.mehrsoll = "";
			obj.mehrhaben = "";
			obj.rechoffen = "";
			obj.habengesamt = "";

			//console.log(obj);
			dataset.push(obj);

		}
		{
			let newSheet = new Object();
			newSheet.name = 'Projekte';
			newSheet.specification = specification;
			newSheet.data = dataset;
			sheets.push(newSheet);
		}
		// -----------------------------------
		let rechnungen = DM.rechnungenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.status !== CONSTANTS.ENTWURF && el.status !== CONSTANTS.DELETE)
		rechnungen.sort((a, b) => {
			let textA = a.rechnr.toUpperCase();
			let textB = b.rechnr.toUpperCase();
			textA = textA.substring(3, textA.length);
			textB = textB.substring(3, textB.length);
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		});
		for (let re = 0; re < rechnungen.length; re++) {
			let obj = new Object();
			obj.rechnr = rechnungen[re].rechnr;
			obj.text = rechnungen[re].vorname + " " + rechnungen[re].name;
			obj.betrag = rechnungen[re].betrag;
			if (typeof rechnungen[re].zahlungseingang != 'undefined') {
				obj.zahlungseingang = rechnungen[re].zahlungseingang;
			} else {
				obj.zahlungseingang = "";
			}
			datasetRechnungen.push(obj);

		}
		{
			let newSheet = new Object();
			newSheet.name = 'Rechnungen';
			newSheet.specification = specificationRechnungen;
			newSheet.data = datasetRechnungen;
			sheets.push(newSheet);
		}
		// ----------------------------------
		let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.status !== CONSTANTS.ENTWURF && el.status !== CONSTANTS.DELETE)

		vertraege.sort((a, b) => {
			if (typeof a.vernr == 'undefined' || typeof b.verner == 'undefined') {
				return 1
			} else {
				let textA = a.vernr.toUpperCase();
				let textB = b.vernr.toUpperCase();

				textA = textA.substring(3, textA.length);
				textB = textB.substring(3, textB.length);

				return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
			}
		});
		//console.log(vertraege.length);
		for (ver = 0; ver < vertraege.length; ver++) {
			if (typeof vertraege[ver].vernr != 'undefined') {
				//console.log(vertraege[ver].vernr);

				let datasetVertrage = new Array();
				positionen = DM.positionenDM.filter(el => el.vertrag == vertraege[ver].id && el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.status !== CONSTANTS.ENTWURF && el.status !== CONSTANTS.DELETE);
				for (pos = 0; pos < positionen.length; pos++) {
					let obj = new Object();
					obj.vertrag = vertraege[ver].vernr;
					obj.text = positionen[pos].text;
					obj.anteil = (typeof positionen[pos].anteil != 'undefined') ? positionen[pos].anteil : "";
					obj.betrag = (typeof positionen[pos].betrag != 'undefined') ? positionen[pos].betrag : "";
					obj.offenerbetrag = (typeof positionen[pos].offenerbetrag != 'undefined') ? positionen[pos].offenerbetrag : "";
					obj.bezahlt = (typeof positionen[pos].bezahlt != 'undefined') ? positionen[pos].bezahlt : "";
					obj.verschickt = (typeof positionen[pos].verschickt != 'undefined') ? positionen[pos].verschickt : "";
					datasetVertrage.push(obj);
				}
				let obj = new Object();
				obj.vertrag = vertraege[ver].vernr;
				obj.text = "Summe";
				obj.anteil = "";
				obj.betrag = (typeof vertraege[ver].betrag != 'undefined') ? vertraege[ver].betrag : "";
				obj.offenerbetrag = (typeof vertraege[ver].offenerbetrag != 'undefined') ? vertraege[ver].offenerbetrag : "";
				obj.bezahlt = (typeof vertraege[ver].bezahlt != 'undefined') ? vertraege[ver].bezahlt : "";
				obj.verschickt = (typeof vertraege[ver].verschickt != 'undefined') ? vertraege[ver].verschickt : "";
				datasetVertrage.push(obj);
				//console.log(obj);

				let newSheet = new Object();
				newSheet.name = (typeof vertraege[ver].vernr != 'undefined') ? vertraege[ver].vernr : "NN";;
				newSheet.specification = specificationVertraege;
				newSheet.data = datasetVertrage;
				sheets.push(newSheet);
			}
		}

		//console.log(sheets);
		const report = excel.buildExport(sheets);
		WIN = electron.remote.getCurrentWindow();
		let options = {
			title: 'Excel speichern',
			defaultPath: 'C:\\Excel.xlsx',
			buttonLabel: 'Als Excel speichern',
			filters: [
				{ name: 'Excel', extensions: ['xlsx'] },
				{ name: 'All Files', extensions: ['*'] }
			]
		};
		let filename = dialog.showSaveDialogSync(WIN, options);
		if (typeof filename !== 'undefined') {
			fs.writeFileSync(filename, report);
		}

		openFileWithDefaultApp(filename);

	}
	catch (e) {
		//myConsole.log(e);
		write2logfile(e.stack, 'a');
		//console.log(e);
		ipcRenderer.send('error:display', e);
	}

}
function specExcel(data) {
	let spec = new Object();

	data.forEach(colname => {
		let colspec = new Object();
		colspec.displayName = colname;
		colspec.headerStyle = {
			font: {
				color: {
					rgb: '000000'
				},
				sz: 14,
				bold: true
			}
		};
		colspec.width = '25';
		switch (colname) {
			case "Typ":
			case "RG-Nr":
				colspec.width = "10";
				break;
			case "Status":
			case "ID":
				colspec.width = "8";
				break;
			case "Rech.Datum":
			case "Rech.Datum":
			case "Datum Auszahlung":
			case "Fällig":
			case "letzte Mahnung":
				colspec.cellStyle = {
					numFmt: "dd.mm.yy",
					alignment: { horizontal: 'center' }
				};
				colspec.headerStyle = {
					alignment: { horizontal: 'center' },
					font: {
						color: {
							rgb: '000000'
						},
						sz: 14,
						bold: true
					}
				};

				colspec.width = "12";
				break;
			case "Prov.Anteil":
				colspec.cellStyle = {
					numFmt: "0.0 %",
					alignment: { horizontal: 'right' }
				};
				break;
			case "Rech.Netto":
			case "Prov.Betrag":
			case "Gesamt Prov.":
			case "Anteil":
			case "Netto":
			case "Betrag":
			case "Brutto":
			case "Offen":
				colspec.cellStyle = {
					numFmt: "0.00 €",
					alignment: { horizontal: 'right' }
				};
				colspec.headerStyle = {
					alignment: { horizontal: 'right' },
					font: {
						color: {
							rgb: '000000'
						},
						sz: 14,
						bold: true
					}
				};
				colspec.width = "12";

				break;
			default:
		}
		spec[colname] = colspec;
	});
	return spec;
}
function writeExcel(header, data) {
	try {
		const report = excel.buildExport([
			{
				name: 'Export',
				specification: specExcel(header),
				data: data

			}
		]);
		WIN = electron.remote.getCurrentWindow();
		let options = {
			title: 'Excel speichern',
			defaultPath: 'C:\\Excel.xlsx',
			buttonLabel: 'Als Excel speichern',
			filters: [
				{ name: 'Excel', extensions: ['xlsx'] },
				{ name: 'All Files', extensions: ['*'] }
			]
		};
		let filename = dialog.showSaveDialogSync(WIN, options);
		if (typeof filename !== 'undefined') {
			fs.writeFileSync(filename, report);
			openFileWithDefaultApp(filename);
		}


	}
	catch (e) {
		//myConsole.log(e);
		console.log(e.message, e.stack, e.name)
		write2logfile(e.stack, 'a');
		if (e.message.indexOf("EBUSY") != -1) {
			e.stack = "Die Datei ist blockiert!";
		}
		ipcRenderer.send('error:display', e);

	}
}
function updateMenu() {
	ById('navtree').innerHTML = "";

	// Arbeitskorb ---------------------------------------------------------------------------

	if (ARBEITSKORB) {
		let menulev1 = document.createElement('li');
		//--- unter li kommt label und input und ol ------------------------------------
		ById('navtree').appendChild(menulev1);
		let label = document.createElement('label');
		//	label.className = 'menurnbclass';
		label.setAttribute("for", "menuark");
		label.innerHTML = "<strong>MITTEILUNGEN</strong>";
		label.id = "menuark";
		menulev1.appendChild(label);
		// let input = document.createElement('input');
		// input.setAttribute("type", "checkbox");
		// input.id = "menuarkcheck";
		// menulev1.appendChild(input);
		// let menulev1content = document.createElement('ol');
		// menulev1content.className = "menuark";
		// menulev1content.id = "menuark";
		// menulev1.appendChild(menulev1content);
		//console.log("Click ", event.target.id);
		menulev1.addEventListener('click', function (event) {
			anzeige.show = "arbeitskorb";
			updateAnzeige();

		}, true);

		// let menulev2 = document.createElement('li');
		// menulev2.className = "menuark";
		// menulev1content.appendChild(menulev2);

		// let menulev2content = document.createElement('a');
		// menulev2content.id = "arbeitskorb";
		// menulev2content.innerHTML = "Meine Inbox";
		// menulev2.appendChild(menulev2content);

	}
	//--- Bauprojekte
	let projekte = DM.projekteDM.filter(el => el.id != 0 && el.status != CONSTANTS.TRASH);
	if (projekte.length > 0) {
		if (projekte.length > 2) {
			projekte.sort((a, b) => {
				let n = a.prjnummer.toUpperCase();
				let m = b.prjnummer.toUpperCase();
				return (n < m) ? -1 : (n > m) ? 1 : 0;
			});
		}
		let elliprj = document.createElement('li');
		ById('navtree').appendChild(elliprj);
		let label = document.createElement('label');
		label.className = 'lstprjall';
		label.setAttribute("for", "prjall");
		//label.id = "projektliste";
		label.innerHTML = "<strong>BAUPROJEKTE</strong>";
		elliprj.appendChild(label);

		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "prjall";
		elliprj.appendChild(input);

		let elol = document.createElement('ol');
		elol.className = "lstprjall";
		elol.id = "projektliste";
		elliprj.appendChild(elol);

		let elliall = document.createElement('li');

		elol.addEventListener('click', function (event) {
			//console.log("    ", event.target.id);
			if (event.target.id == "projektliste") {
				anzeige.show = "projekt";
				updateAnzeige();
			} else {
				let id = parseInt(event.target.id.substring(3, event.target.id.lenght));
				//console.log(event.target);
				switch (event.target.id.substring(0, 3)) {
					case "vla":
					case "veo":
						anzeige.show = "vertrag";
						anzeige.menu = "";
						anzeige.vertragid = id;
						anzeige.click = event.target.id;
						updateAnzeige();
						break;
					case "rec":
						anzeige.show = "rechnung";
						anzeige.rechnungid = id;
						anzeige.menu = "";
						anzeige.menu = "projekte";
						anzeige.click = event.target.id;
						//console.log(anzeige);
						updateAnzeige();

						break;
					case "ola":
						anzeige.show = "einheit";
						anzeige.menu = "";
						anzeige.einheitid = id;
						anzeige.click = event.target.id;
						updateAnzeige();
						break;
					case "opr":
						anzeige.show = "einheit";
						anzeige.menu = "";
						anzeige.einheitid = id;
						anzeige.click = event.target.id;
						updateAnzeige();

						break;
					case "pol":
						anzeige.show = "projekt";
						anzeige.menu = "";
						anzeige.projektid = id;
						anzeige.click = event.target.id;
						updateAnzeige();
						break;
					default:

				}
			}
		}, true);
		elol.appendChild(elliall);

		for (let pr = 0; pr < projekte.length; pr++) {
			let elliprj = document.createElement('li');
			//ById('navtree').appendChild(elliprj);
			elliall.appendChild(elliprj);

			let ellabprj = document.createElement('label');
			let elid = "prj" + projekte[pr].id;
			ellabprj.className = 'lstprj shorten-long-text';
			ellabprj.setAttribute("for", elid);
			ellabprj.id = "pla" + projekte[pr].id;
			ellabprj.innerHTML = projekte[pr].prjnummer + " " + projekte[pr].name;
			elliprj.appendChild(ellabprj);

			let input = document.createElement('input');
			input.setAttribute("type", "checkbox");
			input.id = elid;

			objekte = DM.objekteDM.filter(el => el.projekt == projekte[pr].id &&
				el.status != CONSTANTS.TRASH);
			elliprj.appendChild(input);
			// --------------------------------------------------------------
			if (typeof objekte == 'undefined' || objekte.length > 0) {
				objekte.sort((a, b) => {
					let n = parseInt(a.objnummer);
					let m = parseInt(b.objnummer);
					return (n < m) ? -1 : (n > m) ? 1 : 0;
				});
				let elolobj = document.createElement('ol');
				elolobj.className = "lstprj";
				elolobj.id = "pol" + projekte[pr].id;
				elliprj.appendChild(elolobj);
				for (let oj = 0; oj < objekte.length; oj++) {
					let kunde = DM.kundenDM.find(el => el.id == objekte[oj].kunde);
					let name = "";
					let elid = "obj" + objekte[oj].id;
					if (typeof kunde != 'undefined') { name = kunde.name; }
					let elliobj = document.createElement('li');
					elolobj.appendChild(elliobj);

					let label = document.createElement('label');
					label.className = 'lstobj';
					label.setAttribute("for", elid);
					label.id = "ola" + objekte[oj].id;
					label.innerHTML = objekte[oj].text + " " + objekte[oj].objnummer; //+ " " + name;
					elliobj.appendChild(label);

					let input = document.createElement('input');
					input.setAttribute("type", "checkbox");
					input.id = elid;
					vertraege = DM.vertragDM.filter(el => parseInt(el.objekt) == parseInt(objekte[oj].id) && el.status !== CONSTANTS.TRASH && el.typ != CONSTANTS.VERTRAGALLG);
					//console.log(vertraege);
					elliobj.appendChild(input);

					// --------------------------------------------------------------
					if (typeof vertraege !== "undefined" && vertraege.length > 0) {
						let elolobj = document.createElement('ol');
						elolobj.className = "lstobj";
						elolobj.id = "opr" + objekte[oj].id;
						elliobj.appendChild(elolobj);
						for (let ve = 0; ve < vertraege.length; ve++) {
							let elliver = document.createElement('li');
							elolobj.appendChild(elliver);

							let label = document.createElement('label');
							let elid = "ver" + vertraege[ve].id;
							label.className = 'lstver';
							if (vertraege[ve].status == CONSTANTS.DELETE) {
								label.className += ' is-deleted is-hidden';
							}
							label.setAttribute("for", elid);
							label.id = "vla" + vertraege[ve].id;
							label.innerHTML = vertraege[ve].vernr;
							elliver.appendChild(label);

							let input = document.createElement('input');
							input.setAttribute("type", "checkbox");
							input.id = elid;
							//vertraege[ve].typ CONSTANTS.RESERVIERUNG
							rechnungen = DM.rechnungenDM.filter(el =>
								parseInt(el.vertrag) == parseInt(vertraege[ve].id) &&
								el.status !== CONSTANTS.TRASH);
							elliver.appendChild(input);
							// --------------------------------------------------------------
							if (vertraege[ve].typ != CONSTANTS.RESERVIERUNG && typeof rechnungen != 'undefined' && rechnungen.length > 0) {
								//if (typeof rechnungen != 'undefined' && rechnungen.length > 0) {
								let elolver = document.createElement('ol');
								elolver.id = "veo" + vertraege[ve].id;
								elolver.className = 'lstver';
								elliver.appendChild(elolver);

								for (let re = 0; re < rechnungen.length; re++) {
									let ellirech = document.createElement('li');
									switch (rechnungen[re].status) {
										case CONSTANTS.ENTWURF:
											ellirech.className = 'lstrech is-rech-entwurf';
											break;
										case CONSTANTS.VERSCHICKT:
											ellirech.className = 'lstrech is-rech-mail';
											break;
										case CONSTANTS.BEZAHLT:
											ellirech.className = 'lstrech is-rech-ok';
											break;
										case CONSTANTS.DELETE:
											ellirech.className = 'lstrech is-deleted is-hidden';
											break;
										case CONSTANTS.TRASH:
											ellirech.className = 'lstrech is-hidden';
											break;
										default:
											ellirech.className = 'lstrech is-rech-entwurf';

									}
									elolver.appendChild(ellirech);

									let elid = "rec" + rechnungen[re].id;
									let elarech = document.createElement('a');
									elarech.id = elid;
									elarech.className = "lstrech";
									elarech.innerHTML = rechnungen[re].rechnr;
									// elarech.addEventListener('click', function (event) {
									// 	console.log(event.target);

									// 	anzeige.show = "rechnung";
									// 	anzeige.rechnungid = this.id.substring(3, this.id.length);
									// 	anzeige.click = event.target.id;
									// 	updateAnzeige();
									// 	//console.log("Click", this);
									// 	//showRechnung(this.id.substring(3, this.id.length));
									// }, true);

									ellirech.appendChild(elarech);

								}
							}

						}
					}
				}
			}


		}
	}
	// Darlehen ---------------------------------------------------------------------------
	if (DARLEHENMENU && roleleiter) {
		let elli = document.createElement('li');
		//--- unter li kommt label und input und ol ------------------------------------
		ById('navtree').appendChild(elli);
		let label = document.createElement('label');
		label.className = 'lstdarall';
		label.setAttribute("for", "darall");
		label.innerHTML = "<strong>DARLEHEN</strong>";
		elli.appendChild(label);
		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "darall";
		elli.appendChild(input);
		let elol = document.createElement('ol');
		elol.className = "lstdarall";
		elol.id = "listdar";
		elli.appendChild(elol);
		elli.addEventListener('click', function (event) {
			//console.log("Click ", event.target.id);
			if (event.target.id == "listdar") {
				anzeige.show = "darlehen";
				updateAnzeige();
			} else {
				id = event.target.id.substring(3, event.target.id.length);
				idtyp = event.target.id.substring(0, 3);
				//console.log(idtyp, id);
				switch (idtyp) {
					case "dpi":
						anzeige.show = "vertrag";
						anzeige.vertragid = id;
						anzeige.click = event.target.id;
						updateAnzeige();
						break;
					case "lis":
						anzeige.show = "darlehen";
						updateAnzeige();
						break;
					default:
				}
			}
		}, true);

		//--- Unter OL kommt dann wieder eine Liste ----------------------------------
		let elliall = document.createElement('li');
		elol.appendChild(elliall);

		vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.typ == CONSTANTS.DARLEHEN);
		for (let ve = 0; ve < vertraege.length; ve++) {
			let elliver = document.createElement('li');
			elol.appendChild(elliver);

			let label = document.createElement('label');
			let elid = "dpi" + vertraege[ve].id;
			label.className = 'lstdar';
			if (vertraege[ve].status == CONSTANTS.DELETE) {
				label.className += ' is-deleted is-hidden';
			}
			label.setAttribute("for", elid);
			label.id = "dpl" + vertraege[ve].id;
			let labelstr = vertraege[ve].vernr == "" ? "n.n." : vertraege[ve].vernr;
			if (typeof vertraege[ve].status != 'undefined' && vertraege[ve].status == CONSTANTS.ARCHIV) {
				labelstr = "<strike>" + labelstr + "</strike>";
			}
			label.innerHTML = labelstr;
			elliver.appendChild(label);

			let input = document.createElement('input');
			input.setAttribute("type", "checkbox");
			input.id = elid;
			// input.addEventListener('click', function (event) {
			// 	anzeige.show = "vertrag";
			// 	anzeige.vertragid = this.id.substring(3, this.id.length);
			// 	anzeige.click = event.id;
			// 	updateAnzeige();
			// 	console.log("Click ", event.target);
			// 	//showVertrag(this.id.substring(3, this.id.length));
			// }, true);


			elliver.appendChild(input);


		}


	}
	// Rechnungsbuch ---------------------------------------------------------------------------
	if (RECHNUNGSBUCH) {
		let menulev1 = document.createElement('li');
		//--- unter li kommt label und input und ol ------------------------------------
		ById('navtree').appendChild(menulev1);
		let label = document.createElement('label');
		//	label.className = 'menurnbclass';
		label.setAttribute("for", "menurnb");
		label.innerHTML = "<strong>RECHNUNGSBUCH</strong>";
		menulev1.appendChild(label);
		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "menurnbcheck";

		menulev1.appendChild(input);
		let menulev1content = document.createElement('ol');
		menulev1content.className = "menurnb";
		menulev1content.id = "menurnb";
		menulev1.appendChild(menulev1content);
		//console.log("Click ", event.target.id);
		menulev1.addEventListener('click', function (event) {
			if (event.target.id == "menurnb") {
				if (roleedit) {
					anzeige.show = "RNBNEU";
				} else {
					anzeige.show = "RNBENTWURF";
				}
				updateAnzeige();
			} else {
				anzeige.show = event.target.id;
				updateAnzeige();
			}
		}, true);

		let menulev2 = document.createElement('li');
		menulev2.className = "menurnb";
		menulev1content.appendChild(menulev2);

		let menulev2content = document.createElement('a');

		if (roleedit) {
			menulev2content.id = "RNBNEU";
			menulev2content.innerHTML = "Neu";
			menulev2.appendChild(menulev2content);
		}

		menulev2content = document.createElement('a');
		menulev2content.id = "RNBENTWURF";
		menulev2content.innerHTML = "Entwurf";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "RNBOPL";
		menulev2content.innerHTML = "Versendet";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "RNBHISTRECH";;
		menulev2content.innerHTML = "Bezahlt";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "RNBALLRECH";
		menulev2content.innerHTML = "Alle";
		menulev2.appendChild(menulev2content);

	}

	// Provisionen ---------------------------------------------------------------------------
	if (PROVISIONEN && roleleiter) {
		let menulev1 = document.createElement('li');
		//--- unter li kommt label und input und ol ------------------------------------
		ById('navtree').appendChild(menulev1);
		let label = document.createElement('label');
		//	label.className = 'menurnbclass';
		label.setAttribute("for", "menuprov");
		label.innerHTML = "<strong>PROVISIONEN</strong>";
		menulev1.appendChild(label);
		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "menuprovcheck";
		menulev1.appendChild(input);
		let menulev1content = document.createElement('ol');
		menulev1content.className = "menuprov";
		menulev1content.id = "menuprov";
		menulev1.appendChild(menulev1content);
		//console.log("Click ", event.target.id);
		menulev1.addEventListener('click', function (event) {
			if (event.target.id == "menuprov") {
				anzeige.show = "provlist";
				updateAnzeige();
			} else {
				anzeige.show = event.target.id;
				updateAnzeige();
			}
		}, true);

		let menulev2 = document.createElement('li');
		menulev2.className = "menuprov";
		menulev1content.appendChild(menulev2);

		let menulev2content = document.createElement('a');
		menulev2content.id = "provlisterfasst";
		menulev2content.innerHTML = "Erfasst";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "provlistfreigegeben";
		menulev2content.innerHTML = "Freigegeben";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "provlistabgelehnt";
		menulev2content.innerHTML = "Abgelehnt";
		menulev2.appendChild(menulev2content);

		menulev2content = document.createElement('a');
		menulev2content.id = "provlist";
		menulev2content.innerHTML = "Alle";
		menulev2.appendChild(menulev2content);


	}

	// Planung ---------------------------------------------------------------------------

	if (PLANUNGMENU) {
		let elli = document.createElement('li');
		//--- unter li kommt label und input und ol ------------------------------------
		ById('navtree').appendChild(elli);
		let label = document.createElement('label');
		label.className = 'lstplan';
		label.setAttribute("for", "planall");
		label.innerHTML = "<strong>PLANUNG</strong>";
		elli.appendChild(label);
		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "planall";
		elli.appendChild(input);
		let elol = document.createElement('ol');
		elol.className = "lstplan";
		elol.id = "listplanung";
		elli.appendChild(elol);
		elol.addEventListener('click', function (event) {
			if (event.target.id == "listplanung") {
				ById("planall").checked = true;
				anzeige.show = "arcver";
				updateAnzeige();

			}
		}, true);

		//--- Unter OL kommt dann wieder eine Liste ----------------------------------
		let elliall = document.createElement('li');
		elol.appendChild(elliall);

		vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.typ == CONSTANTS.ARCHVERTRAG);
		for (let ve = 0; ve < vertraege.length; ve++) {
			let elliver = document.createElement('li');
			elol.appendChild(elliver);

			let label = document.createElement('label');
			let elid = "vpi" + vertraege[ve].id;
			label.className = 'lstver';
			if (vertraege[ve].status == CONSTANTS.DELETE) {
				label.className += ' is-deleted is-hidden';
			}
			label.setAttribute("for", elid);
			label.id = "vpl" + vertraege[ve].id;
			label.innerHTML = vertraege[ve].vernr == "" ? "n.n." : vertraege[ve].vernr;
			elliver.appendChild(label);

			let input = document.createElement('input');
			input.setAttribute("type", "checkbox");
			input.id = elid;
			rechnungen = DM.rechnungenDM.filter(el => parseInt(el.vertrag) == parseInt(vertraege[ve].id) && el.status !== CONSTANTS.TRASH);
			if (typeof rechnungen == 'undefined' || rechnungen.length == 0) {
				input.addEventListener('click', function (event) {
					anzeige.show = "vertrag";
					anzeige.vertragid = this.id.substring(3, this.id.length);
					anzeige.click = event.target.id;
					updateAnzeige();
					//console.log("Click", this, anzeige);
					//showVertrag(this.id.substring(3, this.id.length));
				}, true);
			}

			elliver.appendChild(input);
			if (typeof rechnungen != 'undefined' && rechnungen.length > 0) {
				let elolver = document.createElement('ol');
				elolver.id = "vpo" + vertraege[ve].id;;
				elolver.className = 'lstver';
				elolver.addEventListener('click', function (event) {
					anzeige.show = "vertrag";
					anzeige.vertragid = this.id.substring(3, this.id.length);
					anzeige.click = event.target.id;
					updateAnzeige();
					//console.log("Click", this);
					//showVertrag(this.id.substring(3, this.id.length));
				}, true);
				elliver.appendChild(elolver);

				for (let re = 0; re < rechnungen.length; re++) {
					let ellirech = document.createElement('li');
					switch (rechnungen[re].status) {
						case CONSTANTS.ENTWURF:
							ellirech.className = 'lstrech is-rech-entwurf';
							break;
						case CONSTANTS.VERSCHICKT:
							ellirech.className = 'lstrech is-rech-mail';
							break;
						case CONSTANTS.BEZAHLT:
							ellirech.className = 'lstrech is-rech-ok';
							break;
						case CONSTANTS.DELETE:
							ellirech.className = 'lstrech is-deleted is-hidden';
							break;
						case CONSTANTS.TRASH:
							ellirech.className = 'lstrech is-hidden';
							break;
						default:
							ellirech.className = 'lstrech is-rech-entwurf';

					}
					elolver.appendChild(ellirech);

					let elid = "are" + rechnungen[re].id;
					let elarech = document.createElement('a');
					elarech.id = elid;
					elarech.className = "lstrech";
					elarech.innerHTML = rechnungen[re].rechnr;
					elarech.addEventListener('click', function (event) {
						//event.stopPropagation();

						anzeige.show = "rechnung";
						anzeige.rechnungid = this.id.substring(3, this.id.length);
						anzeige.click = event.target.id;
						updateAnzeige();
						//console.log("Click", this);
						//showRechnung(this.id.substring(3, this.id.length));
					}, true);

					ellirech.appendChild(elarech);

				}
			}

		}


	}
	// Kunden ---------------------------------------------------------------------------
	//kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.kunde == true);
	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH);
	if (1) {
		let elli = document.createElement('li');
		ById('navtree').appendChild(elli);
		let label = document.createElement('label');
		label.className = 'lstkun';
		label.setAttribute("for", "kunall");
		label.id = "kundenliste";
		label.innerHTML = "<strong>VERWALTUNG</strong>";
		elli.appendChild(label);

		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "kunall";
		elli.appendChild(input);

		let elol = document.createElement('ol');
		elol.className = "lstkun";
		elol.id = "kundenliste";
		elli.appendChild(elol);

		elol.addEventListener('click', function (event) {
			if (event.target.id == "kundenliste") {
				anzeige.show = "kunde";
				updateAnzeige();
			}
		}, true);
		if (kunden.length > 0) {
			kunden.sort((a, b) => {
				let textA = a.name.toUpperCase();
				let textB = b.name.toUpperCase();
				if (a.lieferant == true) {
					textA = a.vorname.toUpperCase();
				}
				if (b.lieferant == true) {
					textB = b.vorname.toUpperCase();
				}
				return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
			});
			for (let ku = 0; ku < kunden.length; ku++) {
				let ellikun = document.createElement('li');
				if (typeof kunden[ku].anrede != 'undefined') {
					switch (parseInt(kunden[ku].anrede)) {
						case 1:
						case 3:
						case 7:
							// Weiblich
							ellikun.className = "is-kun-w";
							if (typeof kunden[ku].anrede2 != 'undefined' && kunden[ku].anrede2 != '') {
								ellikun.className = "is-kun-b";
							}
							break;
						case 0:
						case 4:
						case 8:
							// Männlich
							ellikun.className = "is-kun-m";
							if (typeof kunden[ku].anrede2 != 'undefined' && kunden[ku].anrede2 != '') {
								ellikun.className = "is-kun-b";
							}
							break;
						case 2:
						case 5:
						case 6:
							// Gemeinsam
							ellikun.className = "is-kun-b";
							break;
						default:
							ellikun.className = "is-kun-w";
					}
				} else {
					ellikun.className = "is-kun-w";
				}
				if (kunden[ku].lieferant == true) {
					ellikun.className = "is-kun-f";
				}
				if (kunden[ku].notar == true) {
					ellikun.className = "is-kun-n";
				}
				elol.appendChild(ellikun);

				let elid = "kun" + kunden[ku].id;
				let ela = document.createElement('a');
				ela.id = elid;
				ela.className = "elemkun";
				let str, kdnr;
				if (typeof kunden[ku].kundennummer != 'undefined') {
					kdnr = kunden[ku].kundennummer;
				} else {
					kdnr = "";
				}

				if (typeof kunden[ku].vorname != 'undefined' && kunden[ku].vorname != "") {
					if (kunden[ku].lieferant == true) {
						str = kunden[ku].vorname + " " + kunden[ku].name;
					} else {
						str = kunden[ku].name + ", " + kunden[ku].vorname;
					}
				} else {
					str = kunden[ku].name;
				}
				if (str.length > 20) {
					str = str.substring(0, 20) + "...";
				}

				ela.innerHTML = str;
				if (kunden[ku].vorname == "" && kunden[ku].name == "") {
					ela.innerHTML = "[" + kdnr + "]" + "NN";
				}
				ela.addEventListener('click', function () {
					anzeige.show = "kunde";
					anzeige.kundeid = this.id.substring(3, this.id.length);
					updateAnzeige();
				}, true);

				ellikun.appendChild(ela);

			}
		}
	}
	// Notariate ---------------------------------------------------------------------------
	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.notar == true);

	if (kunden.length > 0) {
		kunden.sort((a, b) => {
			let textA = a.name.toUpperCase();
			let textB = b.name.toUpperCase();
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		});
		//console.log(kunden);
		let elli = document.createElement('li');
		elli.classList.add("is-hidden");

		ById('navtree').appendChild(elli);
		let label = document.createElement('label');
		label.className = 'lstnot';
		label.setAttribute("for", "notarall");
		label.id = "notareliste";
		label.innerHTML = "<strong>NOTARE</strong>";
		elli.appendChild(label);

		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "notarall";
		elli.appendChild(input);

		let elol = document.createElement('ol');
		elol.className = "lstnot";
		elol.id = "notareliste";
		elli.appendChild(elol);

		elol.addEventListener('click', function (event) {
			if (event.target.id == "notareliste") {
				anzeige.show = "notare";
				updateAnzeige();
			}
		}, true);

		for (let ku = 0; ku < kunden.length; ku++) {
			let ellikun = document.createElement('li');
			if (typeof kunden[ku].anrede != 'undefined') {
				switch (parseInt(kunden[ku].anrede)) {
					case 1:
					case 3:
						ellikun.className = "is-kun-w";

						break;
					case 0:
					case 4:
						ellikun.className = "is-kun-m";

						break;
					default:
						ellikun.className = "is-kun-w";
				}
			} else {
				ellikun.className = "is-kun-w";
			}
			elol.appendChild(ellikun);

			let elid = "ku2" + kunden[ku].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemkun";
			let str;
			if (typeof kunden[ku].vorname != 'undefined' && kunden[ku].vorname != "") {
				str = kunden[ku].name + ", " + kunden[ku].vorname;
			} else {
				str = kunden[ku].name;
			}
			if (str.length > 16) {
				str = str.substring(0, 16) + "...";
			}

			ela.innerHTML = str;
			if (kunden[ku].vorname == "" && kunden[ku].name == "") {
				ela.innerHTML = "NN";
			}
			ela.addEventListener('click', function () {
				anzeige.show = "notar";
				anzeige.kundeid = this.id.substring(3, this.id.length);
				console.log(anzeige.show, anzeige.kundeid);
				updateAnzeige();
			}, true);

			ellikun.appendChild(ela);

		}
	}
	// Firmen ---------------------------------------------------------------------------
	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.lieferant == true);


	//console.log(kunden);
	let elli = document.createElement('li');
	elli.classList.add("is-hidden");
	ById('navtree').appendChild(elli);
	let label = document.createElement('label');
	label.className = 'lstfir';
	label.setAttribute("for", "firall");
	label.id = "firliste";
	label.innerHTML = "<strong>FIRMEN</strong>";
	elli.appendChild(label);

	let input = document.createElement('input');
	input.setAttribute("type", "checkbox");
	input.id = "firall";
	elli.appendChild(input);

	let elol = document.createElement('ol');
	elol.className = "lstfir";
	elol.id = "firliste";
	elli.appendChild(elol);

	elol.addEventListener('click', function (event) {
		if (event.target.id == "firliste") {
			anzeige.show = "fir";
			updateAnzeige();
		}
	}, true);
	if (kunden.length > 0) {
		kunden.sort((a, b) => {
			let textA = a.name.toUpperCase();
			let textB = b.name.toUpperCase();
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		});
		for (let ku = 0; ku < kunden.length; ku++) {
			let ellikun = document.createElement('li');
			if (typeof kunden[ku].anrede != 'undefined') {
				switch (parseInt(kunden[ku].anrede)) {
					case 1:
					case 3:
						ellikun.className = "is-kun-w";

						break;
					case 0:
					case 4:
						ellikun.className = "is-kun-m";

						break;
					default:
						ellikun.className = "is-kun-w";
				}
			} else {
				ellikun.className = "is-kun-w";
			}
			elol.appendChild(ellikun);

			let elid = "ku3" + kunden[ku].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemkun";
			let str;
			if (typeof kunden[ku].vorname != 'undefined' && kunden[ku].vorname != "") {
				str = kunden[ku].name + ", " + kunden[ku].vorname;
			} else {
				str = kunden[ku].name;
			}
			if (str.length > 16) {
				str = str.substring(0, 16) + "...";
			}

			ela.innerHTML = str;
			if (kunden[ku].vorname == "" && kunden[ku].name == "") {
				ela.innerHTML = "NN";
			}
			ela.addEventListener('click', function () {
				anzeige.show = "firma";
				anzeige.kundeid = this.id.substring(3, this.id.length);
				//console.log(anzeige.show, anzeige.kundeid);
				updateAnzeige();
			}, true);

			ellikun.appendChild(ela);

		}
	}

	// Trash  ---------------------------------------------------------------------------
	{
		let elli = document.createElement('li');
		ById('navtree').appendChild(elli);
		let label = document.createElement('label');
		label.className = 'lsttrash';
		label.setAttribute("for", "trashall");
		label.id = "trashliste";
		label.innerHTML = "PAPIERKORB";
		elli.appendChild(label);

		let input = document.createElement('input');
		input.setAttribute("type", "checkbox");
		input.id = "trashall";
		elli.appendChild(input);

		let elol = document.createElement('ol');
		elol.className = "lsttrash";
		elli.appendChild(elol);

		rechnungen = DM.rechnungenDM.filter(el => el.status == CONSTANTS.TRASH);
		for (let re = 0; re < rechnungen.length; re++) {
			let elli = document.createElement('li');
			elli.className = "is-trash";
			elol.appendChild(elli);

			let elid = "trec" + rechnungen[re].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemtrash";
			ela.innerHTML = rechnungen[re].rechnr + " " + rechnungen[re].name;
			elli.appendChild(ela);
			ela.addEventListener('click', function () {
				console.log("Click", this, "Rechnung ", this.id.substring(4, this.id.length));
				anzeige.show = "rechnung";
				anzeige.rechnungid = this.id.substring(4, this.id.length);
				updateAnzeige();
			}, true);

		}
		vertraege = DM.vertragDM.filter(el => el.status == CONSTANTS.TRASH);
		for (let ve = 0; ve < vertraege.length; ve++) {
			let elli = document.createElement('li');
			elli.className = "is-trash";
			elol.appendChild(elli);

			let elid = "tver" + vertraege[ve].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemtrash";
			ela.innerHTML = vertraege[ve].vernr;
			elli.appendChild(ela);
			ela.addEventListener('click', function () {
				//console.log("Click", this, "Vertrag ", this.id.substring(4, this.id.length));
				anzeige.show = "vertrag";
				anzeige.vertragid = this.id.substring(4, this.id.length);
				updateAnzeige();
			}, true);
		}
		objekte = DM.objekteDM.filter(el => el.status == CONSTANTS.TRASH);
		for (let ob = 0; ob < objekte.length; ob++) {
			let elli = document.createElement('li');
			elli.className = "is-trash";
			elol.appendChild(elli);
			//
			let elid = "tobj" + objekte[ob].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemtrash";
			ela.innerHTML = objekte[ob].text + " " + objekte[ob].objnummer;
			elli.appendChild(ela);
		}
		projekte = DM.projekteDM.filter(el => el.status == CONSTANTS.TRASH);
		for (let pr = 0; pr < projekte.length; pr++) {
			let elli = document.createElement('li');
			elli.className = "is-trash";
			elol.appendChild(elli);
			//
			let elid = "tprj" + projekte[pr].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemtrash";
			ela.innerHTML = projekte[pr].prjnummer + " " + projekte[pr].name;
			elli.appendChild(ela);
		}

		kunden = DM.kundenDM.filter(el => el.status == CONSTANTS.TRASH);
		//console.log(kunden);
		for (re = 0; re < kunden.length; re++) {
			let elli = document.createElement('li');
			elli.className = "is-trash";
			elol.appendChild(elli);

			let elid = "tkun" + kunden[re].id;
			let ela = document.createElement('a');
			ela.id = elid;
			ela.className = "elemtrash";
			if (kunden[re].vorname == "" && kunden[re].name == "") {
				ela.innerHTML = " Kunden ID " + kunden[re].id;
			} else {
				ela.innerHTML = kunden[re].vorname + " " + kunden[re].name;
			}

			elli.appendChild(ela);
			ela.addEventListener('click', function () {
				console.log("Click", this, "Kunde ", this.id.substring(4, this.id.length));
				anzeige.show = "kunde";
				anzeige.kundeid = this.id.substring(4, this.id.length);
				updateAnzeige();
			}, true);

		}

	}
	// Archiv ---------------------------------------------------------------------------
	// {
	// 	let elli = document.createElement('li');
	// 	ById('navtree').appendChild(elli);
	// 	let label = document.createElement('label');
	// 	label.className = 'lstarchiv';
	// 	label.setAttribute("for", "archivall");
	// 	label.id = "archivliste";
	// 	label.innerHTML = "Archiviert";
	// 	elli.appendChild(label);
	// }
}
function updateDropDowns() {
	let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
	if (typeof vertrag != 'undefined') {
		switch (vertrag.typ) {
			// case CONSTANTS.DARLEHEN:
			// 	kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.lieferant == true);
			// 	break;
			default:
				kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE);
		}
	} else {
		kunden = DM.kundenDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.DELETE && el.kunde == true);
	}
	ById('dpVerKun-content').innerHTML = "";
	ById('dpVerKun2-content').innerHTML = "";
	ById('dpRnbEditKnd-content').innerHTML = "";
	ById('dpRnbEditKndObj-content').innerHTML = "";

	//let kunden = DM.kundenDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.DELETE && el.kunde == true);
	kunden.sort((a, b) => {
		let textA = a.name.toUpperCase();
		let textB = b.name.toUpperCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});

	//console.log(kunden);
	for (i = 0; i < kunden.length; i++) {
		obj = kunden[i];
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "vku-" + obj.id;
		strtmp = obj.name;
		if (obj.vorname != "") {
			strtmp += ", " + obj.vorname;
		}
		el1.innerHTML = strtmp;

		if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
			if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
				el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

			}
		}
		ById('dpVerKun-content').appendChild(el1);

		obj = kunden[i];
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "vk2-" + obj.id;
		strtmp = obj.name;
		if (obj.vorname != "") {
			strtmp += ", " + obj.vorname;
		}
		el1.innerHTML = strtmp;
		if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
			if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
				el1.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

			}
		}
		ById('dpVerKun2-content').appendChild(el1);

		obj = kunden[i];
		el2 = document.createElement('div');
		el2.className = 'dropdown-item';
		el2.id = "rek-" + obj.id;
		strtmp = obj.name;
		if (obj.vorname != "") {
			strtmp += ", " + obj.vorname;
		}
		el2.innerHTML = strtmp;
		if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
			if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
				el2.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

			}
		}
		ById('dpRnbEditKnd-content').appendChild(el2);

		obj = kunden[i];
		el3 = document.createElement('div');
		el3.className = 'dropdown-item';
		el3.id = "rok-" + obj.id;
		strtmp = obj.name;
		if (obj.vorname != "") {
			strtmp += ", " + obj.vorname;
		}
		el3.innerHTML = strtmp;
		if (typeof obj.name2 != 'undefined' && obj.name2 != "") {
			if (typeof obj.vorname2 != 'undefined' && obj.vorname2 != "") {
				el3.innerHTML += " & " + obj.name2 + ", " + obj.vorname2;

			}
		}
		ById('dpRnbEditKndObj-content').appendChild(el3);


	}

	ById('dpVerNotar-content').innerHTML = "";
	let notare = DM.kundenDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.DELETE && el.notar == true);
	notare.sort((a, b) => {
		let textA = a.name.toUpperCase();
		let textB = b.name.toUpperCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});
	for (i = 0; i < notare.length; i++) {
		obj = notare[i];
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "vno-" + obj.id;
		if (typeof obj.vorname != 'undefined' && obj.vorname != "") {
			el1.innerHTML = obj.vorname + " " + obj.name;
		} else {
			el1.innerHTML = obj.name;
		}
		ById('dpVerNotar-content').appendChild(el1);
	}
	ById('dpVerMwSt-content').innerHTML = "";
	if (typeof DM.mwst != 'undefined') {
		for (i = 0; i < DM.mwst.length; i++) {
			el1 = document.createElement('div');
			el1.className = 'dropdown-item';
			el1.innerHTML = DM.mwst[i];
			ById('dpVerMwSt-content').appendChild(el1);
		}
	}
}
function listenerKontextemenu() {
	// Kontextemenu
	document.addEventListener('contextmenu', (e) => {
		e.preventDefault();
		//let type = e.target.getAttribute('class');
		//console.log(e.target);
		//console.log(e.target);
		const menu = new Menu();
		let found = false;
		let box = e.target.closest("div.box");
		if (typeof box != 'undefined' && box != null && box.id.substring(0, 7) == "comment") {
			commentId = box.id.substring(10, box.id.lenght);
			//console.log(commentId);
			menu.append(new MenuItem({
				label: 'Datei öffnen',
				click() {
					openCommentFile(commentId);
				},
			}));

			found = true;
		}
		if (e.target.classList.contains('lstprj')) {

			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Projekt bearbeiten',
					click() {
						anzeige.show = "projekt";
						anzeige.projektid = e.target.id.substring(3, e.target.id.length);
						showProjekt(anzeige.projektid);
						editStart();

					},
				}));
				found = true;
			}
			//menu.append(new MenuItem({ type: 'separator' }));
			if (roleedit) {

				menu.append(new MenuItem({
					label: 'Einheiten bearbeiten',
					click() {
						anzeige.show = "projekt";
						anzeige.projektid = e.target.id.substring(3, e.target.id.length);
						showProjekt(anzeige.projektid);
						ById('modalObj').classList.add('is-active');
						anzeige.edit = true;
						ipcRenderer.send('display:save', anzeige);
					},
				}));
				found = true;
			}
			if (roleedit) {

				menu.append(new MenuItem({
					label: 'Löschen',
					click() {
						let projektid = e.target.id.substring(3, e.target.id.length);
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(projektid));
						//console.log(message);
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Projekt wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('projekt:delete', projektid);
						}

					}
				}));
				found = true;
			}

		}
		if (e.target.classList.contains('lstobj')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Reservierung eintragen',
					click() {
						let objid = e.target.id.substring(3, e.target.id.length);
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(objid));
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
						let vertraege = DM.vertragDM.filter(el => parseInt(el.objekt) == parseInt(objid) && el.status != CONSTANTS.TRASH);
						let anlegen = true;
						let kunde;
						vertraege.forEach(vertrag => {
							if (vertrag.typ == CONSTANTS.RESERVIERUNG) {
								anlegen = false;
								let options = {
									buttons: ["Ok"],
									message: "Reservierung kann nicht angelegt werden",
									title: "Fehlermeldung",
									detail: "Für diese Einheit exisitiert bereits eine Reservierung.",
									icon: path.join(__dirname, 'assets/icons/png/bandage.png')

								};
								let response = dialog.showMessageBoxSync(getCurrentWindow(), options);



							}
							if (vertrag.typ == CONSTANTS.KAUFVERTRAG) {
								kunde = vertrag.kunde;
							}
						});
						if (anlegen) {
							let item = new Object();
							item.type = "Vertrag";
							item.projekt = objekt.projekt;
							item.betrag = 0;
							item.objekt = objid;
							item.kunde = kunde;
							item.typ = CONSTANTS.RESERVIERUNG;
							item.status = CONSTANTS.AKTIV;
							item.name = "Neue Reservierung";
							item.vernr = DM.vertragstypkz[item.typ] + "-" + projekt.prjnummer + "-" + objekt.objnummer;
							ipcRenderer.send('edit:new', item);
						}

					},
				}));
				let zahltemplate = DM.zahltemplates.find(el => el.name == CONSTANTS.POSMABV);
				menu.append(new MenuItem({
					label: zahltemplate.bezeichnung,
					//label: "Kaufvertrag anlegen",
					click() {
						let objid = e.target.id.substring(3, e.target.id.length);
						//console.log(objid);
						//let vertragstypkz = konfiguration.get('VertragstypKz');
						//let objDM = datenmodell.get('Objekte')
						//let prjDM = datenmodell.get('Projekte')
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(objid));
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
						let vertraege = DM.vertragDM.filter(el => parseInt(el.objekt) == parseInt(objid) && el.status != CONSTANTS.TRASH);
						let anlegen = true;
						let kunde;
						vertraege.forEach(vertrag => {
							if (vertrag.typ == CONSTANTS.KAUFVERTRAG) {
								anlegen = false;
								let options = {
									buttons: ["Ok"],
									message: "Kaufvertrag kann nicht angelegt werden",
									title: "Fehlermeldung",
									detail: "Für diese Einheit exisitiert bereits ein Kaufvertrag.",
									icon: path.join(__dirname, 'assets/icons/png/bandage.png')
								};
								let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
							}
						});
						if (anlegen) {
							let item = new Object();
							item.type = "Vertrag";
							item.projekt = objekt.projekt;
							item.betrag = objekt.betrag;
							item.objekt = objid;
							item.kunde = objekt.kunde;
							item.typ = CONSTANTS.KAUFVERTRAG;
							item.status = CONSTANTS.NEU;
							item.name = "Neuer Vertrag";
							item.vernr = DM.vertragstypkz[item.typ] + "-" + projekt.prjnummer + "-" + objekt.objnummer;
							//console.log(item);
							ipcRenderer.send('edit:new', item);
						}

					},
				}));


				menu.append(new MenuItem({
					label: 'Mehrkosten anlegen',
					click() {
						let objid = e.target.id.substring(3, e.target.id.length);
						//console.log(objid);
						//let vertragstypkz = konfiguration.get('VertragstypKz');
						//let objDM = datenmodell.get('Objekte')
						//let prjDM = datenmodell.get('Projekte')
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(objid));
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
						let item = new Object();
						item.type = "Vertrag";
						item.projekt = objekt.projekt;
						item.objekt = objid;
						item.betrag = 0;
						item.kunde = objekt.kunde;
						item.typ = CONSTANTS.MEHRKOSTEN;
						item.status = CONSTANTS.NEU;
						item.name = "Neuer Vertrag";
						item.vernr = DM.vertragstypkz[item.typ] + "-" + projekt.prjnummer + "-" + objekt.objnummer;
						//console.log(item);
						ipcRenderer.send('edit:new', item);

					},
				}));
				menu.append(new MenuItem({
					label: 'Gutschrift anlegen',
					click() {
						let objid = e.target.id.substring(3, e.target.id.length);
						//console.log(objid);
						//let vertragstypkz = konfiguration.get('VertragstypKz');
						//let objDM = datenmodell.get('Objekte')
						//let prjDM = datenmodell.get('Projekte')
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(objid));
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
						let item = new Object();
						item.type = "Vertrag";
						item.projekt = objekt.projekt;
						item.betrag = 0;
						item.kunde = objekt.kunde;
						item.objekt = objid;
						item.typ = CONSTANTS.GUTSCHRIFT;
						item.status = CONSTANTS.NEU;
						item.name = "Gutschrift";
						item.vernr = DM.vertragstypkz[item.typ] + "-" + projekt.prjnummer + "-" + objekt.objnummer;
						ipcRenderer.send('edit:new', item);

					},
				}));
				menu.append(new MenuItem({
					label: 'Einheit löschen',
					click() {
						let einheitid = e.target.id.substring(3, e.target.id.length);
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(einheitid));

						//console.log(message);
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Einheit wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('objekt:delete', einheitid);
						}

					}
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstver')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Bearbeiten',
					click() {
						anzeige.vertragid = e.target.id.substring(3, e.target.id.length);
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
						switch (vertrag.typ) {
							case CONSTANTS.ARCHVERTRAG:
								anzeige.show = "vertragARCedit";
								break;
							case CONSTANTS.DARLEHEN:
								anzeige.show = "vertragDARedit";
								break;
							case CONSTANTS.RESERVIERUNG:
								anzeige.show = "vertragReservierungedit";
								break;
							default:
								anzeige.show = "vertragedit";
								break;

						}
						updateAnzeige();
						//showVertrag(anzeige.vertragid);
						//editStart();

					},
				}));
				menu.append(new MenuItem({ type: 'separator' }));
				menu.append(new MenuItem({
					label: 'Neue Rechnung/ Gutschrift',
					click() {
						let objid = e.target.id.substring(3, e.target.id.length);
						//let verDM = datenmodell.get('Vertraege');
						//let kunDM = datenmodell.get('Kunden');
						//console.log(objid);
						let vertrag = DM.vertragDM.find(el => el.id == objid);
						let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(vertrag.objekt));
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
						//console.log(vertrag);
						let kunde = DM.kundenDM.find(el => el.id == vertrag.kunde);
						if (typeof vertrag.kunde == 'undefined' || vertrag.kunde == "") {
							showSimpleInfo("Kunde nicht zugeordnet", "Bitte zuerst im Vertrag " + vertrag.vernr + " den Kunden zuordnen.")
						} else {
							let item = new Object();
							item.type = "Rechnung";
							item.vertrag = vertrag.id;
							if (vertrag.typ == CONSTANTS.GUTSCHRIFT) {
								item.betreff = "Gutschrift";
							} else {
								item.betreff = "Rechnung";
							}
							item.anrede = DM.anredeliste[kunde.anrede];
							item.name = kunde.name;
							item.vorname = kunde.vorname;
							item.strasse = kunde.strasse;
							item.plz = kunde.plz;
							item.ort = kunde.ort;
							item.rechdat = ""; //getDatum(1);
							item.faelligdat = ""; //getDatum(20);
							item.status = CONSTANTS.ENTWURF;
							let rechstr = "";
							let nextnr = nextRechnr(vertrag);
							//item.vernr = DM.vertragstypkz[item.typ] + "-" + projekt.prjnummer + "-" + objekt.objnummer;
							//rechstr = vertrag.vernr + "-" + nextnr;
							//console.log(projekt.prjnummer + "-" + nextnr + "-"+ objekt.objnummer + "-" + kunde.name );
							rechstr = DM.vertragstypkz[vertrag.typ] + "-"+ projekt.prjnummer + "-" + nextnr + "-"+ objekt.objnummer + "-" + kunde.name;

							item.rechnr = rechstr;
							ipcRenderer.send('edit:new', item);
						}

					},
				}));
				menu.append(new MenuItem({
					label: 'Vertrag löschen',
					click() {
						let vertragid = e.target.id.substring(3, e.target.id.length);
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(vertragid));

						//console.log(message);
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Vertrag wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('vertrag:delete', vertragid);
						}

					},
				}));
				found = true;
			}

		}
		if (e.target.classList.contains('lstdar')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Darlehen bearbeiten',
					click() {
						anzeige.vertragid = e.target.id.substring(3, e.target.id.length);
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
						switch (vertrag.typ) {
							case CONSTANTS.ARCHVERTRAG:
								anzeige.show = "vertragARCedit";
								break;
							case CONSTANTS.DARLEHEN:
								anzeige.show = "vertragDARedit";
								break;
							case CONSTANTS.RESERVIERUNG:
								anzeige.show = "vertragReservierungedit";
								break;
							default:
								anzeige.show = "vertragedit";
								break;

						}
						updateAnzeige();
						//showVertrag(anzeige.vertragid);
						//editStart();

					},
				}));
				menu.append(new MenuItem({
					label: 'Darlehen löschen',
					click() {
						let vertragid = e.target.id.substring(3, e.target.id.length);
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(vertragid));

						//console.log(message);
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Vertrag wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('vertrag:delete', vertragid);
						}

					},
				}));
				found = true;
			}

		}
		if (e.target.classList.contains('lstrech')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: "Bearbeiten",
					click() {
						anzeige.show = "rechnung";
						anzeige.rechnungid = e.target.id.substring(3, e.target.id.length);
						let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(anzeige.rechnungid));
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
						showRechnung(anzeige.rechnungid);
						editStart();

					}
				}));
				menu.append(new MenuItem({ type: 'separator' }));

				menu.append(new MenuItem({
					label: 'Löschen',
					click() {
						let rechnungid = e.target.id.substring(3, e.target.id.length);
						let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(rechnungid));
						let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Rechnung wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};
						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('rechnung:delete', rechnungid);
						}


					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstkun')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neue Person',
					click() {

						ById('modalKundeChange').classList.add('is-active');
						ById('kun-id').value = "NEU";
						ById('dpKunAnrede').classList.remove('is-active');
						ById('dpKunAnrede2').classList.remove('is-active');
						ById('dpKunAnrede-button').innerHTML = "Nicht erfasst";
						ById('dpKunAnrede2-button').innerHTML = "Nicht erfasst";
						ById('dpKunStatus').classList.remove('is-active');

						ById('kun-kundenr').value = neueKundennummer();

						ById('kun-name').value = "";
						ById('kun-vorname').value = "";
						ById('kun-strasse').value = "";
						ById('kun-plz').value = "";
						ById('kun-ort').value = "";
						ById('kun-telefon').value = "";
						ById('kun-telefon2').value = "";
						ById('kun-email').value = "";

						ById('kun-name2').value = "";
						ById('kun-vorname2').value = "";
						ById('kun-strasse2').value = "";
						ById('kun-plz2').value = "";
						ById('kun-ort2').value = "";
						ById('kun-telefon3').value = "";
						ById('kun-telefon4').value = "";
						ById('kun-email2').value = "";
						ById('fldkunde').checked = true;
						ById('fldlieferant').checked = false;
						ById('fldnotar').checked = false;
						ById('fldmitarbeiter').checked = false;
						ById('dpKunAnrede-id').value = "";
						ById('dpKunAnrede2-id').value = "";
						ById('dpKunStatus-id').value = CONSTANTS.AKTIV;

						// let item = new Object();
						// item.type = "Kunde";
						// item.kunde = true;
						// ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstfir')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neue Firma',
					click() {
						let item = new Object();
						item.type = "Kunde";
						item.lieferant = true;
						ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstnot')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neuer Notar',
					click() {
						let item = new Object();
						item.type = "Kunde";
						item.notar = true;
						ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstplan')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neuer Vertrag',
					click() {
						let item = new Object();
						item.type = "Vertrag";
						item.projekt = 0;
						item.typ = CONSTANTS.ARCHVERTRAG;
						item.status = CONSTANTS.AKTIV;
						item.name = "Neuer Vertrag";

						let nextnr = nextVertragNr(CONSTANTS.ARCHVERTRAG);
						let nextnrstr = "";
						nextnr < 10 ? nextnrstr = "0" + nextnr : nextnrstr = nextnr;
						item.vernr = DM.vertragstypkz[item.typ] + "-" + + nextnrstr;

						ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstdarall')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neues Darlehen',
					click() {
						let item = new Object();
						item.type = "Vertrag";
						item.projekt = 0;
						item.typ = CONSTANTS.DARLEHEN;
						item.status = CONSTANTS.NEU;
						item.sdat = new Date();
						item.text = "Neues Darlehen";

						let nextnr = nextVertragNr(CONSTANTS.DARLEHEN);
						let nextnrstr = "";
						nextnr < 10 ? nextnrstr = "0" + nextnr : nextnrstr = nextnr;
						item.vernr = DM.vertragstypkz[item.typ] + "-" + + nextnrstr;

						ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lstprjall')) {
			if (roleedit) {
				menu.append(new MenuItem({
					label: 'Neues Projekt',
					click() {
						ById('modalProjektChange').classList.add('is-active');
						ById('prj-id').value = "NEU";
						ById('prj-bautraeger').value = "";
						ById('prj-name').value = "Projektname";
						ById('prj-prjnummer').value = "00";
						ById('prj-beschreibung').value = "";
						ById('prj-bauleiter').value = "";
						ById('prj-iban').value = "";
						ById('prj-bic').value = "";
						ById('prj-bank').value = "";
						ById('prj-ibanE').value = "";

						// let item = new Object();
						// item.type = "Projekt";
						// ipcRenderer.send('edit:new', item);
					},
				}));
				found = true;
			}

		}
		if (e.target.classList.contains('elemkun')) {
			if (roleedit) {

				menu.append(new MenuItem({
					label: 'Bearbeiten',
					click() {
						anzeige.show = "kunde";
						showKunde(e.target.id.substring(3, e.target.id.length));
						editStart();
					},
				}));

				menu.append(new MenuItem({
					label: 'Löschen',
					click() {
						//console.log(message);
						let options = {
							buttons: ["Löschen", "Abbrechen"],
							message: "Kunde wirklich löschen?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						//console.log(response);
						if (response == 0) {
							ipcRenderer.send('kunde:delete', e.target.id.substring(3, e.target.id.length));
						}
					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('lsttrash')) {
			if (roleedit) {

				menu.append(new MenuItem({
					label: 'Papierkorb leeren',
					click() {
						//console.log(message);
						let options = {
							buttons: ["Leeren", "Abbrechen"],
							message: "Papierkorb wirklich leeren?",
							title: "Frage",
							detail: "",
							type: "question"
						};

						let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
						if (response == 0) {
							ipcRenderer.send('trash:empty', "");
						}


					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('elemtrash')) {
			if (roleedit) {

				menu.append(new MenuItem({
					label: 'Wiederherstellen',
					click() {
						if (e.target.id.substring(1, 4) == "rec") {
							//let recDM = datenmodell.get('Rechnungen');
							//let vertragDM = datenmodell.get('Vertraege');

							let rechnung = new Object();
							rechnung.id = e.target.id.substring(4, e.target.id.length);
							rechnung.status = CONSTANTS.ENTWURF;
							//console.log("Rechnung ", rechnung.id)
							let rec = DM.rechnungenDM.find(el => el.id == rechnung.id);
							let ver = DM.vertragDM.find(el => el.id == rec.vertrag);
							//console.log(rec, ver);
							if (typeof ver == 'undefined') {
								showSimpleInfo("Wiederherstellen nicht möglich", "Vertrag zur Rechnung exisitiert nicht mehr.")
							} else {
								if (ver.status == CONSTANTS.TRASH) {
									showSimpleInfo("Wiederherstellen nicht möglich", "Vertrag liegt im Mülleimer.")
								} else {
									ipcRenderer.send('rechnung:change', rechnung);


								}
							}


						}
						if (e.target.id.substring(1, 4) == "ver") {
							let vertrag = new Object();
							vertrag.id = e.target.id.substring(4, e.target.id.length);
							let ver = DM.vertragDM.find(el => el.id == vertrag.id);

							let obj = DM.objekteDM.find(el => el.id == ver.objekt);
							//console.log(ver);
							if (typeof obj == 'undefined' && ver.projekt != 0) {
								showSimpleInfo("Wiederherstellen nicht möglich", "Die Einheit zu diesem Vertrag exisitiert nicht mehr.")
							} else {
								if (typeof obj != 'undefined' && obj.status == CONSTANTS.TRASH) {
									showSimpleInfo("Wiederherstellen nicht möglich", "Die Einheit (" + obj.text + " " + obj.objnummer + ") zu diesem Vertrag liegt im Mülleimer.")
								} else {
									ver.status = CONSTANTS.ENTWURF;
									ipcRenderer.send('vertrag:change', ver);

									// Alle Positionen wiederherstellen
									let pos = DM.positionenDM.filter(el => el.vertrag == ver.id);
									pos.forEach(el => {
										el.status = CONSTANTS.AKTIV;
										ipcRenderer.send('position:change', el);

									});

									// Alle Rechnungen wiederherstellen
									let rec = DM.rechnungenDM.filter(el => el.vertrag == ver.id);
									rec.forEach(el => {
										el.status = CONSTANTS.AKTIV;
										ipcRenderer.send('rechnung:change', el);

									});


								}
							}

						}
						if (e.target.id.substring(1, 4) == "prj") {
							let projekt = new Object();

							projekt.id = e.target.id.substring(4, e.target.id.length);

							projekt.status = CONSTANTS.ENTWURF;
							ipcRenderer.send('projekt:change', projekt);

						}


						if (e.target.id.substring(1, 4) == "obj") {
							let objekt = new Object();
							objekt.id = e.target.id.substring(4, e.target.id.length);
							//console.log("Einheit ", objekt.id)
							objekt.status = CONSTANTS.AKTIV;
							ipcRenderer.send('obj:change', objekt);
						}
						if (e.target.id.substring(1, 4) == "kun") {
							let objekt = new Object();
							objekt.id = e.target.id.substring(4, e.target.id.length);
							//console.log("Einheit ", objekt.id)
							objekt.status = CONSTANTS.AKTIV;
							ipcRenderer.send('kunde:change', objekt);
						}

					},
				}));
				found = true;
			}
		}
		if (e.target.classList.contains('docvorlage')) {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let msg = new Object();
					//console.log(e.target.innerHTML);
					msg = {
						text: e.target.innerHTML,
						pfad: "D:",
						neu: false,
						rechid: anzeige.rechnungid
					}
					ipcRenderer.send('template:changeRec', msg);
				}
			}));
			menu.append(new MenuItem({
				label: 'Buttonbeschriftung ändern',
				click() {
					ById('inprecBtnsInput').classList.remove('is-hidden');
					ById('inprecBtnsInput').value = "";
					ById('inprecBtnsInput').setAttribute("freifeld", e.target.innerHTML);
					ById('inprecBtnsInput').focus();
				}
			}));
			menu.append(new MenuItem({
				label: 'Weitere Vorlage hinzufügen',
				click() {
					let msg = new Object();
					msg = {
						text: "Neue Vorlage",
						pfad: "",
						neu: true,
						rechid: anzeige.rechnungid
					}
					ipcRenderer.send('template:changeRec', msg);
				}
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlage1') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
					if (vertrag.typ == CONSTANTS.ARCHVERTRAG) {
						msg = {
							text: "Vorlage für Rechnung auswählen (Bereich: Architektenverträge)",
							template: CONSTANTS.TEMPLATER0,
							prj: 0
						}
					} else {
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
						msg = {
							text: "Vorlage für Rechnung auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
							template: CONSTANTS.TEMPLATER0,
							prj: anzeige.projektid
						}
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlage2') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
					if (vertrag.typ == CONSTANTS.ARCHVERTRAG) {
						msg = {
							text: "Vorlage für Schlussrechnung auswählen (Bereich: Architektenverträge)",
							template: CONSTANTS.TEMPLATER1,
							prj: 0
						}
					} else {
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
						msg = {
							text: "Vorlage für Schlussrechnung auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
							template: CONSTANTS.TEMPLATER1,
							prj: anzeige.projektid
						}
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlage3') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
					if (vertrag.typ == CONSTANTS.ARCHVERTRAG) {
						msg = {
							text: "Vorlage für Rechnung auswählen (Bereich: Architektenverträge)",
							template: CONSTANTS.TEMPLATER2,
							prj: 0
						}
					} else {
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
						msg = {
							text: "Vorlage für Brief auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
							template: CONSTANTS.TEMPLATER2,
							prj: anzeige.projektid
						}
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlageR3') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));

					let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
					msg = {
						text: "Vorlage für Mehrkostenrechnung auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
						template: CONSTANTS.TEMPLATER3,
						prj: anzeige.projektid
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlageR4') {
			menu.append(new MenuItem({

				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));

					let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
					msg = {
						text: "Vorlage für Gutschrift auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
						template: CONSTANTS.TEMPLATER4,
						prj: anzeige.projektid
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnRechAllg') {
			menu.append(new MenuItem({

				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
					let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
					msg = {
						text: "Vorlage für Rechnung auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
						template: CONSTANTS.TEMPLATER5,
						prj: anzeige.projektid
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlageV1') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let text = "";
					let msg = new Object();
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(anzeige.vertragid));
					if (vertrag.typ == CONSTANTS.ARCHVERTRAG) {
						msg = {
							text: "Vorlage für Tabellenexport auswählen (Bereich: Architektenverträge)",
							template: CONSTANTS.TEMPLATEV1,
							prj: 0
						}
					} else {
						let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
						msg = {
							text: "Vorlage für Tabellenexport auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
							template: CONSTANTS.TEMPLATEV1,
							prj: anzeige.projektid
						}
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlageP1') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let msg = new Object();
					msg = {
						text: "Vorlage für Tabellenexport auswählen",
						template: CONSTANTS.TEMPLATEP1,
						prj: anzeige.projektid
					}
					ipcRenderer.send('template:change', msg);
				},
			}));
			found = true;
		}
		if (e.target.id == 'btnvorlageS1') {
			menu.append(new MenuItem({
				label: 'Vorlage ändern',
				click() {
					let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(anzeige.projektid));
					let msg = {
						text: "Vorlage für Serienbrief auswählen (Projekt " + projekt.prjnummer + " " + projekt.name + ")",
						template: CONSTANTS.TEMPLATES1,
						prj: anzeige.projektid
					}
					ipcRenderer.send('template:change', msg);

				},
			}));
			found = true;
		}
		// menu.append(new MenuItem({
		// 	label: 'Kopieren',
		// 	role: 'copy',
		// 	click: () => { }
		// }));
		if (found) {

			menu.popup({ window: electron.remote.getCurrentWindow() });
			menu.once('menu-will-close', () => {
				delete menu;
			});
		}
	});
}
function showSearchResults() {
	ById('kunall').checked = false;
	ById('prjall').checked = false;

	let objekte = ById('navtree').getElementsByClassName("lstobj");
	for (let i = 0; i < objekte.length; i++) {
		objekte[i].classList.remove("blinking");
	}
	let kunden = ById('navtree').getElementsByClassName("elemkun");
	for (let i = 0; i < kunden.length; i++) {
		kunden[i].classList.remove("blinking");
		kunden[i].classList.remove("is-hidden");
	}
	let vertraege = ById('navtree').getElementsByClassName("lstver");
	for (let i = 0; i < vertraege.length; i++) {
		vertraege[i].classList.remove("blinking");
	}
	let rechnungen = ById('navtree').getElementsByClassName("lstrech");
	for (let i = 0; i < rechnungen.length; i++) {
		rechnungen[i].classList.remove("blinking");
	}
	for (let i = 0; i < kunden.length; i++) {
		if (anzeige.suche != "") {
			index = kunden[i].id.substring(3, kunden[i].length);
			let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(index));
			if (typeof kunde != 'undefined') {
				let such = 0;
				if (typeof kunde.name2 != 'undefined') {
					if (kunde.name2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.vorname2 != 'undefined') {
					if (kunde.vorname2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.name != 'undefined') {
					if (kunde.name.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.vorname != 'undefined') {
					if (kunde.vorname.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}

				if (typeof kunde.kundennummer != 'undefined') {
					if (kunde.kundennummer.toString().toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}

				if (such > 0) {
					//	kunden[i].classList.add("blinking");
					ById('kunall').checked = true;
				} else {
					kunden[i].classList.add("is-hidden");

				}
			}
		}
	}
	for (let i = 0; i < vertraege.length; i++) {
		if (anzeige.suche != "") {
			index = vertraege[i].id.substring(3, vertraege[i].length);
			let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(index));
			if (typeof vertrag != 'undefined') {
				let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
				let such = 0;
				if (vertrag.vernr.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
					such += 1;
				}
				if (typeof kunde != 'undefined') {
					if (typeof kunde.name2 != 'undefined') {
						if (kunde.name2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
							such += 1;
						}
					}
					if (typeof kunde.vorname2 != 'undefined') {
						if (kunde.vorname2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
							such += 1;
						}
					}
					if (typeof kunde.name != 'undefined') {
						if (kunde.name.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
							such += 1;
						}
					}
					if (typeof kunde.vorname != 'undefined') {
						if (kunde.vorname.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
							such += 1;
						}
					}
				}
				if (such > 0) {
					ById('vla' + vertrag.id).classList.add("blinking");
					ById('vla' + vertrag.id).classList.add("blinking");
					ById('ver' + vertrag.id).checked = true;

					ById('ola' + vertrag.objekt).classList.add("blinking");
					ById('obj' + vertrag.objekt).checked = true;

					ById('prj' + vertrag.projekt).checked = true;
					ById('prjall').checked = true;

					//console.log('kun' + vertrag.kunde);
					//ById('kun' + vertrag.kunde).classList.add("blinking");
					//console.log('class blinking');

					ById('kunall').checked = true;
				} else {

				}
			}
		}

	}
	for (let i = 0; i < rechnungen.length; i++) {
		if (anzeige.suche != "") {
			index = rechnungen[i].id.substring(3, rechnungen[i].length);
			let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(index));
			if (typeof rechnung != 'undefined') {
				let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
				let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));

				let such = 0;
				if (rechnung.rechnr.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
					such += 1;
				}
				if (typeof kunde.name2 != 'undefined') {
					if (kunde.name2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.vorname2 != 'undefined') {
					if (kunde.vorname2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.name != 'undefined') {
					if (kunde.name.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}
				if (typeof kunde.vorname != 'undefined') {
					if (kunde.vorname.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
						such += 1;
					}
				}

				if (such > 0) {
					rechnungen[i].classList.add("blinking");
					ById('vla' + rechnung.vertrag).classList.add("blinking");
					ById('ver' + rechnung.vertrag).checked = true;

					ById('ola' + vertrag.objekt).classList.add("blinking");
					ById('obj' + vertrag.objekt).checked = true;

					ById('prj' + vertrag.projekt).checked = true;
					ById('prjall').checked = true;
					//ById('kun' + vertrag.kunde).classList.add("blinking");
					ById('kunall').checked = true;
				}
			}
		}
	};




	// ById('navtree').childNodes.forEach(el=>{


	// });

	// level1.children().each(function (index, level2) {
	// 	console.log("  --> " + level2);

	// });
	//			ById(val.id).classList.remove("blinking");



	// let tree = ById('navtree');
	// let elements = tree.getElementsByTagName("input");
	// for (let i = 0; i < elements.length; i++) {
	// 	//elements[i].checked = false;
	// 	elements[i].classList.remove("blinking");
	// }
	// elements = tree.getElementsByClassName("elemkun");
	// for (let i = 0; i < elements.length; i++) {
	// 	elements[i].classList.remove("is-hidden");
	// 	elements[i].classList.remove("blinking");

	// }
	// elements = tree.getElementsByClassName("lstver");
	// for (let i = 0; i < elements.length; i++) {
	// 	elements[i].classList.remove("is-hidden");
	// 	elements[i].remove("blinking");
	// }
	// elements = tree.getElementsByClassName("lstdar");
	// for (let i = 0; i < elements.length; i++) {
	// 	elements[i].classList.remove("is-hidden");
	// }


	// if (anzeige.suche != "") {
	// 	// let findKunId = new Array();
	// 	// for (let pr = 0; pr < DM.projekteDM.length; pr++) {

	// 	// 	let elem = ById("pla" + DM.projekteDM[pr].id);
	// 	// 	if (elem != null) { elem.classList.add("is-hidden"); }
	// 	// 	elem = ById("pol" + DM.projekteDM[pr].id);
	// 	// 	if (elem != null) { elem.classList.add("is-hidden"); }

	// 	// 	let objekte = DM.objekteDM.filter(el => el.projekt == DM.projekteDM[pr].id && el.status != CONSTANTS.TRASH);
	// 	// 	for (let oj = 0; oj < objekte.length; oj++) {
	// 	// 		let elem = ById("opr" + objekte[oj].id);
	// 	// 		if (elem != null) { elem.classList.add("is-hidden") }
	// 	// 		elem = ById("ola" + objekte[oj].id);
	// 	// 		if (elem != null) { elem.classList.add("is-hidden") }
	// 	// 		elem = ById("obj" + objekte[oj].id);
	// 	// 		if (elem != null) { elem.classList.add("is-hidden") }
	// 	// 	}
	// 	// }

	// 	// for (let ku = 0; ku < DM.kundenDM.length; ku++) {
	// 	// 	//console.log(ku);
	// 	// 	if (DM.kundenDM[ku].status != CONSTANTS.TRASH) {
	// 	// 		let treffer = 0;
	// 	// 		if (typeof DM.kundenDM[ku].name2 != 'undefined') {
	// 	// 			if (DM.kundenDM[ku].name2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
	// 	// 				treffer += 1;
	// 	// 			}
	// 	// 		}
	// 	// 		if (typeof DM.kundenDM[ku].vorname2 != 'undefined') {
	// 	// 			if (DM.kundenDM[ku].vorname2.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
	// 	// 				treffer += 1;
	// 	// 			}
	// 	// 		}
	// 	// 		if (typeof DM.kundenDM[ku].name != 'undefined') {
	// 	// 			if (DM.kundenDM[ku].name.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
	// 	// 				treffer += 1;
	// 	// 			}
	// 	// 		}
	// 	// 		if (typeof DM.kundenDM[ku].vorname != 'undefined') {
	// 	// 			if (DM.kundenDM[ku].vorname.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
	// 	// 				treffer += 1;
	// 	// 			}
	// 	// 		}
	// 	// 		if (treffer > 0) {
	// 	// 			//console.log("Found ", DM.kundenDM[ku].name, DM.kundenDM[ku].vorname)
	// 	// 			findKunId.push(DM.kundenDM[ku].id);
	// 	// 			ById("kunall").checked = true;
	// 	// 			ById("kun" + DM.kundenDM[ku].id).classList.add("blinking");


	// 	// 		} else {
	// 	// 			if (DM.kundenDM[ku].kunde == true) {
	// 	// 				let elem = ById("kun" + DM.kundenDM[ku].id);
	// 	// 				if (typeof elem != 'undefined' || elem == "null") {
	// 	// 					elem.classList.add("is-hidden");
	// 	// 				}
	// 	// 			}
	// 	// 			if (DM.kundenDM[ku].notar == true) {
	// 	// 				//let elem = ById("ku2" + DM.kundenDM[ku].id);
	// 	// 				//Zusammengelegt
	// 	// 				let elem = ById("kun" + DM.kundenDM[ku].id);

	// 	// 				if (typeof elem != 'undefined' || elem == "null") {
	// 	// 					elem.classList.add("is-hidden");
	// 	// 				}
	// 	// 			}
	// 	// 			if (DM.kundenDM[ku].lieferant == true) {
	// 	// 				//let elem = ById("ku3" + DM.kundenDM[ku].id);
	// 	// 				//Zusammengelegt
	// 	// 				let elem = ById("kun" + DM.kundenDM[ku].id);
	// 	// 				if (typeof elem != 'undefined' || elem == "null") {
	// 	// 					elem.classList.add("is-hidden");
	// 	// 				}
	// 	// 			}

	// 	// 		}
	// 	// 	}

	// 	// }

	// 	// for (let re = 0; re < DM.rechnungenDM.length; re++) {
	// 	// 	let such = 0;
	// 	// 	if (typeof DM.rechnungenDM[re].rechnr != 'undefined') {
	// 	// 		if (DM.rechnungenDM[re].rechnr.toLowerCase().indexOf(anzeige.suche.toLowerCase()) >= 0) {
	// 	// 			such += 1;
	// 	// 		}
	// 	// 	}
	// 	// 	//console.log(anzeige.suche, "Rechnung", re);
	// 	// 	if (such > 0) {
	// 	// 		if (ById("rec" + DM.rechnungenDM[re].id) != null) {

	// 	// 			ById("rec" + DM.rechnungenDM[re].id).classList.add("blinking");
	// 	// 		}
	// 	// 		//console.log("rec" + DM.rechnungenDM[re].id);
	// 	// 		let vertrag = DM.vertragDM.find(el => el.id == DM.rechnungenDM[re].vertrag && el.status != CONSTANTS.TRASH && el.status != CONSTANTS.DELETE);
	// 	// 		if (typeof vertrag != 'undefined') {
	// 	// 			ById("ver" + vertrag.id).checked = true;
	// 	// 			if (ById("vla" + vertrag.id) != null) {
	// 	// 				ById("vla" + vertrag.id).classList.add("blinking");
	// 	// 			}
	// 	// 			ById("prjall").checked = true;
	// 	// 			ById("prj" + vertrag.projekt).checked = true;
	// 	// 			ById("obj" + vertrag.objekt).checked = true;

	// 	// 			ById("opr" + vertrag.objekt).classList.remove("is-hidden");
	// 	// 			ById("ola" + vertrag.objekt).classList.remove("is-hidden");
	// 	// 			ById("obj" + vertrag.objekt).classList.remove("is-hidden");

	// 	// 		}
	// 	// 	}
	// 	// }
	// 	// for (let ve = 0; ve < DM.vertragDM.length; ve++) {
	// 	// 	//console.log(anzeige.suche, "Rechnung", re);
	// 	// 	if (typeof DM.vertragDM[ve].status != 'undefined' && DM.vertragDM[ve].status !== CONSTANTS.TRASH) {
	// 	// 		let treffer = 0;
	// 	// 		findKunId.forEach(el => { if (el == DM.vertragDM[ve].kunde) { treffer++; } });
	// 	// 		if (treffer > 0) {
	// 	// 		//	console.log("Treffer ", DM.vertragDM[ve].vernr, DM.vertragDM[ve].kunde, findKunId);

	// 	// 			switch (DM.vertragDM[ve].typ) {
	// 	// 				case CONSTANTS.ARCHVERTRAG:
	// 	// 					ById("planall").checked = true;
	// 	// 					elem = ById("vpi" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.remove("is-hidden"); }
	// 	// 					break;
	// 	// 				case CONSTANTS.DARLEHEN:
	// 	// 					ById("darall").checked = true;
	// 	// 					elem = ById("dpi" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.remove("is-hidden"); }
	// 	// 					break;
	// 	// 				default:
	// 	// 					ById("ver" + DM.vertragDM[ve].id).checked = true;
	// 	// 					if (ById("vla" + DM.vertragDM[ve].id) != null) {
	// 	// 						ById("vla" + DM.vertragDM[ve].id).classList.add("blinking");
	// 	// 					}
	// 	// 					ById("prjall").checked = true;
	// 	// 					ById("pla" + DM.vertragDM[ve].projekt).classList.remove("is-hidden");
	// 	// 					ById("pol" + DM.vertragDM[ve].projekt).classList.remove("is-hidden");
	// 	// 					ById("prj" + DM.vertragDM[ve].projekt).checked = true;
	// 	// 					ById("obj" + DM.vertragDM[ve].objekt).checked = true;
	// 	// 					ById("opr" + DM.vertragDM[ve].objekt).classList.remove("is-hidden")
	// 	// 					ById("ola" + DM.vertragDM[ve].objekt).classList.remove("is-hidden")
	// 	// 					ById("obj" + DM.vertragDM[ve].objekt).classList.remove("is-hidden")
	// 	// 			}


	// 	// 		} else {
	// 	// 			switch (DM.vertragDM[ve].typ) {
	// 	// 				case CONSTANTS.ARCHVERTRAG:
	// 	// 					elem = ById("vpl" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.add("is-hidden"); }
	// 	// 					elem = ById("vpo" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.add("is-hidden"); }
	// 	// 					break;
	// 	// 				case CONSTANTS.DARLEHEN:
	// 	// 					elem = ById("dpl" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.add("is-hidden"); }
	// 	// 					elem = ById("dpo" + DM.vertragDM[ve].id);
	// 	// 					if (elem != null) { elem.classList.add("is-hidden"); }
	// 	// 					break;
	// 	// 				default:
	// 	// 					elem = ById("vla" + DM.vertragDM[ve].id);
	// 	// 					ById("vla" + DM.vertragDM[ve].id).classList.remove("blinking");

	// 	// 					if (elem != null) { elem.classList.add("is-hidden"); }

	// 	// 			}
	// 	// 		}
	// 	// 	}

	// 	// }


	// }
	// else {
	// 	for (let pr = 0; pr < DM.projekteDM.length; pr++) {

	// 		let elem = ById("pla" + DM.projekteDM[pr].id);
	// 		if (elem != null) { elem.classList.remove("is-hidden"); }
	// 		elem = ById("pol" + DM.projekteDM[pr].id);
	// 		if (elem != null) { elem.classList.remove("is-hidden"); }

	// 		let objekte = DM.objekteDM.filter(el => el.projekt == DM.projekteDM[pr].id && el.status != CONSTANTS.TRASH);
	// 		for (let oj = 0; oj < objekte.length; oj++) {

	// 			let elem = ById("opr" + objekte[oj].id);
	// 			if (elem != null) { elem.classList.remove("is-hidden") }
	// 			elem = ById("ola" + objekte[oj].id);
	// 			if (elem != null) { elem.classList.remove("is-hidden") }
	// 			elem = ById("obj" + objekte[oj].id);
	// 			if (elem != null) { elem.classList.remove("is-hidden") }
	// 		}
	// 	}
	// }


}
function showSimpleInfo(message, detail) {
	//console.log(message);
	let options = {
		buttons: ["Ok"],
		message: message,
		title: "Hinweis",
		detail: detail,
		type: "info"
	};

	let response = dialog.showMessageBoxSync(getCurrentWindow(), options);
	//console.log(response);
	return response;
}
function openCommentFile(commentId) {
	//let commentDM = datenmodell.get('Kommentare');
	let comment = DM.commentDM.find(c => c.id == commentId);
	if (typeof comment !== 'undefined' &&
		comment !== 'undefined' &&
		typeof comment.datei !== 'undefined'
	) {
		//console.log("Versuch Datei zu öffnen: ", comment.datei)
		openFileWithDefaultApp(comment.datei);

	}
}
function getTransactionsforCustomer(kunID) {
	//let zahlungseingangDM = datenmodell.get('Zahlungseingaenge');
	//let rechnungenDM = datenmodell.get('Rechnungen');
	//let vertragDM = datenmodell.get('Vertraege');
	//let positionenDM = datenmodell.get('Positionen');
	let transactions = new Array();
	let vertraege = DM.vertragDM.filter(el => parseInt(el.kunde) == parseInt(kunID) && el.status !== CONSTANTS.TRASH);
	if (vertraege.length > 0) {
		for (let ve = 0; ve < vertraege.length; ve++) {
			//if (vertragDM[ve].typ !== CONSTANTS.RESERVIERUNG) {
			let rechnungen = DM.rechnungenDM.filter(el => parseInt(el.vertrag) == parseInt(vertraege[ve].id) && el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ENTWURF);
			if (rechnungen.length > 0) {

				for (let re = 0; re < rechnungen.length; re++) {
					// let ta = new Object();
					// ta.date = formatDatum(rechnungen[re].rechdat);
					// if (vertraege[ve].typ !== CONSTANTS.RESERVIERUNG) {
					// 	ta.value = - parseFloat(rechnungen[re].betrag);
					// 	ta.text = rechnungen[re].rechnr + " " + rechnungen[re].betreff;
					// 	transactions.push(ta);
					// }

					if (typeof (DM.zeDM) != 'undefined' && DM.zeDM != 'undefined') {
						let zahlungseingang = DM.zeDM.filter(el => parseInt(el.rechnung) == parseInt(rechnungen[re].id));
						if (typeof (zahlungseingang) !== 'undefined' && zahlungseingang.length > 0) {
							//console.log(zahlungseingang);
							for (let ze = 0; ze < zahlungseingang.length; ze++) {
								let taZE = new Object();
								taZE.date = formatDatum(zahlungseingang[ze].datum);
								taZE.value = parseFloat(zahlungseingang[ze].betrag);
								if (vertraege[ve].typ !== CONSTANTS.RESERVIERUNG) {
									taZE.text = zahlungseingang[ze].text;
								} else {
									// Bei Reservierungen wird der Zahlungseinangstext
									// Automatisch erzeugt. Da nehmen wir den Betreff vom Vertrag
									taZE.text = rechnungen[re].betreff;

								}
								transactions.push(taZE);
							}
						}

					}
				}
			}
			//}
		}
	}
	transactions.sort((a, b) => {
		let n = string2Date(a.date).valueOf();
		let m = string2Date(b.date).valueOf();
		return (n < m) ? -1 : (n > m) ? 1 : 0;
	});
	return transactions;
}
function checkBearbeiterOk(user, projektid) {
	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(projektid));
	if (projekt.bearbeiter == user || projekt.id == 0) {
		return true;
	} else {
		let owner = "";
		typeof projekt.bearbeiter == 'undefined' ? owner = "Bisher gibt es noch keine\\n Bearbeiter\\in." : owner = "Bearbeiter\\in ist " + projekt.bearbeiter;

		let options = {
			buttons: ["Ok"],
			message: "Sie sind diesem Projekt nicht \n als Bearbeiter\\in zugewiesen.",
			title: "Hinweis",
			detail: owner,
			type: "info"
		};
		dialog.showMessageBoxSync(getCurrentWindow(), options);
		return false;
	}
}
function collectArcData() {
	let data = new Object();
	let verArray = new Array()
	let vertraegeSum = new Array();

	data.datum = getDatum(0);
	data.typ = CONSTANTS.TEMPLATEARCLIST;

	let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH &&
		el.status !== CONSTANTS.ARCHIV &&
		el.typ == CONSTANTS.ARCHVERTRAG);
	if (typeof vertraege != 'undefined') {
		let totalbetrag = 0, totaloffen = 0, totalbezahlt = 0;

		cLen = vertraege.length;
		for (ve = 0; ve < cLen; ve++) {
			let ver = new Object();

			totalbetrag += vertraege[ve].betrag;
			totaloffen += vertraege[ve].offenerbetrag;
			totalbezahlt += vertraege[ve].bezahlt;

			ver.vernr = vertraege[ve].vernr;
			ver.name = vertraege[ve].name;
			ver.gegenstand = vertraege[ve].gegenstand;
			ver.betrag = formatBetrag(vertraege[ve].betrag);
			ver.bezahlt = formatBetrag(vertraege[ve].bezahlt);
			ver.offenerbetrag = formatBetrag(vertraege[ve].offenerbetrag);
			verArray.push(ver);
		}

		let verSum = new Object();
		verSum.betrag = formatBetrag(totalbetrag);
		verSum.offen = formatBetrag(totaloffen);
		verSum.bezahlt = formatBetrag(totalbezahlt);
		vertraegeSum.push(verSum);
	}
	data.vertraege = verArray;
	data.vertraegeSum = vertraegeSum;

	return data;
}
function collectPrjData(idPrj) {
	let prj = DM.projekteDM.find(el => el.id == idPrj);
	let data = new Object();
	data.typ = CONSTANTS.TEMPLATEP1;
	data.datum = getDatum(0);

	let prjpos = new Array()
	let prjposSum = new Array()
	data.prjname = prj.name;
	data.bautraeger = prj.bautraeger;
	data.prjbeschreibung = prj.beschreibung;
	data.bauleiter = prj.bauleiter;
	data.bank = prj.bank;
	data.bic = prj.bic;
	data.ibana = prj.iban;
	data.ibane = prj.ibane;
	data.prjnummer = prj.prjnummer;

	obj = DM.objekteDM.filter(el => el.projekt == idPrj && el.status != CONSTANTS.TRASH);
	obj.sort((a, b) => {
		return parseInt(a.objnummer) - parseInt(b.objnummer);
	});
	let cLen = obj.length;
	if (typeof cLen !== 'undefined' && cLen > 0) {
		for (i = 0; i < cLen; i++) {
			let pos = new Object();
			pos.name = obj[i].text + " " + obj[i].objnummer

			let kunde = DM.kundenDM.find(el => el.id == obj[i].kunde);
			let kaeufer = "";
			if (typeof kunde != 'undefined') {
				pos.kaeufer = kunde.name;
			} else {
				pos.kaeufer = "";
			}
			if (typeof obj[i].kaufpreis == 'undefined') {
				pos.kaufpreis = "";
			} else {
				pos.kaufpreis = formatBetrag(obj[i].kaufpreis);
			}
			pos.whgsoll = formatBetrag(obj[i].whgsoll);
			pos.whghaben = formatBetrag(obj[i].whghaben);
			pos.rechoffen = formatBetrag(obj[i].rechoffen);
			pos.mehrsoll = formatBetrag(obj[i].mehrsoll);
			pos.mehrhaben = formatBetrag(obj[i].mehrhaben);
			pos.gutschriftensoll = formatBetrag(obj[i].gutschriften);
			pos.gutschriftenhaben = formatBetrag(obj[i].gutschriftenhaben);
			pos.habengesamt = formatBetrag(obj[i].habengesamt);
			prjpos.push(pos);
		}
		let posSum = new Object();
		posSum.kaufpreis = formatBetrag(prj.kaufpreis);
		posSum.whgsoll = formatBetrag(prj.whgsoll);
		posSum.whghaben = formatBetrag(prj.whghaben);
		posSum.rechoffen = formatBetrag(prj.rechoffen);
		posSum.mehrsoll = formatBetrag(prj.mehrsoll);
		posSum.mehrhaben = formatBetrag(prj.mehrhaben);
		posSum.gutschriftensoll = formatBetrag(prj.gutschriften);
		posSum.gutschriftenhaben = formatBetrag(prj.gutschriftenhaben);
		posSum.habengesamt = formatBetrag(prj.habengesamt);
		prjposSum.push(posSum);
	}
	data.prjpos = prjpos;
	data.posSum = prjposSum;
	return data;
}
function collectdata(vertrag, rechnung) {
	let data = new Object();
	data.datum = getDatum(0);
	if (typeof vertrag != 'undefined') {
		data.Vorname = vertrag.vorname;
		data.Name = vertrag.name;
		data.urkundenr = typeof vertrag.urnr != 'undefined' ? vertrag.urnr : " ";
		data.urkundedatum = typeof vertrag.urdat != 'undefined' ? vertrag.urdat : " ";
		data.notariat = typeof vertrag.notariat != 'undefined' ? vertrag.notariat : " ";
		data.fertig1datum = typeof vertrag.fdat1 != 'undefined' ? vertrag.fdat1 : " ";
		data.fertig2datum = typeof vertrag.fdat2 != 'undefined' ? vertrag.fdat2 : " ";
		if (typeof vertrag.mwst != 'undefined') {
			data.vertraegmwst = vertrag.mwst
		} else {
			data.vertraegmwst = "";
		}
		data.uebergabedatum = typeof vertrag.udat != 'undefined' ? vertrag.udat : " ";
		data.vertragsnr = vertrag.vernr;
		data.vertragsgegenstand = typeof vertrag.gegenstand != 'undefined' ? vertrag.gegenstand : " ";
		data.vertragbetrag = formatBetrag(vertrag.betrag);

		let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
		if (typeof kunde != 'undefined') {
			typeof DM.anredeliste[kunde.anrede] == 'undefined' ? data.rechanrede = "" : data.rechanrede = DM.anredeliste[kunde.anrede];
			data.rechanrede = DM.anredeliste[kunde.anrede];
			data.rechvorname = kunde.vorname;
			data.rechname = kunde.name;
			data.lieferant = kunde.lieferant;
			data.rechstrasse = kunde.strasse;
			data.rechplz = kunde.plz;
			data.rechort = kunde.ort;
			data.rechtelefon = kunde.telefon;
			data.rechemail = kunde.email;
			typeof DM.anredeliste[kunde.anrede2] == 'undefined' ? data.rechanrede2 = "" : data.rechanrede2 = DM.anredeliste[kunde.anrede2];
			if (typeof kunde.vorname2 != 'undefined' && kunde.vorname2 != "") {
				data.rechvorname2 = kunde.vorname2;
			} else {
				data.rechvorname2 = "";
			}
			if (typeof kunde.name2 != 'undefined' && kunde.name2 != "") {
				data.rechname2 = kunde.name2;
			} else {
				data.rechname2 = "";
			}
			if (data.rechvorname2 != "" && data.rechname2 != "") {
				if (kunde.lieferant == true) {
					data.anredezeile2 = "Sehr " + data.rechanrede2 + " " + data.rechname2 + ",";
					data.anredezeile1 = "";
					data.rechund = "\n";
				} else {
					data.anredezeile1 = "Sehr " + data.rechanrede + " " + data.rechname + ",";
					data.anredezeile2 = "sehr " + data.rechanrede2 + " " + data.rechname2 + ",";
					data.rechund = "und";
				}
			} else {
				data.anredezeile2 = "Sehr " + data.rechanrede + " " + data.rechname + ",";
				data.anredezeile1 = "";
				data.rechund = "";
			}
			data.rechtelefon2 = kunde.telefon2;
			data.rechtelefon3 = kunde.telefon3;
			data.rechemail2 = kunde.email2;
			data.rechstrasse2 = kunde.strasse2;
			data.rechplz2 = kunde.plz2;
			data.rechort2 = kunde.ort2;
		}



		// Objekte dazu
		let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(vertrag.objekt));
		if (typeof objekt != 'undefined') {
			data.einheit = objekt.text + " " + objekt.objnummer;
			data.objnummer = objekt.objnummer;
			data.kaufpreis = formatBetrag(objekt.kaufpreis);
			// Kunde dazu
			let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(objekt.kunde));
			if (typeof kunde != 'undefined') {
				typeof DM.anredeliste[kunde.anrede] == 'undefined' ? data.kanrede = "" : data.kanrede = DM.anredeliste[kunde.anrede];
				data.kanrede = DM.anredeliste[kunde.anrede];
				data.kvorname = kunde.vorname;
				data.kname = kunde.name;
				data.kstrasse = kunde.strasse;
				data.kplz = kunde.plz;
				data.kort = kunde.ort;
				data.ktelefon = kunde.telefon;
				data.kemail = kunde.email;
				typeof DM.anredeliste[kunde.anrede2] == 'undefined' ? data.kanrede2 = "" : data.kanrede2 = DM.anredeliste[kunde.anrede2];
				if (typeof kunde.vorname2 != 'undefined' && kunde.vorname2 != "") { data.kvorname2 = kunde.vorname2; } else { data.kvorname2 = ""; }
				if (typeof kunde.name2 != 'undefined' && kunde.name2 != "") { data.kname2 = kunde.name2; } else { data.kname2 = ""; }
				if (data.kvorname2 != "" && data.kname2 != "") { data.kund = "und"; } else { data.kund = ""; }
				data.ktelefon2 = kunde.telefon2;
				data.ktelefon3 = kunde.telefon3;
				data.kemail2 = kunde.email2;
				data.kstrasse2 = kunde.strasse2;
				data.kplz2 = kunde.plz2;
				data.kort2 = kunde.ort2;
			}
			// Projekt dazu
			let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
			if (typeof projekt != 'undefined') {
				data.prjname = projekt.name;
				data.prjnummer = projekt.prjnummer;
				data.prjbeschreibung = typeof projekt.beschreibung != 'undefined' ? projekt.beschreibung : "";
				data.bauleiter = projekt.bauleiter;
				data.bautraeger = projekt.bautraeger;

				data.bank = projekt.bank;
				data.bic = projekt.bic;
				data.ibana = projekt.iban;
				data.ibane = projekt.ibanE;
			}
		}
		// Positionen dazu
		positionen = DM.positionenDM.filter(el => parseInt(el.vertrag) == parseInt(anzeige.vertragid));
		if (typeof positionen != 'undefined') {
			cLen = positionen.length;
			data.alleraten = new Array(cLen);
			data.totaloffen = 0;
			data.totalbetrag = 0;
			data.totalbezahlt = 0;
			data.totalgestellt = 0;
			for (i = 0; i < cLen; i++) {
				let rate = new Object();
				data.alleraten[i] = rate;
				let rechzupos = rechnungen.filter(re => re.id == positionen[i].rechnung);
				rate.text = positionen[i].text;
				rate.betrag = formatBetrag(positionen[i].betrag);
				data.totalbetrag += positionen[i].betrag;
				rate.anteil = formatProzent(positionen[i].anteil / 100);
				rate.offenerbetrag = formatBetrag(positionen[i].offenerbetrag);
				data.totaloffen += positionen[i].offenerbetrag;
				rate.bezahlt = formatBetrag(positionen[i].bezahlt);
				data.totalbezahlt += positionen[i].bezahlt;
				rate.gestellt = formatBetrag(positionen[i].verschickt);
				data.totalgestellt += positionen[i].verschickt;

				if (rechzupos.length > 0) {
					rate.rechnr = rechzupos[0].rechnr;
				} else {
					rate.rechnr = "";
				}
			}
			data.totaloffen = formatBetrag(data.totaloffen);
			data.totalbetrag = formatBetrag(data.totalbetrag);
			data.totalbezahlt = formatBetrag(data.totalbezahlt);
			data.totalgestellt = formatBetrag(data.totalgestellt);
		}
	}
	if (typeof rechnung != 'undefined') {
		data.rechnr = rechnung.rechnr;
		data.rechdat = rechnung.rechdat;
		data.betreff = rechnung.betreff;
		data.faelligdat = rechnung.faelligdat;
		data.kostenstelle = rechnung.kostenstelle;
		data.objektnr = rechnung.objektnr;
		data.txtprj = rechnung.txtprj;
		data.txtbeschreibung = rechnung.txtbeschreibung;
		data.txtobj = rechnung.txtobj;
		data.txtkunde = rechnung.txtkunde;
		data.m1dat = rechnung.m1dat;
		data.m2dat = rechnung.m2dat;
		data.m3dat = rechnung.m3dat;
		data.mahnstufe = rechnung.mahnstufe;


		if (typeof rechnung.objektnr == 'undefined' || rechnung.objektnr == "") {
			data.objektnr = data.prjnummer + "-" + data.objnummer;
		}
		//console.log(rechnung.objektnr, data.objektnr, data.prjnummer + "-" + data.objnummer);
		data.leistungszeitraum = typeof rechnung.leistungszeitraum != 'undefined' ? rechnung.leistungszeitraum : "";
		data.betrag = formatBetrag(rechnung.betrag);
		data.netto = formatBetrag(rechnung.netto);
		data.mwst = formatBetrag(rechnung.betrag - rechnung.netto);
		// Positionen
		positionen = DM.positionenDM.filter(el => parseInt(el.rechnung) == parseInt(anzeige.rechnungid));
		if (typeof positionen != 'undefined') {
			positionen.sort((a, b) => {
				let n = parseInt(a.rechpos);
				let m = parseInt(b.rechpos);
				return (n < m) ? -1 : (n > m) ? 1 : 0;
			});
			let cLen = positionen.length;
			data.positionen = new Array(cLen);
			for (i = 0; i < cLen; i++) {
				let datapos = new Object();
				data.positionen[i] = datapos;
				datapos.rechpos = positionen[i].rechpos;
				datapos.text = positionen[i].text;
				datapos.mwst = typeof positionen[i].mwst != 'undefined' ? formatProzent(positionen[i].mwst / 100) : " ";
				datapos.netto = formatBetrag(positionen[i].netto);
				datapos.betrag = formatBetrag(positionen[i].betrag);
				gesamtBetrag = parseFloat(positionen[i].betrag) / parseFloat(positionen[i].anteil) * 100;
				datapos.anteil = typeof positionen[i].anteil != 'undefined' ? formatProzent(positionen[i].anteil / 100) : "";
				datapos.faktor = typeof positionen[i].faktor != 'undefined' ? formatNumber(positionen[i].faktor) : "";
				datapos.posbetrag = formatBetrag(positionen[i].bmgBetrag);
				datapos.bmgbetrag = formatBetrag(positionen[i].bmgBetrag);
			}
		}

	}
	return data;
}
function updateBtnVorlagen(prjid) {
	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(prjid));
	//console.log("updateBtnVorlagen", projekt);
	if (typeof projekt[CONSTANTS.TEMPLATER0] == 'undefined') {
		ById('btnvorlage1').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage1').disabled = true;
	} else {
		// ById('btnvorlage1').disabled = false;
		ById('btnvorlage1').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER0]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATER1] == 'undefined') {
		ById('btnvorlage2').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage2').disabled = true;
	} else {
		// ById('btnvorlage2').disabled = false;
		ById('btnvorlage2').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER1]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATER2] == 'undefined') {
		ById('btnvorlage3').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage3').disabled = true;
	} else {
		// ById('btnvorlage3').disabled = false;
		ById('btnvorlage3').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER2]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATER3] == 'undefined') {
		ById('btnvorlageR3').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage2').disabled = true;
	} else {
		// ById('btnvorlage2').disabled = false;
		ById('btnvorlageR3').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER3]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATER4] == 'undefined') {
		ById('btnvorlageR4').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage2').disabled = true;
	} else {
		// ById('btnvorlage2').disabled = false;
		ById('btnvorlageR4').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER4]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATER5] == 'undefined') {
		ById('btnRechAllg').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlage2').disabled = true;
	} else {
		// ById('btnvorlage2').disabled = false;
		ById('btnRechAllg').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATER5]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATEV1] == 'undefined') {
		ById('btnvorlageV1').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlageV1').disabled = true;
	} else {
		// ById('btnvorlageV1').disabled = false;
		ById('btnvorlageV1').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATEV1]);
	}
	if (typeof projekt[CONSTANTS.TEMPLATES1] == 'undefined') {
		ById('btnvorlageS1').setAttribute("data-tooltip", "Keine Vorlage ausgewählt! Bitte eine Vorlage auswählen!");
		// ById('btnvorlageS1').disabled = true;
	} else {
		// ById('btnvorlageS1').disabled = false;
		ById('btnvorlageS1').setAttribute("data-tooltip", "Vorlage: " + projekt[CONSTANTS.TEMPLATES1]);
	}

	// ById('btnvorlage1').innerHTML = DM.template.templateR0;
	// ById('btnvorlage2').innerHTML = DM.template.templateR1;
	// ById('btnvorlage3').innerHTML = DM.template.templateR2;
	// ById('btnvorlageV1').innerHTML = DM.template.templateV1;
	// ById('btnvorlageS1').innerHTML = DM.template.templateS1;
}
function btnVorlageAction(rechnungid, template) {

	let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(rechnungid));
	let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(vertrag.projekt));
	let data = collectdata(vertrag, rechnung);
	data.typ = template;
	generiereBrief(projekt[template], data);

}
function checkFileExists(file) {
	if (!fs.existsSync(file)) {
		let options = {
			buttons: ["Ok"],
			message: "Vorlage nicht vorhanden!",
			title: "Fehler",
			detail: "Die Vorlage für diesen Brief ist nicht vorhanden.\n Bitte mit Rechtsklick auf den Button eine Vorlage auswählen.",
			icon: path.join(__dirname, 'assets/icons/png/bandage.png'),
			type: "error"
		};
		dialog.showMessageBoxSync(getCurrentWindow(), options);
		return 1;
	}
	return 0;
}
function showRechnung(idRech) {
	let str;
	//	console.log(idRech);
	// Objekt holen
	if (typeof idRech === 'undefined') {
		i = anzeige.rechnungid;
	}
	if (idRech == -1) {
		return;
	}
	objrech = DM.rechnungenDM.find(re => re.id == idRech);
	let vertrag = DM.vertragDM.find(el => el.id == objrech.vertrag);
	let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(vertrag.objekt));
	let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
	let projekt = DM.projekteDM.find(el => el.id == vertrag.projekt && el.status != CONSTANTS.TRASH);

	//console.log(objrech);
	// ById('rech-box').classList.remove('is-hidden');
	anzeige.rechnungid = objrech.id;
	ById('rech-id').value = anzeige.rechnungid;
	ById('ver-id').value = objrech.vertrag;
	anzeige.vertragid = objrech.vertrag;
	anzeige.einheitid = vertrag.objekt;
	anzeige.projektid = vertrag.projekt;
	anzeige.show = "rechnung";
	ipcRenderer.send('display:save', anzeige);
	//console.log(anzeige);

	updateBtnVorlagen(vertrag.projekt);

	ById('btnvorlage3').innerHTML = "Brief";
	ById('rech-offen').classList.remove('is-hidden');
	ById('rech-faelligdat-label').innerHTML = "Fällig am";
	ById('btnZEadd').innerHTML = "Zahlung erfassen";
	ById('butRecEditPos').innerHTML = "Positionen anlegen";
	ById('modalRechnungChangeTitle').innerHTML = "Rechnung bearbeiten";
	ById('labRecChangeReNr').innerHTML = "Re-Nr.";
	ById('labRecChangeReDat').innerHTML = "Re-Datum";
	ById('titleRePos').innerHTML = "Rechnungspositionen";
	ById('fldRecLeistungszeitrum').classList.add('is-hidden');
	ById('fldbtnExportRechnung').classList.add('is-hidden');
	ById('fldbtnExportMehrkosten').classList.add('is-hidden');
	ById('fldbtnExportGutschrift').classList.add('is-hidden');
	ById('fldbtnRechAllg').classList.add('is-hidden');
	ById('fldbtnExportSchlussrechnung').classList.add('is-hidden');
	ById('recBtnsStandard').classList.remove('is-hidden');
	ById('recBtns').classList.add('is-hidden');

	if (roleedit) {
		ById('btnrechEdit').classList.remove('is-hidden');
	} else {
		ById('btnrechEdit').classList.add('is-hidden');
	}
	if (rolecomment) {
		ById('btnrechNotiz').classList.remove('is-hidden');
	} else {
		ById('btnrechNotiz').classList.add('is-hidden');
	}
	if (anzeige.menu == "RNBENTWURF" ||
		anzeige.menu == "RNBOPL" ||
		anzeige.menu == "RNBNEU" ||
		anzeige.menu == "RNBHISTRECH" ||
		anzeige.menu == "RNBALLRECH" ||
		anzeige.menu == "arbeitskorb") {
		ById('btnrechmenuback').classList.remove('is-hidden');
	} else {
		ById('btnrechmenuback').classList.add('is-hidden');
	}

	if (objrech.status == CONSTANTS.ENTWURF && roleedit) {
		ById('btnrechDelete').classList.remove('is-hidden');
	} else {
		ById('btnrechDelete').classList.add('is-hidden');
	}

	let lblReEmpf = "Rechungsempfänger";
	let lblReNr = "Rechnungsnummer";
	let lblReDat = "Rechnungsdatum";
	let lblReBet = "Rechnungsbetrag";
	switch (vertrag.typ) {
		case CONSTANTS.KAUFVERTRAG:
			ById('fldbtnExportRechnung').classList.remove('is-hidden');
			ById('butRecEditPos').innerHTML = "Positionen zuordnen";
			break;
		case CONSTANTS.MEHRKOSTEN:
			ById('fldbtnExportMehrkosten').classList.remove('is-hidden');
			ById('butRecEditPos').innerHTML = "Positionen zuordnen";
			break
		case CONSTANTS.GUTSCHRIFT:
			ById('fldbtnExportGutschrift').classList.remove('is-hidden');
			ById('btnZEadd').innerHTML = "Gutschrift erfassen";
			ById('modalRechnungChangeTitle').innerHTML = "Gutschrift bearbeiten";
			ById('labRecChangeReNr').innerHTML = "Gutschrift-Nr.";
			ById('labRecChangeReDat').innerHTML = "Gutschrift-Datum";
			ById('titleRePos').innerHTML = "Gutschrift-Einzelpositionen";
			ById('butRecEditPos').innerHTML = "Positionen zuordnen";

			lblReEmpf = "Gutschriftsempfänger ";
			lblReNr = "Gutschriftsnummer";
			lblReDat = "Gutschriftsdatum";
			lblReBet = "Gutschriftsbetrag";

			break
		case CONSTANTS.RESERVIERUNG:
			ById('rech-offen').classList.add('is-hidden');
			break;
		case CONSTANTS.ARCHVERTRAG:
			//ById('rech-faelligdat-label').innerHTML = "Leistungszeitraum";
			ById('fldRecLeistungszeitrum').classList.remove('is-hidden');
			ById('btnvorlage3').innerHTML = "Rechnung";
			break;
		case CONSTANTS.VERTRAGALLG:
			ById('fldbtnRechAllg').classList.remove('is-hidden');
			//console.log(vertrag,objrech);
			ById('RnbEditPrj').classList.add("is-hidden");
			ById('RnbEditObj').classList.add("is-hidden");
			ById('RnbEditKndObj').classList.add("is-hidden");

			// Vorbelegung des Edit Modals
			let kaeufer = "";
			dbRnbEditPrjContent();
			if (typeof projekt != "undefined") {
				if (projekt.id != 0) {
					ById('dpRnbEditPrj-button').innerHTML = projekt.prjnummer + " " + projekt.name;
				} else {
					ById('dpRnbEditPrj-button').innerHTML = "Kein Projekt ausgewählt"
				}
				ById('dpRnbEditPrj-id').value = vertrag.projekt;
				if (typeof objekt != "undefined") {
					ById('dpRnbEditObj-button').innerHTML = objekt.text + " " + objekt.objnummer;
					kaeufer = DM.kundenDM.find(el => parseInt(el.id) == parseInt(objekt.kunde));
					ById('dpRnbEditObj-id').value = vertrag.objekt;
					if (typeof kaeufer != "undefined") {
						ById('indpRnbEditKndObj').value = kaeufer.vorname + " " + kaeufer.name;
						ById('dpRnbEditKndObj-id').value = objekt.kunde;
					} else {
						ById('indpRnbEditKndObj').value = "";
						ById('dpRnbEditKndObj-id').value = 0;
					}

				} else {
					ById('dpRnbEditObj-button').innerHTML = "zuerst Projekt wählen";
					ById('dpRnbEditObj-id').value = 0;
				}
			} else {
				ById('dpRnbEditPrj-button').innerHTML = "";
				ById('dpRnbEditPrj-id').value = 0;
			}


			if (typeof kunde != "undefined") {
				ById('indpRnbEditKnd').value = kunde.vorname + " " + kunde.name;
				ById('dpRnbEditKnd-id').value = vertrag.kunde;
			} else {
				ById('indpRnbEditKnd').value = "";
				ById('dpRnbEditKnd-id').value = 0;
			}
			ById('recBtnsStandard').classList.add('is-hidden');
			ById('recBtns').classList.remove('is-hidden');
			ById('RnbEditRechdat').value = checkValue(objrech.rechdat);
			ById('RnbEditRechNr').value = checkValue(objrech.rechnr);
			ById('RnbEditFaelligdat').value = checkValue(objrech.faelligdat);
			ById('RnbEditM1dat').value = checkValue(objrech.m1dat);
			ById('RnbEditM2dat').value = checkValue(objrech.m2dat);
			ById('RnbEditM3dat').value = checkValue(objrech.m3dat);
			ById('RnbEditMahnstufe').value = checkValue(objrech.mahnstufe);
			ById('RnbEditleistungszeitraum').value = typeof objrech.leistungszeitraum != 'undefined' ? objrech.leistungszeitraum : "";
			ById('RnbEditrechtyp').value = typeof objrech.rechtyp != 'undefined' ? objrech.rechtyp : "";
			ById('RnbEditobjektnr').value = typeof objrech.objektnr != 'undefined' ? objrech.objektnr : "";
			ById('RnbEditkostenstelle').value = typeof objrech.kostenstelle != 'undefined' ? objrech.kostenstelle : "";
			ById('RnbEditprojektfrei').value = typeof objrech.txtprj != 'undefined' ? objrech.txtprj : "";
			ById('RnbEditbeschreibungfrei').value = typeof objrech.txtbeschreibung != 'undefined' ? objrech.txtbeschreibung : "";
			ById('RnbEditkunde').value = typeof objrech.txtkunde != 'undefined' ? objrech.txtkunde : "";
			ById('RnbEditobjektfrei').value = typeof objrech.txtobj != 'undefined' ? objrech.txtobj : "";
			ById('modRnbEditBezug').value = objrech.bezug;
			//console.log(objrech.vorlagen);
			ById('recBtns').innerHTML = "";


			if (typeof objrech.vorlagenrech == 'undefined' | (typeof objrech.vorlagenrech !== 'undefined' && objrech.vorlagenrech.length == 0)) {
				vorlagen = new Array;
				dummy = new Object();
				dummy.text = "Nicht belegt";
				dummy.pfad = "Keine Vorlage ausgewählt";
				vorlagen.push(dummy);
				objrech.vorlagenrech = vorlagen;
			}
			objrech.vorlagenrech.forEach(vorlage => {
				div = document.createElement("div")
				div.classList.add("field");
				div.classList.add("is-grouped");
				ById('recBtns').appendChild(div);

				ctrl = document.createElement("div")
				ctrl.classList.add("control");
				div.appendChild(ctrl);

				btn = document.createElement("button")
				btn.classList.add("docvorlage");
				btn.classList.add("button");
				btn.classList.add("is-outlined");
				btn.classList.add("is-multiline");
				btn.setAttribute("style", 'width: 12em;');
				btn.innerHTML = vorlage.text;
				btn.setAttribute("data-tooltip", "Vorlage: " + vorlage.pfad);

				ctrl.appendChild(btn);
				btn.addEventListener('click', function (event) {
					//console.log(event.target);
					let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(anzeige.rechnungid));
					let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(rechnung.vertrag));
					let vorlage = rechnung.vorlagenrech.find(el => el.text == event.target.innerHTML);
					let pf = "";

					if (vorlage.pfad.indexOf('.') == 0 && typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
						pf = path.join(process.env.PORTABLE_EXECUTABLE_FILE, vorlage.pfad);

					} else {
						pf = vorlage.pfad;
					}

					let data = collectdata(vertrag, rechnung);
					generiereBrief(pf, data);
				});




			})

			break;
		default:
	}
	ById('rechnung-comments').classList.add('is-hidden');
	ById('rechnung-comments').innerHTML = '';

	str = "";
	let rechname = "";
	let rechtitle = "";
	if (typeof kunde !== 'undefined') {
		rechname = kunde.vorname + " " + kunde.name;
		rechtitle = rechname;

		if (kunde.lieferant == false) {
			// Kunde ist keine Firma, der zweite Name wird mit -UND- angehängt

			if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
				(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
				rechname += " und " + kunde.vorname2 + " " + kunde.name2;
			}

		} else {
			// Kunde ist eine Firma, der zweite Name ist der Ansprechpartner

			if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
				(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
				rechname += " <br>Ansprechpartner: " + kunde.vorname2 + " " + kunde.name2;
				rechtitle += ": " + kunde.vorname2 + " " + kunde.name2;
			}


		}

	} else {
		rechname = "Kein Rechnungsempfänger erfasst";
		rechtitle = rechname;
	}

	ById('rechnung-title').innerHTML = rechtitle;

	showStatus(objrech.status);

	str = "";
	str += "<table class='table is-narrow'>";
	str += "<tr>";
	str += "<td><strong>" + lblReEmpf + "</strong></td><td>" + rechname + "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<td><strong>" + lblReNr + "</strong></td><td>" + setValueofId('rech-rechnr', objrech.rechnr) + "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<td><strong>" + lblReDat + "</strong></td><td>" + setValueofId('rech-rechdat', objrech.rechdat) + "</td>";
	str += "</tr>";
	if (vertrag.typ == CONSTANTS.ARCHVERTRAG) {
		str += "<tr>";
		str += "<td><strong>Leistungszeitraum</strong></td><td>" + setValueofId('rech-leistungszeitraum', objrech.leistungszeitraum) + "</td>";
		str += "</tr>";
	}
	str += "<tr>";

	str += "<td><strong>Fälligkeitsdatum</strong></td><td>" + setValueofId('rech-faelligdat', objrech.faelligdat) + "</td>";

	str += "</tr>";
	str += "<tr>";
	str += "<td><strong>" + lblReBet + "</strong></td><td>" + setValueofId('rech-betrag', formatBetrag(strdef(objrech.betrag))) + "</td>";
	str += "</tr>";

	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		let txtobjekt = "";
		let txtkaeufer = "nicht erfasst";
		let txtprojekt = "";
		let txtleistungszeitraum = typeof objrech.leistungszeitraum != 'undefined' ? objrech.leistungszeitraum : "";
		let txtrechtyp = typeof objrech.rechtyp != 'undefined' ? objrech.rechtyp : "";
		let txtkostenstelle = typeof objrech.kostenstelle != 'undefined' ? objrech.kostenstelle : "";
		let txtprj = typeof objrech.txtprj != 'undefined' ? objrech.txtprj : "";
		let txtbeschreibung = typeof objrech.txtbeschreibung != 'undefined' ? objrech.txtbeschreibung : "";
		let txtkunde = typeof objrech.txtkunde != 'undefined' ? objrech.txtkunde : "";
		let txtobj = typeof objrech.txtobj != 'undefined' ? objrech.txtobj : "";
		let txtobjnr = typeof objrech.objektnr != 'undefined' ? objrech.objektnr : "";

		let txtm1dat = typeof objrech.m1dat != 'undefined' ? objrech.m1dat : "";
		let txtm2dat = typeof objrech.m2dat != 'undefined' ? objrech.m2dat : "";
		let txtm3dat = typeof objrech.m3dat != 'undefined' ? objrech.m3dat : "";
		let txtmahnstufe = typeof objrech.mahnstufe != 'undefined' ? objrech.mahnstufe : "";

		// if (typeof objekt != "undefined") {
		// 	txtobjekt = objekt.text + " " + objekt.objnummer;
		// 	let kaeufer = DM.kundenDM.find(el => parseInt(el.id) == parseInt(objekt.kunde));

		// 	if (typeof kaeufer != 'undefined') {
		// 		txtkaeufer = kaeufer.vorname + " " + kaeufer.name;
		// 		if (typeof kaeufer.name2 != 'undefined' && kaeufer.name2 != "") {
		// 			txtkaeufer += " & " + kaeufer.vorname2 + " " + kaeufer.name2
		// 		}
		// 	}

		// 	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
		// 	if (typeof projekt != "undefined") {
		// 		txtprojekt = projekt.name;
		// 		txtbeschreibung = typeof projekt.beschreibung != 'undefined' ? projekt.beschreibung : "";
		// 	}
		// }
		if (typeof objekt != 'undefined') {
			// str += "<tr>";
			// str += "<td><strong>Projekt</strong></td><td>" + txtprojekt + " " + txtbeschreibung + "</td>";
			// str += "</tr>";
			// str += "<tr>";
			// str += "<td><strong>Einheit</strong></td><td>" + txtobjekt + "</td>";
			// str += "</tr>";
			// str += "<tr>";
			str += "<td><strong>Käufer/ Mieter</strong></td><td>" + txtkaeufer + "</td>";
			str += "</tr>";
			str += "<tr>";
			let strpreis = objekt.kaufpreis != 0 ? formatBetrag(objekt.kaufpreis) : "nicht erfasst";
			str += "<td><strong>Preis</strong></td><td>" + strpreis + "</td>";
			str += "</tr>";
		}
		str += "<tr>";
		str += "<td><strong>Leistungszeitraum</strong></td><td>" + txtleistungszeitraum + "</td>";
		str += "</tr>";

		if (txtm1dat != '') {
			str += "<tr>";
			str += "<td><strong>Zahlungserinnerung</strong></td><td>" + txtm1dat + "</td>";
			str += "</tr>";
		}
		if (txtm2dat != '') {
			str += "<tr>";
			str += "<td><strong>Datum 1. Mahnung</strong></td><td>" + txtm2dat + "</td>";
			str += "</tr>";
		}
		if (txtm3dat != '') {
			str += "<tr>";
			str += "<td><strong>Datum 2. Mahnung</strong></td><td>" + txtm3dat + "</td>";
			str += "</tr>";
		}
		if (txtmahnstufe != '') {
			str += "<tr>";
			str += "<td><strong>Mahnstufe</strong></td><td>" + txtmahnstufe + "</td>";
			str += "</tr>";
		}

		str += "<tr>";
		str += "<td><strong>Rechnungstyp</strong></td><td>" + txtrechtyp + "</td>";
		str += "</tr>";

		str += "<tr>";
		str += "<td><strong>Kostenstelle</strong></td><td>" + txtkostenstelle + "</td>";
		str += "</tr>";

		str += "<tr>";
		str += "<td><strong>Projekt</strong></td><td>" + txtprj + "</td>";
		str += "</tr>";

		str += "<tr>";
		str += "<td><strong>Projektnummer</strong></td><td>" + txtobjnr + "</td>";
		str += "</tr>";

		str += "<tr>";
		str += "<td><strong>Projektanschrift</strong></td><td>" + txtbeschreibung + "</td>";
		str += "</tr>";


		str += "<tr>";
		str += "<td><strong>Objekt/Einheit</strong></td><td>" + txtobj + "</td>";
		str += "</tr>";






		str += "<tr>";
		str += "<td><strong>Käufer/Mieter</strong></td><td>" + txtkunde + "</td>";
		str += "</tr>";

	}

	str += "</table>";
	ById('txtRecPos1').innerHTML = str;
	//
	// Zahlungsvorgänge
	//
	if (objrech.status !== CONSTANTS.ENTWURF) {
		str = "";
		let strtmp = "";
		let summe = 0;
		//let zeDM = datenmodell.get('Zahlungseingaenge');
		//console.log(DM.zeDM);
		if (typeof DM.zeDM !== 'undefined' && DM.zeDM !== 'undefined') {
			let ze = DM.zeDM.filter(el => parseInt(el.rechnung) == parseInt(objrech.id));
			clen = ze.length;
			ze.sort((a, b) => {
				let n = string2Date(a.datum).valueOf();
				let m = string2Date(b.datum).valueOf();
				return (n < m) ? -1 : (n > m) ? 1 : 0;
			});


			if (clen > 0) {
				//strtmp += "<table class='table is-bordered is-narrow'>"
				for (i = 0; i < ze.length; i++) {
					strtmp += "<tr>";
					strtmp += "<td class='has-text-left referenz' refnr=" + ze[i].id + " style='width: 10em;'>" + ze[i].datum + "</td>";
					strtmp += "<td class='has-text-right' style='width: 10em;'>" + formatBetrag(ze[i].betrag) + "</td>";
					strtmp += "<td class='has-text-left' style='width: 34em;'>" + ze[i].text + "</td>";
					summe += ze[i].betrag;
					strtmp += "</tr>";
				}
				strtmp += "<tr>";
				strtmp += "<td><strong>Summe</strong></td><td class='has-text-right'> <strong>" + formatBetrag(summe) + "</strong></td><td></td>";
				strtmp += "</tr>";

				// strtmp += "</table>"
			}
		}
		// str += "<p><strong>Zahlungsvorgänge</strong> </p>";
		str += strtmp;
		let offen = objrech.betrag.toFixed(2) - summe.toFixed(2);
		ById('rech-offen').innerHTML = "Offener Betrag: " + formatBetrag(offen);
		ById('txtRecPos3').innerHTML = str;
		ById('sectionZE').classList.remove('is-hidden');
		ById('butRecEditPos').classList.add('is-hidden');
		if (roleedit) {
			ById('btnZEadd').classList.remove('is-hidden');
		} else {
			ById('btnZEadd').classList.add('is-hidden');

		}

		if (objrech.status == CONSTANTS.BEZAHLT) {
			ById('btnZEadd').classList.add('is-hidden');
		} else {
			if (roleedit) {
				ById('btnZEadd').classList.remove('is-hidden');
			} else {
				ById('btnZEadd').classList.add('is-hidden');
			}
		}


	} else {
		if (roleedit) {
			ById('butRecEditPos').classList.remove('is-hidden');
		} else {
			ById('butRecEditPos').classList.add('is-hidden');

		}
		ById('rech-offen').innerHTML = "";
		ById('txtRecPos3').innerHTML = "";
		ById('sectionZE').classList.add('is-hidden');
		ById('btnZEadd').classList.add('is-hidden');
	}
	//
	// Rechungspositionen
	//
	// Alle Rechnungspositionen holen
	//let loaddata = datenmodell.get('Positionen');
	positionen = DM.positionenDM.filter(el => parseInt(el.rechnung) == parseInt(anzeige.rechnungid));
	positionen.sort((a, b) => {
		let n = parseInt(a.rechpos);
		let m = parseInt(b.rechpos);
		return (n < m) ? -1 : (n > m) ? 1 : 0;
	});
	let strh = "";
	strh += "<th class='has-text-right'>Pos</th>";
	strh += "<th class='has-text-left'>Beschreibung</th>";
	strh += "<th class='has-text-right'>Netto</th>";
	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		strh += "<th class='has-text-right'>Faktor</th>";
	}
	strh += "<th class='has-text-right'>Anteil</th>";
	strh += "<th class='has-text-right'>Netto Gesamt</th>";
	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		strh += "<th class='has-text-right'>MwSt</th>";
		strh += "<th class='has-text-right'>Rechnungsbetrag</th>";
	}
	ById('rec-pos-tablehead').innerHTML = strh;
	let strf = "";
	strf += "<td> </td>";
	strf += "<td><strong>Summe</strong></td>";
	strf += "<td> </td>";
	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		strf += "<td> </td>";
	}
	strf += "<td> </td>";
	strf += "<td class='has-text-right' id='recpostablefootNetto'></td>";
	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		strf += "<td> </td>";
		strf += "<td class='has-text-right' id='recpostablefootBetrag'></td>";
	}
	ById('rec-pos-tablefoot').innerHTML = strf;


	ById('rec-pos-tablebody').innerHTML = "";
	let gesamtnetto = 0;
	let cLen = positionen.length;
	for (i = 0; i < cLen; i++) {
		let prozentAnteil;
		let netto;
		let mwst;
		let betrag;
		let bmgBetrag = "";
		let faktor = "";

		zeile = document.createElement('tr');


		let str = "";
		if (typeof positionen[i].rechpos != "undefined") {
			str += "<td class='has-text-right referenz' refnr='" + positionen[i].id + "' style='width: 4em;'>" + positionen[i].rechpos + "</td>";
		} else {
			str += "<td> </td>";
		}
		if (typeof positionen[i].text != "undefined") {

			let lines = positionen[i].text.split("\n");
			let postext = lines.join("<br>");

			str += "<td class='has-text-left' refnr='" + positionen[i].id + "'  style='width: 32em;'>" + postext + "</td>";
		} else {
			str += "<td></td>";
		}
		betrag = formatBetrag(positionen[i].betrag);
		mwst = formatProzent(typeof positionen[i].mwst != 'undefined' ? positionen[i].mwst / 100 : 0);
		netto = formatBetrag(typeof positionen[i].netto != 'undefined' ? positionen[i].netto : positionen[i].betrag / (1 + parseFloat(mwst)));
		gesamtnetto = gesamtnetto + convPreisField(netto);
		bmgBetrag = checkValue(formatBetrag(positionen[i].bmgBetrag));
		prozentAnteil = checkValue(formatProzent(positionen[i].anteil / 100));
		faktor = formatNumber(checkValue(positionen[i].faktor));

		str += "<td class='has-text-right' style='width: 12em;'>" + bmgBetrag + "</td>";
		if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
			str += "<td class='has-text-right' style='width: 4em;'>" + faktor + "</td>";
		}
		str += "<td class='has-text-right' style='width: 4em;'>" + prozentAnteil + "</td>";


		str += "<td class='has-text-right' style='width: 12em;'>" + netto + "</td>";
		if (vertrag.typ == CONSTANTS.VERTRAGALLG) {

			str += "<td class='has-text-right' style='width: 12em;'>" + mwst + "" + "</td>";
			str += "<td class='has-text-right' style='width: 12em;'>" + betrag + "</td>";
		}
		zeile.innerHTML = str;
		ById('rec-pos-tablebody').appendChild(zeile);

	}
	ById('recpostablefootNetto').innerHTML = "<strong>" + formatBetrag(gesamtnetto) + "</strong>";
	if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
		ById('recpostablefootBetrag').innerHTML = "<strong>" + formatBetrag(objrech.betrag) + "</strong>";
	}

	// Kommentare ------------------------------
	if (anzeige.show == "rechnung") {
		ById('paneKommentare').classList.add('is-hidden');
		let comments = DM.commentDM.filter(el => el.modus == anzeige.show && el.refid == anzeige.rechnungid &&
			(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
		fillComments(comments);
	}


}
function editRechnung() {
	ById('modalRechnungChange').classList.add('is-active');
	ById('rech-id').value = anzeige.rechnungid;
	let rechnung = DM.rechnungenDM.find(el => el.id == anzeige.rechnungid);
	addElementToRechnungsPos(rechnung.vertrag);

}
function saveRechnung() {
	ById('modalRechnungChange').classList.remove('is-active');
	let rechnung = new Object();
	if (ById('rech-rechnr').value != "") { rechnung.rechnr = ById('rech-rechnr').value; }
	if (ById('rech-rechdat').value != "") { rechnung.rechdat = ById('rech-rechdat').value; }
	rechnung.faelligdat = ById('rech-faelligdat').value;
	rechnung.leistungszeitraum = ById('rech-leistungszeitraum').value;
	if (ById('dpRechStatus-id').value != "") { rechnung.status = ById('dpRechStatus-id').value; }
	rechnung.id = ById('rech-id').value;
	//rechnung.vertrag = ById('dpRechVer-id').value;
	//loaddata = datenmodell.get('Positionen');
	positionen = DM.positionenDM.filter(el => el.rechnung == rechnung.id);
	cLen = positionen.length;
	let betrag = 0;
	for (i = 0; i < cLen; i++) {
		betrag += parseFloat(positionen[i].betrag);
	}
	rechnung.betrag = betrag;
	//if (ById('rech-betrag').value != "" ) { rechnung.betrag = convPreisField(ById('rech-betrag').value);}
	ipcRenderer.send('rechnung:change', rechnung);
}
function rechFieldsReadonly() {
	ById('rech-name').setAttribute('readonly', 'true');
	ById('rech-vorname').setAttribute('readonly', 'true');
	ById('rech-strasse').setAttribute('readonly', 'true');
	ById('rech-plz').setAttribute('readonly', 'true');
	ById('rech-ort').setAttribute('readonly', 'true');
	ById('rech-telefon').setAttribute('readonly', 'true');
	ById('rech-email').setAttribute('readonly', 'true');
	ById('rech-betrag').setAttribute('readonly', 'true');
	ById('rech-rechnr').setAttribute('readonly', 'true');
	ById('rech-rechdat').setAttribute('readonly', 'true');
	ById('rech-faelligdat').setAttribute('readonly', 'true');
	ById('rech-betreff').setAttribute('readonly', 'true');
}
function rechFieldsEditable() {
	ById('rech-name').removeAttribute('readonly');
	ById('rech-vorname').removeAttribute('readonly');
	ById('rech-strasse').removeAttribute('readonly');
	ById('rech-plz').removeAttribute('readonly');
	ById('rech-ort').removeAttribute('readonly');
	ById('rech-telefon').removeAttribute('readonly');
	ById('rech-email').removeAttribute('readonly');
	ById('rech-betrag').removeAttribute('readonly');
	ById('rech-rechnr').removeAttribute('readonly');
	ById('rech-rechdat').removeAttribute('readonly');
	ById('rech-faelligdat').removeAttribute('readonly');
	ById('rech-betreff').removeAttribute('readonly');
}
function showStatus(status) {

	ById('dpRechStatus-id').value = status;
	ById('btnrechStatus').classList.remove('is-hidden')
	ById('btnrechStatus').classList.remove('is-info')
	ById('btnrechStatus').classList.remove('is-warning')
	ById('btnrechStatus').classList.remove('is-success')

	//console.log(status);
	switch (status) {
		case CONSTANTS.ENTWURF:
			ById('dpRechStatus-button').innerHTML = CONSTANTS.ENTWURF;
			ById('btnrechStatus').innerHTML = CONSTANTS.ENTWURF;
			ById('btnrechStatus').classList.add('is-info')
			break;
		case CONSTANTS.VERSCHICKT:
			ById('btnrechStatus').classList.add('is-warning')
			ById('btnrechStatus').innerHTML = CONSTANTS.VERSCHICKT;
			ById('dpRechStatus-button').innerHTML = CONSTANTS.VERSCHICKT;
			break;
		case CONSTANTS.BEZAHLT:
			ById('btnrechStatus').classList.add('is-success')
			ById('dpRechStatus-button').innerHTML = CONSTANTS.BEZAHLT;
			ById('btnrechStatus').innerHTML = CONSTANTS.BEZAHLT;
			break;
		case CONSTANTS.DELETE:
			ById('dpRechStatus-button').innerHTML = CONSTANTS.DELETE;
			ById('btnrechStatus').innerHTML = CONSTANTS.DELETE;
			break;
		default:
			ById('dpRechStatus-button').innerHTML = CONSTANTS.BITTEEINGEBEN;
			ById('btnrechStatus').innerHTML = CONSTANTS.BITTEEINGEBEN;

	}
}
function addElementToRechnungsPos(vertragid) {
	// Alle Positionen zu diesem Vertrag holen
	let strdis = "";
	//loaddata = datenmodell.get('Positionen');
	positionen = DM.positionenDM.filter(el => el.vertrag == vertragid);
	cLen = positionen.length;
	for (i = 0; i < cLen; i++) {
		let zeile = document.createElement('p');
		let str = "";
		// Ist das die aktuelle Rechnung??
		if (typeof positionen[i].rechnung !== 'undefined' 
			&& positionen[i].rechnung !== anzeige.rechnungid 
			&& positionen[i].rechnung !== ""
			&& positionen[i].rechnung !== 0) {
			strdis = "disabled";
		} else {
			strdis = "";

		}
		let id1 = "buttonRechPosAdd-" + positionen[i].id;
		let id2 = "buttonRechPosDel-" + positionen[i].id;

		//Spalte 1 - Rechnungsposition
		let idpos = "fieldRechPos-" + positionen[i].id;
		let postxt = "";
		if (typeof positionen[i].rechpos != "undefined") {
			if (positionen[i].rechnung == anzeige.rechnungid) {
				postxt = positionen[i].rechpos;
			} else {
				let rechnung = DM.rechnungenDM.filter(el => parseInt(el.id) == parseInt(positionen[i].rechnung));
				if (rechnung.length > 0) {
					postxt = rechnung[0].rechnr;
				}

			}
		}

		str += "<div class='field is-horizontal'>";
		str += "<div class='field'>" +
			"<p class='control'>" +
			"<input class='input' type='text'  id='" + idpos + "'" + strdis + " onpressenter='" + id1 + "'  value='" + postxt + "' placeholder='Pos.' style='width: 4em; margin-right:1em;'>" +
			"</p>" +
			"</div>";
		//Spalte 2 - Beschreibung
		let idtext = "fieldRechPosText-" + positionen[i].id;
		str += "<div class='field'>" +
			"<p class='control'>" +
			"<input class='input' type='text'" +
			"id='" + idtext + "' value='" + positionen[i].text + "' " + strdis + " onpressenter='" + id1 + "' placeholder='Beschreibung' style='width: 35em; margin-right:1em;' >" +
			"</p>" +
			"</div>";
		//Spalte 3 Anteil		
		if (typeof positionen[i].anteil != "undefined") {
			betrag = formatBetrag(positionen[i].betrag);
			gesamtBetrag = parseFloat(positionen[i].betrag) / parseFloat(positionen[i].anteil) * 100;
			beschreibung = formatProzent(positionen[i].anteil / 100);//  + "% von " + formatBetrag(gesamtBetrag);
			let idanteil = "fieldRechPosAnteil-" + positionen[i].id;
			str += "<div class='field has-addons'>" +
				"<p class='control'>" +
				"<input class='input' type='text'" +
				"id='" + idanteil + "' disabled value='" + beschreibung + "' placeholder='Beschreibung' style='width: 12em;'>" +
				"</p>" +
				"<p class='control'><a class='button is-static' style='margin-right:1em;'>%</a></p>" +
				"</div>";
		} else {
			str += "<p> </p>";
		}
		//Spalte 4 - Betrag
		str += "<div class='field'>" +
			"<p class='control'><input class='input'  disabled type='text' onpressenter='" + id1 + "' value='" + formatBetrag(positionen[i].betrag) + "' " + strdis + " placeholder='Betrag' style='width: 8em; margin-right:1em;'></p>" +
			"</div>";

		//Spalte 5 - Buttons	 

		str += "<div class='field is-grouped is-flex-grow-1'>" +
			"<div class='field-body'>";
		if (postxt == "") {
			str += "<button class='button is-info is-light' " + strdis + " id='" + id1 + "'>ADD</button>";
		} else {
			str += "<button class='button is-warning is-light'  " + strdis + " id='" + id2 + "'>DEL</button>";
		}
		str += "</div>";
		str += "</div>"; //Buttons	
		str += "</div>";
		zeile.innerHTML = str;
		ById('itabrecpos').appendChild(zeile);

		if (postxt == "") {
			ById(id1).addEventListener('click', function () {
				// "buttonRechPosAdd-"+positionen[i].id;	
				index = this.id.substring(17, this.id.length);
				//console.log(index, this.id);
				let position = new Object();
				position.id = index
				if (ById("fieldRechPos-" + index).value !== "") {
					//console.log("Feld: ", ById("fieldRechPos-" + index).value )
					position.rechpos = ById("fieldRechPos-" + index).value;
					position.rechnung = anzeige.rechnungid;

				}
				//console.log(position.recpos);
				position.text = ById("fieldRechPosText-" + index).value;
				ipcRenderer.send('position:rechnung:change', position);
			});
		} else {
			ById(id2).addEventListener('click', function () {
				// "buttonRechPosDel-"+positionen[i].id;	
				index = this.id.substring(17, this.id.length);
				//console.log(index, this.id);
				let position = new Object();
				position.id = index
				position.rechnung = "";
				position.rechpos = "";
				//console.log(index, this.id);
				ipcRenderer.send('position:rechnung:change', position);

			});
		}



	}

}
function editRechnungZE() {
	ById('tabrecZE').innerHTML = "";
	// Neuer Eintrag
	let neuerEintrag = {
		id: -1,
		datum: "",
		text: "",
		betrag: 0,
	}
	editRechnungZEEintrag(neuerEintrag, true, false);
	// Alle ZE zu dieser Rechnung holen
	//zeDM = datenmodell.get('Zahlungseingaenge');
	if (typeof DM.zeDM !== 'undefined' && DM.zeDM !== 'undefined') {
		ze = DM.zeDM.filter(el => el.rechnung == anzeige.rechnungid);
		cLen = ze.length;
		for (i = 0; i < cLen; i++) {
			editRechnungZEEintrag(ze[i], true, true);
		}
	}
}
function editRechnungZEEintrag(eintrag, add, del) {
	let zeile = document.createElement('p');
	let str = "";
	let btn1id = "buttonrezeadd-" + eintrag.id;
	let btn2id = "buttonrezedel-" + eintrag.id;

	//Spalte 1 - Datum
	let datid = "rezedat-" + eintrag.id;
	let dattxt = "";
	if (typeof eintrag.datum != "undefined") {
		dattxt = eintrag.datum;
	}
	str += "<div class='field is-horizontal'>";
	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' type='text'  id='" + datid + "' formater='date' onpressenter='" + btn1id + "' value='" + dattxt + "' placeholder='Datum' style='width: 8em; margin-right:1em;'>" +
		"</p>" +
		"</div>";

	//Spalte 2 - Betrag
	let betragid = "rezebetrag-" + eintrag.id;
	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' type='text'" +
		"id='" + betragid + "' value='" + formatBetrag(eintrag.betrag) + "' onpressenter='" + btn1id + "' placeholder='Betrag' style='width: 8em; margin-right:1em;' >" +
		"</p>" +
		"</div>";

	//Spalte 3 - Buttons	
	str += "<div class='field' style='width: 8em; margin-right:1em;'>" +
		"<div class='field-body'>";
	if (add == true) {
		str += "<button class='button is-info is-light' id='" + btn1id + "'>OK</button>";
	}
	if (del == true) {
		str += "<button class='button is-warning is-light'  id='" + btn2id + "'>DEL</button>";
	}
	str += "</div>";
	str += "</div>"; //Buttons	


	//Spalte 4 - Beschreibung
	let textid = "rezetext-" + eintrag.id;
	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' type='text'" +
		"id='" + textid + "' value='" + eintrag.text + "' placeholder='Beschreibung'  onpressenter='" + btn1id + "' style='width: 14em; margin-right:1em;' >" +
		"</p>" +
		"</div>";


	str += "</div>";
	zeile.innerHTML = str;
	ById('tabrecZE').appendChild(zeile);


	if (add == true) {
		ById(btn1id).addEventListener('click', function () {
			// "buttonrezeadd-"+.id;	
			index = this.id.substring(14, this.id.length);
			let ze = new Object();
			let datid = "rezedat-" + index;
			let textid = "rezetext-" + index;
			let betragid = "rezebetrag-" + index;
			//console.log(index, datid, textid, betragid);
			ze.id = index;
			ze.rechnung = ById('rech-id').value;
			ze.text = ById(textid).value;
			ze.datum = ById(datid).value;
			ze.betrag = convPreisField(ById(betragid).value);
			ipcRenderer.send('ze:change', ze);

			// Buchung erzeugen
			let rechnung = DM.rechnungenDM.find(el => el.id == ze.rechnung);
			let vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag);
			let buchung = new Object();
			buchung.type = "Buchung";
			buchung.bdat = new Date();
			buchung.vdat = string2Date(ById(datid).value);
			buchung.text = ze.text;
			buchung.pn = CONSTANTS.PNZAHLUNGSEINGANG;
			buchung.konto = parseInt(CONSTANTS.KKOOFFSET + vertrag.kunde);
			buchung.gegenkonto = CONSTANTS.HBKERTRAG;
			buchung.betrag = ze.betrag
			buchung.quelle = "Rechnung-" + ze.rechnung;
			ipcRenderer.send('edit:new', buchung);

		});
	}
	if (del == true) {
		ById(btn2id).addEventListener('click', function () {
			// "buttonPosDel-"+positionen[i].id;	
			index = this.id.substring(14, this.id.length);
			ipcRenderer.send('ze:delete', index);

		});
	}
}
function editRechnungPosten(vertragid) {
	ById('modalRechnungPostenChange').classList.add('is-active');
	ById('tabrecpos').innerHTML = "";
	ById('rech-id').value = anzeige.rechnungid;
	addElementToRechnungsPos(vertragid);
}
function showVertrag(idVertrag) {
	let str = "";
	let i = 0;
	let kunde;
	let erstattet = false;
	let erhalten = false;

	ById('btnDarStatusForward').classList.add('is-hidden');
	ById('btnDarStatusBack').classList.add('is-hidden');
	ById('butVerEditPos').classList.remove('is-hidden');
	ById('sectionverPos').classList.add('is-hidden');
	ById('btnVerChange').classList.add('is-hidden');
	ById('fldVerKun2').classList.add('is-hidden');
	if (typeof idVertrag === 'undefined') {
		return -1;
	}
	if (idVertrag == -1) {
		return;
	}
	ById('ver-box').classList.remove('is-hidden');

	let objver = DM.vertragDM.find(re => re.id == idVertrag);
	anzeige.vertragid = objver.id;
	anzeige.rechnungid = 0;
	ById('rech-id').value = 0;
	anzeige.menu = "";
	anzeige.einheitid = parseInt(objver.objekt);
	ById('ver-id').value = anzeige.vertragid;
	ipcRenderer.send('display:save', anzeige);
	let strtmp = "";
	let titlestr = "";
	// DP Kunde -----------------------------------
	if (typeof objver.kunde !== 'undefined' && objver.kunde != "") {
		kunde = DM.kundenDM.find(re => re.id == objver.kunde);
		if (typeof kunde != 'undefined') {
			strtmp = kunde.vorname + " " + kunde.name;
			titlestr = kunde.name;
			if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
				(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
				strtmp += " und " + kunde.vorname2 + " " + kunde.name2;
				titlestr += "/ " + kunde.name2;

			}
		} else {
			strtmp = "n.n.";

		}

		ById('inpVerKunAutolookup').value = strtmp;
		ById('dpVerKun-id').value = objver.kunde;
	} else {
		ById('inpVerKunAutolookup').value = "";
	}
	// DP Status -----------------------------------
	if (typeof objver.status !== 'undefined' && objver.status != "") {
		ById('dpVerStatus-button').innerHTML = objver.status;
		ById('dpVerStatus-id').value = objver.status;
	} else {
		ById('dpVerStatus-button').innerHTML = "Kein Status";
	}
	//-----------------------------------
	ById('dp-Ver-Pos-Template-objekt').classList.add('is-hidden');

	rechnungen = DM.rechnungenDM.filter(re => parseInt(re.vertrag) == parseInt(anzeige.vertragid));
	positionen = DM.positionenDM.filter(re => re.vertrag == anzeige.vertragid);
	let revstat;
	ById('btnArcVerExport').classList.add('is-hidden');
	ById('modalVertragChangeTitle').innerHTML = "Vertrag editieren";
	ById('ver-preis').disabled = false;

	if (typeof objver.typ !== 'undefined') {
		switch (objver.typ) {
			case CONSTANTS.MEHRKOSTEN:
				ById('vertrag-title').innerHTML = DM.vertragstypen[objver.typ] + "  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				updateBtnVorlagen(objver.projekt);
				ById('sectionverPos').classList.remove('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('ChgVertragSdat').classList.add('is-hidden');
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('sectionDarPos').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				ById('butVerEditPos').classList.remove('is-hidden');
				ById('vertableheader1').classList.add('is-hidden');
				ById('vertableheader2').classList.remove('is-hidden');
				ById('vertablefooter1').classList.add('is-hidden');
				ById('vertablefooter2').classList.remove('is-hidden');
				break;
			case CONSTANTS.GUTSCHRIFT:
				ById('vertrag-title').innerHTML = DM.vertragstypen[objver.typ] + "  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;
				ById('sectionDarPos').classList.add('is-hidden');
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				ById('sectionverPos').classList.remove('is-hidden');
				ById('butVerEditPos').classList.remove('is-hidden');
				ById('vertableheader1').classList.add('is-hidden');
				ById('vertableheader2').classList.remove('is-hidden');
				ById('vertablefooter1').classList.add('is-hidden');
				ById('vertablefooter2').classList.remove('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				ChgVertragStatus
				break;
			case CONSTANTS.KAUFVERTRAG:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				updateBtnVorlagen(objver.projekt);
				ById('butVerEditPos').classList.add('is-hidden');
				ById('sectionverPos').classList.remove('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('btnvorlageV1').classList.remove('is-hidden');
				ById('vertrag-title').innerHTML = DM.vertragstypen[objver.typ] + "  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;
				ById('ChgVertragSdat').classList.add('is-hidden');
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('sectionDarPos').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				ById('vertableheader1').classList.remove('is-hidden');
				ById('vertableheader2').classList.add('is-hidden');
				ById('vertablefooter1').classList.remove('is-hidden');
				ById('vertablefooter2').classList.add('is-hidden');

				break;
			case CONSTANTS.DARLEHEN:
				if (roleedit) {
					ById('btnDarStatusForward').classList.remove('is-hidden');
					ById('btnDarStatusBack').classList.remove('is-hidden');
					ById('btnDarBuchenAus').classList.remove('is-hidden');
					ById('btnDarBuchenZins').classList.remove('is-hidden');
					ById('btnDarBuchenRueck').classList.remove('is-hidden');
				} else {
					ById('btnDarStatusForward').classList.add('is-hidden');
					ById('btnDarStatusBack').classList.add('is-hidden');
					ById('btnDarBuchenAus').classList.add('is-hidden');
					ById('btnDarBuchenZins').classList.add('is-hidden');
					ById('btnDarBuchenRueck').classList.add('is-hidden');
				}
				ById('sectionDarPos').classList.remove('is-hidden');
				ById('butVerEditPos').classList.add('is-hidden');
				ById('sectionverPos').classList.add('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('ChgVertragSdat').classList.remove('is-hidden');
				ById('fldVerKun2').classList.remove('is-hidden');
				ById('vertrag-title').innerHTML = "Darlehen  " + setValueofId('ver-vernr', objver.vernr);
				let strtmp = "";
				// DP Kunde 2 (Darlehensgeber)-----------------------------------
				if (typeof objver.firma !== 'undefined' && objver.firma != "") {
					firma = DM.kundenDM.find(re => re.id == objver.firma);
					if (typeof firma != 'undefined') {
						strtmp = firma.vorname + " " + firma.name;
						if ((typeof firma.vorname2 !== 'undefined' && firma.vorname2 !== "") &&
							(typeof firma.name2 !== 'undefined' && firma.name2 !== "")) {
							strtmp += " und " + firma.vorname2 + " " + firma.name2;
						}
					} else {
						strtmp = "n.n.";

					}

					ById('inpVerKun2Autolookup').value = strtmp;
					ById('dpVerKun2-id').value = objver.firma;
				} else {
					ById('inpVerKun2Autolookup').value = "";
				}
				ById('vertableheader1').classList.remove('is-hidden');
				ById('vertableheader2').classList.add('is-hidden');
				ById('vertablefooter1').classList.remove('is-hidden');
				ById('vertablefooter2').classList.add('is-hidden');

				break;
			case CONSTANTS.ARCHVERTRAG:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('sectionDarPos').classList.add('is-hidden');
				ById('butVerEditPos').classList.remove('is-hidden');
				ById('sectionverPos').classList.remove('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('ChgVertragSdat').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				ById('vertableheader1').classList.remove('is-hidden');
				ById('vertableheader2').classList.add('is-hidden');
				ById('vertablefooter1').classList.remove('is-hidden');
				ById('vertablefooter2').classList.add('is-hidden');
				ById('vertrag-title').innerHTML = "Architektenvertrag  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;


				break;
			case CONSTANTS.RESERVIERUNG:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				updateBtnVorlagen(objver.projekt);
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('ChgVertragSdat').classList.add('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('sectionDarPos').classList.add('is-hidden');
				ById('sectionverPos').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				revstat = statusReserverierung(objver.id)

				ById('btnVerChange').classList.add('is-hidden');
				ById('btnVerChange').setAttribute('mode', 0);

				if (!revstat.erhalten && !revstat.erstattet) {
					ById('btnVerChange').innerHTML = 'Reservierungsgebühr eintragen';
					ById('btnVerChange').classList.remove('is-hidden');
					ById('btnVerChange').setAttribute('mode', CONSTANTS.EINBUCHEN);

				}
				if (revstat.erhalten && !revstat.erstattet) {
					ById('btnVerChange').innerHTML = 'Reservierungsgebühr erstatten';
					ById('btnVerChange').classList.remove('is-hidden');
					ById('btnVerChange').setAttribute('mode', '');
					ById('btnVerChange').setAttribute('mode', CONSTANTS.AUSBUCHEN);

				}
				ById('vertrag-title').innerHTML = DM.vertragstypen[objver.typ] + "  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;
				break;
			case CONSTANTS.VERTRAGALLG:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');
				updateBtnVorlagen(objver.projekt);
				ById('btnDarBuchenAus').classList.add('is-hidden');
				ById('btnDarBuchenZins').classList.add('is-hidden');
				ById('btnDarBuchenRueck').classList.add('is-hidden');
				ById('ChgVertragSdat').classList.add('is-hidden');
				ById('btnvorlageV1').classList.add('is-hidden');
				ById('sectionDarPos').classList.add('is-hidden');
				ById('sectionverPos').classList.add('is-hidden');
				ById('fldVerKun2').classList.add('is-hidden');
				ById('btnVerChange').classList.add('is-hidden');
				ById('btnVerChange').setAttribute('mode', 0);
				ById('modalVertragChangeTitle').innerHTML = "Kunde auswählen";
				ById('vertrag-title').innerHTML = DM.vertragstypen[objver.typ] + "  " + setValueofId('ver-vernr', objver.vernr) + ": " + titlestr;
				break;
			default:



		}
	} else {
		ById('vertrag-title').innerHTML = 'Vertrag ' + setValueofId('ver-vernr', objver.vernr);
	}
	strtmp = "";

	setStatusButtons(objver.status);
	let betrag;

	switch (objver.typ) {
		case CONSTANTS.DARLEHEN:
			// Darlehen ----------------------------------
			str += "<table class='table is-narrow'>";
			if (typeof objver.status != 'undefined') {
				str += "<tr>";
				str += "<td><strong>Status </strong></td><td>" + objver.status + "</td>";
				str += "</tr>";
			}

			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";
			str += "<td style='width: 10em;'><strong> Darlehensnehmer </strong></td>";
			strtmp = "";
			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;

				}
			} else {
				strtmp = "n.n."
			}
			str += "<td>" + strtmp + "</td>";
			str += "</tr>";

			str += "<tr id='verkundelink' value='" + objver.firma + "'>";
			str += "<td style='width: 10em;'><strong> Darlehensgeber </strong></td>";
			strtmp = "";
			if (typeof firma != 'undefined') {
				strtmp = firma.vorname + " " + firma.name;
				if ((typeof firma.vorname2 !== 'undefined' && firma.vorname2 !== "") &&
					(typeof firma.name2 !== 'undefined' && firma.name2 !== "")) {
					strtmp += " und " + firma.vorname2 + " " + firma.name2;

				}
			} else {
				strtmp = "n.n."
			}
			str += "<td>" + strtmp + "</td>";
			str += "</tr>";

			str += "<tr>";
			str += "<td><strong>Start </strong></td><td>" + setValueofId('ver-sdat', formatDateObject(objver.sdat)) + "</td>";
			str += "</tr>";

			let darkonto = parseInt(CONSTANTS.DAROFFSET + objver.id);
			ById('btnmodBuchungOk').setAttribute('konto', darkonto);
			ById('btnDarStatusForward').setAttribute('ref', objver.id);
			ById('btnDarStatusForwardText').setAttribute('ref', objver.id);
			ById('btnDarStatusBack').setAttribute('ref', objver.id);
			ById('btnDarStatusBackText').setAttribute('ref', objver.id);
			//ById('btnDarBuchenAus').setAttribute('konto', darkonto);
			//ById('btnDarBuchenZins').setAttribute('konto', darkonto);
			//ById('btnDarBuchenRueck').setAttribute('konto', darkonto);
			// str += "<tr>";
			// str += "<td><strong>Darlehenskonto (intern) </strong></td><td>" + darkonto + "</td>";
			// str += "</tr>";

			ById('txtVerPos1').innerHTML = str;
			ById('darposTableBody').innerHTML = "";
			let buchungen = DM.buchungen.filter(el => el.konto == darkonto || el.gegenkonto == darkonto);
			let totalsaldo = 0;
			let zeile = document.createElement('tr');
			if (typeof buchungen != 'undefined') {
				cLen = buchungen.length;
				for (i = 0; i < cLen; i++) {
					let zeile = document.createElement('tr');
					totalsaldo += buchungen[i].betrag;
					str = "";
					str += "<td class='has-text-left referenz'>" + buchungen[i].id + "</td>"; // Ref
					str += "<td class='has-text-left'>" + formatDateObject(buchungen[i].vdat) + "</td>"; // Datum
					str += "<td class='has-text-left'>" + buchungen[i].pn + "</td>"; // PN
					str += "<td class='has-text-left'>" + buchungen[i].text + "</td>"; // Text
					str += "<td class='has-text-right'>" + formatBetrag(buchungen[i].betrag) + "</td>"; // Betrag
					str += "<td class='has-text-right'>" + formatBetrag(totalsaldo) + "</td>"; // Saldo
					zeile.innerHTML = str;
					ById('darposTableBody').appendChild(zeile);
				}
			} else {
				str = "";
				str += "<td class='has-text-left'>" + "" + "</td>"; // Ref
				str += "<td class='has-text-left'>" + "" + "</td>"; // Datum
				str += "<td class='has-text-left'>" + "" + "</td>"; // PN
				str += "<td class='has-text-left'>" + "Keine Umsätze" + "</td>"; // Text
				str += "<td class='has-text-right'>" + "" + "</td>"; // Betrag
				str += "<td class='has-text-right'>" + formatBetrag(0) + "</td>"; // Saldo
				zeile.innerHTML = str;
				ById('darposTableBody').appendChild(zeile);
			}
			ById('darposTablefootSaldo').innerHTML = "<strong>" + formatBetrag(totalsaldo) + "</strong>";

			break;
		case CONSTANTS.ARCHVERTRAG:
			// ARCHVERTRAG ----------------------------------
			str += "<table class='table is-narrow'>";
			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";
			str += "<td style='width: 10em;'><strong> Vertragspartner </strong></td>";
			strtmp = "";
			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;

				}
			} else {
				strtmp = "n.n."
			}
			str += "<td>" + strtmp + "</td>";
			str += "<tr>";
			if (typeof objver.gegenstand != 'undefined') {
				str += "<td><strong>Vertragsgegenstand/ Objekt </strong></td><td>" + setValueofId('ver-gegenstand', objver.gegenstand) + "</td>";
			}
			str += "</tr>";
			str += "<tr>";
			if (typeof objver.mwst != 'undefined') {
				str += "<td><strong>MwSt </strong></td><td>" + objver.mwst + "</td>";
				ById('dpVerMwSt-button').innerHTML = objver.mwst;

			}

			str += "</tr>";

			str += "</tr>";

			ById('txtVerPos1').innerHTML = str;



			break;
		case CONSTANTS.RESERVIERUNG:
			str += "<table class='table is-narrow'>";
			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";

			str += "<td style='width: 10em;'><strong>Interessent </strong></td>";
			strtmp = "";
			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;
				}
			} else {
				strtmp = "n.n.";
			}
			str += "<td>" + strtmp + "</td>";
			//str += "<td>" + "" + "</td>";
			str += "</tr>";

			if (!revstat.erhalten && !revstat.erstattet) {
				col1 = red;

				str += "<tr>";
				str += "<td ><strong >Vereinbarte Reservierungsgebühr</strong></td>";
				str += "<td style='color: " + col1 + "'>" + formatBetrag(objver.betrag) + "</td>";
				str += "</tr>";

				str += "<tr>";
				str += "<td colspan='3'>" + "" + "</td>";
				str += "</tr>";

				str += "<tr>";
				str += "<td colspan='2'><strong style='color: " + col1 + "'>Reservierungsgebühr noch nicht erhalten!</strong></td>";
				str += "</tr>";

			}

			if (revstat.erhalten && !revstat.erstattet) {
				col1 = black;

				str += "<tr>";
				str += "<td ><strong style='color: " + col1 + "' >Reservierungsgebühr</strong></td>";
				str += "<td style='color: " + col1 + "'>" + formatBetrag(objver.betrag) + "</td>";
				str += "</tr>";

				str += "<tr>";
				str += "<td colspan='3'>" + "" + "</td>";
				str += "</tr>";

				col1 = black;

				str += "<tr>";
				str += "<td colspan='2'><strong style='color: " + col1 + "'>Reservierungsgebühr erhalten</strong></td>";
				str += "</tr>";

			}

			if (revstat.erhalten && revstat.erstattet) {
				col1 = black;

				// str += "<tr>";
				// str += "<td ><style='color: " + col1 + "' >Reservierungsgebühr</td>";
				// str += "<td style='color: " + col1 + "'>" + formatBetrag(objver.betrag) + "</td>";
				// str += "</tr>";

				str += "<tr>";
				str += "<td colspan='2'><strong style='color: " + col1 + "'>Die Reservierungsgebühr wurde erstattet</strong></td>";
				str += "</tr>";

			}



			str += "</table>";
			ById('txtVerPos1').innerHTML = str;

			ById('verkundelink').addEventListener('click', function () {
				let kundeid = ById('verkundelink').getAttribute('value');
				let kunde = DM.kundenDM.find(el => el.id == kundeid);
				//console.log(kunde);
				if (typeof kunde != 'undefined') {
					anzeige.kundeid = kundeid;
					anzeige.show = "kunde";
					ipcRenderer.send('display:save', anzeige);
					updateAnzeige();
				}
			});

			break;
		case CONSTANTS.KAUFVERTRAG:
			betrag = objver.betrag;
			str += "<table class='table is-narrow'>";
			str += "<tr>";
			str += "<td><strong>Kaufpreis </strong></td><td>" + formatBetrag(betrag) + "</td>";
			str += "</tr>";
			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";
			str += "<td style='width: 10em;'><strong>Käufer </strong></td>";
			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;
				}
			} else {
				strtmp = "n.n.";
			}
			str += "<td>" + strtmp + "</td>";
			str += "</tr>";

			str += "<tr>";
			strtmp = "";
			if (objver.urdat !== "") {
				strtmp = " (vom " + setValueofId('ver-urdat', objver.urdat) + ")";
			}
			str += "<tr>";
			str += "<td><strong>Urkunde </strong></td><td>" + setValueofId('ver-urnr', objver.urnr) + strtmp + "</td>";
			str += "</tr>";
			str += "<tr>";
			str += "<td><strong>Notariat </strong></td><td>" + objver.notariat + "</td>";
			if (typeof objver.notariat == 'undefined' || objver.notariat == 'undefined' || objver.notariat == '') {
				ById('dpVerNotar-button').innerHTML = 'Bitte auswählen';
			} else {
				ById('dpVerNotar-button').innerHTML = objver.notariat;
			}
			str += "</tr>";
			str += "<tr>";
			str += "<td><strong>Fertigstellung 1 </strong></td><td>" + setValueofId('ver-fdat1', objver.fdat1) + "</td>";
			str += "</tr>";
			str += "<tr>";
			str += "<td><strong>Fertigstellung 2 </strong></td><td>" + setValueofId('ver-fdat2', objver.fdat2) + "</td>";
			str += "</tr>";
			str += "<tr>";
			str += "<td><strong>Abnahme </strong></td><td>" + setValueofId('ver-abdat', objver.abdat) + "</td>";
			str += "</tr>";
			str += "<tr>";
			str += "<td><strong>Übergabe </strong></td><td>" + setValueofId('ver-udat', objver.udat) + "</td>";
			str += "</tr>";

			str += "</table>";
			ById('txtVerPos1').innerHTML = str;

			ById('verkundelink').addEventListener('click', function () {
				let kundeid = ById('verkundelink').getAttribute('value');
				let kunde = DM.kundenDM.find(el => el.id == kundeid);
				//console.log(kunde);
				if (typeof kunde != 'undefined') {
					anzeige.kundeid = kundeid;
					anzeige.show = "kunde";
					ipcRenderer.send('display:save', anzeige);
					updateAnzeige();
				}
			});

			betrag = formatBetrag(checkValueN(objver.betrag));
			ById('ver-preis').value = betrag;
			let rechnungen = DM.rechnungenDM.filter(el => el.vertrag == objver.id && el.status != CONSTANTS.TRASH);
			let disabled = false;
			rechnungen.forEach(rech => {
				if (rech.status != CONSTANTS.ENTWURF) {
					disabled = true;
				}
			});
			ById('ver-preis').disabled = disabled;

			break;
		case CONSTANTS.MEHRKOSTEN:
			betrag = objver.betrag;
			str += "<table class='table is-narrow'>";
			str += "<tr>";
			str += "<td><strong>Mehrkosten </strong></td><td>" + formatBetrag(betrag) + "</td>";
			str += "</tr>";
			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";
			str += "<td style='width: 10em;'><strong>Käufer </strong></td>";


			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;
				}
			} else {
				strtmp = "n.n.";
			}


			str += "<td>" + strtmp + "</td>";
			str += "</tr>";

			str += "</table>";
			ById('txtVerPos1').innerHTML = str;

			ById('verkundelink').addEventListener('click', function () {
				let kundeid = ById('verkundelink').getAttribute('value');
				let kunde = DM.kundenDM.find(el => el.id == kundeid);
				//console.log(kunde);
				if (typeof kunde != 'undefined') {
					anzeige.kundeid = kundeid;
					anzeige.show = "kunde";
					ipcRenderer.send('display:save', anzeige);
					updateAnzeige();
				}
			});
			(typeof objver.betrag !== 'undefined') ?
				betrag = formatBetrag(objver.betrag)
				: betrag = "";
			ById('ver-preis').value = betrag;
			break;
		case CONSTANTS.GUTSCHRIFT:
			betrag = objver.betrag;
			str += "<table class='table is-narrow'>";
			str += "<tr>";
			str += "<td><strong>Gutschrift </strong></td><td>" + formatBetrag(betrag) + "</td>";
			str += "</tr>";
			str += "<tr id='verkundelink' value='" + objver.kunde + "'>";
			str += "<td style='width: 10em;'><strong>Käufer </strong></td>";


			if (typeof kunde != 'undefined') {
				strtmp = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strtmp += " und " + kunde.vorname2 + " " + kunde.name2;
				}
			} else {
				strtmp = "n.n.";
			}


			str += "<td>" + strtmp + "</td>";
			str += "</tr>";

			str += "</table>";
			ById('txtVerPos1').innerHTML = str;
			ById('verkundelink').addEventListener('click', function () {
				let kundeid = ById('verkundelink').getAttribute('value');
				let kunde = DM.kundenDM.find(el => el.id == kundeid);
				//console.log(kunde);
				if (typeof kunde != 'undefined') {
					anzeige.kundeid = kundeid;
					anzeige.show = "kunde";
					ipcRenderer.send('display:save', anzeige);
					updateAnzeige();
				}
			});



			(typeof objver.betrag !== 'undefined') ?
				betrag = formatBetrag(objver.betrag)
				: betrag = "";
			ById('ver-preis').value = betrag;
			break;

		default:
		//  ----------------------------------


	}



	// Positionen ----------------------------------
	{
		total = parseFloat(objver.betrag);
		offen = 0;
		totaloffen = 0;
		gezahlt = 0;
		verschickt = 0;
		ById('ver-pos-tablebody').innerHTML = "";

		positionen.sort((a, b) => {
			let numA = a.rechnung;
			let numB = b.rechnung;
			return numA - numB;
		});


		let aktrecnr = 0;
		let recNew = false;
		let aktbezahlt = 0;
		cLen = positionen.length;
		for (i = 0; i < cLen; i++) {
			if (aktrecnr == positionen[i].rechnung) {
				recNew = false;
			} else {
				recNew = true;
				aktrecnr = positionen[i].rechnung;
			}
			let zeile = document.createElement('tr');
			let rechzupos = rechnungen.filter(re => re.id == positionen[i].rechnung);
			let str = "";
			//objver.typ == CONSTANTS.GUTSCHRIFT ? postotal -= positionen[i].betrag : postotal += positionen[i].betrag;
			//objver.typ == CONSTANTS.GUTSCHRIFT ? offen -= positionen[i].betrag : offen = positionen[i].betrag;

			// postotal += positionen[i].betrag;
			//offen = positionen[i].betrag;
			// Ratenanteil Prozent und Betrag
			str += "<td style='width: 30em;' refnr='" + positionen[i].id + "' >" + positionen[i].text + "</td>";
			// Ratenbezeichnung
			// Anteil & Betrag
			if (objver.typ != CONSTANTS.MEHRKOSTEN && objver.typ != CONSTANTS.GUTSCHRIFT) {
				if (typeof positionen[i].anteil != "undefined") {
					str += "<td class='has-text-right' style='width: 2%;'>" + formatProzent(positionen[i].anteil / 100) + "</td>"; // Anteil 
				} else {
					str += "<td> </td>"; // Anteil
				}
			}
			betrag = positionen[i].betrag

			str += "<td class='has-text-right' style='width: 2%;'>" + formatBetrag(betrag) + "</td>"; // Betrag


			let strZE = "";
			let strRe = "";
			if (rechzupos.length > 0) {
				strRe = rechzupos[0].rechnr;
				//let zeDM = datenmodell.get('Zahlungseingaenge');
				if (typeof DM.zeDM !== 'undefined' && DM.zeDM !== 'undefined') {
					let ze = DM.zeDM.filter(el => parseInt(el.rechnung) == parseInt(positionen[i].rechnung));
					if (ze.length > 0) {
						for (let j = 0; j < ze.length; j++) {
							strZE += "<p>" + ze[j].datum + " " + formatBetrag(ze[j].betrag) + "</p>"
						}
					}
				}
			}
			//totaloffen += offen;
			// Offener Betrag
			objver.typ == CONSTANTS.GUTSCHRIFT ? betrag = positionen[i].offenerbetrag : betrag = positionen[i].offenerbetrag;
			col = betrag != 0 ? red : black;
			str += "<td class='has-text-right' style='width: 2%; color: " + col + ";'>" + formatBetrag(betrag) + "</td>"; // Offener Betrag
			// Bezahlt/ Erhalten
			objver.typ == CONSTANTS.GUTSCHRIFT ? betrag = positionen[i].bezahlt : betrag = positionen[i].bezahlt;
			col = betrag != 0 ? green : black;
			str += "<td class='has-text-right'style='width: 2%; color: " + col + ";'>" + formatBetrag(betrag) + "</td>"; // Bezahlt
			// Zahlungseingang
			// str += "<td>" + strZE + "</td>";
			// Aktuell gestellt
			if (objver.typ != CONSTANTS.MEHRKOSTEN && objver.typ != CONSTANTS.GUTSCHRIFT) {

				objver.typ == CONSTANTS.GUTSCHRIFT ? betrag = positionen[i].verschickt : betrag = positionen[i].verschickt;
				col = betrag != 0 ? red : black;
				str += "<td class='has-text-right'style='width: 2%; color: " + col + ";'>" + formatBetrag(betrag) + "</td>"; // Verschickt
			}
			// Rechnungsnr
			str += "<td class='rechlink' value='" + positionen[i].rechnung + "'  class='has-text-left' >" + strRe + "</td>"; // Rechnung


			zeile.innerHTML = str;
			ById('ver-pos-tablebody').appendChild(zeile);
		}

		if (objver.typ == CONSTANTS.MEHRKOSTEN || objver.typ == CONSTANTS.GUTSCHRIFT) {
			betrag = objver.betrag;
			if (typeof betrag === 'undefined') { betrag = 0; }
			ById('verpostablefootRaten2').innerHTML = "<strong>" + formatBetrag(betrag) + "</strong>";

			betrag = objver.offenerbetrag;
			if (typeof betrag === 'undefined') { betrag = 0; }
			col = betrag != 0 ? red : black;
			ById('verpostablefootOffen2').innerHTML = "<strong style='color: " + col + ";'>" + formatBetrag(betrag) + "</strong>";

			betrag = objver.bezahlt;
			if (typeof betrag === 'undefined') { betrag = 0; }
			col = betrag > 0 ? green : black;
			ById('verpostablefootBezahlt2').innerHTML = "<strong style='color: " + col + ";'>" + formatBetrag(betrag) + "</strong>";

		} else {
			betrag = objver.betrag;
			if (typeof betrag === 'undefined') { betrag = 0; }
			ById('verpostablefootRaten').innerHTML = "<strong>" + formatBetrag(betrag) + "</strong>";

			betrag = objver.offenerbetrag;
			if (typeof betrag === 'undefined') { betrag = 0; }
			col = betrag != 0 ? red : black;
			ById('verpostablefootOffen').innerHTML = "<strong style='color: " + col + ";'>" + formatBetrag(betrag) + "</strong>";

			betrag = objver.bezahlt;
			if (typeof betrag === 'undefined') { betrag = 0; }
			col = betrag > 0 ? green : black;
			ById('verpostablefootBezahlt').innerHTML = "<strong style='color: " + col + ";'>" + formatBetrag(betrag) + "</strong>";

			betrag = objver.verschickt;
			if (typeof betrag === 'undefined') { betrag = 0; }
			col = betrag != 0 ? red : black;
			ById('verpostablefootGestellt').innerHTML = "<strong style='color: " + col + ";'>" + formatBetrag(betrag) + "</strong>";
		}
	}

	ById('ver-pos-tablebody').addEventListener('click', function (event) {
		if (event.target.classList.contains('rechlink')) {
			let rechnungid = event.target.getAttribute('value');
			if (rechnungid != 'undefined' && rechnungid != "") {
				let rechnung = DM.rechnungenDM.find(el => el.id == rechnungid);

				if (typeof rechnung != 'undefined') {
					anzeige.rechnungid = rechnungid;
					anzeige.show = "rechnung";
					ipcRenderer.send('display:save', anzeige);
					updateAnzeige();
				}
			}
		}
		let refnr = event.target.getAttribute('refnr');
		if (parseInt(refnr) > 0) {
			anzeige.edit = true;
			ipcRenderer.send('display:save', anzeige);

			let vertrag = DM.vertragDM.find(el => el.id == anzeige.vertragid);
			let position = DM.positionenDM.find(el => parseInt(el.id) == parseInt(refnr));

			if (typeof vertrag != 'undefined' &&
				(vertrag.typ == CONSTANTS.VERTRAGALLG || vertrag.typ == CONSTANTS.MEHRKOSTEN || vertrag.typ == CONSTANTS.GUTSCHRIFT)) {
				anzeige.show = "vertragposedit";
				let objekt = DM.objekteDM.find(el => el.id == vertrag.objekt);
				let betrag = typeof objekt != 'undefined' ? objekt.betrag : 0;
				if (typeof position.rechnung != 'undefined') {
					ById('rech-id').value = position.rechnung;
				}
				if (position != 'undefined') {
					ById('modalVertragPostenChange').classList.add('is-active');
					ById('tabverpos').innerHTML = "";
					appendPosZeile(position.id, position.rechpos, position.text,
						position.anteil, position.netto, position.mwst, position.betrag, true, true, false, rnb = false, position.bmgBetrag, position.faktor);
				}
			}
			ipcRenderer.send('display:save', anzeige);


		}

	});


	// Kommentare ------------------------------
	if (anzeige.show == "vertrag") {
		ById('paneKommentare').classList.add('is-hidden');
		let comments = DM.commentDM.filter(el => el.vertrag == anzeige.vertragid &&
			(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
		fillComments(comments);
	}


	// EoFunc
}
function statusReserverierung(verid) {
	let status = new Object();
	status.erstattet = false;
	status.erhalten = false;
	rechnungen = DM.rechnungenDM.filter(re => parseInt(re.vertrag) == parseInt(verid));

	for (let re = 0; re < rechnungen.length; re++) {
		if (rechnungen[re].rechnr == "Erstattung" && rechnungen[re].status != CONSTANTS.TRASH) {
			status.erstattet = true;
		}
		if (rechnungen[re].rechnr == "Reservierungsgebühr" && rechnungen[re].status != CONSTANTS.TRASH) {
			status.erhalten = true;
		}
	}

	return status;
}
function editVertrag() {
	ById('modalVertragChange').classList.add('is-active');
	anzeige.edit = true;
	ipcRenderer.send('display:save', anzeige);
	ById('dpVerKun').classList.remove('is-active');
	ById('dpVerKun2').classList.remove('is-active');
	// ById('ver-dropdown-objekt').classList.remove('is-active');
	ById('dpVerStatus').classList.remove('is-active');
	ById('ver-id').value = anzeige.vertragid;
}
function saveVertrag() {
	ById('modalVertragChange').classList.remove('is-active');
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);

	let vertrag = new Object();
	vertrag.id = ById('ver-id').value;
	vertrag.gegenstand = ById('ver-gegenstand').value;
	vertrag.mwst = ById('dpVerMwSt-button').innerHTML == "Bitte auswählen" ? "" : ById('dpVerMwSt-button').innerHTML;
	vertrag.urnr = ById('ver-urnr').value;
	vertrag.urdat = ById('ver-urdat').value;
	vertrag.notariat = ById('dpVerNotar-button').innerHTML == "Bitte auswählen" ? "" : ById('dpVerNotar-button').innerHTML;
	vertrag.fdat1 = ById('ver-fdat1').value;
	vertrag.fdat2 = ById('ver-fdat2').value;
	vertrag.abdat = ById('ver-abdat').value;
	vertrag.udat = ById('ver-udat').value;
	vertrag.vernr = ById('ver-vernr').value;
	vertrag.betrag = convPreisField(ById('ver-preis').value);
	vertrag.status = ById('dpVerStatus-id').value == CONSTANTS.NEU ? CONSTANTS.AKTIV : ById('dpVerStatus-id').value;
	vertrag.kunde = ById('dpVerKun-id').value;
	if (typeof vertrag.kunde !== 'undefined' && vertrag.kunde != "") {
		kunde = DM.kundenDM.find(re => re.id == vertrag.kunde);
		if (typeof kunde != 'undefined') {
			vertrag.vorname = kunde.vorname;
			vertrag.name = kunde.name;
		}
	}
	// -------------- DAR Felder ------------
	vertrag.firma = ById('dpVerKun2-id').value;
	vertrag.sdat = ById('ver-sdat').value == "" ? "" : string2Date(ById('ver-sdat').value);
	ipcRenderer.send('vertrag:change', vertrag);
}
function addElementToVertragsPos(vertrag, rnb, bmg) {
	appendPosZeile(0, 0, "", 100, "", "", "", false, false, true, rnb, bmg, 1);
	// Alle Rechnungspositionen holen
	positionen = DM.positionenDM.filter(el => parseInt(el.rechnung) == parseInt(anzeige.rechnungid));
	if (typeof positionen !== 'undefined' && positionen !== 'undefined') {
		cLen = positionen.length;
		positionen.sort((a, b) => {
			return parseInt(a.rechpos) - parseInt(b.rechpos);
		});
		for (i = 0; i < cLen; i++) {
			let netto = typeof positionen[i].netto != 'undefined' ? positionen[i].netto : positionen[i].betrag;
			let mwst = typeof positionen[i].mwst != 'undefined' ? positionen[i].mwst : 0;
			appendPosZeile(positionen[i].id, positionen[i].rechpos, positionen[i].text,
				positionen[i].anteil, netto, mwst, positionen[i].betrag, true, true, false, rnb, positionen[i].bmgBetrag, positionen[i].faktor);
		}
	}

}
function appendPosZeile(id, rechpos, text, anteil, netto, mwst, betrag, add, del, neu, rnb, bmg, faktor) {
	let hidden = bmg == 0 ? "" : "";
	let zeile = document.createElement('div');
	zeile.className = 'block';

	let str = "";
	let disabled = "";
	let idtext = "fieldPosText-" + id;
	let id1 = "buttonPosEdit-" + id;
	let id2 = "buttonPosDel-" + id;
	let id3 = "buttonPosNew-" + id;
	let identer = id == 0 ? id3 : id1;
	let idpos = "fieldRechPos-" + id;
	let rpos = typeof rechpos == 'undefined' || rechpos == 0 ? "" : rechpos;
	str += "<div >";

	str += "<div class='columns'>";
	str += "<div class='column is-10'>";

	{
		str += "<div class='colums'>";

		str += "<div class='field is-grouped is-size-5'>";
		if (rnb) {
			str += "<div class='column is-1'>";
			str += "<div class='field'>" +
				"<p class='control'><input class='input' type='text' id='" + idpos
				+ "' onpressenter='" + identer + "' value='" + rpos
				+ "' placeholder='Pos'></p>" +
				"</div>";
			str += "</div>";
		}
		str += "<div class='column is-11'>";
		str += "<div class='field is-expanded'>" +
			"<p class='control'><textarea rows='6' class='textarea' type='text' id='" + idtext
			+ "' value='" + text
			+ "' placeholder='Beschreibung'>" + text + "</textarea></p>" +
			"</div>";
		str += "</div>";

		str += "</div>";
		str += "</div>";

	}
	{
		str += "<div class='colums'>";
		str += "<div class='field is-grouped'>";
		let idanteil = "fieldPosAnteil-" + id;
		let idfaktor = "fieldPosFaktor-" + id;
		let idbmgBetrag = "fieldPosBmgBetrag-" + id;
		let idbetrag = "fieldPosBetrag-" + id;
		let idnetto = "fieldPosNetto-" + id;
		let idmwst = "fieldPosMwst-" + id;
		let strbmgBetrag = "";
		let stranteil = "";
		let strfaktor = "";
		if (typeof bmg != "undefined" && bmg != 0) {
			strbmgBetrag = formatBetrag(bmg);
		}
		if (typeof anteil != "undefined" || anteil == "") {
			stranteil = formatProzent(anteil / 100);
			strfaktor = formatNumber(checkValue(faktor));
		}
		str += "<div class='column is-2'>";
		str += "<div class='field'>" +
			"<p class='control'><input class='input " + hidden + "' type='text'  id='" + idbmgBetrag
			+ "' onpressenter='" + identer + "'  value='" + strbmgBetrag
			+ "' placeholder='Bemessungsbetrag in €'></p>" +
			"</div>";
		if (id == 0) {
			str += "<p class='help'>Nettobetrag</p>";
		}
		else {
			str += "<p class='help'>Bemessungsbetrag</p>";
		}
		str += "</div>";

		str += "<div class='column is-1'>";
		str += "<div class='field'>" +
			"<p class='control'><input class='input " + hidden + "' type='text'  id='" + idfaktor
			+ "' onpressenter='" + identer + "'  value='" + strfaktor
			+ "' placeholder=' Faktor' ></p>" +
			"</div>";
		if (id == 0) {
			str += "<p class='help'>Faktor</p>";
		}
		else {
			str += "<p class='help'>Faktor</p>";
		}
		str += "</div>";

		str += "<div class='column is-1'>";
		str += "<div class='field'>" +
			"<p class='control'><input class='input " + hidden + "' type='text'  id='" + idanteil
			+ "' onpressenter='" + identer + "'  value='" + stranteil
			+ "' placeholder='Anteil' ></p>" +
			"</div>";
		if (id == 0) {
			str += "<p class='help'>Prozent</p>";
		}
		else {
			str += "<p class='help'>Anteil</p>";
		}
		str += "</div>";



		// str += "<div class='column is-3'>";
		// let strnetto = typeof netto != "undefined" & netto != "" ? formatBetrag(netto) : "";
		// str += "<div class='field'>" +
		// 	"<p class='control'><input class='input' type='text'  id='" + idnetto
		// 	+ "' onpressenter='" + identer + "'  value='" + strnetto
		// 	+ "' placeholder='Netto in €' ></p>" +
		// 	"</div>";
		// if (id == 0) {
		// 	str += "<p class='help'>Der Nettobetrag kann direkt eingegeben werden <strong>oder</strong>  wird aus Bemessungsbetrag * Faktor * Anteil berechnet. ";
		// 	str += "Falls nur ein Bruttobetrag angegeben ist wird der Nettobetrag aus Brutto / MwSt berechnet werden.</p>";
		// }
		// else {
		// 	str += "<p class='help'>Nettobetrag</p>";
		// }
		//str += "</div>";

		str += "<div class='column is-2'>";
		//console.log("mwst=" + mwst);
		let strmwst = typeof mwst != "undefined" & mwst != "" ? formatProzent(mwst / 100) : "";
		str += "<div class='field'>" +
			"<p class='control'><input class='input' type='text'  id='" + idmwst
			+ "' onpressenter='" + identer + "'  value='" + strmwst
			+ "' placeholder='MwSt in %'  ></p>" +
			"</div>";
		str += "<p class='help'>Mehrwertsteuer</p>";

		str += "</div>";

		str += "<div class='column is-3'>";
		let strbetrag = typeof betrag != "undefined" & betrag != "" ? formatBetrag(betrag) : "";
		str += "<div class='field'>" +
			"<p class='control'><input class='input' type='text'  id='" + idbetrag
			+ "' onpressenter='" + identer + "'  value='" + strbetrag
			+ "' placeholder='Brutto in €'  ></p>" +
			"</div>";
		if (id == 0) {
			str += "<p class='help'>Bruttobetrag</p>";
		}
		else {
			str += "<p class='help'>Bruttobetrag</p>";
		}
		str += "</div>";


		str += "</div>";

		str += "</div>";
	}

	str += "</div>";

	str += "<div class='column is-2'>";

	str += "<div class='field is-grouped'>" +
		"<div class='field-body'>";
	add == true ? disabled = "" : disabled = "disabled";
	str += "<button class='button is-info is-light' " + disabled + " id='" + id1 + "'>OK</button>";
	del == true ? disabled = "" : disabled = "disabled";
	str += "<button class='button is-warning is-light' " + disabled + " id='" + id2 + "'>DEL</button>";
	neu == true ? disabled = "" : disabled = "disabled";
	str += "<button class='button is-primary is-light' " + disabled + " id='" + id3 + "'>ADD</button>";
	str += "</div>";
	str += "</div>"; //Buttons	

	str += "</div>";

	str += "</div>";
	str += "</div>";


	zeile.innerHTML = str;

	ById('tabverpos').appendChild(zeile);
	// let hr = document.createElement('hr');
	// ById('tabverpos').appendChild(hr);

	if (del == true) {
		// Button DEL
		document.querySelector('#' + id2).addEventListener('click', function () {
			// "buttonPosDel-"+positionen[i].id;	
			index = this.id.substring(13, this.id.length);
			//console.log(index, this.id);
			ipcRenderer.send('position:delete', index);
		});
	}
	if (add == true) {
		// Button OK
		document.querySelector('#' + id1).addEventListener('click', function () {
			// "buttonPosEdit-"+.id;	
			index = this.id.substring(14, this.id.length);
			let position = new Object();
			let idtext = "fieldPosText-" + index;
			let idanteil = "fieldPosAnteil-" + index;
			let idbetrag = "fieldPosBetrag-" + index;
			let idnetto = "fieldPosNetto-" + index;
			let idmwst = "fieldPosMwst-" + index;
			let idfaktor = "fieldPosFaktor-" + index;
			let idbmgBetrag = "fieldPosBmgBetrag-" + index;
			position.vertrag = ById('ver-id').value;
			position.id = index;
			position.vertrag = ById('ver-id').value;
			position.text = ById(idtext).value;
			position.rechnung = ById('rech-id').value;

			if (ById("fieldRechPos-" + index) !== null) {
				position.rechpos = ById("fieldRechPos-" + index).value;
				//Wenn man die Position rausnimmt ist die Zurodnung zur Rechnung auch weg
				// console.log("Wenn man die Position rausnimmt ist die Zurodnung zur Rechnung auch weg")
				// if (position.rechpos == "") {
				// 	position.rechnung = 0;
				// }
			}
			position.mwst = convProzentField(ById(idmwst).value);
			position.mwst = isNaN(position.mwst) ? 0 : position.mwst;
			position.bmgBetrag = isNaN(convPreisField(ById(idbmgBetrag).value)) || ById(idbmgBetrag).value == "" ? 0 : roundToTwo(convPreisField(ById(idbmgBetrag).value));
			position.faktor = isNaN(parseFloat(ById(idfaktor).value)) ? 1 : parseFloat(ById(idfaktor).value.replace(",", "."));
			position.anteil = ById(idanteil).value != "" ? convProzentField(ById(idanteil).value) : 100;

			if (position.bmgBetrag != 0) {
				position.netto = position.anteil * position.faktor * position.bmgBetrag / 100;
				position.betrag = position.netto * (1 + position.mwst / 100);
			} else {
				position.betrag = ById(idbetrag).value != "" ? convPreisField(ById(idbetrag).value) : 0;
				position.netto = position.betrag / (1 + position.mwst / 100);
			}

			position.betrag = roundToTwo(position.betrag);
			position.netto = roundToTwo(position.netto);

			ipcRenderer.send('position:change', position);

		});
	}
	if (neu == true) {
		// Button ADD
		document.querySelector('#' + id3).addEventListener('click', function () {
			// "buttonPosNew-"+.id;	
			index = this.id.substring(13, this.id.length);
			let position = new Object();
			let idtext = "fieldPosText-" + index;
			let idanteil = "fieldPosAnteil-" + index;
			let idfaktor = "fieldPosFaktor-" + index;
			let idbetrag = "fieldPosBetrag-" + index;
			let idnetto = "fieldPosNetto-" + index;
			let idmwst = "fieldPosMwst-" + index;
			let idbmgBetrag = "fieldPosBmgBetrag-" + index;
			position.vertrag = ById('ver-id').value;
			position.text = ById(idtext).value;
			if (ById("fieldRechPos-" + index) !== null) {
				position.rechpos = ById("fieldRechPos-" + index).value;
			}
			position.mwst = convProzentField(ById(idmwst).value);
			position.mwst = isNaN(position.mwst) ? 0 : position.mwst;
			position.bmgBetrag = isNaN(convPreisField(ById(idbmgBetrag).value)) || ById(idbmgBetrag).value == "" ? 0 : roundToTwo(convPreisField(ById(idbmgBetrag).value));
			position.faktor = isNaN(parseFloat(ById(idfaktor).value)) ? 1 : parseFloat(ById(idfaktor).value.replace(",", "."));
			position.anteil = ById(idanteil).value != "" ? convProzentField(ById(idanteil).value) : 100;

			if (position.bmgBetrag != 0) {

				position.netto = position.anteil * position.faktor * position.bmgBetrag / 100;
				position.betrag = position.netto * (1 + position.mwst / 100);

			} else {
				position.betrag = ById(idbetrag).value != "" ? convPreisField(ById(idbetrag).value) : 0;
				position.netto = position.betrag / (1 + position.mwst / 100);
			}

			position.betrag = roundToTwo(position.betrag);
			position.netto = roundToTwo(position.netto);
			position.rechnung = ById('rech-id').value;
			ipcRenderer.send('position:neu', position);

		});
	}

}
function fillDPPostTemplate() {
	dpVerPosTemplate.innerHTML = "";
	//let zahltemplates = datenmodell.get('Zahltemplate');
	// Alle Templates durchlaufen
	for (i = 0; i < zahltemplates.length; i++) {
		obj = DM.zahltemplates[i];
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "ztp-" + obj.id;
		el1.innerHTML = obj.name + '<hr class="dropdown-divider">';
		dpVerPosTemplate.appendChild(el1);

	}
}
function addZTP(id) {
	//dlet zahltemplates = datenmodell.get('Zahltemplate');
	let zahltemplate = DM.zahltemplates.find(el => el.id == id);
	let positionen = zahltemplate.positionen;
	cLen = positionen.length;
	for (i = 0; i < cLen; i++) {
		let position = new Object();

		position.vertrag = ById('ver-id').value;
		position.text = positionen[i].text;

		if (positionen[i].anteil != "") {
			position.anteil = positionen[i].anteil;
			position.betrag = position.anteil * convPreisField(ById('ver-preis').value) / 100;
		} else {
			position.betrag = positionen[i].betrag;
		}

		ipcRenderer.send('position:neu', position);
	}


}
function nextRechnr(vertrag) {
	//let loaddata = datenmodell.get('Rechnungen');
	let recmax = 0;


	rechnungen = DM.rechnungenDM.filter(re => parseInt(re.vertrag) == parseInt(anzeige.vertragid) && re.status != CONSTANTS.TRASH);
	for (i = 0; i < rechnungen.length; i++) {
		let text = rechnungen[i].rechnr;
		let pos = text.lastIndexOf("-");
		let nr = 0;
		// if (pos > 0) {
		// 	nr = parseInt(text.substring(pos + 1, text.length));
		// }


		let strArray = text.split("-");
		nr = parseInt(strArray[2]);

		if (nr > recmax) {
			recmax = nr;
		}

	}



	recmax++;
	recmax = ("00" + recmax).slice(-5);
	return recmax;


}
function nextVertragNr(typ) {
	vertrage = DM.vertragDM.filter(el => parseInt(el.projekt) == 0 && re.status != CONSTANTS.TRASH && el.typ == typ);
	let recmax = 0;
	for (i = 0; i < vertrage.length; i++) {
		let text = vertrage[i].vernr;
		let pos = text.lastIndexOf("-");
		let nr = 0;
		if (pos > 0) {
			nr = parseInt(text.substring(pos + 1, text.length));
		}
		if (nr > recmax) {
			recmax = nr;
		}
	}
	return recmax + 1;
}
function editKunde() {
	ById('modalKundeChange').classList.add('is-active');
	ById('kun-id').value = anzeige.kundeid;
	ById('dpKunAnrede').classList.remove('is-active');
	ById('dpKunAnrede2').classList.remove('is-active');
	ById('dpKunStatus').classList.remove('is-active');
}
function saveKunde() {
	console.log("Aktuelle ID: " + ById('kun-id').value + " KdNr: " + ById('kun-kundenr').value);

	let kunde = DM.kundenDM.filter(el => el.kundennummer == ById('kun-kundenr').value);

	if (typeof kunde != 'undefined') {
		kunde.forEach(el => {
			if (el.id != ById('kun-id').value) {
				let options = {
					buttons: ["OK"],
					message: "Die Kundennummer",
					title: "Kundennummer bereits vergeben",
					detail: "Ist belegt durch " + kunde.name,
					type: "question"
				};
				dialog.showMessageBoxSync(getCurrentWindow(), options);
				return false;
			}
		})


	}

	kunde = new Object();
	kunde.kundennummer = ById('kun-kundenr').value;
	kunde.name = ById('kun-name').value;
	kunde.vorname = ById('kun-vorname').value;
	kunde.strasse = ById('kun-strasse').value;
	kunde.plz = ById('kun-plz').value;
	kunde.ort = ById('kun-ort').value;
	kunde.telefon = ById('kun-telefon').value;
	kunde.telefon2 = ById('kun-telefon2').value;
	kunde.email = ById('kun-email').value;
	kunde.name2 = ById('kun-name2').value;
	kunde.vorname2 = ById('kun-vorname2').value;
	kunde.strasse2 = ById('kun-strasse2').value;
	kunde.plz2 = ById('kun-plz2').value;
	kunde.ort2 = ById('kun-ort2').value;
	kunde.telefon3 = ById('kun-telefon3').value;
	kunde.telefon4 = ById('kun-telefon4').value;
	kunde.email2 = ById('kun-email2').value;

	kunde.kunde = ById('fldkunde').checked;
	kunde.lieferant = ById('fldlieferant').checked;
	kunde.notar = ById('fldnotar').checked;
	kunde.mitarbeiter = ById('fldmitarbeiter').checked;
	kunde.anrede = ById('dpKunAnrede-id').value;
	kunde.anrede2 = ById('dpKunAnrede2-id').value;
	kunde.status = ById('dpKunStatus-id').value;
	kunde.id = ById('kun-id').value;

	if (kunde.id == "NEU") {
		kunde.type = "Kunde";
		ipcRenderer.send('edit:new', kunde);

	} else {
		ipcRenderer.send('kunde:change', kunde);
	}
	return true;

}
function showKunde(idKunde) {
	//var kunden = datenmodell.Kunden;
	//let kunden = datenmodell.get('Kunden');
	let str;
	//console.log(idKunde, anzeige);
	if (typeof idKunde === 'undefined') {
		return -1;
	}
	if (idKunde == -1) {
		return;
	}
	ById('kun-box').classList.remove('is-hidden');
	let kunde = DM.kundenDM.find(re => re.id == idKunde);
	anzeige.kundeid = kunde.id;
	ById('kun-id').value = kunde.id;
	ipcRenderer.send('display:save', anzeige);

	//let anredeliste = konfiguration.get('Anrede');

	if (typeof kunde.anrede !== 'undefined' && kunde.anrede !== 'keine' && kunde.anrede !== "") {
		ById('dpKunAnrede-button').innerHTML = DM.anredeliste[kunde.anrede];
		ById('dpKunAnrede-id').value = kunde.anrede;
	} else {
		ById('dpKunAnrede-button').innerHTML = "Nicht erfasst";
	}
	if (typeof kunde.anrede2 !== 'undefined' && kunde.anrede2 !== 'keine' && kunde.anrede2 !== '') {
		ById('dpKunAnrede2-button').innerHTML = DM.anredeliste[kunde.anrede2];
		ById('dpKunAnrede2-id').value = kunde.anrede2;
	} else {
		ById('dpKunAnrede2-button').innerHTML = "Nicht erfasst";
	}
	if (typeof kunde.status !== 'undefined') {
		ById('dpKunStatus-button').innerHTML = kunde.status;
		ById('dpKunStatus-id').value = kunde.status;
	} else {
		ById('dpKunStatus-button').innerHTML = "Nicht erfasst";
	}

	let strttitle = "Kunde";

	if ((typeof kunde.vorname !== 'undefined' && kunde.vorname !== "") &&
		(typeof kunde.name !== 'undefined' && kunde.name !== "")) {
		strttitle = strdef(kunde.vorname) + " " + strdef(kunde.name);
	}

	if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
		(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
		if (kunde.lieferant == true) {
			strttitle += "- ";
		} else {
			strttitle += " und  ";

		}
		strttitle += strdef(kunde.vorname2) + " " + strdef(kunde.name2);
	}

	ById('kunde-title').innerHTML = strttitle; //(" + strdef(kunde.kundennummer) + ")";


	if (typeof kunde.kundennummer != 'undefined' && kunde.kundennummer != "") {
		//ById('trKunNummer').classList.remove('is-hidden');
		ById('fldKunNummer').innerHTML = "Kundenr.: " + strdef(kunde.kundennummer);
	} else {
		ById('trKunNummer').classList.add('is-hidden');
	}
	if (typeof kunde.kundennummer !== 'undefined' && kunde.kundennummer !== "") {
		ById('kun-kundenr').value = strdef(kunde.kundennummer);
	} else {
		ById('kun-kundenr').value = neueKundennummer();
	}

	if (typeof kunde.anrede != 'undefined' && kunde.anrede != 'keine' && kunde.anrede !== '') {
		ById('trKunAnrede').classList.remove('is-hidden');
		ById('fldKunAnrede').innerHTML = DM.anredeliste[kunde.anrede];
	} else {
		ById('trKunAnrede').classList.add('is-hidden');
	}

	if (typeof kunde.vorname != 'undefined' && kunde.vorname != "") {
		ById('trKunVorname').classList.remove('is-hidden');
		ById('fldKunVorname').innerHTML = strdef(kunde.vorname);
	} else {
		ById('trKunVorname').classList.add('is-hidden');
	}
	ById('kun-vorname').value = strdef(kunde.vorname);

	if (typeof kunde.name != 'undefined' && kunde.name != "") {
		ById('trKunName').classList.remove('is-hidden');
		ById('fldKunName').innerHTML = strdef(kunde.name);
	} else {
		ById('trKunName').classList.add('is-hidden');
	}
	ById('kun-name').value = strdef(kunde.name);

	if (typeof kunde.anrede2 != 'undefined' && kunde.anrede2 != 'keine' && kunde.anrede2 !== '') {
		ById('trKunAnrede2').classList.remove('is-hidden');
		ById('fldKunAnrede2').innerHTML = DM.anredeliste[kunde.anrede2];
	} else {
		ById('trKunAnrede2').classList.add('is-hidden');
	}


	if (typeof kunde.vorname2 != 'undefined' && kunde.vorname2 != "") {
		ById('trKunVorname2').classList.remove('is-hidden');
		ById('fldKunVorname2').innerHTML = strdef(kunde.vorname2);
	} else {
		ById('trKunVorname2').classList.add('is-hidden');
	}
	ById('kun-vorname2').value = strdef(kunde.vorname2);

	if (typeof kunde.name2 != 'undefined' && kunde.name2 != "") {
		ById('trKunName2').classList.remove('is-hidden');
		ById('fldKunName2').innerHTML = strdef(kunde.name2);
	} else {
		ById('trKunName2').classList.add('is-hidden');
	}
	ById('kun-name2').value = strdef(kunde.name2);

	if (typeof kunde.strasse != 'undefined' && kunde.strasse != "") {
		ById('trKunStrasse').classList.remove('is-hidden');
		ById('fldKunStrasse').innerHTML = strdef(kunde.strasse);
	} else {
		ById('trKunStrasse').classList.add('is-hidden');
	}
	ById('kun-strasse').value = strdef(kunde.strasse);

	if (typeof kunde.plz != 'undefined' && kunde.plz != "" && typeof kunde.ort != 'undefined' && kunde.ort != "") {
		ById('trKunOrt').classList.remove('is-hidden');
		ById('fldKunOrt').innerHTML = strdef(kunde.plz) + " " + strdef(kunde.ort);
	} else {
		ById('trKunOrt').classList.add('is-hidden');
	}
	ById('kun-plz').value = strdef(kunde.plz);
	ById('kun-ort').value = strdef(kunde.ort);

	if (typeof kunde.strasse2 != 'undefined' && kunde.strasse2 != "") {
		ById('trKunStrasse2').classList.remove('is-hidden');
		ById('fldKunStrasse2').innerHTML = strdef(kunde.strasse2);
	} else {
		ById('trKunStrasse2').classList.add('is-hidden');
	}
	ById('kun-strasse2').value = strdef(kunde.strasse2);

	if (typeof kunde.plz2 != 'undefined' && kunde.plz2 != "" && typeof kunde.ort2 != 'undefined' && kunde.ort2 != "") {
		ById('trKunOrt2').classList.remove('is-hidden');
		ById('fldKunOrt2').innerHTML = strdef(kunde.plz2) + " " + strdef(kunde.ort2);
	} else {
		ById('trKunOrt2').classList.add('is-hidden');
	}
	ById('kun-plz2').value = strdef(kunde.plz2);
	ById('kun-ort2').value = strdef(kunde.ort2);

	if (typeof kunde.telefon != 'undefined' && kunde.telefon != "") {
		ById('trKunMobil1').classList.remove('is-hidden');
		ById('fldKunMobil1').innerHTML = strdef(kunde.telefon);
	} else {
		ById('trKunMobil1').classList.add('is-hidden');
	}
	ById('kun-telefon').value = strdef(kunde.telefon);

	if (typeof kunde.telefon2 != 'undefined' && kunde.telefon2 != "") {
		ById('trKunMobil2').classList.remove('is-hidden');
		ById('fldKunMobil2').innerHTML = strdef(kunde.telefon2);
	} else {
		ById('trKunMobil2').classList.add('is-hidden');
	}
	ById('kun-telefon2').value = strdef(kunde.telefon2);

	if (typeof kunde.telefon3 != 'undefined' && kunde.telefon3 != "") {
		ById('trKunMobil12').classList.remove('is-hidden');
		ById('fldKunMobil12').innerHTML = strdef(kunde.telefon3);
	} else {
		ById('trKunMobil12').classList.add('is-hidden');
	}
	ById('kun-telefon3').value = strdef(kunde.telefon3);

	if (typeof kunde.telefon4 != 'undefined' && kunde.telefon4 != "") {
		ById('trKunMobil22').classList.remove('is-hidden');
		ById('fldKunMobil22').innerHTML = strdef(kunde.telefon4);
	} else {
		ById('trKunMobil22').classList.add('is-hidden');
	}
	ById('kun-telefon4').value = strdef(kunde.telefon4);

	if (typeof kunde.email != 'undefined' && kunde.email != "") {
		ById('trKunEmail').classList.remove('is-hidden');
		ById('fldKunEmail').innerHTML = strdef(kunde.email);
	} else {
		ById('trKunEmail').classList.add('is-hidden');
	}
	ById('kun-email').value = strdef(kunde.email);

	if (typeof kunde.email2 != 'undefined' && kunde.email2 != "") {
		ById('trKunEmail2').classList.remove('is-hidden');
		ById('fldKunEmail2').innerHTML = strdef(kunde.email2);
	} else {
		ById('trKunEmail2').classList.add('is-hidden');
	}
	ById('kun-email2').value = strdef(kunde.email2);
	//
	str = "";
	if (typeof kunde.kunde != 'undefined' && kunde.kunde == true) {
		ById('fldkunde').checked = true;
		ById('tagKunde').classList.remove("is-hidden");
	} else {
		ById('fldkunde').checked = false;
		ById('tagKunde').classList.add("is-hidden");

	}
	if (typeof kunde.lieferant != 'undefined' && kunde.lieferant == true) {
		ById('fldlieferant').checked = true;
		ById('tagFirma').classList.remove("is-hidden");
	} else {
		ById('fldlieferant').checked = false;
		ById('tagFirma').classList.add("is-hidden");
	}
	if (typeof kunde.notar != 'undefined' && kunde.notar == true) {
		ById('fldnotar').checked = true;
		ById('tagNotar').classList.remove("is-hidden");
	} else {
		ById('fldnotar').checked = false;
		ById('tagNotar').classList.add("is-hidden");
	}
	if (typeof kunde.mitarbeiter != 'undefined' && kunde.mitarbeiter == true) {
		ById('fldmitarbeiter').checked = true;
		ById('tagMitarbeiter').classList.remove("is-hidden");
	} else {
		ById('fldmitarbeiter').checked = false;
		ById('tagMitarbeiter').classList.add("is-hidden");
	}



	// -------------- Daten info 

	ById('txtkundatchange').innerHTML = "Stand: " + checkValue(kunde.datchange);
	ById('txtkunuser').innerHTML = "Erfasst von: " + checkValue(kunde.user);


	//
	let transactions = getTransactionsforCustomer(kunde.id);
	//console.log(transactions);
	if (transactions.length > 0) {
		ById('sectionKunZE').classList.remove("is-hidden");
		ById('taTbl').classList.remove("is-hidden");

		ById('taTblBody').innerHTML = "";
		let saldo = 0;
		for (let ti = 0; ti < transactions.length; ti++) {
			let str = "";
			let zeile = document.createElement('tr');
			str += "<td class='has-text-left'>" + transactions[ti].date + "</td>";
			str += "<td class='has-text-left'>" + transactions[ti].text + "</td>";
			str += "<td class='has-text-right'>" + formatBetrag(transactions[ti].value) + "</td>";
			zeile.innerHTML = str;
			ById('taTblBody').appendChild(zeile);
			saldo += transactions[ti].value;
		}
		let str = "";
		let zeile = document.createElement('tr');
		str += "<td class='has-text-left'>" + "</td>";
		str += "<td class='has-text-left'>" + "<strong>Saldo </strong>" + "</td>";
		str += "<td class='has-text-right'> <strong>" + formatBetrag(saldo) + " </strong></td>";
		zeile.innerHTML = str;
		ById('taTblBody').appendChild(zeile);
	} else {
		ById('sectionKunZE').classList.add("is-hidden");
		ById('taTbl').classList.add("is-hidden");
	}
	if (anzeige.show == "kunde") {
		ById('paneKommentare').classList.add('is-hidden');
		let comments = DM.commentDM.filter(el => el.kunde == anzeige.kundeid &&
			(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
		fillComments(comments);
	}

}
function updateDPBenutzer() {
	ById('dpcommentTo-content').innerHTML = "";
	el1 = document.createElement('div');
	el1.className = 'dropdown-item';
	el1.id = "cbe-999";
	el1.innerHTML = "Alle";
	ById('dpcommentTo-content').appendChild(el1);
	DM.benutzerDM.forEach((el, index) => {
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "cbe-" + index;
		el1.innerHTML = el.name;
		ById('dpcommentTo-content').appendChild(el1);
	});
}
function dropdownKunAnrede() {
	ById('dpKunAnrede-content').innerHTML = "";
	ById('dpKunAnrede2-content').innerHTML = "";
	//let anredeliste = konfiguration.get('Anrede');
	el1 = document.createElement('div');
	el1.className = 'dropdown-item';
	el1.id = "kan-keine";
	el1.innerHTML = "keine Anrede";
	ById('dpKunAnrede-content').appendChild(el1);
	el2 = document.createElement('div');
	el2.className = 'dropdown-item';
	el2.id = "ka2-keine";
	el2.innerHTML = "keine Anrede";
	ById('dpKunAnrede2-content').appendChild(el2);

	for (i = 0; i < DM.anredeliste.length; i++) {
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "kan-" + i;
		el1.innerHTML = DM.anredeliste[i];
		ById('dpKunAnrede-content').appendChild(el1);
		el2 = document.createElement('div');
		el2.className = 'dropdown-item';
		el2.id = "ka2-" + i;
		el2.innerHTML = DM.anredeliste[i];
		ById('dpKunAnrede2-content').appendChild(el2);
	}

}
function editProjekt() {
	ById('modalProjektChange').classList.add('is-active');
	anzeige.edit = true;
	ipcRenderer.send('display:save', anzeige);
}
function saveProjekt() {
	ById('modalProjektChange').classList.remove('is-active');
	anzeige.edit = false;
	ipcRenderer.send('display:save', anzeige);
	let projekt = new Object();
	if (ById('prj-bautraeger').value != "") { projekt.bautraeger = ById('prj-bautraeger').value; }
	if (ById('prj-name').value != "") { projekt.name = ById('prj-name').value; }
	if (ById('prj-prjnummer').value != "") { projekt.prjnummer = ById('prj-prjnummer').value; }
	if (ById('prj-beschreibung').value != "") { projekt.beschreibung = ById('prj-beschreibung').value; }
	if (ById('prj-bauleiter').value != "") { projekt.bauleiter = ById('prj-bauleiter').value; }
	if (ById('prj-iban').value != "") { projekt.iban = formatIBAN(ById('prj-iban').value); }
	if (ById('prj-bic').value != "") { projekt.bic = ById('prj-bic').value; }
	if (ById('prj-bank').value != "") { projekt.bank = ById('prj-bank').value; }
	if (ById('prj-ibanE').value != "") { projekt.ibanE = formatIBAN(ById('prj-ibanE').value); }
	// if (ById('prj-bicE').value != "") { projekt.bicE = ById('prj-bicE').value; }
	// if (ById('prj-bankE').value != "") { projekt.bankE = ById('prj-bankE').value; }
	projekt.id = ById('prj-id').value;
	projekt.bearbeiter = user;
	if (projekt.id == "NEU") {
		projekt.type = "Projekt";
		ipcRenderer.send('edit:new', projekt);
	} else {
		ipcRenderer.send('projekt:change', projekt);
	}
}
function getNewIdProjekt() {
	ipcRenderer.send('projekt:getNewID', "");
}
function showArcVer(idObj) {
	let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH &&
		el.status !== CONSTANTS.ARCHIV &&
		el.typ == CONSTANTS.ARCHVERTRAG);
	ById('tableBodyArcVerDetails').innerHTML = "";
	ById('btnArcVerExport').classList.remove('is-hidden');

	if (typeof vertraege != 'undefined') {
		let totalbetrag = 0, totaloffen = 0, totalbezahlt = 0;

		cLen = vertraege.length;
		for (ve = 0; ve < cLen; ve++) {
			let zeile = document.createElement('tr');
			str = "";
			str += "<td class='has-text-left'>" + vertraege[ve].vernr + "</td>"; // Nummer
			str += "<td class='has-text-left'>" + vertraege[ve].name + "</td>"; // Nehmer
			str += "<td class='has-text-left'>" + vertraege[ve].gegenstand + "</td>"; // ObjeKt
			str += "<td class='has-text-right'>" + formatBetrag(vertraege[ve].betrag) + "</td>"; // BEtragg
			col = (vertraege[ve].bezahlt > 0) ? green : black;
			str += "<td class='has-text-right'  style='color: " + col + "'>" + formatBetrag(vertraege[ve].bezahlt) + "</td>"; // Betrag
			col = (vertraege[ve].offenerbetrag > 0) ? red : black;
			str += "<td class='has-text-right'  style='color: " + col + "'>" + formatBetrag(vertraege[ve].offenerbetrag) + "</td>"; // Datum
			zeile.innerHTML = str;
			ById('tableBodyArcVerDetails').appendChild(zeile);
			totalbetrag += vertraege[ve].betrag;
			totaloffen += vertraege[ve].offenerbetrag;
			totalbezahlt += vertraege[ve].bezahlt;

		}

		ById('fldArcVerBetrag').innerHTML = formatBetrag(totalbetrag);
		ById('fldArcVerOffen').innerHTML = formatBetrag(totaloffen);
		ById('fldArcVerBezahlt').innerHTML = formatBetrag(totalbezahlt);


	}

}
function showArbeitskorb(modus) {
	anzeige.show = modus;
	ipcRenderer.send('display:save', anzeige);


	// ---- Arbeitskorb1 --------------------------------------------

	ById('paneArbeitskorbTitle').innerHTML = "<h1 class='title is-3'>Meine Mitteilungen</h1>";
	let comments = DM.commentDM.filter(el =>
		typeof el.datei == 'undefined' && typeof el.eintrag != 'undefined' && el.empf != "Alle" && typeof el.empf != 'undefined' && (el.empf == DM.username || el.verfasser == DM.username));

	comments.sort((a, b) => {
		return b.datum > a.datum ? 1 : -1;
	}
	);


	ById('panelArbeitskorbList').innerHTML = "";

	comments.forEach(com => {
		if (checkComment(com)) {

			d1 = document.createElement('div');
			d1.className = 'card-content';
			d1.id = "akb-" + com.id;
			ById('panelArbeitskorbList').appendChild(d1);

			d2 = document.createElement('div');
			d2.className = 'msg-header';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-from';
			d3.innerHTML = "Von: " + com.verfasser;
			d2.appendChild(d3);
			d3 = document.createElement('span');
			d3.className = 'msg-timestamp';
			d3.innerHTML = formatDateTime(com.datum);
			d2.appendChild(d3);

			let empf = typeof com.empf != 'undefined' ? com.empf : "Alle";
			d2 = document.createElement('div');
			d2.className = 'msg-header';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-to';
			d3.innerHTML = "An: " + empf;
			d2.appendChild(d3);

			d2 = document.createElement('div');
			d2.className = 'msg-subject';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-subject';
			d3.innerHTML = "<strong>" + getEintrag(com) + "</strong>";
			d2.appendChild(d3);

			d2 = document.createElement('div');
			d2.className = 'msg-subject';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-subject';
			d3.innerHTML = markdown.toHTML(com.eintrag);
			d2.appendChild(d3);

			d2 = document.createElement('p');
			d1.appendChild(d2);
		} else {
			//	console.log("Comment target ist gelöscht.", com)
		}

	});
	if (comments.length == 0) {
		d1 = document.createElement('div');
		d1.className = 'card-content';
		ById('panelArbeitskorbList').appendChild(d1);

		d2 = document.createElement('div');
		d2.className = 'msg-subject';
		d1.appendChild(d2);
		d3 = document.createElement('span');
		d3.className = 'msg-subject';
		d3.innerHTML = "Aktuell keine Mitteilungen.";
		d2.appendChild(d3);
	}

	ById('panelArbeitskorbList').addEventListener('click', function (event) {
		let node = event.target.closest('.card-content');
		i = node.id.substring(4, node.id.length);
		let comment = DM.commentDM.find(el => el.id == i);
		commentShowTarget(comment);
	});
	// ---- Arbeitskorb2 --------------------------------------------

	ById('paneArbeitskorb2Title').innerHTML = "<h1 class='title is-3'>Mitteilungen an Alle</h1>";
	comments = DM.commentDM.filter(el =>
		typeof el.datei == 'undefined' && typeof el.eintrag != 'undefined' && (typeof el.empf == 'undefined' || el.empf == "Alle"));

	comments.sort((a, b) => {
		return b.datum > a.datum ? 1 : -1;
	}
	);

	ById('panelArbeitskorb2List').innerHTML = "";

	comments.forEach(com => {
		if (checkComment(com)) {
			d1 = document.createElement('div');
			d1.className = 'card-content';
			d1.id = "akb-" + com.id;
			ById('panelArbeitskorb2List').appendChild(d1);

			d2 = document.createElement('div');
			d2.className = 'msg-header';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-from';
			d3.innerHTML = "Von: " + com.verfasser;
			d2.appendChild(d3);
			d3 = document.createElement('span');
			d3.className = 'msg-timestamp';
			d3.innerHTML = formatDateTime(com.datum);
			d2.appendChild(d3);

			let empf = typeof com.empf != 'undefined' ? com.empf : "Alle";
			d2 = document.createElement('div');
			d2.className = 'msg-header';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-to';
			d3.innerHTML = "An: " + empf;
			d2.appendChild(d3);

			d2 = document.createElement('div');
			d2.className = 'msg-subject';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-subject';
			d3.innerHTML = "<strong>" + getEintrag(com) + "</strong>";
			d2.appendChild(d3);

			d2 = document.createElement('div');
			d2.className = 'msg-subject';
			d1.appendChild(d2);
			d3 = document.createElement('span');
			d3.className = 'msg-subject';
			d3.innerHTML = markdown.toHTML(com.eintrag);
			d2.appendChild(d3);

			d2 = document.createElement('p');
			d1.appendChild(d2);
		} else {
			//console.log("Comment target ist gelöscht.", com)
		}

	});
	if (comments.length == 0) {
		d1 = document.createElement('div');
		d1.className = 'card-content';
		ById('panelArbeitskorb2List').appendChild(d1);

		d2 = document.createElement('div');
		d2.className = 'msg-subject';
		d1.appendChild(d2);
		d3 = document.createElement('span');
		d3.className = 'msg-subject';
		d3.innerHTML = "Aktuell keine Mitteilungen.";
		d2.appendChild(d3);
	}

	ById('panelArbeitskorb2List').addEventListener('click', function (event) {
		let node = event.target.closest('.card-content');
		i = node.id.substring(4, node.id.length);
		let comment = DM.commentDM.find(el => el.id == i);
		commentShowTarget(comment);

	});

	// ---- Arbeitskorb3 --------------------------------------------

	ById('paneArbeitskorb3Title').innerHTML = "<h1 class='title is-3'>Hinweise</h1>";

	comments = systemComments();

	ById('panelArbeitskorb3List').innerHTML = "";

	comments.forEach(com => {
		d1 = document.createElement('div');
		d1.className = 'card-content';
		d1.id = com.id;
		ById('panelArbeitskorb3List').appendChild(d1);

		d2 = document.createElement('div');
		d2.className = 'msg-subject';
		d1.appendChild(d2);
		d3 = document.createElement('span');
		d3.className = 'msg-subject';
		d3.innerHTML = "<strong>" + getEintrag(com) + "</strong>";
		d2.appendChild(d3);

		d2 = document.createElement('div');
		d2.className = 'msg-subject';
		d1.appendChild(d2);
		d3 = document.createElement('span');
		d3.className = 'msg-subject';
		d3.innerHTML = markdown.toHTML(com.eintrag);
		d2.appendChild(d3);

		d2 = document.createElement('p');
		d1.appendChild(d2);

	});

	ById('panelArbeitskorb3List').addEventListener('click', function (event) {
		let node = event.target.closest('.card-content');
		let i = node.id.substring(6, node.id.length);
		let typ = node.id.substring(0, 5);
		switch (typ) {
			case "akrec":
				anzeige.rechnungid = i;
				anzeige.refid = i;
				anzeige.show = "rechnung";
				break;
			default:
		}
		anzeige.menu = "arbeitskorb";
		ipcRenderer.send('display:save', anzeige);
		updateAnzeige();
	});

}
function getEintrag(com) {
	switch (com.modus) {
		case "rechnung":
			let rechnung = DM.rechnungenDM.find(el => parseInt(el.id) == parseInt(com.refid));
			eintrag = "Rechnung " + rechnung.rechnr;
			break;
		case "projekt":
			let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(com.refid));
			eintrag = "Projekt " + projekt.prjnummer + " " + projekt.name;
			break;
		case "RNBOPL":
			eintrag = CONSTANTS.TITLE_RNBOPL;
			break;
		case "RNBENTWURF":
			eintrag = CONSTANTS.TITLE_RNBENTWURF;
			break;
		case "RNBALLRECH":
			eintrag = CONSTANTS.TITLE_RNBALLRECH;
			break;
		case "RNBHISTRECH":
			eintrag = CONSTANTS.TITLE_RNBHISTRECH;
			break;
		case "provlist":
			eintrag = CONSTANTS.TITLE_PROVLIST;
			break;
		case "provlisterfasst":
			eintrag = CONSTANTS.TITLE_PROVLISTERFASST;
			break;
		case "provlistfreigegeben":
			eintrag = CONSTANTS.TITLE_PROVLISTFREIGEGEBEN;
			break;
		case "provlistabgelehnt":
			eintrag = CONSTANTS.TITLE_PROVLISTABGELEHNT;
			break;
		default:
			eintrag = "";
	}
	return eintrag;
}
function systemComments() {
	let syscom = new Array();
	let jetzt = new Date();
	let rechnungen = DM.rechnungenDM.filter(el => el.status == CONSTANTS.VERSCHICKT);
	for (let re = 0; re < rechnungen.length; re++) {
		rechnr = checkValue(rechnungen[re].rechnr);
		betrag = checkValueN(rechnungen[re].betrag);
		zahlungseingang = checkValueN(rechnungen[re].zahlungseingang);
		fdat = checkValue(rechnungen[re].faelligdat);
		let ct = 0;
		if (string2Date(fdat).valueOf() < jetzt.valueOf()) {
			let eintrag = "Die Rechnung mit der ReNr. " + rechnr + " war am " + formatDatum(fdat) + " fällig.";
			ct += rechnungen[re].id;
			let comment = {
				"id": "akrec-" + ct,
				"eintrag": eintrag,
				"datum": new Date(),
				"verfasser": "Muster",
				"modus": "rechnung",
				"empf": "Alle"
			}
			comment.refid = rechnungen[re].id;

			syscom.push(comment);
		}
	}
	syscom.sort((a, b) => {
		return b.datum > a.datum ? 1 : -1;
	});
	if (syscom.length == 0) {
		let comment = {
			"id": "no",
			"eintrag": "Keine Hinweise",
			"datum": new Date(),
			"verfasser": "Muster",
			"modus": "",
			"empf": "Alle"
		}
		syscom.push(comment);
	}

	return syscom;

}
function showPROV(modus) {
	anzeige.show = modus;
	let provisionen;
	switch (modus) {
		case "provlist":
			ById('panePROVTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_PROVLIST + "</h1>";
			provisionen = DM.provisionDM.filter(el => el.status != CONSTANTS.TRASH);
			break;
		case "provlisterfasst":
			ById('panePROVTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_PROVLISTERFASST + "</h1>";
			provisionen = DM.provisionDM.filter(el => el.status != CONSTANTS.TRASH && el.status == 0);
			break;
		case "provlistfreigegeben":
			ById('panePROVTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_PROVLISTFREIGEGEBEN + "</h1>";
			provisionen = DM.provisionDM.filter(el => el.status != CONSTANTS.TRASH && el.status == 1);
			break;
		case "provlistabgelehnt":
			ById('panePROVTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_PROVLISTABGELEHNT + "</h1>";
			provisionen = DM.provisionDM.filter(el => el.status != CONSTANTS.TRASH && el.status == 2);
			break;
		default:
			return;
	}
	ipcRenderer.send('display:save', anzeige);
	ById('PanelTblPROV').classList.remove('is-hidden');
	let data = new Array();
	// ------------------------------------------------------------------------
	for (let pro = 0; pro < provisionen.length; pro++) {
		let rechnung = DM.rechnungenDM.find(el => el.id == provisionen[pro].rechnung);
        //&& checkYear(el.rechdat, filterYear))
		if(typeof(rechnung) !='undefined'){
	    let vertrag = DM.vertragDM.find(el => el.id == rechnung.vertrag);
	    let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
		let anteilstr = provisionen[pro].anteil == 0 ? "" : formatProzent(provisionen[pro].anteil / 100);
		let rechanteil = anteilstr == "" ? "" : formatBetrag(parseFloat(rechnung.netto) * provisionen[pro].anteil / 100);
		let rechnetto = formatBetrag(rechnung.netto);
		let gesamtprov = parseFloat(rechnung.netto) * provisionen[pro].anteil / 100 + provisionen[pro].betrag
		let status = provisionen[pro].status;
		let dataus = provisionen[pro].dataus;
		let kommentar = checkValue(provisionen[pro].kommentar);
		let	txtprj = checkValue(rechnung.txtprj);
	    let objektnr = checkValue(rechnung.objektnr);
	    let	txtobj = checkValue(rechnung.txtobj);
		if(checkYear(rechnung.rechdat,filterYear)){
			console.log("J", rechnung.rechnr);
				let dataset = new Array();
				dataset.push(
					provisionen[pro].id,
					rechnung.rechnr,
					txtprj,
					objektnr,
					txtobj,	
					kunde.vorname + " " + kunde.name,
					provisionen[pro].mitarbeiter,
					anteilstr,
					rechnetto,
					rechanteil,
					formatBetrag(provisionen[pro].betrag),
					formatBetrag(gesamtprov),
					kommentar,
					formatDatum(dataus),
					status
				);

				data.push(dataset);
			}
		}
	}
	// ------------------------------------------------------------------------

	if (typeof provtable != 'undefined') {
		provtable.clear();
		provtable.rows.add(data).draw();
		let summe = 0;

		provtable.rows({ search: 'applied' }).data().each(function (value, index) {
			//	console.log("rows ", value, index);
			summe += convPreisField(value[11]);
		});
		ById('fldPROVGesamtsumme').innerHTML = "Gesamtsumme der angezeigten Liste: " + formatBetrag(summe);


	} else {
		// Wird neu angelegt
		provtable = $('#tblprov').DataTable({
			data: data,
			"iDisplayLength": 100,
			columns: [
				{ title: "ID" },
				{ title: "RG-Nr", "width": "8em" },
				{ title: "Projekt", "width": "8em" },
				{ title: "Obj.Nr", "width": "8em" },
				{ title: "Einheit", "width": "8em" },
				{ title: "RG-Empf", "width": "6em", "searchable":true },
				{
					title: "Name", "searchable": true
				},
				{ title: "Prov.Anteil", "width": "6em" },
				{ title: "Rech.Netto", "width": "6em" },
				{ title: "Anteil", "width": "6em" },
				{ title: "Prov.Betrag", "width": "6em" },
				{ title: "Gesamt Prov.", "width": "6em" },
				{ title: "Kommentar" },
				{
					title: "Datum Auszahlung", "width": "6em"
				},
				{
					title: "Status",
					render: function (data, type, row) {
						if (data == 0) {
							return "<a class='button is-small  is-info' style='width:6em;''href='#'>" + CONSTANTS.STATUSPROV[data] + "</a>";
						}
						if (data == 1) {
							return "<a class='button is-small  is-success' style='width:6em;''href='#'>" + CONSTANTS.STATUSPROV[data] + "</a>";
						}
						if (data == 2) {
							return "<a class='button is-small  ' style='width:6em;''href='#'>" + CONSTANTS.STATUSPROV[data] + "</a>";
						}
						if (data == 3) {
							return "<a class='button is-small  ' style='width:6em;''href='#'>" + CONSTANTS.STATUSPROV[data] + "</a>";
						}
						return "";
					}
					, "width": "8em"

				}
			]
		});
		let summe = 0;
		provtable.rows({ search: 'applied' }).data().each(function (value, index) {
			//	console.log("rows ", value, index);
			summe += convPreisField(value[11]);
		});
		ById('fldPROVGesamtsumme').innerHTML = "Gesamtsumme der angezeigten Liste: " + formatBetrag(summe);

		provtable.on('click', 'tr td', function () {
			let rowindex = provtable.row(this).index();
			let colindex = provtable.column(this).index();
			let data = provtable.row(this).data();
			editRowProv(data);
		});

		provtable.on('search.dt', function () {
			//console.log("Search ", provtable.search());
			let summe = 0;
			provtable.rows({ search: 'applied' }).data().each(function (value, index) {
				//	console.log("rows ", value, index);
				summe += convPreisField(value[11]);
			});
			ById('fldPROVGesamtsumme').innerHTML = "Gesamtsumme der angezeigten Liste: " + formatBetrag(summe);

		});

		provtable.column(0).visible(false);


	}
	// ------------------------------------------------------------------------

	ById('btnsPROV').innerHTML = "";
	btn = document.createElement('button');
	btn.className = 'button is-light';
	btn.id = 'btnExportExcelPROV';
	btn.innerHTML = "Export Excel";
	ById('btnsPROV').appendChild(btn);
	ById('btnExportExcelPROV').addEventListener('click', function (e) {
		WIN = electron.remote.getCurrentWindow();
		let data = provtable.rows({ search: 'applied' }).data().toArray();
		let vis = provtable.columns().visible();
		let name = provtable.columns().header().toArray();
		let headerAll = new Array();
		for (i = 0; i < name.length; i++) {
			headerAll.push(name[i].innerHTML);

		}
		let headerVis = new Array();

		for (i = 0; i < name.length; i++) {
			if (vis[i]) {
				headerVis.push(name[i].innerHTML);
			}
		}
		let exportdata = new Array();
		Object.values(data).forEach(row => {
			let zeile = new Object();
			for (i = 0; i < vis.length; i++) {
				if (vis[i]) {
					switch (headerAll[i]) {
						case "Prov.Anteil":
							zeile[headerAll[i]] = row[i] == "" ? "" : parseFloat(convProzentField(row[i]) / 100);
							break;
						case "Datum":
							zeile[headerAll[i]] = string2Date(row[i]);
							break;
						case "Rech.Netto":
						case "Prov.Betrag":
						case "Gesamt Prov.":
						case "Anteil":
						case "Netto":
						case "Betrag":
						case "Brutto":
						case "Offen":
							zeile[headerAll[i]] = row[i] == "" ? "" : parseFloat(convPreisField(row[i]));
							break;
						case "Status":
							zeile[headerAll[i]] = CONSTANTS.STATUSPROV[row[i]];
							break;
						default:
							zeile[headerAll[i]] = removeSpace(row[i]);
					}
				}
			}
			exportdata.push(zeile);

		});
		writeExcel(headerVis, exportdata);
	});


		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnPROVFilterYear1';
	    if ( ( filterYear & ( 1 << 0) ) >0) {
				// Bit 1 ist gesetzt 
				btn.classList.remove('is-light');
		}
		btn.innerHTML = getYear().toString();	
		ById('btnsPROV').appendChild(btn);
		ById('btnPROVFilterYear1').addEventListener('click', function (e) {
		   filterYear ^= (1<<0);		
		   ById('btnPROVFilterYear3').classList.add('is-light');
		   showPROV(modus);
		});


		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnPROVFilterYear2';
	    if ( ( filterYear & ( 1 << 1) ) >0) {
				// Bit 2 ist gesetzt 
				btn.classList.remove('is-light');
		}
		btn.innerHTML = (getYear()-1).toString();	
		ById('btnsPROV').appendChild(btn);
		ById('btnPROVFilterYear2').addEventListener('click', function (e) {
		   filterYear ^= (1<<1);		
		   ById('btnPROVFilterYear3').classList.add('is-light');
		   showPROV(modus);
		});

		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnPROVFilterYear3';
	    if (  filterYear  == 0) {
				btn.classList.remove('is-light');
		}
		btn.innerHTML = "Alle";
		ById('btnsPROV').appendChild(btn);
		ById('btnPROVFilterYear3').addEventListener('click', function (e) {
		   filterYear = 0 ;		

		   ById('btnPROVFilterYear1').classList.add('is-light');
		   ById('btnPROVFilterYear2').classList.add('is-light');
		   showPROV(modus);
		});

	let comments = DM.commentDM.filter(el => el.modus == modus &&
		(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
	fillComments(comments);

}
function showOPL(modus) {
	anzeige.show = modus;
	ipcRenderer.send('display:save', anzeige);
	let data = new Array();
	let aktconf = new Object();;
	defconf = [
		false,
		false, 	/* 1 spnr   */
		true, 	/* 2 spempf */
		true,   /* 3 spprj  */
		true,   /* 4 spbeschreibung */
		true, 	/* 5 sptyp */
		true, 	/* 6 spname */
		true,	/* 7 speinheit*/
		true,   /* 8 Objeknr */
		true,   /* 9 Kostenstelle */
		true,	/* 10 spredat*/
		true,	/* 11 spnetto*/
		true,	/* 12 spbrutto*/
		true,	/* 13 spfdat*/
		true,	/* 14 spoffen*/
		false,	/* 15 spmdat*/
		false,	/* 16 spmstufe*/
		true,	/* 17 Prov. Berechtigter*/
		true,	/* 18 Prov. Betrag*/
		true,	/* 19 spstatus*/
	];
	aktconf = defconf;
	if (typeof DM.conf[modus] != 'undefined') {
		aktconf = DM.conf[modus];
	} else {
		console.log(DM.conf);

		DM.conf[modus] = defconf;
		ipcRenderer.send('conf:change', DM.conf);
	}
	let spmdat = 15, spmstufe = 16, spnr = 1, spempf = 2, spprj = 3, spbeschreibung = 4, sptyp = 5, spname = 6, speinheit = 7,
		spkst = 8, spobjnr = 9, spredat = 10, spnetto = 11, spbrutto = 12, spfdat = 13, spoffen = 14, spstatus = 19,
		sppbr = 17, sppbt = 18;

	let color = "", str = "", debitor = "", rechnr = "", betreff = "", netto = 0, betrag = 0, zahlungseingang = 0, fdat = "", rdat = "", offen = 0;
	switch (modus) {
		case "RNBHISTRECH":
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status == CONSTANTS.BEZAHLT && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_RNBHISTRECH + "</h1>";
			//aktconf = DM.conf.RNBHISTRECH;
			if (typeof aktconf == 'undefined' || aktconf.length == 0) {
				aktconf = defconf;
				aktconf[spnetto] = false;
				aktconf[spmdat] = false;
				aktconf[spmstufe] = false;
				aktconf[spoffen] = false;
			}
			break;
		case "RNBNEU":
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>Neue Rechnung anlegen</h1>";
			break;
		case "RNBALLRECH":
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_RNBALLRECH + "</h1>";
			//	aktconf = DM.conf.RNBALLRECH;
			if (typeof aktconf == 'undefined' || aktconf.length == 0) {
				aktconf = defconf;
				aktconf[spnetto] = false;
				aktconf[spmdat] = false;
				aktconf[spmstufe] = false;
			}
			break;
		case "RNBENTWURF":
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status == CONSTANTS.ENTWURF && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_RNBENTWURF + "</h1>";
			//aktconf = DM.conf.RNBENTWURF;
			if (typeof aktconf == 'undefined' || aktconf.length == 0) {
				aktconf = defconf;
				aktconf[spoffen] = false;
			}
			break;
		case "RNBOPL":
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status == CONSTANTS.VERSCHICKT && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>" + CONSTANTS.TITLE_RNBOPL + "</h1>";
			//aktconf = DM.conf.RNBOPL;
			if (typeof aktconf == 'undefined' || aktconf.length == 0) {
				aktconf = defconf;
				aktconf[spnetto] = false;
				aktconf[spmdat] = false;
				aktconf[spmstufe] = false;
			}
			break;
		default:
			rechnungen = DM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.BEZAHLT && checkYear(el.rechdat, filterYear));
			ById('paneOPLTitle').innerHTML = "<h1 class='title is-3'>nn</h1>";

	}

	let jetzt = new Date();
	if (modus != "RNBNEU") {
		let aktrownumber = -1;
		for (let re = 0; re < rechnungen.length; re++) {
			str = "";
			let showrow = true;
			let mdat = "", txtmstufe = "";
			if (checkValue(rechnungen[re].m1dat) != "") {
				mdat = checkValue(rechnungen[re].m1dat)
			}
			if (checkValue(rechnungen[re].m2dat) != "") {
				mdat = checkValue(rechnungen[re].m2dat)
			}
			if (checkValue(rechnungen[re].m3dat) != "") {
				mdat = checkValue(rechnungen[re].m3dat)
			}
			txtmstufe = checkValue(rechnungen[re].mahnstufe);
			rechnr = checkValue(rechnungen[re].rechnr);
			kostenstelle = checkValue(rechnungen[re].kostenstelle);
			txtprj = checkValue(rechnungen[re].txtprj);
			txtbeschreibung = checkValue(rechnungen[re].txtbeschreibung);
			txtobj = checkValue(rechnungen[re].txtobj);
			txtkunde = checkValue(rechnungen[re].txtkunde);
			objektnr = checkValue(rechnungen[re].objektnr);
			fdat = checkValue(rechnungen[re].faelligdat);
			rdat = checkValue(rechnungen[re].rechdat);
			betreff = checkValue(rechnungen[re].betreff);
			betrag = checkValueN(rechnungen[re].betrag);
			netto = checkValueN(rechnungen[re].netto);
			let txtrechtyp = checkValue(rechnungen[re].rechtyp);
			zahlungseingang = checkValueN(rechnungen[re].zahlungseingang);
			if (rechnungen[re].id == anzeige.rechnungid) {
				color = 'has-background-info-light';
				aktrownumber = re;
			} else {
				color = '';
			}
			//console.log(betrag, zahlungseingang);
			offen = parseFloat(betrag) - parseFloat(zahlungseingang);
			let txtprojekt = "";
			let txtobjekt = "";
			let txtname = "";
			let provnames = "";
			let provnamesCount = 0, provwert = 0;
			let vertrag = DM.vertragDM.find(el => el.id == rechnungen[re].vertrag && el.status != CONSTANTS.TRASH);
			if (typeof vertrag != 'undefined') {
				// let objekt = DM.objekteDM.find(el => parseInt(el.id) == parseInt(vertrag.objekt));
				// if (typeof objekt != 'undefined') {
				// 	txtobjekt = objekt.text + " " + objekt.objnummer;
				// 	let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(objekt.kunde));
				// 	if (typeof kunde != 'undefined') {
				// 		txtkunde = kunde.vorname + " " + kunde.name;
				// 		if (typeof kunde.name2 != 'undefined' && kunde.name2 != "") {
				// 			txtkunde += " & " + kunde.vorname2 + " " + kunde.name2
				// 		}
				// 	}
				// 	let projekt = DM.projekteDM.find(el => parseInt(el.id) == parseInt(objekt.projekt));
				// 	txtprojekt = projekt.name;
				// 	txtbeschreibung = typeof projekt.beschreibung != 'undefined' ? projekt.beschreibung : "";
				// }
				let rgempf = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
				if (typeof rgempf != 'undefined') {
					debitor = checkValue(rgempf.vorname) + " " + checkValue(rgempf.name);
				} else {
					debitor = "";
				}

				let provs = DM.provisionDM.filter(el => el.rechnung == rechnungen[re].id && el.status != CONSTANTS.TRASH);

				if (typeof provs != 'undefined') {
					provs.forEach(prov => {
						if (provnamesCount == 0) {
							provnames = prov.mitarbeiter;
						} else {
							provnames += ", " + prov.mitarbeiter;

						}
						provnamesCount += 1;
						provwert += parseFloat(netto) * parseFloat(prov.anteil) / 100 + prov.betrag;
					});

				}
				if (vertrag.typ == CONSTANTS.VERTRAGALLG) {
					let dataset = new Array();
					dataset.push(rechnungen[re].id, rechnr,
						debitor,
						txtprj, txtbeschreibung,
						txtrechtyp,
						txtkunde,
						txtobj, objektnr,
						kostenstelle,
						formatDatum(rdat),
						formatBetrag(netto),
						formatBetrag(betrag),
						rechnungen[re].status + formatDatum(fdat),
						formatBetrag(offen),
						formatDatum(mdat),
						txtmstufe,
						provnames,
						formatBetrag(provwert),
						rechnungen[re].status,

					);
					data.push(dataset);
				}
			}

		}
		// jQuery DataTable
		// https://stackoverflow.com/questions/38599493/jquery-datatable-data-disappears-after-sorting
		//
		// https://datatables.net/manual/tech-notes/3
		//
		if (typeof rnbtable != 'undefined') {
			rnbtable.clear();
			rnbtable.rows.add(data).draw();
			if (aktrownumber >= 0) {
				$(rnbtable.cells().nodes()).removeClass('highlight');
				$(rnbtable.row(aktrownumber).nodes()).addClass('highlight');
			}


		} else {
			rnbtable = $('#tblrnb').DataTable({
				data: data,
				"iDisplayLength": 50,
				columnDefs: [{ targets: [11,12,14,18], className: 'has-text-right' },
				{ targets: [5], className: 'has-text-centered' }],
				columns: [
					{ title: "ID" },
					{ title: "RG-Nr" },
					{
						title: "Empfänger",
						"searchable": true
					},

					{ title: "Projekt" },
					{ title: "Beschreibung" },
					{
						title: "Typ",
						"searchable": false
					},
					{ title: "Name" },
					{ title: "Einheit" },
					{ title: "Obj.Nr" },
					{ title: "KST" },
					{
						title: "Rech.Datum",
						type: 'date-eu'
					},
					{
						title: "Netto"
					},
					{ title: "Brutto" },
					{
						title: "Fällig",
						type: 'date-fdat',
						render: function (data, type, row) {
							let fdat = data.substring(data.length - 10, data.length);
							let stat = data.substring(0, data.indexOf(fdat));
							col = (string2Date(fdat).valueOf() < jetzt.valueOf() && stat != CONSTANTS.BEZAHLT && stat != CONSTANTS.ENTWURF) ? red : black;
							return '<span style="color:' + col + '">' + fdat + '</span>';
						}
					},
					{
						title: "Offen",
						render: function (data, type, row) {
							let col = parseFloat(convPreisField(data)) > 0 ? red : '';
							return '<span style="color:' + col + '">' + data + '</span>';

						}
					},
					{ title: "Letzte Mahnung" },
					{ title: "Mahnstufe" },
					{ title: "Prov.Berechtigter" },
					{ title: "Prov.Betrag" },
					{
						title: "Status",
						render: function (data, type, row) {
							if (data == CONSTANTS.VERSCHICKT) {
								return "<a class='button is-small  is-warning' style='width:6em;''href='#'>" + CONSTANTS.VERSCHICKT + "</a>";
							}
							if (data == CONSTANTS.ENTWURF) {
								return "<a class='button is-small  is-info' style='width:6em;''href='#'>" + CONSTANTS.ENTWURF + "</a>";
							}
							if (data == CONSTANTS.BEZAHLT) {
								return "<a class='button is-small  is-success' style='width:6em;''href='#'>" + CONSTANTS.BEZAHLT + "</a>";
							}
							return "";
						}
					}


				]
			});


			rnbtable.on('click', 'tr td', function () {
				let rowindex = rnbtable.row(this).index();
				let colindex = rnbtable.column(this).index();
				let data = rnbtable.row(this).data();
				let msg = Object();
				if (typeof data != 'undefined') {
					msg.rechnungid = data[0];

					if (anzeige.show != 'rechnung') {
						msg.menu = anzeige.show;
					} else {
						msg.menu = anzeige.menu;

					}
				}
				console.log(colindex);
				if (colindex == 17 || colindex == 18) {
					editProvision(msg.rechnungid);
				} else {
					msg.show = "rechnung";
					ipcRenderer.send('show:rechnung', msg);
				}


			});
			// accessing the elements with same classname
			let elements = document.querySelectorAll('.toggle-vis');

			// adding the event listener by looping
			elements.forEach(element => {
				element.addEventListener('click', (e) => {
					e.preventDefault();
					// Get the column API object
					let colnr = e.target.getAttribute('data-column');
					let column = rnbtable.column(colnr);
					// Toggle the visibility
					column.visible(!column.visible());
					console.log(anzeige.show, DM.conf);

					aktconf = DM.conf[anzeige.show];
					aktconf[colnr] = column.visible();
					console.log(aktconf);

					DM.conf[anzeige.show][colnr] = column.visible();
					ipcRenderer.send('conf:change', DM.conf);
				});
			});
		}
		rnbtable.column(0).visible(false);
		for (i = 1; i < aktconf.length; i++) {
			rnbtable.column(i).visible(aktconf[i]);
		}



		elements = document.querySelectorAll('.rightadmin');
		elements.forEach(element => {
			let colnr = element.getAttribute('data-column');
			let column = rnbtable.column(colnr);
			if (!DM.roleleiter) {
				element.classList.add('is-hidden');
				column.visible(false);
			}

		});


	}
	//
	// Einstellungen abspeichern
	//
	saveconfSpalte(modus, aktconf);
	let BUTTONS = false;

	// Buttons anlegen
	ById('btnsOPL').innerHTML = "";
	if (modus == "RNBNEU") {
		ById('dpRnbEditObj-id').value = 0;
		ById('dpRnbEditPrj-id').value = 0;
		ById('dpRnbEditKnd-id').value = 0;
		ById('dpRnbEditKndObj-id').value = 0;
		if (BUTTONS) {
			let btns = document.createElement('buttons');
			btns.className = 'buttons';
			let btn = document.createElement('button');
			btn.className = 'button is-pulled-left is-light';
			btn.id = 'btnrechNeu';
			btn.innerHTML = "Blanko";
			btns.appendChild(btn);
			ById('btnsOPL').appendChild(btns);

		} else {
			let btn = document.createElement('a');
			btn.className = 'panel-block';
			btn.innerHTML = "Blanko";
			btn.id = 'btnrechNeu';
			ById('btnsOPL').appendChild(btn);


		}
		let zahltemplates = DM.zahltemplates.filter(el => el.aktiv == true &&
			el.name != CONSTANTS.POSMABV &&
			el.name != CONSTANTS.POSRESERVIERUNG &&
			el.name != CONSTANTS.POSTYP3);

		if (typeof zahltemplates != 'undefined') {
			zahltemplates.sort((a, b) => {
				return parseInt(a.zeile) - parseInt(b.zeile);
			});

			let aktzeile = 0;
			if (BUTTONS) {
				let btns = document.createElement('buttons');
				btns.className = 'buttons';
			} else {

			}
			zahltemplates.forEach(zt => {
				if (BUTTONS) {
					// if (aktzeile == 0) {
					// 	aktzeile = zt.zeile;
					// 	ById('btnsOPL').appendChild(btns);
					// }
					if (aktzeile == 0 || aktzeile != zt.zeile) {
						aktzeile = zt.zeile;
						btns = document.createElement('buttons');
						btns.className = 'buttons';
						ById('btnsOPL').appendChild(btns);
					}
					btn = document.createElement('button');
					btn.className = 'button is-pulled-left is-light';
					btn.id = 'rop' + zt.id;
					btn.innerHTML = zt.bezeichnung;
					btns.appendChild(btn);
				} else {
					let btn = document.createElement('a');
					btn.className = 'panel-block';
					btn.innerHTML = zt.bezeichnung;
					btn.id = 'rop' + zt.id;
					ById('btnsOPL').appendChild(btn);
				}
			});
			ById('btnsOPL').addEventListener('click', function (event) {
				id = parseInt(event.target.id.substring(3, event.target.id.length));
				if (event.target.id == 'btnrechNeu'){
						id = 0;
				}

				if ( !isNaN(id) ){
						ById('RnbEdittemplate').value = id;
						ById('RnbEditRechdat').value = getDatum(0);
						ById('RnbEditobjektnr').value = "";
						ById('RnbEditkostenstelle').value = "";
						ById('RnbEditprojektfrei').value = "";
						ById('RnbEditbeschreibungfrei').value = "";
						ById('RnbEditkunde').value = "";
						ById('RnbEditobjektfrei').value = "";
						ById('RnbEditRechNr').value = "";
						ById('RnbEditrechtyp').value = "";
						ById('RnbEditleistungszeitraum').value = "";
						ById('RnbEditFaelligdat').value = getDatum(7);
						newRechnungTemplate();
				}
			});
		}
	} else {
		let btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnrechCSV';
		btn.innerHTML = "Export CSV";
		ById('btnsOPL').appendChild(btn);
		ById('btnrechCSV').addEventListener('click', function (e) {
			WIN = electron.remote.getCurrentWindow();
			let data = rnbtable.rows({ search: 'applied' }).data().toArray();
			let vis = rnbtable.columns().visible();
			//console.log(data, vis);

			let exportdata = new Array();
			Object.values(data).forEach(row => {
				let n = 0;
				let zeile = new Array();
				for (i = 0; i < vis.length; i++) {
					if (vis[i]) {
						zeile[n] = '"' + removeSpace(row[i]) + '"';
						n += 1;
					}
				}
				exportdata.push(zeile);

			});
			exportdata = exportdata.map(row => row.join(',')).join('\n');
			let options = {
				title: 'CSV speichern',
				defaultPath: 'C:',
				buttonLabel: 'Als CSV speichern',
				filters: [
					{ name: 'CSV', extensions: ['csv'] },
					{ name: 'All Files', extensions: ['*'] }
				]
			};
			let filename = dialog.showSaveDialogSync(WIN, options);
			if (typeof filename !== 'undefined') {
				fs.writeFileSync(filename, exportdata);
				//openFileWithDefaultApp(filename);
			}

		});
		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnrechExcel';
		btn.innerHTML = "Export Excel";
		ById('btnsOPL').appendChild(btn);
		ById('btnrechExcel').addEventListener('click', function (e) {
			WIN = electron.remote.getCurrentWindow();
			let data = rnbtable.rows({ search: 'applied' }).data().toArray();
			let vis = rnbtable.columns().visible();
			let name = rnbtable.columns().header().toArray();
			let headerAll = new Array();
			for (i = 0; i < name.length; i++) {
				headerAll.push(name[i].innerHTML);

			}
			let headerVis = new Array();

			for (i = 0; i < name.length; i++) {
				if (vis[i]) {
					headerVis.push(name[i].innerHTML);
				}
			}
			let exportdata = new Array();
			Object.values(data).forEach(row => {
				let zeile = new Object();
				for (i = 0; i < vis.length; i++) {
					if (vis[i]) {
						switch (headerAll[i]) {
							case "Rech.Datum":
							case "Fällig":
							case "letzte Mahnung":
								zeile[headerAll[i]] = string2Date(row[i]);
								break;
							case "Netto":
							case "Offen":
							case "Prov.Betrag":
							case "Brutto":
								zeile[headerAll[i]] = parseFloat(convPreisField(row[i]));
								break;
							default:
								zeile[headerAll[i]] = removeSpace(row[i]);
						}
					}
				}
				exportdata.push(zeile);

			});
			writeExcel(headerVis, exportdata);
		});
		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnFilterYear1';
	    if ( ( filterYear & ( 1 << 0) ) >0) {
				// Bit 1 ist gesetzt 
				btn.classList.remove('is-light');
		}
		btn.innerHTML = getYear().toString();	
		ById('btnsOPL').appendChild(btn);
		ById('btnFilterYear1').addEventListener('click', function (e) {
		   filterYear ^= (1<<0);		
		   ById('btnFilterYear3').classList.add('is-light');
		   showOPL(modus);
		});


		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnFilterYear2';
	    if ( ( filterYear & ( 1 << 1) ) >0) {
				// Bit 2 ist gesetzt 
				btn.classList.remove('is-light');
		}
		btn.innerHTML = (getYear()-1).toString();	
		ById('btnsOPL').appendChild(btn);
		ById('btnFilterYear2').addEventListener('click', function (e) {
		   filterYear ^= (1<<1);		
		   ById('btnFilterYear3').classList.add('is-light');
		   showOPL(modus);
		});

		btn = document.createElement('button');
		btn.className = 'button is-light';
		btn.id = 'btnFilterYear3';
	    if (  filterYear  == 0) {
				btn.classList.remove('is-light');
		}
		btn.innerHTML = "Alle";
		ById('btnsOPL').appendChild(btn);
		ById('btnFilterYear3').addEventListener('click', function (e) {
		   filterYear = 0 ;		

		   ById('btnFilterYear1').classList.add('is-light');
		   ById('btnFilterYear2').classList.add('is-light');
		   showOPL(modus);
		});

	}
	let comments = DM.commentDM.filter(el => el.modus == modus &&
		(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
	fillComments(comments);

}
function btnText(){
		let returnstring = "";
			let jahr = getYear();
			switch(filterYear){
					case 1:
						returnstring = 'Rechnungsjahr: ' + jahr.toString();
						break;
					case 2:
						returnstring = 'Rechnungsjahr: ' + jahr.toString() +"/"+ (jahr-1).toString();
					break;
					default:
						returnstring = "Rechnungsjahr: Alle";
			}
		return returnstring;
}
function saveconfSpalte(modus, aktconf) {
	switch (modus) {
		case "RNBHISTRECH":
			DM.conf.RNBHISTRECH = aktconf;
			break;
		case "RNBALLRECH":
			DM.conf.RNBALLRECH = aktconf;
			break;
		case "RNBENTWURF":
			DM.conf.RNBENTWURF = aktconf;
			break;
		case "RNBOPL":
			DM.conf.RNBOPL = aktconf;
			break;
		default:
	}
	ipcRenderer.send('conf:change', DM.conf);
}
function showDarlehen(idObj) {
	let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.typ == CONSTANTS.DARLEHEN)
	ById('tableBodyDarlehenDetails').innerHTML = "";
	ipcRenderer.send('display:save', anzeige);
	let geber = new Array();
	let nehmer = new Array();
	let firmen = new Array();
	exportArr = new Array();
	if (typeof vertraege != 'undefined') {
		let totalsaldo = 0;
		cLen = vertraege.length;
		for (i = 0; i < cLen; i++) {
			let exportObj = new Object();

			if (geber.indexOf(vertraege[i].kunde) < 0) {
				geber.push(vertraege[i].kunde);
			}
			if (nehmer.indexOf(vertraege[i].firma) < 0) {
				nehmer.push(vertraege[i].firma);
			}
			let firma = firmen.find(el => el.name == vertraege[i].kunde);
			if (typeof firma == 'undefined') {
				f = new Object();
				f.name = vertraege[i].kunde;
				firmen.push(f);
			}
			firma = firmen.find(el => el.name == vertraege[i].firma);
			if (typeof firma == 'undefined') {
				f = new Object();
				f.name = vertraege[i].firma;
				firmen.push(f);
			}
			let kontosaldo = 0;
			let zeile = document.createElement('tr');
			darkonto = parseInt(CONSTANTS.DAROFFSET + vertraege[i].id);
			let buchungen = DM.buchungen.filter(el => el.konto == darkonto);
			//console.log(buchungen);
			if (typeof buchungen != 'undefined') {
				for (bu = 0; bu < buchungen.length; bu++) {
					kontosaldo += buchungen[bu].betrag;
					//console.log(kontosaldo, buchungen[bu].betrag);
				}
			}
			kunde = DM.kundenDM.find(re => re.id == vertraege[i].kunde);
			if (typeof kunde != 'undefined') {
				strkunde = kunde.vorname + " " + kunde.name;
				if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
					(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
					strkunde += " und " + kunde.vorname2 + " " + kunde.name2;
				}
			} else {
				strkunde = "n.n.";

			}

			firma = firmen.find(el => el.name == vertraege[i].firma);


			firma = DM.kundenDM.find(re => re.id == vertraege[i].firma);
			if (typeof firma != 'undefined') {
				strfirma = firma.vorname + " " + firma.name;
				if ((typeof firma.vorname2 !== 'undefined' && firma.vorname2 !== "") &&
					(typeof firma.name2 !== 'undefined' && firma.name2 !== "")) {
					strfirma += " und " + firma.vorname2 + " " + firma.name2;
				}
			} else {
				strfirma = "n.n.";

			}
			exportObj.nr = vertraege[i].vernr;
			exportObj.kunde = strkunde;
			exportObj.firma = strfirma;
			exportObj.datum = formatDateObject(vertraege[i].sdat);
			exportObj.betrag = formatBetrag(kontosaldo);
			exportArr.push(exportObj);
			str = "";
			str += "<td class='has-text-left'>" + vertraege[i].vernr + "</td>"; // Geber
			str += "<td class='has-text-left'>" + strkunde + "</td>"; // Geber
			str += "<td class='has-text-left'>" + strfirma + "</td>"; // Nehmer
			str += "<td class='has-text-right'>" + formatDateObject(vertraege[i].sdat) + "</td>"; // Datum
			str += "<td class='has-text-right'>" + formatBetrag(kontosaldo) + "</td>"; // Betrag
			zeile.innerHTML = str;
			ById('tableBodyDarlehenDetails').appendChild(zeile);
			totalsaldo += kontosaldo;
			//console.log(totalsaldo);
		}
		ById('fldDarlehenSummeAll').innerHTML = formatBetrag(totalsaldo);
	}
	//tableBodyDarlehenDetails2
	ById('tableBodyDarlehenDetails2').innerHTML = "";
	geber.forEach(element => {
		let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV &&
			el.typ == CONSTANTS.DARLEHEN && parseInt(el.kunde) == parseInt(element));
		let sum = 0;
		for (i = 0; i < vertraege.length; i++) {
			darkonto = parseInt(CONSTANTS.DAROFFSET + vertraege[i].id);
			let buchungen = DM.buchungen.filter(el => el.konto == darkonto);
			//console.log(buchungen);
			if (typeof buchungen != 'undefined') {
				for (bu = 0; bu < buchungen.length; bu++) {
					sum += buchungen[bu].betrag;
					//console.log(kontosaldo, buchungen[bu].betrag);
				}
			}
			let firma = firmen.find(el => el.name == vertraege[i].kunde);
			firma.verbindlichkeiten = sum;
		}
	});
	nehmer.forEach(element => {
		let vertraege = DM.vertragDM.filter(el => el.status !== CONSTANTS.TRASH && el.status !== CONSTANTS.ARCHIV && el.typ == CONSTANTS.DARLEHEN && el.firma == element);
		let sum = 0;
		for (i = 0; i < vertraege.length; i++) {
			darkonto = parseInt(CONSTANTS.DAROFFSET + vertraege[i].id);
			let buchungen = DM.buchungen.filter(el => el.konto == darkonto);
			//console.log(buchungen);
			if (typeof buchungen != 'undefined') {
				for (bu = 0; bu < buchungen.length; bu++) {
					sum += buchungen[bu].betrag;
					//console.log(kontosaldo, buchungen[bu].betrag);
				}
			}
			let firma = firmen.find(el => el.name == vertraege[i].firma);
			firma.forderungen = sum;
		}
	});
	firmen.forEach(element => {
		kunde = DM.kundenDM.find(re => re.id == element.name);
		if (typeof kunde != 'undefined') {
			strkunde = kunde.vorname + " " + kunde.name;
			if ((typeof kunde.vorname2 !== 'undefined' && kunde.vorname2 !== "") &&
				(typeof kunde.name2 !== 'undefined' && kunde.name2 !== "")) {
				strkunde += " und " + kunde.vorname2 + " " + kunde.name2;
			}
		} else {
			strkunde = "n.n.";

		}
		let zeile = document.createElement('tr');
		forderungen = typeof element.forderungen == 'undefined' ? 0 : element.forderungen;
		verbindlichkeiten = typeof element.verbindlichkeiten == 'undefined' ? 0 : element.verbindlichkeiten;
		str = "";
		str += "<td class='has-text-left'>" + strkunde + "</td>"; // Firma
		str += "<td class='has-text-right'>" + formatBetrag(forderungen) + "</td>"; // Forderungen
		str += "<td class='has-text-right'>" + formatBetrag(verbindlichkeiten) + "</td>"; // Verbindlichkeiten
		str += "<td class='has-text-right'>" + formatBetrag(forderungen - verbindlichkeiten) + "</td>"; // Verbindlichkeiten
		zeile.innerHTML = str;
		ById('tableBodyDarlehenDetails2').appendChild(zeile);
	});
	if (anzeige.show == "darlehen") {
		ById('paneKommentare').classList.add('is-hidden');
		let comments = DM.commentDM.filter(el => el.projekt == anzeige.vertragid &&
			(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
		fillComments(comments);
	}

}
function showObjekt(idObj) {
	if (typeof idObj !== 'undefined' && idObj != 0) {
		let obj = DM.objekteDM.find(el => el.id == idObj);
		let kunde = DM.kundenDM.find(el => el.id == obj.kunde);
		let prj = DM.projekteDM.find(el => el.id == obj.projekt);
		let kaeufer = "";
		let str = "";
		if (typeof kunde != 'undefined') {
			//kaeufer = " (" + kunde.vorname + " " + kunde.name + ")";
			kaeufer = " " + kunde.name + "";
		}
		anzeige.einheitid = idObj;
		anzeige.projektid = obj.projekt;
		ById('obj-id').value = anzeige.einheitid;
		//		ById("prj2").checked = true;
		anzeige.menu = "";
		ipcRenderer.send('display:save', anzeige);
		//  titleObjekt ----------------------------------------
		ById('titleObjekt').innerHTML = prj.name + " " + obj.text + " " + obj.objnummer + ": " + kaeufer;
		// contentObjekt ----------------------------------------

		let revver = DM.vertragDM.find(el => el.projekt == obj.projekt && el.objekt == idObj
			&& el.typ == CONSTANTS.RESERVIERUNG && el.status != CONSTANTS.TRASH);
		let strmp = "", revstatus = "";
		let revbetrag = 0, revstat = false;
		if (typeof revver != 'undefined') {
			revstat = statusReserverierung(revver.id);
			if (revstat.erhalten == false) {
				//strmp = "noch nicht erhalten: " + formatBetrag(revver.betrag);
				strmp = formatBetrag(revver.betrag);
				revstatus = "vereinbart / noch nicht erhalten"
				col1 = red;

			} else {
				if (revstat.erstattet == false) {
					col1 = green;
					strmp = formatBetrag(obj.reservierungbezahlt) + "";
					revstatus = "erhalten"

				}
				if (revstat.erstattet == true) {
					col1 = black;
					//strmp = formatBetrag(obj.reservierungbezahlt+obj.reservierungerstattet) + "";
					strmp = "erstattet";
					revstatus = "";
				}

			}
			revbetrag = revver.betrag;

		} else {
			strmp = formatBetrag(0);
			revstatus = "keine Reservierung erfasst"

			col1 = red;

		}

		str = "";
		str += "<table class='table is-narrow'>";
		str += "<tr>";
		str += "<td><strong>Kaufpreis </strong></td><td class='has-text-right'>" + formatBetrag(obj.kaufpreis) + "</td>";
		str += "<td></td>";
		str += "</tr>";
		str += "<tr>";
		str += "<td><strong>Reservierungsgebühr</strong> </td><td class='has-text-right' style='color: " + col1 + "' >" + strmp + "</td>";
		str += "<td>" + revstatus + "</td>";
		str += "</tr>";

		str += "</table>";
		ById('contentObjekt').innerHTML = str;

		//  ----------------------------------------
		ById('panelObjektSub1').classList.remove('is-hidden');
		col1 = (obj.whghaben > 0) ? green : black;
		col2 = (obj.whgsoll > 0) ? red : black;

		str = "";


		str += "<tr>";
		col1 = (0 > 0) ? green : black;
		col2 = (0 > 0) ? red : black;
		let erhalten = 0, offen = 0;
		if (obj.reservierungbezahlt > 0) {
			erhalten = obj.reservierungbezahlt + obj.reservierungerstattet;
			revbetrag = revbetrag - obj.reservierungbezahlt;
			offen = revbetrag;





		}
		if (revstat.erhalten && !revstat.erstattet) {
			str += "<td><strong>Reservierung </strong></td><td class='has-text-right'>" + "" +
				"</td><td class='has-text-right' style='color: " + col1 + "'>" + formatBetrag(erhalten) +
				//	"</td><td class='has-text-right' style='color: " + col2 + "'>" + formatBetrag(offen) + "</td>";
				"</td><td class='has-text-right' style='color: " + col2 + "'>" + "</td>";
			str += "</tr>";

			str += "<tr class='has-background-white-bis'>";
			str += "<td colspan='4'>" + "" + "</td>";
			str += "</tr>";
		}


		str += "<tr>";
		str += "<td><strong>Kaufvertrag </strong></td><td class='has-text-right'>" + formatBetrag(obj.kaufpreis) +
			"</td><td class='has-text-right' style='color: " + col1 + "'>" + formatBetrag(obj.whghaben) +
			"</td><td class='has-text-right' style='color: " + col2 + "'>" + formatBetrag(obj.whgsoll) + "</td>";
		str += "</tr>";
		col1 = (obj.mehrhaben > 0) ? black : black;
		col2 = (obj.mehrsoll > 0) ? black : black;
		str += "<tr>";
		str += "<td><strong>Mehrkosten </strong><td class='has-text-right'>" + formatBetrag(obj.mehrsoll + obj.mehrhaben) +
			"</td><td class='has-text-right' style='color: " + col1 + "'>" + formatBetrag(obj.mehrhaben) +
			"</td><td class='has-text-right' style='color: " + col2 + "'>" + formatBetrag(obj.mehrsoll) + "</td>";
		str += "</tr>";
		col1 = (obj.gutschriftenhaben > 0) ? black : black;
		col2 = (obj.gutschriften > 0) ? black : black;
		str += "<tr>";
		str += "<td><strong>Gutschriften </strong><td class='has-text-right'>" + formatBetrag(obj.gutschriften + obj.gutschriftenhaben) +
			"</td><td class='has-text-right' style='color: " + col1 + "'>" + formatBetrag(obj.gutschriftenhaben) +
			"</td><td class='has-text-right' style='color: " + col2 + "'>" + formatBetrag(obj.gutschriften) + "</td>";
		str += "</tr>";

		ById('tableBodyObjektDetails').innerHTML = str;


		ById('fldObjektDetailSummeBetrag').innerHTML = "<strong>" +
			formatBetrag(obj.kaufpreis + obj.mehrsoll + obj.mehrhaben - obj.gutschriften - obj.gutschriftenhaben) + "</strong>";

		col = (obj.whghaben + obj.mehrhaben - obj.gutschriftenhaben > 0) ? green : black;
		ById('fldObjektDetailSummeHaben').innerHTML = "<strong style='color: " + col + "'>" +
			formatBetrag(obj.whghaben + obj.mehrhaben - obj.gutschriftenhaben) + "</strong>";

		col = (obj.whgsoll + obj.mehrsoll - obj.gutschriften > 0) ? red : black;
		ById('fldObjektDetailSummeOffen').innerHTML = "<strong style='color: " + col + "'>" +
			formatBetrag(obj.whgsoll + obj.mehrsoll - obj.gutschriften) + "</strong>";


		//  ----------------------------------------
		if (anzeige.show == "einheit") {
			ById('paneKommentare').classList.add('is-hidden');
			let comments = DM.commentDM.filter(el => el.einheit == anzeige.einheitid &&
				(el.empf == "Alle" || el.empf == DM.username || el.verfasser == DM.username));
			fillComments(comments);
		}
	}
}
function showProjekt(idPrj) {
	if (typeof idPrj === 'undefined') {
		idPrj = anzeige.projektid;
	}
	if (idPrj == -1) {
		return;
	}
	prj = DM.projekteDM.find(el => el.id == idPrj);
	//console.log(idPrj, prj);
	anzeige.menu = "";
	anzeige.projektid = prj.id;
	ById('prj-id').value = anzeige.projektid;
	ipcRenderer.send('display:save', anzeige);
	updateBtnVorlagen(idPrj);

	editObjekt();
	obj = DM.objekteDM.filter(el => el.projekt == prj.id && el.status != CONSTANTS.TRASH);
	obj.sort((a, b) => {
		return parseInt(a.objnummer) - parseInt(b.objnummer);
	});
	ById('prj-box').classList.remove('is-hidden');
	//console.log(anzeige)
	if (typeof prj.beschreibung == 'undefined') {
		strtmp = prj.name;
	} else {
		strtmp = prj.name + " - " + prj.beschreibung;
	}
	ById('btnvorlageP1').setAttribute('prj', idPrj);
	ById('projekt-title').innerHTML = strtmp;
	let str;
	str = "";
	str += "<table class='table is-narrow'>";

	str += "<tr>";
	str += "<td><strong>Bauträger</strong></td><td>" + setValueofId('prj-bautraeger', prj.bautraeger) + "</td>";
	str += "</tr>";

	str += "<tr>";
	str += "<td><strong>Bauleiter*in</strong></td><td>" + setValueofId('prj-bauleiter', prj.bauleiter) + "</td>";
	str += "</tr>";



	str += "<tr>";
	str += "<td><strong>Projektnummer</strong></td><td>" + setValueofId('prj-prjnummer', prj.prjnummer) + "</td>";
	str += "</tr>";



	str += "<td><strong>IBAN EIN</strong></td><td>" + setValueofId('prj-ibanE', prj.ibanE) + "</td>";
	str += "</tr>";

	str += "<tr>";
	str += "<td><strong>IBAN AUS</strong></td><td>" + setValueofId('prj-iban', prj.iban) + "</td>";
	str += "</tr>";

	str += "<tr>";
	str += "<td><strong>Bank</strong></td><td>" + setValueofId('prj-bank', prj.bank) + "</td>";
	str += "</tr>";

	str += "<tr>";
	str += "<td><strong>BIC </strong></td><td>" + setValueofId('prj-bic', prj.bic) + "</td>";
	str += "</tr>";

	str += "</table>";
	ById('txtPrjPos1').innerHTML = str;

	setValueofId('prj-beschreibung', prj.beschreibung);
	setValueofId('prj-name', prj.name);
	let totalres = 0;
	let cLen = obj.length;
	if (typeof cLen !== 'undefined' && cLen > 0) {
		ById('prj-vertab').classList.remove('is-hidden');
		ById('prj-vertab-body').innerHTML = "";

		for (i = 0; i < cLen; i++) {
			let kunde = DM.kundenDM.find(el => el.id == obj[i].kunde);
			let kaeufer = "";
			if (typeof kunde != 'undefined') {
				//console.log(kunde);
				//kaeufer = " (" + kunde.vorname + " " + kunde.name + ")";
				kaeufer = " " + kunde.name + "";
			}

			let zeile = document.createElement('tr');
			if (typeof obj[i].kaufpreis == 'undefined') {
				strtmp = "";
			} else {
				strtmp = formatBetrag(obj[i].kaufpreis);
			}
			let col;
			if (obj[i].whgsoll == 0 && obj[i].kaufpreis > 0) {
				backcol = greenlaurel;
				//backcol =  white; 
			} else {
				backcol = white;
			}

			str = "";
			str += "<td class='has-text-left' style='background-color: " + backcol + "'>" + obj[i].text + " " + obj[i].objnummer + kaeufer + "</td>";
			let restr = formatBetrag(0);
			totalres += obj[i].reservierungbezahlt + obj[i].reservierungerstattet;
			if (obj[i].reservierungbezahlt > 0) {
				if (obj[i].reservierungbezahlt + obj[i].reservierungerstattet == 0) {
					col = black;
					restr = "erstattet";
				} else {
					col = green;
					restr = formatBetrag(obj[i].reservierungbezahlt + obj[i].reservierungerstattet);
				}
			}
			str += "<td class='has-text-right' style='color: " + col + "'>" + restr + "</td>";
			str += "<td class='has-text-right' style='background-color: " + backcol + "'>" + strtmp + "</td>"; // Kaufpreis
			col = (obj[i].whgsoll > 0) ? red : black;
			if (obj[i].whgsoll == 0 && obj[i].kaufpreis > 0) {
				strtmp = "bezahlt";
				txtalign = "has-text-centered";
			} else {
				strtmp = formatBetrag(obj[i].whgsoll);
				txtalign = "has-text-right";
			}
			str += "<td class='" + txtalign + "'  style='background-color: " + backcol + "; color: " + col + "'>" + strtmp + "</td>"; //  Wohnungen Soll
			col = (obj[i].whghaben > 0) ? green : black;
			str += "<td class='has-text-right' style='background-color: " + backcol + ";color: " + col + "' >" + formatBetrag(obj[i].whghaben) + "</td>"; //  Wohnungen Haben
			col = (obj[i].rechoffen > 0) ? red : black;
			str += "<td class='has-text-right' style='background-color: " + backcol + "; color: " + col + ";'>" + formatBetrag(obj[i].rechoffen) + "</td>"; // Rechnungen offen
			col = (obj[i].mehrsoll > 0) ? red : black;
			str += "<td class='has-text-right'  style='background-color: " + white + ";color: " + col + "'>" + formatBetrag(obj[i].mehrsoll) + "</td>"; // Mehrkosten 
			col = (obj[i].mehrhaben > 0) ? green : black;
			str += "<td class='has-text-right' style='background-color: " + white + ";color: " + col + "'>" + formatBetrag(obj[i].mehrhaben) + "</td>"; // Mehrkosten erhalten
			col = (obj[i].gutschriften > 0) ? red : black;
			str += "<td class='has-text-right' style='background-color: " + white + ";color: " + col + "'>" + formatBetrag(obj[i].gutschriften) + "</td>"; // Gutschriften 
			col = (obj[i].gutschriftenhaben > 0) ? green : black;
			str += "<td class='has-text-right'  style='background-color: " + white + ";color: " + col + "'>" + formatBetrag(obj[i].gutschriftenhaben) + "</td>"; // Gutschriften ausgezahlt
			col = (obj[i].habengesamt > 0) ? green : black;
			str += "<td class='has-text-right' style='background-color: " + white + "; color: " + col + "'>" + formatBetrag(obj[i].habengesamt) + "</td>"; // Gesamtbetrag
			zeile.innerHTML = str;
			ById('prj-vertab-body').appendChild(zeile);

			// Info: Gesamtbetrag errechnet sich aus Kaufpreis + Mehrkosten erhalten – Gutschriften ausgezahlt
		}
		// Summenzeile
		let zeile = document.createElement('tr');
		str = "";
		str += "<td class='has-text-left'> <strong> Summen </strong></td>";
		//str += "<td class='has-text-right'></td>";
		col = (totalres > 0) ? green : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(totalres) + "</strong></td>"; //  Wohnungen Soll
		str += "<td class='has-text-right'> <strong>" + formatBetrag(prj.kaufpreis) + "</strong></td>"; // Kaufpreis
		col = (prj.whgsoll > 0) ? red : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.whgsoll) + "</strong></td>"; //  Wohnungen Soll
		col = (prj.whghaben > 0) ? green : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.whghaben) + "</strong></td>"; //  Wohnungen Haben
		col = (prj.rechoffen > 0) ? red : black;
		str += "<td class='has-text-right' style='background-color: " + white + "; ' ><strong style='color: " + col + ";'>" + formatBetrag(prj.rechoffen) + "</strong></td>"; // Rechnungen offen
		col = (prj.mehrsoll > 0) ? red : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.mehrsoll) + "</strong></td>"; // Mehrkosten 
		col = (prj.mehrhaben > 0) ? green : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.mehrhaben) + "</strong></td>"; // Mehrkosten erhalten
		col = (prj.gutschriften > 0) ? red : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.gutschriften) + "</strong></td>"; // Gutschriften 
		str += "<td class='has-text-right'><strong>" + formatBetrag(prj.gutschriftenhaben) + "</strong></td>"; // Gutschriften ausgezahlt
		col = (prj.habengesamt > 0) ? green : black;
		str += "<td class='has-text-right'><strong style='color: " + col + "'>" + formatBetrag(prj.habengesamt) + "</strong></td>"; // Haben Gesamt
		zeile.innerHTML = str;
		ById('prj-vertab-body').appendChild(zeile);

	} else {
		ById('prj-vertab').classList.add('is-hidden');
	}
	if (anzeige.show == "projekt") {
		ById('paneKommentare').classList.add('is-hidden');
		let comments = DM.commentDM.filter(el => el.projekt == anzeige.projektid &&
			el.empf == "Alle");
		fillComments(comments);
	}
}
function editProjektPosten() {
	ById('modalProjektPostenChange').classList.add('is-active');
	ById('tabprjpos').innerHTML = "";
	ById('rech-id').value = anzeige.rechnungid;
	addElementToRechnungsPos();
}
function editObjekt() {
	ById('tabObj').innerHTML = "";

	// Neuer Eintrag
	let neuerEintrag = {
		id: -1,
		objnummer: "",
		text: "Wohnung",
		betrag: ""
	}
	//console.log(neuerEintrag);
	editObjEintrag(neuerEintrag, true, false);
	let hr = document.createElement('hr');
	ById('tabObj').appendChild(hr);
	// Alle Objekte zu diesem Projekt holen
	//objDM = datenmodell.get('Objekte');
	if (typeof DM.objekteDM !== 'undefined') {
		obj = DM.objekteDM.filter(el => el.projekt == ById('prj-id').value && el.status != CONSTANTS.TRASH);
		//console.log(obj);
		obj.sort((a, b) => {
			return parseInt(a.objnummer) - parseInt(b.objnummer);
		});

		cLen = obj.length;
		for (i = 0; i < cLen; i++) {
			let add = true, del = true;
			vertrag = DM.vertragDM.find(el => el.typ == CONSTANTS.KAUFVERTRAG && el.status != CONSTANTS.TRASH && el.objekt == obj[i].id);
			if (typeof vertrag != 'undefined') {
				add = false;
				del = false;
			}
			editObjEintrag(obj[i], add, del);
		}
	}


}
function editObjEintrag(eintrag, add, del) {
	let zeile = document.createElement('p');
	let str = "";

	let btn1id = "buttonobjadd-" + eintrag.id;
	let btn2id = "buttonobjdel-" + eintrag.id;

	//Spalte 1 - Beschreibung
	let textid = "objText-" + eintrag.id;
	str += "<div class='field is-horizontal is-grouped'>";

	add == true ? disabled = "" : disabled = "disabled";


	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' type='text'" +
		"id='" + textid + "' value='" + eintrag.text + "' " + disabled + " placeholder='Beschreibung' onpressenter='" + btn1id + "' style='width: 8em; margin-right:1em;' >" +
		"</p>" +
		"</div>";
	//Spalte 2 - Kuezel
	let objnrid = "objKuerzel-" + eintrag.id;

	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' type='text' " + disabled + " id='" + objnrid + "' value='" + eintrag.objnummer + "' onpressenter='" + btn1id + "' placeholder='Nummer' style='width: 8em; margin-right:1em;' >" +
		"</p>" +
		"</div>";
	//Spalte 3 - Betrag
	let betragid = "objBetrag-" + eintrag.id;
	let betragstr = eintrag.betrag == "" ? "" : formatBetrag(eintrag.betrag);
	str += "<div class='field'>" +
		"<p class='control'>" +
		"<input class='input' " + disabled + " type='text'" +
		"id='" + betragid + "' value='" + betragstr + "' formater='betrag' placeholder='Betrag' onpressenter='" + btn1id + "' style='width: 8em; margin-right:1em;'>" +
		"</p>" +
		"</div>";

	//Spalte4 - Buttons	
	//let btn1id = "buttonobjadd-" + eintrag.id;
	//let btn2id = "buttonobjdel-" + eintrag.id;
	str += "<div class='field '>" +
		"<div class='field-body'>";
	if (add == true) {
		str += "<button class='button is-info is-light'  id='" + btn1id + "'>OK</button>";
	}
	if (del == true) {
		str += "<button class='button is-warning is-light'  id='" + btn2id + "'>DEL</button>";
	}
	str += "</div>";
	str += "</div>"; //Buttons	
	str += "</div>";
	zeile.innerHTML = str;
	ById('tabObj').appendChild(zeile);


	if (add == true) {
		ById(btn1id).addEventListener('click', function () {
			// "buttonrezeadd-"+.id;	
			// "buttonobjadd-"+.id;	
			index = this.id.substring(13, this.id.length);
			let obj = new Object();
			let objnrid = "objKuerzel-" + index;
			let textid = "objText-" + index;
			let betragid = "objBetrag-" + index;
			//console.log(index, datid, textid, betragid);
			obj.id = index;
			obj.text = ById(textid).value;
			obj.objnummer = ById(objnrid).value;
			obj.projekt = ById('prj-id').value;
			obj.betrag = convPreisField(ById(betragid).value);
			ipcRenderer.send('obj:change', obj);
		});
	}
	if (del == true) {
		ById(btn2id).addEventListener('click', function () {
			// "buttonobjdel-"+.id;	
			index = this.id.substring(13, this.id.length);
			ipcRenderer.send('obj:delete', index);

		});
	}


}


function nextStatus(status) {
	if (typeof status != 'undefined') {
		switch (status) {
			case CONSTANTS.NEU:
				return CONSTANTS.INARBEIT;
				break;
			case CONSTANTS.AKTIV:
				return CONSTANTS.ERLEDIGT;
				break;
			case CONSTANTS.INARBEIT:
				return CONSTANTS.ERLEDIGT;
				break;
			case CONSTANTS.ERLEDIGT:
				return CONSTANTS.ARCHIV;
				break;
			default:

		}

	} else {
		return CONSTANTS.NEU;
	}
}
function lastStatus(status) {
	if (typeof status != 'undefined') {
		switch (status) {
			case CONSTANTS.INARBEIT:
				return CONSTANTS.NEU;
				break;
			case CONSTANTS.AKTIV:
				return CONSTANTS.NEU;
				break;
			case CONSTANTS.ERLEDIGT:
				return CONSTANTS.INARBEIT;
				break;
			case CONSTANTS.ARCHIV:
				return CONSTANTS.INARBEIT;
				break;
			default:

		}

	} else {
		return CONSTANTS.NEU;
	}
}

function setStatusButtons(status) {
	if (typeof status != 'undefined') {

		switch (status) {
			case CONSTANTS.NEU:
				ById('btnDarStatusBack').classList.add('is-hidden');
				ById('btnDarStatusForwardText').innerHTML = "in Berarbeitung";
				break;
			case CONSTANTS.INARBEIT:
				ById('btnDarStatusForwardText').innerHTML = "Status auf erledigt setzen";
				ById('btnDarStatusBackText').innerHTML = "Status auf Neu setzen";
				break;
			case CONSTANTS.ERLEDIGT:
				ById('btnDarStatusForwardText').innerHTML = "Archivieren";
				ById('btnDarStatusBackText').innerHTML = "Wieder aktivieren";
				break;
			case CONSTANTS.ARCHIV:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBackText').innerHTML = "Wieder aktivieren";
				break;
			case CONSTANTS.AKTIV:
				ById('btnDarStatusForwardText').innerHTML = "Status auf erledigt setzen";
				ById('btnDarStatusBackText').innerHTML = "Status auf Neu setzen";
				break;
			default:
				ById('btnDarStatusForward').classList.add('is-hidden');
				ById('btnDarStatusBack').classList.add('is-hidden');

		}
	} else {
		ById('btnDarStatusForwardText').innerHTML = "Status Neu setzen";
		ById('btnDarStatusBack').classList.add('is-hidden');
	}

}
function checkValue(el) {
	if (typeof el != 'undefined' && el != "") {
		return el;
	} else {
		return "";
	}
}
function checkValueN(el) {
	if (typeof el != 'undefined' && el != "") {
		return el;
	} else {
		return 0;
	}
}

function newRechnungTemplate() {
	let template = DM.zahltemplates.find(el => el.id == ById('RnbEdittemplate').value);
	if (0) {
		//if (typeof template == "undefined" || template.bezug == true) {
		ById('RnbEditPrj').classList.remove("is-hidden");
		ById('RnbEditObj').classList.remove("is-hidden");
		ById('RnbEditKndObj').classList.remove("is-hidden");
		ById('modRnbEditBezug').value = 'true';

	} else {
		ById('RnbEditPrj').classList.add("is-hidden");
		ById('RnbEditObj').classList.add("is-hidden");
		ById('RnbEditKndObj').classList.add("is-hidden");
		ById('modRnbEditBezug').value = 'false';
	}
	if (typeof template != "undefined") {
		ById('RnbEditrechtyp').value = template.rechtyp;
	}
	ById('RnbEditRechNr').disabled = true;
	ById('dpRnbEditPrj-button').innerHTML = "ggf. auswählen";
	ById('dpRnbEditObj-button').innerHTML = "zuerst Projekt auswählen";
	ById('indpRnbEditKndObj').value = "ggf. auswählen";
	ById('indpRnbEditKnd').value = ""
	dbRnbEditPrjContent();
	ById('indpRnbEditKnd').value = ""


	ById('modRnbEdit-id').value = 0;
	ById('modRnbEdit').classList.add('is-active');
	anzeige.edit = true;
	ipcRenderer.send('display:save', anzeige);

}
function dbRnbEditPrjContent() {
	let projekte = DM.projekteDM.filter(el => el.id != 0 && el.status != CONSTANTS.TRASH);
	if (projekte.length > 0) {
		if (projekte.length > 2) {
			projekte.sort((a, b) => {
				let n = a.prjnummer.toUpperCase();
				let m = b.prjnummer.toUpperCase();
				return (n < m) ? -1 : (n > m) ? 1 : 0;
			});
			//DP Projekte füllen
		}
		ById('dpRnbEditPrj-content').innerHTML = "";
		projekte.forEach(projekt => {
			let el1 = document.createElement('div');
			el1.className = 'dropdown-item';
			el1.id = "rpe-" + projekt.id;
			el1.innerHTML = projekt.prjnummer + " " + projekt.name;
			ById('dpRnbEditPrj-content').appendChild(el1);
		});

	}

}
function initdpRnbEditKndObj(kunid) {
	if (typeof kunid != 'undefined') {
		if (kunid == 0) {
			ById('dpRnbEditKndObj-id').value = 0;
			ById('indpRnbEditKndObj').value = "bisher kein Käufer zugeordnet";

		} else {
			kun = DM.kundenDM.find(el => el.id == kunid);
			ById('dpRnbEditKndObj-id').value = kunid;
			ById('indpRnbEditKndObj').value = kun.vorname + " " + kun.name;
		}

	}
}
function editRowProv(data) {
	ById('modProvEditProvision').classList.add('is-active');
	anzeige.edit = true;
	ipcRenderer.send('display:save', anzeige)
	let provision = DM.provisionDM.find(el => el.id == data[0]);
	// ------------------
	ById('dpProvEdit-id').value = provision.id;
	ById('dpProvEdit-rechnetto').value = data[8];
	// ------------------
	ById('fldProvEditInfo').innerHTML = "";
	let fldlabel = document.createElement('label');
	fldlabel.className = 'label';
	fldlabel.innerHTML = "Provision für " + data[6] + " zur Rechnung " + data[1] + " Betrag (netto): " + data[8];
	ById('fldProvEditInfo').appendChild(fldlabel);
	fldlabel = document.createElement('label');
	fldlabel.className = 'label';
	fldlabel.innerHTML = "Erfasst durch: " + provision.erfasser;
	ById('fldProvEditInfo').appendChild(fldlabel);
	if (provision.freigeber != "" || (parseInt(provision.status) != 1 & parseInt(provision.status) != 2)) {
		fldlabel = document.createElement('label');
		fldlabel.className = 'label';
		switch (parseInt(provision.status)) {
			case 1:
				fldlabel.innerHTML = "Freigegeben durch: " + provision.freigeber;
				break;
			case 2:
				fldlabel.innerHTML = "Abgelehnt durch: " + provision.freigeber;
				break;
			default:
		}
		ById('fldProvEditInfo').appendChild(fldlabel);
	}

	// ------------------
	ById('inpProvEditanteil').value = data[7];
	// ------------------
	ById('inpProvEditbetrag').value = data[10];
	// ------------------
	let anteilstr = data[7] == "" ? formatProzent(0) : data[7];
	ById('inpProvEditgesamtinfo').innerHTML = "Provision Gesamt =  " + data[8]
		+ " * " + anteilstr + " + " + data[10] + " = ";
	ById('inpProvEditgesamtbetrag').innerHTML = data[11];
	// ------------------
	ById('inpProvEditkommentar').value= provision.kommentar;
	// ------------------
	ById('inpProvEditdataus').value = formatDatum(provision.dataus);
	// ------------------ DropDownStatus
	ById('dpProvEditStatus-button').innerHTML = CONSTANTS.STATUSPROV[provision.status];
	ById('dpProvEditStatus-id').value = provision.status;

	ById('dpProvEditStatus-content').innerHTML = "";

	CONSTANTS.STATUSPROV.forEach((el, index) => {
		el1 = document.createElement('div');
		el1.className = 'dropdown-item';
		el1.id = "dps-" + index;
		el1.innerHTML = el;
		ById('dpProvEditStatus-content').appendChild(el1);
	});


	//---------

}
function editProvision(id) {
	ById('modRnbEditProvision').classList.add('is-active');
	anzeige.edit = true;
	ipcRenderer.send('display:save', anzeige);

	ById('modRnbEditProvision-id').value = id;
	let provs = DM.provisionDM.filter(el => el.rechnung == id);
	// Daten zusammentragen
	let data = new Array();
	let dataset = new Array();

	if (typeof provs !== 'undefined') {
		provs.forEach(prov => {
			let set = new Array();
			set.push(prov.id, prov.mitarbeiter, prov.anteil, prov.betrag);
			data.push(set);
		});
	}
	dataset.push(-1, "", 0, 0);
	data.push(dataset);

	ById('tblEditProvisionHead').innerHTML = "";
	ById('tblEditProvisionHead').innerHTML = "<th>Mitarbeiter</th>	<th style='width:1em'>Anteil</th> 	<th style='width:1em'>Betrag</th>";

	ById('tblEditProvisionBody').innerHTML = "";
	data.forEach(el => {
		let tr = document.createElement('tr');
		let str = "";
		str += "<td class='pro-ma' prov-id=" + el[0] + ">" + el[1] + "</td>";
		str += "<td class='pro-anteil' style='width:2em'>" + formatProzent(el[2] / 100) + "</td>";
		str += "<td class='pro-betrag' style='width:2em'>" + formatBetrag(el[3]) + "</td>";
		tr.innerHTML = str;
		ById('tblEditProvisionBody').appendChild(tr);

	});
	ById('foundinfo').innerHTML = "";

	$('#tblEditProvision').editableTableWidget().provtableinput().find('td:first').focus();

}
function getDatafromTable() {
	let daten = new Array();
	let trs = "";

	trs = ById('tblEditProvisionBody').childNodes;
	trs.forEach(tr => {
		let datensatz = new Object();
		let tds = tr.childNodes;
		tds.forEach(td => {
			if (td.classList.contains('pro-id')) {
				datensatz.id = td.innerHTML;
			}
			if (td.classList.contains('pro-ma')) {
				datensatz.mitarbeiter = td.innerHTML;
				datensatz.id = td.getAttribute('prov-id');
			}
			if (td.classList.contains('pro-anteil')) {
				datensatz.anteil = convProzentField(td.innerHTML);
			}
			if (td.classList.contains('pro-betrag')) {
				datensatz.betrag = convProzentField(td.innerHTML);
			}
		});
		daten.push(datensatz);

	});

	return daten;

}
function checkComment(comment) {
	let check = 0;
	let status = 0;
	switch (comment.modus) {
		case "rechnung":
			let rechnung = DM.rechnungenDM.find(el => el.id == comment.refid);
			status = rechnung.status;
			break;
		case "projekt":
			let projekt = DM.projekteDM.find(el => el.id == comment.refid);
			status = projekt.status;
			break;
		case "vertrag":
			let vertrag = DM.vertragDM.find(el => el.id == comment.refid);
			status = vertrag.status;
			break;
		case "einheit":
			let objekt = DM.objekteDM.find(el => el.id == comment.refid);
			status = objekt.status;
			break;
		default:
			status = 0;
	}
	if (status == CONSTANTS.TRASH) {
		check = 0;
	} else {
		check = 1;
	}
	return check;
}
function commentShowTarget(comment) {
	if (typeof comment != 'undefined') {
		anzeige.show = comment.modus;
		switch (anzeige.show) {
			case "rechnung":
				anzeige.rechnungid = comment.refid;
				break;
			case "projekt":
				anzeige.projektid = comment.refid;
				break;
			case "vertrag":
				anzeige.vertragid = comment.refid;
				break;
			case "einheit":
				anzeige.einheitid = comment.refid;
				break;
			default:
		}
		anzeige.menu = "arbeitskorb";
		ipcRenderer.send('display:save', anzeige);
		updateAnzeige();
	}
}
function copyDataFields(objnr) {
	let objrech = DM.rechnungenDM.find(el => el.status != CONSTANTS.TRASH && el.objektnr == objnr);
	ById('RnbEditleistungszeitraum').value = typeof objrech.leistungszeitraum != 'undefined' ? objrech.leistungszeitraum : "";
	ById('RnbEditrechtyp').value = typeof objrech.rechtyp != 'undefined' ? objrech.rechtyp : "";
	ById('RnbEditkostenstelle').value = typeof objrech.kostenstelle != 'undefined' ? objrech.kostenstelle : "";
	ById('RnbEditprojektfrei').value = typeof objrech.txtprj != 'undefined' ? objrech.txtprj : "";
	ById('RnbEditbeschreibungfrei').value = typeof objrech.txtbeschreibung != 'undefined' ? objrech.txtbeschreibung : "";
	ById('RnbEditobjektfrei').value = typeof objrech.txtobj != 'undefined' ? objrech.txtobj : "";
	ById('RnbEditRechdat').value = checkValue(objrech.rechdat);
	ById('RnbEditRechNr').value = checkValue(objrech.rechnr);
	ById('RnbEditFaelligdat').value = checkValue(objrech.faelligdat);
	ById('RnbEditM1dat').value = checkValue(objrech.m1dat);
	ById('RnbEditM2dat').value = checkValue(objrech.m2dat);
	ById('RnbEditM3dat').value = checkValue(objrech.m3dat);
	ById('RnbEditMahnstufe').value = checkValue(objrech.mahnstufe);

	let vertrag = DM.vertragDM.find(el => parseInt(el.id) == parseInt(objrech.vertrag));
	let kunde = DM.kundenDM.find(el => parseInt(el.id) == parseInt(vertrag.kunde));
	//console.log(objrech, kunde);
	if (typeof kunde != 'undefined') {
		ById('RnbEditkunde').value = kunde.vorname + " " + kunde.name;
	} else {
		ById('RnbEditkunde').value = typeof objrech.txtkunde != 'undefined' ? objrech.txtkunde : "";
	}

}

function neueKundennummer() {
	let neueKundennummer = 0;
	while (neueKundennummer == 0) {
		neueKundennummer = Math.floor(Math.random() * 100001) + 100001;
		let kunde = DM.kundenDM.find(el => el.kundennummer == neueKundennummer);
		if (typeof kunde != 'undefined') {
			neueKundennummer = 0;
		}
	}
	return neueKundennummer;
}
//EOF

////
// Menü geht bei Update zu? Anzeige Update mit aufgeklappten Berechen
