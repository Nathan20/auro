const electron = require('electron');
const url = require('url');
const path = require('path');
const fs = require('fs');
const os = require('os');
const base = require('./modules/base.js');
// https://attacomsian.com/blog/nodejs-encrypt-decrypt-data
const { encrypt, decrypt } = require('./modules/crypt');
const { openFileWithDefaultApp, getDatum } = base;
const Store = require('./modules/store.js');
const Ablage = require('./modules/ablage.js');
const { app, BrowserWindow, Menu, nativeImage, dialog, ipcMain, shell } = electron;
let CONSTANTS = require('./modules/constants');
const { type } = require('os');
const contextMenu = require('electron-context-menu');
const { ok } = require('assert');
const { isBoolean } = require('util');
const { performance } = require('perf_hooks');
const bcrypt = require('bcryptjs');
const propertiesReader = require('properties-reader');
// https://github.com/weixiyen/messenger.js
const appVersion = electron.app.getVersion();
const { ImportedRootElementAttributes } = require('docx');
const { exit } = require('process');
const Main = require('electron/main');
const isRelative = (path) => !/^([a-z]+:)?[\\/]/i.test(path);

const Client = require('node-rest-client').Client;


let REST = false;

if (REST) {
		// https://www.npmjs.com/package/node-rest-client
		global.client = new Client();
		global.apibase = 'http://localhost:3000/v1/';
		//client.get(apibase+"vertraege/12", el => console.log(el));
}

// Variablen
let mainWindow;
let anzeige;
let SQL, SQLprop, USEFILES = true;
let user = "";
// Umgebung einstellen
process.env.NODE_ENV = 'production';
// properties
let properties;
let HELPMENU = true;
let PRGNAME = "";
let PRGMANDANT = 0;
let PRGCOLOR = "is-blue";
let PRGLOGO = "logo.jpg";
try {
		properties = propertiesReader('./extraResources/properties.ini');
		HELPMENU = properties.get('functions.menu.help');
		PRGNAME = properties.get('constants.prg.name');
		process.env.NODE_ENV = properties.get('modules.env.glob');
		SQLprop = properties.get('functions.database.sql');
		SQL = SQLprop;

		PRGLOGO = properties.get('constants.prg.logo');
		PRGCOLOR = properties.get('constants.prg.color');
		PRGMANDANT = properties.get('constants.prg.mandant');
		PRGRECHNR = properties.get('constants.prg.rechnungsnr');

} catch (e) {
		// Anweisungen für nicht festgelegte Fehlertypen
		console.log(e); // Objekt an die Fehler-Funktion geben
}
let configName= PRGMANDANT==0 ? 'user-preferences' : 'user-preferences'+PRGMANDANT;
// Instantiate Store class
let store = new Store({
		// We'll call our data file 'user-preferences'
		configName: configName,
		defaults: {
				windowBounds: { width: 1400, height: 600 },
				windowpos: { xoff: 10, yoff: 10 },
				datapath: __dirname + "\\..\\..\\..",
				templatepath: __dirname + "\\..\\..\\..",
				datamodell: __dirname + "\\..\\..\\..\\Datenbank.json",
		}
});
store.set("Init", 1);
let pfad = store.get('datapath');
debug("Debugmodus eingeschaltet");

contextMenu({
		prepend: (params, browserWindow) => [{
				label: 'Rainbow',
				// Only show it when right-clicking images
				visible: params.mediaType === 'image'
		}]
});

let MainDM = new Object();
// Listen for app to be ready

app.on('ready', function () {
		//	let { width, height } = store.get('windowBounds');
		//	let { xoff, yoff } = store.get('windowpos');
		// Create Window
		mainWindow = new BrowserWindow({
				//		width: width,
				//		height: height,
				//		x: xoff,
				//		y: yoff,
				backgroundColor: '#2e2c29',
				show: false,
				webPreferences: {
						nodeIntegration: true,
						enableRemoteModule: true
				}
		});
		mainWindow.once('ready-to-show', () => {
				mainWindow.show()
		})

		// The BrowserWindow class extends the node.js core EventEmitter class, so we use that API
		// to listen to events on the BrowserWindow. The resize event is emitted when the window size changes.
		mainWindow.on('resize', () => {
				// The event doesn't pass us the window size, so we call the `getBounds` method which returns an object with
				// the height, width, and x and y coordinates.
				let windowName = mainWindow.getTitle();
				if (windowName.toLowerCase().indexOf('login') == -1) {
						let { width, height } = mainWindow.getBounds();
						let [xoff, yoff] = mainWindow.getPosition();
						store.set('windowBounds', { width, height });
						store.set('windowpos', { xoff, yoff });
				}
		});
		mainWindow.on('move', () => {
				let windowName = mainWindow.getTitle();

				if (windowName.toLowerCase().indexOf('login') == -1) {
						let [xoff, yoff] = mainWindow.getPosition();
						store.set('windowpos', { xoff, yoff });
				}

		});
		// Test ob in der User Preference die Pfade enthalten sind
		process.platform == 'linux' ? user = process.env.USER : user = process.env.USERNAME;
		osuser = user;

		let filename = store.get('datamodell');
		// message = new Object();
		// message.name = "Datenbankinfo";
		// message.detail = "Filename: " + filename;
		// errorDisplay(message);

		//		log("150: "+filename);
		//		if (isRelative(filename) && typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
		//				filename = path.join(process.env.PORTABLE_EXECUTABLE_FILE, filename);
		//		}	
		//		log("154: "+filename);

		// message = new Object();
		// message.name = "Datenbankname";
		// message.detail = "Filename: " + filename;
		// errorDisplay(message);

		let status = 1;

		if (!fs.existsSync(filename)) {
				if (!onStartCheckDatenbank()) {
						datenablage.close();
						app.quit();
				}
		} else {
				global.datenmodell = new Store({
						filename: filename,
						defaults: {}
				});
		}

		if (typeof datenmodell.get("Vertraege") == 'undefined' || datenmodell.get("Vertraege") == 'undefined') {
				// Vertraege nicht da
				status = onStartCheckDatenbank();
		}
		if (status == 0) {
				//mainWindow.close();
				datenablage.close();
				app.quit();
		} else {
				let benutzerDM = datenmodell.get("Benutzer");
				let benutzer = benutzerDM.find(el => el.name.toLowerCase() == user.toLowerCase());
				if (typeof benutzer == 'undefined') {
						user = "";
				}
				if (user == "") {
						// Load html
						mainWindow.setTitle(PRGNAME + " Login             Windowsuser: " + osuser);
						mainWindow.setBounds({ x: 0, y: 0, width: 1000, height: 650 });
						mainWindow.center();
						mainWindow.loadURL(url.format({
								pathname: path.join(__dirname, 'login.html'),
								protocol: 'file:',
								slashes: true
						}));


				} else {
						initLogin();
				}


				// Quit App wenn Window geschlosen wird
				mainWindow.on('closed', function () {
						datenmodell.makebackup();
						app.quit();
				});
				// Build menu from template
				const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
				// Insert menu
				mainWindow.setMenu(mainMenu);
				//Menu.setApplicationMenu(mainMenu);
		}
		workerWin = new BrowserWindow({
				show: false,
				webPreferences: {
						nodeIntegration: true,
						enableRemoteModule: true
				}
		});
		workerWin.loadFile('worker.html');
		const workerMenu = Menu.buildFromTemplate(workerMenuTemplate);
		workerWin.setMenu(workerMenu);
		workerWin.toggleDevTools();

});

function initLogin() {
		mainWindow.loadURL(url.format({
				pathname: path.join(__dirname, 'mainWindow.html'),
				protocol: 'file:',
				slashes: true
		}));
		let { width, height } = store.get('windowBounds');
		let { xoff, yoff } = store.get('windowpos');
		mainWindow.setBounds({ x: xoff, y: yoff, width: width, height: height });
		setTitle();
		if (SQL) {
				let sqlData = store.get('sql');
				if (typeof sqlData == 'undefined') {
						getSQLData();
				}
				global.datenablage = new Ablage({
						sql: sqlData,
						defaults: {}
				});
				checksql(showAlert = SQLprop).then(el => {
						datenablage.checktables();
						convdm2msg().then(msg => {
								MainDM = msg;

						});

				}).catch(err => {
						SQL = false;
						let message = new Object();
						message.name = "Fehler der Datenbank: " + err.code
						message.detail = err.detail;
						if (SQLprop) {
								errorDisplay(message);
						}
				});
		}
		if (USEFILES) {
				MainDM = dm2msg();
				checkKundenNummer();

		}

}
// ----- OPL Window -------
function openWindowOPL() {
		// Create Window
		let windowOPLBounds = store.get('windowOPLBounds');
		//let { width, height } = store.get('windowOPLBounds');
		if (typeof windowOPLBounds == 'undefined') {
				width = 1400;
				height = 600;
		} else {
				width = windowOPLBounds.width;
				height = windowOPLBounds.height;
		}
		let windowOPLpos = store.get('windowOPLpos');
		//let { xoff, yoff } = store.get('windowOPLpos');
		if (typeof windowOPLpos == 'undefined') {
				xoff = 10;
				yoff = 10;
		} else {
				xoff = windowOPLpos.xoff;
				yoff = windowOPLpos.yoff;
		}
		if (typeof OPLWindow == 'undefined' || OPLWindow == null) {
				OPLWindow = new BrowserWindow({
						width: width,
						height: height,
						x: xoff,
						y: yoff,
						title: 'OPL',
						webPreferences: {
								nodeIntegration: true,
								enableRemoteModule: true
						}
				});
				OPLWindow.on('resize', () => {
						let { width, height } = OPLWindow.getBounds();
						store.set('windowOPLBounds', { width, height });
						let [xoff, yoff] = OPLWindow.getPosition();
						store.set('windowOPLpos', { xoff, yoff });
				});

				OPLWindow.on('move', () => {
						let [xoff, yoff] = OPLWindow.getPosition();
						store.set('windowOPLpos', { xoff, yoff });
				});


				// Load html
				OPLWindow.loadURL(url.format({
						pathname: path.join(__dirname, 'opl.html'),
						protocol: 'file:',
						slashes: true
				}));

				// Garbage
				OPLWindow.on('close', function () {
						OPLWindow = null;
				});
				// Build menu from template
				const oplWinMenu = Menu.buildFromTemplate(oplWinMenuTemplate);
				// Insert menu
				OPLWindow.setMenu(oplWinMenu);
		} else {
				OPLWindow.show();

		}
}
ipcMain.on('oplWindow:loaded', function (e, item) {
		let rechDM = datenmodell.get('Rechnungen');
		OPLWindow.webContents.send('oplWindow:start', rechDM);
});
ipcMain.on('oplWindow:close', function (e, item) {
		OPLWindow.close();
});
ipcMain.on('show:rechnung', function (e, msg) {
		let anzeige = store.get('anzeige');
		anzeige.show = msg.show;
		anzeige.menu = msg.menu;
		anzeige.rechnungid = msg.rechnungid;
		mainWindow.webContents.send('display:set', anzeige);
});
// ------------ Template ------------
function openWindowTemplateMABV() {
		// Create Window
		//let { width, height } = store.get('windowTemplateMABVBounds');
		//if (typeof width == 'undefined') {
		width = 625;
		height = 900;
		//}
		let test = store.get('windowTemplateMABVpos');
		let xoff = 0, yoff = 0;
		if (typeof test == 'undefined') {
				xoff = 200;
				yoff = 50;
		} else {
				xoff = test.xoff;
				yoff = test.yoff;
		}
		if (typeof TemplateMABVWindow == 'undefined' || TemplateMABVWindow == null) {
				TemplateMABVWindow = new BrowserWindow({
						width: width,
						height: height,
						x: xoff,
						y: yoff,
						resizable: true,
						title: 'Vorlagen',
						webPreferences: {
								nodeIntegration: true,
								enableRemoteModule: true
						}
				});
				TemplateMABVWindow.on('resize', () => {
						let { width, height } = TemplateMABVWindow.getBounds();
						store.set('windowTemplateMABVBounds', { width, height });
						let [xoff, yoff] = TemplateMABVWindow.getPosition();
						store.set('windowTemplateMABVpos', { xoff, yoff });
				});

				TemplateMABVWindow.on('move', () => {
						let [xoff, yoff] = TemplateMABVWindow.getPosition();
						store.set('windowTemplateMABVpos', { xoff, yoff });
				});


				// Load html
				TemplateMABVWindow.loadURL(url.format({
						pathname: path.join(__dirname, 'editor.html'),
						protocol: 'file:',
						slashes: true
				}));

				// Garbage
				TemplateMABVWindow.on('close', function () {
						TemplateMABVWindow = null;
				});
				// Build menu from template
				const TemplateMABVWinMenu = Menu.buildFromTemplate(TemplateMABVWinMenuTemplate);
				// Insert menu
				TemplateMABVWindow.setMenu(TemplateMABVWinMenu);
		} else {
				TemplateMABVWindow.show();

		}
}

ipcMain.on('timer:ready', function (e, item) {
		try {
				let anzeige = store.get('anzeige');
				setTitle();
				if (anzeige.edit == false && parseInt(MainDM.DMVersion) != parseInt(datenmodell.get('DMVersion'))) {
						datenmodell.reload();
						MainDM = dm2msg();
						calcData(MainDM);
						MainDM.success = "Datenupdate auf Version " + MainDM.DMVersion;
						mainWindow.webContents.send('reload', MainDM);
						mainWindow.webContents.send('display:set', anzeige);
						setTitle();

				}
				mainWindow.webContents.send('timer:set', "");

		} catch (err) {
				//console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});


ipcMain.on('conf:change', function (e, item) {
		store.set('conf', item);

});
ipcMain.on('TemplateMABVWindow:loaded', function (e, item) {
		if (SQL) {
				updateDMZahltemplate().then(zahltemplates => {
						let zahltemplateMABV = zahltemplates.find(el => el.name == CONSTANTS.POSMABV);
						TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplateMABV);
						TemplateMABVWindow.webContents.send('TemplateMABVWindow:selectdata', zahltemplates);
				});
		}
		else {
				let zahltemplates = datenmodell.get('Zahltemplate');
				let zahltemplateMABV = zahltemplates.find(el => el.name == CONSTANTS.POSMABV);
				TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplateMABV);
				TemplateMABVWindow.webContents.send('TemplateMABVWindow:selectdata', zahltemplates);
		}
});
ipcMain.on('TemplateMABVWindow:close', function (e, item) {
		TemplateMABVWindow.close();
});
ipcMain.on('TemplateMABVWindow:update', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let zahltemplates = MainDM.zahltemplates;
				switch (item.typ) {
						case CONSTANTS.POSMABV:
								let zahltemplateMABV = zahltemplates.find(el => el.name == CONSTANTS.POSMABV);
								zahltemplateMABV.positionen = item.daten;
								zahltemplateMABV.aktiv = true;
								zahltemplateMABV.bezeichnung = item.bezeichnung;
								datenmodell.setIdOnly('Zahltemplate', zahltemplateMABV, zahltemplateMABV.id)

								if (SQL) {
										datenablage.setIdOnly('ztemplate', zahltemplateMABV);
										updateDMZahltemplate().then(zahltemplates => {
												mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSMABV));
										});
								}
								else {
										mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplateMABV);
								}
								break;
						case CONSTANTS.POSRESERVIERUNG:
								let zahltemplateReservierung = zahltemplates.find(el => el.name == CONSTANTS.POSRESERVIERUNG);
								zahltemplateReservierung.bezeichnung = item.bezeichnung;
								zahltemplateReservierung.aktiv = true;

								for (i = 0; i < zahltemplateReservierung.positionen.length; i++) {
										if (zahltemplateReservierung.positionen[i].betrag > 0) {
												zahltemplateReservierung.positionen[i].betrag = item.betrag;
										}
										if (zahltemplateReservierung.positionen[i].betrag < 0) {
												zahltemplateReservierung.positionen[i].betrag = -item.betrag;
										}
								}
								datenmodell.setIdOnly('Zahltemplate', zahltemplateReservierung, zahltemplateReservierung.id)
								if (SQL) {
										datenablage.setIdOnly('ztemplate', zahltemplateReservierung);

										updateDMZahltemplate().then(zahltemplates => {
												mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSRESERVIERUNG));
										});
								}
								else {
										mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplateReservierung);
								}
								break;
						case CONSTANTS.POSTYP3:
								let zahltemplate = zahltemplates.find(el => el.name == CONSTANTS.POSTYP3);
								zahltemplate.positionen = item.daten;
								zahltemplate.bezeichnung = item.bezeichnung;
								zahltemplate.aktiv = true;
								datenmodell.setIdOnly('Zahltemplate', zahltemplate, zahltemplate.id)

								if (SQL) {
										datenablage.setIdOnly('ztemplate', zahltemplate);

										updateDMZahltemplate().then(zahltemplates => {
												mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => CONSTANTS.POSTYP3));
										});
								}
								else {
										mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplate);
								}
								break;
						default:
								let zt = zahltemplates.find(el => el.name == item.typ);
								zt.name = item.name;
								zt.aktiv = item.aktiv;
								zt.bezug = item.bezug;
								zt.zeile = item.zeile;
								zt.positionen = item.daten;
								zt.vorlagen = item.vorlagen;
								zt.bezeichnung = item.bezeichnung;
								zt.rechtyp = item.rechtyp;
								datenmodell.setIdOnly('Zahltemplate', zt, zt.id)
								if (SQL) {
										datenablage.setIdOnly('ztemplate', zt);
										updateDMZahltemplate().then(zahltemplates => {
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSMABV));
												mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == item.typ));
										});
								}
								else {
										mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zt);
								}
				}
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('TemplateMABVWindow:mode', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let zahltemplates = MainDM.zahltemplates;
				switch (item) {
						case CONSTANTS.POSMABV:
								if (SQL) {
										updateDMZahltemplate().then(zahltemplates => {
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSMABV));
										});
								}
								else {
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSMABV));
								}
								TemplateMABVWindow.setSize(625, 900);
								break;
						case CONSTANTS.POSRESERVIERUNG:
								if (SQL) {
										updateDMZahltemplate().then(zahltemplates => {
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSRESERVIERUNG));
										});
								}
								else {
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSRESERVIERUNG));
								}
								TemplateMABVWindow.setSize(625, 500);
								break;
						case CONSTANTS.POSTYP3:
								if (SQL) {
										updateDMZahltemplate().then(zahltemplates => {
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSTYP3));
										});
								}
								else {
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSTYP3));
								}
								TemplateMABVWindow.setSize(625, 700);
								break;
						default:
								if (SQL) {
										updateDMZahltemplate().then(zahltemplates => {
												TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == item));
										});
								}
								else {
										TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == item));
								}
								TemplateMABVWindow.setSize(950, 900);
				}
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('TemplateMABVWindow:addLine', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let zahltemplates = MainDM.zahltemplates;

				let zahltemplateMABV = zahltemplates.find(el => item.typ == el.name);
				let pos = zahltemplateMABV.positionen;
				let newpos = new Object();
				newpos.nr = "";
				newpos.text = "Neuer Eintrag";
				newpos.anteil = 0;
				newpos.betrag = 0;
				newpos.mwst = 0;
				pos.push(newpos);

				datenmodell.setIdOnly('Zahltemplate', zahltemplateMABV, zahltemplateMABV.id);
				mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);

				TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplateMABV);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('TemplateMABVWindow:TemplateNew', function (e, senditem) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let zahltemplates = MainDM.zahltemplates;
				let newid = 0;
				for (let i = 0; i < zahltemplates.length; i++) {
						let id = parseFloat(zahltemplates[i].id);
						if (newid <= id) {
								newid = id + 1;
						}
				}

				let item = new Object();
				item.id = newid;
				item.name = "NN" + newid;
				item.bezeichnung = "NN";
				let positionen = new Array();
				positionen[0] = {
						"nr": "1",
						"text": "",
						"anteil": 3,
						"betrag": 0,
						"mwst": 0
				};
				item.positionen = positionen;
				datenmodell.setIdNew('Zahltemplate', item);

				TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', item);
				MainDM = dm2msg();
				zahltemplates = MainDM.zahltemplates;
				TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('TemplateMABVWindow:TemplateDel', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let zahltemplateDM = MainDM.zahltemplates;
				if (item != CONSTANTS.POSMABV && item != CONSTANTS.POSRESERVIERUNG && item != CONSTANTS.POSTYP3) {
						let zahltemplates = zahltemplateDM.filter(el => el.name != item);
						datenmodell.set('Zahltemplate', zahltemplates);

						mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
						TemplateMABVWindow.webContents.send('TemplateMABVWindow:start', zahltemplates.find(el => el.name == CONSTANTS.POSMABV));
						TemplateMABVWindow.webContents.send('TemplateMABVWindow:updateselectdata', zahltemplates);


				} else {

				}
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
// ------------ Benutzer ------------
function openWindowBenutzer() {
		width = 1200;
		height = 900;
		if (typeof BenutzerWindow == 'undefined' || BenutzerWindow == null) {
				BenutzerWindow = new BrowserWindow({
						width: width,
						height: height,
						x: 20,
						y: 20,
						resizable: false,
						title: 'Benutzerverwaltung',
						webPreferences: {
								nodeIntegration: true,
								enableRemoteModule: true
						}
				});
				// Load html
				BenutzerWindow.loadURL(url.format({
						pathname: path.join(__dirname, 'benutzer.html'),
						protocol: 'file:',
						slashes: true
				}));

				// Garbage
				BenutzerWindow.on('close', function () {
						BenutzerWindow = null;
				});
				// Build menu from template
				const BenutzerWindowMenu = Menu.buildFromTemplate(BenutzerWindowMenuTemplate);
				// Insert menu
				BenutzerWindow.setMenu(BenutzerWindowMenu);
		} else {
				BenutzerWindow.show();

		}
}
ipcMain.on('WindowBenutzer:loaded', function (e, item) {
		let benutzerDM = datenmodell.get('Benutzer');
		let benutzer = benutzerDM.filter(el => { if (typeof el.name !== 'undefined') { return true; } else { return false; } });
		let msg = new Object();
		msg.benutzerDM = benutzer;
		msg.user = user;
		msg.osuser = osuser;
		rechte(msg);
		BenutzerWindow.webContents.send('WindowBenutzer:start', msg);
});
ipcMain.on('WindowBenutzer:update', function (e, item) {
		waitforlock();
		datenmodell.reload();
		MainDM = dm2msg();
		let benutzerDM = item.daten;
		MainDM.benutzerDM = benutzerDM;
		rechte(MainDM);
		datenmodell.set('Benutzer', MainDM.benutzerDM, MainDM.user);
		MainDM.benutzerDM = datenmodell.get('Benutzer');
		let msg = new Object();
		msg.benutzerDM = MainDM.benutzerDM;
		rechte(msg);
		msg.user = user;
		msg.osuser = osuser;
		msg.show = 'benutzer';
		BenutzerWindow.webContents.send('updateDM:benutzerDM', msg);
		mainWindow.webContents.send('reload', MainDM);
		let anzeige = store.get('anzeige');
		mainWindow.webContents.send('display:set', anzeige);
		datenmodell.releaseLock(MainDM.user);
		setTitle();
});
ipcMain.on('WindowBenutzer:updateSingle', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				benutzer = MainDM.benutzerDM.find(el => el.name.toLowerCase() == item.name.toLowerCase());
				changeProperty(benutzer, item, 'name', 'Benutzer');
				changeProperty(benutzer, item, 'email', 'Benutzer');
				changeProperty(benutzer, item, 'password', 'Benutzer');
				rechte(MainDM);
				datenmodell.setIdOnly('Benutzer', benutzer, benutzer.id);
				MainDM = dm2msg();
				let msg = new Object();
				msg.benutzerDM = MainDM.benutzerDM;
				rechte(msg);
				msg.user = item.name;
				msg.osuser = osuser;
				msg.show = 'daten';
				BenutzerWindow.webContents.send('updateDM:benutzerDM', msg);
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('WindowBenutzer:addLine', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();

				let newBenutzer = new Object();
				newBenutzer.name = "Neuer Benutzer";
				let newid = 0;
				let cLen = MainDM.benutzerDM.length;
				for (let cnt = 0; cnt < cLen; cnt++) {
						if (newid <= parseFloat(MainDM.benutzerDM[cnt].id)) {
								newid = parseFloat(MainDM.benutzerDM[cnt].id) + 1;
						}

				}
				newBenutzer.id = newid;
				rechte(MainDM);
				MainDM.benutzerDM.push(newBenutzer);
				datenmodell.setIdNew('Benutzer', newBenutzer, newBenutzer.id);
				MainDM.benutzerDM = datenmodell.get('Benutzer');
				let msg = new Object();
				msg.benutzerDM = MainDM.benutzerDM;
				rechte(msg);
				msg.user = user;
				msg.show = 'benutzer';
				BenutzerWindow.webContents.send('updateDM:benutzerDM', msg);
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
// WorkerWin ---------
ipcMain.on('workerWin:loaded', function (e, item) {
});
ipcMain.on('calcData:done', function (e, msg) {

});
// -------- Main Window----------------------------------------
ipcMain.on('mainWindow:loaded', function (e, item) {
		sendDM2mainWindow(MainDM);
});
ipcMain.on('loginWindow:loaded', function (e, item) {
		let benutzerDM = datenmodell.get('Benutzer');
		mainWindow.webContents.send('loginWindow:start', benutzerDM);
		let iniMandant ={
				color: PRGCOLOR,
				logo: PRGLOGO
		};

		mainWindow.webContents.send('loginWindow:mandant', iniMandant);
});
ipcMain.on('loginuser', function (e, item) {
		user = item;
		initLogin();
});
ipcMain.on('edit:new', function (e, item) {
		editNew(item);
});
ipcMain.on('trash:empty', function (e, item) {
		emptyTrash();
		let anzeige = store.get('anzeige');
		mainWindow.webContents.send('display:set', anzeige);
});
ipcMain.on('position:neu', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				newPosition(item);
				MainDM = dm2msg();
				calcData(MainDM);
				MainDM.success = "Position hinzugefügt.";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "vertragposedit";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}

});
ipcMain.on('position:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				position = MainDM.positionenDM.find(el => el.id == item.id);
				changeProperty(position, item, 'text', 'Positionen');
				changeProperty(position, item, 'anteil', 'Positionen');
				changeProperty(position, item, 'faktor', 'Positionen');
				changeProperty(position, item, 'betrag', 'Positionen');
				changeProperty(position, item, 'mwst', 'Positionen');
				changeProperty(position, item, 'netto', 'Positionen');
				changeProperty(position, item, 'rechpos', 'Positionen');
				changeProperty(position, item, 'rechnung', 'Positionen');
				changeProperty(position, item, 'status', 'Positionen');
				changeProperty(position, item, 'bmgBetrag', 'Positionen');
				calcData(MainDM);
				if (SQL) {
						datenablage.setIdOnly('positionenver', position);
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Positionen', position, item.id);
				}
				MainDM = dm2msg();
				calcData(MainDM);

				MainDM.success = "Position geändert";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "vertragposedit";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('position:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				position = MainDM.positionenDM.find(el => el.id == item);
				index = MainDM.positionenDM.indexOf(position);
				if (index > -1) {
						MainDM.positionenDM.splice(index, 1);
				}
				if (SQL) {
						datenablage.deleteDataSetID('positionenver', item);
				}
				if (USEFILES) {
						datenmodell.set('Positionen', MainDM.positionenDM);
				}
				MainDM = dm2msg();
				MainDM.success = "Position gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}

});
ipcMain.on('obj:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let objDM = MainDM.objekteDM;

				if (item.id == '-1') {
						newObject(item);
				} else {
						changeObjekt(item);
				}
				MainDM = dm2msg();
				MainDM.danger = "";
				MainDM.success = "Ok";
				calcData(MainDM);
				mainWindow.webContents.send('reload', MainDM);
				mainWindow.webContents.send('display:set', store.get('anzeige'));
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('buchung:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let buchungen = MainDM.buchungenALL;
				buchung = buchungen.find(el => el.id == item.id);
				changeProperty(buchung, item, 'bdat', 'Buchung');
				changeProperty(buchung, item, 'vdat', 'Buchung');
				changeProperty(buchung, item, 'text', 'Buchung');
				changeProperty(buchung, item, 'pn', 'Buchung');
				changeProperty(buchung, item, 'konto', 'Buchung');
				changeProperty(buchung, item, 'betrag', 'Buchung');
				datenmodell.setIdOnly('Buchungen', buchung, item.id);
				MainDM = dm2msg();
				MainDM.danger = "";
				MainDM.success = "Vertrag abgespeichert.";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('buchung:delete', function (e, buchungid) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let buchungen = MainDM.buchungenALL;
				buchung = buchungen.find(el => el.id == buchungid);
				buchung.status = CONSTANTS.TRASH;
				datenmodell.setIdOnly('Buchungen', buchung, buchungid);
				MainDM = dm2msg();
				MainDM.danger = "";
				MainDM.success = "Vertrag abgespeichert.";
				mainWindow.webContents.send('reload', MainDM);

				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('provision:changeRech', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				daten = item.data;
				let provisionDM = MainDM.provisionDM;
				let maxid = 0;
				let provisionen = new Array();
				provisionDM.forEach(pro => {
						if (parseInt(pro.id) > parseInt(maxid)) {
								maxid = parseInt(pro.id);
						}
						if (pro.rechnung != item.rechnung) {
								provisionen.push(pro);
						}
				});
				daten.forEach(el => {
						if (el.mitarbeiter !== "") {
								let pn = new Object();

								if (parseInt(el.id) == -1) {
										pn.id = parseInt(maxid) + 1;
								} else {
										pn.id = parseInt(el.id);
								}

								pn.mitarbeiter = el.mitarbeiter;
								pn.rechnung = item.rechnung;
								pn.anteil = el.anteil;
								pn.status = 0;
								pn.dataus = "";
								pn.erfasser = user;
								pn.freigeber = "";
								pn.kommentar = "";
								pn.betrag = el.betrag;
								provisionen.push(pn);
						}
				});

				MainDM.provisionDM = provisionen;

				if (SQL) {

				}
				if (USEFILES) {
						datenmodell.set('provisionen', MainDM.provisionDM);
				}

				mainWindow.webContents.send('reload', MainDM);
				mainWindow.webContents.send('update:prov', item.rechnung);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('provision:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				provision = MainDM.provisionDM.find(el => el.id == item.id);

				changeProperty(provision, item, 'kommentar', 'Provision');
				changeProperty(provision, item, 'dataus', 'Provision');
				changeProperty(provision, item, 'status', 'Provision');
				changeProperty(provision, item, 'anteil', 'Provision');
				changeProperty(provision, item, 'erfasser', 'Provision');
				changeProperty(provision, item, 'freigeber', 'Provision');
				changeProperty(provision, item, 'betrag', 'Provision');
				changeProperty(provision, item, 'gesamtprov', 'Provision');

				datenmodell.setIdOnly('provisionen', provision, provision.id);
				MainDM = dm2msg();
				MainDM.success = "Provisionsdaten geändert";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.rechungid = item.id;
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('vertrag:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();

				changeVertrag(item);

				datenmodell.reload();
				MainDM = dm2msg();
				MainDM.danger = "";
				MainDM.success = "Vertrag abgespeichert.";
				mainWindow.webContents.send('reload', MainDM);
				MainDM.success = "";
				let anzeige = store.get('anzeige');
				let vertrag = MainDM.vertragDM.find(ver => ver.id == item.id);
				if (vertrag.typ != CONSTANTS.VERTRAGALLG) {
						anzeige.vertragid = item.id;
				} else {
						anzeige.show = "rechnung";
				}
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();

				if (REST) {
						let args = {
								headers: { "Content-Type": "application/json" },
								data: MainDM.vertragDM
						};

						let req = client.post(apibase + "vertraege", args, function (data, response) {
								//console.log(data);
						});

						req.on('error', function (err) {
								console.log('request error', err.errno);
						});


				}


		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);
		}

});
ipcMain.on('rechnung:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				rechnung = MainDM.rechnungenDM.find(el => el.id == item.id);
				changeProperty(rechnung, item, 'anrede', 'Rechnung');
				changeProperty(rechnung, item, 'name', 'Rechnung');
				changeProperty(rechnung, item, 'vorname', 'Rechnung');
				changeProperty(rechnung, item, 'strasse', 'Rechnung');
				changeProperty(rechnung, item, 'plz', 'Rechnung');
				changeProperty(rechnung, item, 'ort', 'Rechnung');
				changeProperty(rechnung, item, 'telefon', 'Rechnung');
				changeProperty(rechnung, item, 'email', 'Rechnung');
				changeProperty(rechnung, item, 'betrag', 'Rechnung');
				changeProperty(rechnung, item, 'rechnr', 'Rechnung');
				changeProperty(rechnung, item, 'rechtyp', 'Rechnung');
				changeProperty(rechnung, item, 'objektnr', 'Rechnung');
				changeProperty(rechnung, item, 'kostenstelle', 'Rechnung');
				changeProperty(rechnung, item, 'rechdat', 'Rechnung');
				changeProperty(rechnung, item, 'm1dat', 'Rechnung');
				changeProperty(rechnung, item, 'm2dat', 'Rechnung');
				changeProperty(rechnung, item, 'm3dat', 'Rechnung');
				changeProperty(rechnung, item, 'mahnstufe', 'Rechnung');
				changeProperty(rechnung, item, 'faelligdat', 'Rechnung');
				changeProperty(rechnung, item, 'leistungszeitraum', 'Rechnung');
				changeProperty(rechnung, item, 'txtprj', 'Rechnung');
				changeProperty(rechnung, item, 'txtbeschreibung', 'Rechnung');
				changeProperty(rechnung, item, 'txtkunde', 'Rechnung');
				changeProperty(rechnung, item, 'txtobj', 'Rechnung');
				changeProperty(rechnung, item, 'betreff', 'Rechnung');
				let statuschange = changeProperty(rechnung, item, 'status', 'Rechnung');
				let successmeldung = "";
				if (statuschange == 1 && item.status == CONSTANTS.BEZAHLT) {
						// Geändert auf BEZAHLT
						let ze = new Object();
						ze.rechnung = item.id;
						ze.datum = getDatum(0);
						ze.betrag = item.betrag-item.zahlungseingang;
						ze.text = "Automatisch angelegt durch Statuswechsel";
						ze.id = -1;
						successmeldung = "Zahlungseingang angelegt nacht Statuswechsel <br>";
						changeZE(ze);
				}
				calcData(MainDM);
				datenmodell.setIdOnly('Rechnungen', rechnung, rechnung.id);
				let anzeige = store.get('anzeige');
				MainDM = dm2msg();
				MainDM.success = successmeldung + "Rechnung geändert";
				mainWindow.webContents.send('reload', MainDM);
				anzeige.rechungid = item.id;
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('rechnung:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();

				rechnung = MainDM.rechnungenDM.find(el => el.id == item);
				positionen = MainDM.positionenDM.filter(el => el.rechnung == item);
				rechnung.status = CONSTANTS.TRASH;
				for (let po = 0; po < positionen.length; po++) {
						positionen[po].rechnung = "";
						if (SQL) {

						}
						if (USEFILES) {
								datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
						}
				}
				calcData(MainDM);

				if (SQL) {
						datenablage.setIdOnly('rechnung', rechnung);
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Rechnungen', rechnung, rechnung.id)
				}
				MainDM = dm2msg();
				MainDM.success = "Rechnung gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "vertrag";
				anzeige.rechnungid = 0;
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('vertrag:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let vertrag = MainDM.vertragDM.find(el => el.id == item);
				let rechnungen = MainDM.rechnungenDM.filter(el => el.vertrag == vertrag.id);
				for (let re = 0; re < rechnungen.length; re++) {
						rechnungen[re].status = CONSTANTS.TRASH;

						if (SQL) {
								datenablage.setIdOnly('rechnung', rechnungen[re]);
						}
						if (USEFILES) {
								datenmodell.setIdOnly('Rechnungen', rechnungen[re], rechnungen[re].id);
						}
						let ze = MainDM.zeDM.filter(el => el.rechnung == rechnungen[re].id);

						if (typeof ze != 'undefined') {
								for (let i = 0; i < ze.length; i++) {
										ze[i].status = CONSTANTS.TRASH;
										if (SQL) {
												datenablage.setIdOnly('zahlungseingang', ze[i]);
										}
										if (USEFILES) {
												datenmodell.setIdOnly('Zahlungseingaenge', ze[i], ze[i].id);
										}
								}
						}
				}
				let positionen = MainDM.positionenDM.filter(el => el.vertrag == vertrag.id);
				for (let po = 0; po < positionen.length; po++) {
						positionen[po].status = CONSTANTS.TRASH;
						// if (SQL) {

						// }
						// if (USEFILES) {
						// 	datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
						// }
				}
				datenmodell.set('Positionen', MainDM.positionenDM, MainDM.user);
				calcData(MainDM);

				vertrag.status = CONSTANTS.TRASH;
				if (SQL) {
						datenablage.setIdOnly('vertrag', vertrag);
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Vertraege', vertrag, vertrag.id);

						MainDM = dm2msg();
						MainDM.success = "Vertrag erfolgreich gelöscht.";
						mainWindow.webContents.send('reload', MainDM);
						let anzeige = store.get('anzeige');
						if (vertrag.typ != CONSTANTS.VERTRAGALLG) {
								anzeige.vertragid = item.id;
						} else {
								anzeige.show = "rechnung";
						}
						mainWindow.webContents.send('display:set', anzeige);
						datenmodell.releaseLock(MainDM.user);
						setTitle();
				}

				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);

				let anzeige = store.get('anzeige');
				switch (vertrag.typ) {
						case CONSTANTS.DARLEHEN:
								anzeige.show = "DAR-direkt";
								mainWindow.webContents.send('display:set', anzeige);
								break;
						case CONSTANTS.ARCHVERTRAG:
								anzeige.show = "prj0-direkt";
								mainWindow.webContents.send('display:set', anzeige);
								break;
						case CONSTANTS.VERTRAGALLG:
								anzeige.show = "RNBALLRECH";
								mainWindow.webContents.send('display:set', anzeige);
								break;
						default:
								anzeige.vertragid = 0;
								anzeige.projektid = vertrag.projekt;
								anzeige.einheitid = vertrag.objekt;
								anzeige.click = "opr";
								anzeige.show = "einheit";
								mainWindow.webContents.send('display:set', anzeige);
				}
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('projekt:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let projekte = MainDM.projekteDM;
				let projekt = projekte.find(el => el.id == item.id);
				changeProperty(projekt, item, 'name', 'Projekt');
				changeProperty(projekt, item, 'prjnummer', 'Projekt');
				changeProperty(projekt, item, 'bautraeger', 'Projekt');
				changeProperty(projekt, item, 'beschreibung', 'Projekt');
				changeProperty(projekt, item, 'bearbeiter', 'Projekt');
				changeProperty(projekt, item, 'bauleiter', 'Projekt');
				changeProperty(projekt, item, 'bank', 'Projekt');
				changeProperty(projekt, item, 'iban', 'Projekt');
				changeProperty(projekt, item, 'bic', 'Projekt');
				changeProperty(projekt, item, 'status', 'Projekt');
				changeProperty(projekt, item, 'ibanE', 'Projekt');
				calcData(MainDM);
				datenmodell.setIdOnly('Projekte', projekt, item.id);
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.projektid = item.id;
				mainWindow.webContents.send('display:set', anzeige);
				setTitle();
				datenmodell.releaseLock(MainDM.user);
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('ze:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				changeZE(item);
				datenmodell.reload();
				MainDM = dm2msg();
				MainDM.danger = "";
				MainDM.success = "Zahlungseingang angelegt.";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "rechnung";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('ze:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let ze = MainDM.zeDM.find(el => el.id == item);
				index = MainDM.zeDM.indexOf(ze);
				if (index > -1) {
						MainDM.zeDM.splice(index, 1);
				}
				calcData(MainDM);
				if (SQL) {
						datenablage.deleteDataSetID('zahlungseingang', item);
				}
				if (USEFILES) {
						datenmodell.set('Zahlungseingaenge', MainDM.zeDM);
				}

				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "rechnung";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('position:rechnung:change', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let positionen = MainDM.positionenDM;
				position = positionen.find(el => el.id == item.id);
				if (item.rechnung == "") {
						rechid = position.rechnung;
				} else {
						rechid = item.rechnung;
				}
				changeProperty(position, item, 'text', 'Positionen');
				changeProperty(position, item, 'anteil', 'Positionen');
				changeProperty(position, item, 'betrag', 'Positionen');
				changeProperty(position, item, 'rechpos', 'Positionen');
				changeProperty(position, item, 'rechnung', 'Positionen');
				datenmodell.setIdOnly('Positionen', position, item.id);
				if (typeof rechid != 'undefined' && rechid != 'undefined' && rechid > 0) {
						recpos = positionen.filter(el => el.rechnung == rechid);
						cLen = recpos.length;
						let betrag = 0;
						for (i = 0; i < cLen; i++) {
								betrag += parseFloat(recpos[i].betrag);
						}
						let rechnungen = datenmodell.get('Rechnungen');
						let rechnung = rechnungen.find(el => el.id == rechid);
						let recupdate = new Object();
						recupdate.betrag = betrag;
						changeProperty(rechnung, recupdate, 'betrag', 'Rechnung');
						datenmodell.setIdOnly('Rechnungen', rechnung, rechid);
				}
				MainDM = dm2msg();
				MainDM.success = "Position geändert";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "rechnungposedit";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();

		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('projekt:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let projekteDM = MainDM.projekteDM;
				let objekteDM = MainDM.objekteDM;
				let vertragDM = MainDM.vertragDM;
				let recDM = MainDM.rechnungenDM;
				let zeDM = MainDM.zeDM;
				let posDM = MainDM.positionenDM;
				let projekt = projekteDM.find(el => el.id == item);
				let vertrag = vertragDM.filter(el => el.projekt == item);

				for (let ve = 0; ve < vertrag.length; ve++) {
						vertrag[ve].status = CONSTANTS.TRASH;
						if (USEFILES) {
								datenmodell.setIdOnly('Vertraege', vertrag[ve], vertrag[ve].id);
						}
						if (SQL) {
								datenablage.setIdOnly('vertrag', vertrag[ve]);
						}
						let rechnungen = recDM.filter(el => el.vertrag == vertrag[ve].id);
						for (let re = 0; re < rechnungen.length; re++) {
								rechnungen[re].status = CONSTANTS.TRASH;
								if (USEFILES) {
										datenmodell.setIdOnly('Rechnungen', rechnungen[re], rechnungen[re].id);
								}
								let ze = zeDM.filter(el => el.rechnung == rechnungen[re].id);
								if (typeof ze != 'undefined') {
										for (let i = 0; i < ze.length; i++) {
												ze[i].status = CONSTANTS.TRASH;
												if (USEFILES) {
														datenmodell.setIdOnly('Zahlungseingaenge', ze[i], ze[i].id);
												}
										}
								}
						}
						let positionen = posDM.filter(el => el.vertrag == vertrag[ve].id);
						for (let po = 0; po < positionen.length; po++) {
								positionen[po].status = CONSTANTS.TRASH;
								if (USEFILES) {
										datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
								}
						}
				}
				projekt.status = CONSTANTS.TRASH;
				if (USEFILES) {
						datenmodell.setIdOnly('Projekte', projekt, projekt.id);
				}
				let objekt = objekteDM.filter(el => el.projekt == item);
				for (let ob = 0; ob < objekt.length; ob++) {
						objekt[ob].status = CONSTANTS.TRASH;
						if (USEFILES) {
								datenmodell.setIdOnly('Objekte', objekt[ob], objekt[ob].id);
						}
				}
				MainDM = dm2msg();
				calcData(MainDM);
				MainDM.success = "Projekt gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "projekte";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('objekt:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let objekteDM = MainDM.objekteDM;
				let vertragDM = MainDM.vertragDM;
				let recDM = MainDM.rechnungenDM;
				let zeDM = MainDM.zeDM;
				let posDM = MainDM.positionenDM;
				let objekt = objekteDM.find(el => el.id == item);
				let vertrag = vertragDM.filter(el => el.objekt == item);
				for (let ve = 0; ve < vertrag.length; ve++) {
						vertrag[ve].status = CONSTANTS.TRASH;
						datenmodell.setIdOnly('Vertraege', vertrag[ve], vertrag[ve].id);
						if (SQL) {
								datenablage.setIdOnly('vertrag', vertrag[ve]);
						}
						let rechnungen = recDM.filter(el => el.vertrag == vertrag[ve].id);
						for (let re = 0; re < rechnungen.length; re++) {
								rechnungen[re].status = CONSTANTS.TRASH;
								datenmodell.setIdOnly('Rechnungen', rechnungen[re], rechnungen[re].id);
								let ze = zeDM.filter(el => el.rechnung == rechnungen[re].id);
								if (typeof ze != 'undefined') {
										for (let i = 0; i < ze.length; i++) {
												ze[i].status = CONSTANTS.TRASH;
												datenmodell.setIdOnly('Zahlungseingaenge', ze[i], ze[i].id);
										}
								}
						}
						let positionen = posDM.filter(el => el.vertrag == vertrag[ve].id);
						for (let po = 0; po < positionen.length; po++) {
								positionen[po].status = CONSTANTS.TRASH;
								datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
						}

				}
				objekt.status = CONSTANTS.TRASH;
				datenmodell.setIdOnly('Objekte', objekt, objekt.id);
				MainDM = dm2msg();
				calcData(MainDM);
				MainDM.success = "Objekt gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				anzeige.show = "projekte";
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('obj:delete', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let obj = MainDM.objekteDM.find(el => el.id == item);
				let vertraege = MainDM.vertragDM.filter(el => el.objekt == item && el.status != CONSTANTS.TRASH);

				if (vertraege.length > 0) {
						let options = {
								buttons: ["Löschen", "Abbrechen"],
								message: "Einheit wirklich löschen?",
								title: "Frage",
								detail: "Es gibt zugeordnete Verträge/ Rechnungen! Diese werden ebenfalls gelöscht.",
								icon: path.join(__dirname, 'assets/icons/png/thinking.png')
						};
						let response = dialog.showMessageBoxSync(mainWindow, options);
						if (response == 0) {
								for (let ve = 0; ve < vertraege.length; ve++) {
										vertraege[ve].status = CONSTANTS.TRASH;
										let rechnungen = MainDM.rechnungenDM.filter(el => el.vertrag == vertraege[ve].id);
										for (let re = 0; re < rechnungen.length; re++) {
												rechnungen[re].status = CONSTANTS.TRASH;
										}
								}
								obj.status = CONSTANTS.TRASH;
						}
				} else {
						obj.status = CONSTANTS.TRASH;

				}
				datenmodell.setIdOnly('Objekte', obj, obj.id);
				MainDM = dm2msg();
				MainDM.success = "Gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('comment:add', function (e, item) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let comments = MainDM.commentDM;
				let i;
				if (typeof item.id != 'undefined' && parseInt(item.id) > 0) {
						// Hier kommt kein neuer Kommentar, sondern ein alter.....
						comment = comments.find(c => c.id == item.id);
						index = comments.indexOf(comment);
						comment.eintrag = item.notiz;
						//comment.datum = logdatum();
						comment.datum = new Date();
						comment.verfasser = user;
						comment.empf = item.empf;
						comment.bereich = item.bereich;
				} else {
						// Das ist was neues
						let newid = 0;
						let cLen = comments.length;
						for (i = 0; i < cLen; i++) {
								let id = parseFloat(comments[i].id);
								if (newid <= id) {
										newid = id + 1
								}
						}
						let comment = {
								"id": newid,
								"eintrag": item.notiz,
								"datum": new Date(),
								"verfasser": item.verfasser,
								"bereich": item.bereich,
								"datei": item.datei,
								"modus": item.modus,
								"empf": item.empf
						}
						if (typeof item.rechnung != 'undefined') {
								comment.rechnung = item.rechnung;
								comment.refid = item.rechnung;
						}
						if (typeof item.vertrag != 'undefined') {
								comment.vertrag = item.vertrag;
								comment.refid = item.vertrag;
						}
						if (typeof item.kunde != 'undefined') {
								comment.kunde = item.kunde;
								comment.refid = item.kunde;
						}
						if (typeof item.projekt != 'undefined') {
								comment.projekt = item.projekt;
								comment.refid = item.projekt;
						}
						if (typeof item.einheit != 'undefined') {
								comment.einheit = item.einheit;
								comment.refid = item.einheit;
						}

						if (typeof comments !== 'undefined') {
								comments.push(comment);

						} else {
								comments = [comment];

						}
				}
				datenmodell.set('Kommentare', comments);
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('comment:del', function (e, commentID) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let comments = datenmodell.get('Kommentare');
				comment = comments.find(c => c.id == commentID);
				index = comments.indexOf(comment);
				if (index > -1) {
						comments.splice(index, 1);
				}
				datenmodell.set('Kommentare', comments);
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('display:save', function (e, anzeige) {
		store.set('anzeige', anzeige);
});
ipcMain.on('error:display', function (e, message) {
		errorDisplay(message);
});
ipcMain.on('template:change', function (e, message) {
		selectTemplate(message.text, message.template, message.prj);

});
ipcMain.on('template:changeRec', function (e, message) {
		selectTemplateRec(message.pfad, message.text, message.rechid, message.neu);
});
ipcMain.on('template:changeRecName', function (e, message) {
		//
		// Ändern der Buttonbeschriftung bei den Vorlagen der Rechnungen
		//
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();

				let rechnung = MainDM.rechnungenDM.find(el => parseInt(el.id) == parseInt(message.rechid));
				if (typeof rechnung.vorlagenrech == 'undefined') {
						let vor = new Array();
						let v2 = new Object();
						v2.text = message.neu;
						v2.pfad = "Keine Vorlage ausgewählt";
						vor.push(v2);
						rechnung.vorlagenrech = vor;
				} else {
						vorlagenrech = rechnung.vorlagenrech.find(el => el.text == message.alt);
						if (typeof vorlagenrech != 'undefined') {
								vorlagenrech.text = message.neu;
						}
				}
				if (SQL) {
						let filter = {
								"text": message.alt,
								"rechnung": message.rechid
						};
						let fields = {
								"text": message.neu
						};
						datenablage.updateDataFields("vorlagenrech", fields, filter);
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Rechnungen', rechnung, message.rechid);
				}
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
// ----------------------------------------
ipcMain.on('kunde:change', function (e, item) {
		try {
				if (USEFILES) {
						waitforlock();
						datenmodell.reload();
						MainDM = dm2msg();
						changeKunde(item);
						datenmodell.reload();
						MainDM = dm2msg();
						MainDM.success = "Kundendaten geändert";
						mainWindow.webContents.send('reload', MainDM);
						let anzeige = store.get('anzeige');
						anzeige.kundeid = item.id;
						mainWindow.webContents.send('display:set', anzeige);
						datenmodell.releaseLock(MainDM.user);
						setTitle();
				}
				if (SQL) {
						changeKunde(item);
						convdm2msg().then(msg => {
								MainDM = msg;
								MainDM.success = "Kundendaten geändert";
								mainWindow.webContents.send('reload', MainDM);
								let anzeige = store.get('anzeige');
								anzeige.kundeid = item.id;
								mainWindow.webContents.send('display:set', anzeige);
								setTitle();
						});
				}
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
ipcMain.on('kunde:delete', function (e, item) {
		try {
				if (USEFILES) {
						waitforlock();
						datenmodell.reload();
						MainDM = dm2msg();
						let kunDM = MainDM.kundenDM;
						let kunde = kunDM.find(el => el.id == item);
						kunde.status = CONSTANTS.TRASH;
						datenmodell.setIdOnly('Kunden', kunde, item);
						MainDM = dm2msg();
						MainDM.success = "Kunde gelöscht";
						mainWindow.webContents.send('reload', MainDM);
						let anzeige = store.get('anzeige');
						anzeige.show = "kunde";
						anzeige.kundeid = 0;
						mainWindow.webContents.send('display:set', anzeige);
						datenmodell.releaseLock(MainDM.user);
						setTitle();
				}
				if (SQL) {
						let kunDM = MainDM.kundenDM;
						let kunde = kunDM.find(el => el.id == item);
						kunde.status = CONSTANTS.TRASH;
						datenablage.setIdOnly('kunde', kunde);


						convdm2msg().then(msg => {
								MainDM = msg;
								MainDM.success = "Kunde gelöscht";
								mainWindow.webContents.send('reload', MainDM);
								let anzeige = store.get('anzeige');
								anzeige.show = "kunde";
								anzeige.kundeid = 0;
								mainWindow.webContents.send('display:set', anzeige);
								setTitle();
						});
				}

		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
});
// ----------------------------------------
function checkKundenNummer() {
		let kunden = MainDM.kundenDM.filter(el => typeof el.kundennummer == 'undefined' || el.kundennummer == "");
		kunden.forEach(kunde => {
				datenmodell.reload();
				MainDM = dm2msg();
				kunde.kundennummer = neueKundennummer();
				changeKunde(kunde);
				datenmodell.reload();
				MainDM = dm2msg();

		});
		kunden = MainDM.kundenDM.filter(el => typeof el.kundennummer == 'undefined' || el.kundennummer == "");

}
function neueKundennummer() {
		let neueKundennummer = 0;
		while (neueKundennummer == 0) {
				neueKundennummer = Math.floor(Math.random() * 100001) + 100001;
				let kunde = MainDM.kundenDM.find(el => el.kundennummer == neueKundennummer);
				if (typeof kunde != 'undefined') {
						neueKundennummer = 0;
				}
		}
		return neueKundennummer;
}
function errorDisplay(message) {
		let options = {
				buttons: ["Ok"],
				message: message.name,
				title: "Fehlermeldung",
				detail: message.detail,
				icon: path.join(__dirname, 'assets/icons/png/bandage.png')

		};
		let response = dialog.showMessageBoxSync(mainWindow, options);

}

function editNew(input) {
		try {
				let anzeige = store.get('anzeige');
				if (USEFILES) {
						waitforlock();
						datenmodell.reload();
						MainDM = dm2msg();
						let rechDM = datenmodell.get('Rechnungen');
						let prjDM = datenmodell.get('Projekte');
						let objDM = datenmodell.get('Objekte');
						if (typeof input !== 'undefined') {
								let newid = 1;
								let newidrec = 1;
								let cLen = 0;
								let item = new Object();
								let msg = new Object();
								let kun;
								switch (input.type) {
										case "Vertrag":
												let verid = newVertrag(input);
												MainDM = dm2msg();
												MainDM.success = "Vertrag erfolgreich angelegt.";
												mainWindow.webContents.send('reload', MainDM);

												switch (input.typ) {
														case CONSTANTS.RESERVIERUNG:
																addPositionen(verid, input.betrag, CONSTANTS.POSRESERVIERUNG);
																anzeige.show = "vertragReservierungedit";
																break;
														case CONSTANTS.ARCHVERTRAG:
																addPositionen(verid, input.betrag, CONSTANTS.POSTYP3);
																anzeige.show = "vertragARCedit";
																break;
														case CONSTANTS.KAUFVERTRAG:
																addPositionen(verid, input.betrag, CONSTANTS.POSMABV);
																break;
														case CONSTANTS.DARLEHEN:
																anzeige.show = "vertragDARedit";
																break;
														default:
																anzeige.show = "vertragedit";
												}
												anzeige.vertragid = verid;
												mainWindow.webContents.send('display:set', anzeige);

												break;
										case "VertragUndRechnung":
												let projekt, objekt;
												let recmax = 0;
												let template = input.template;
												// -------------------
												let aktver = MainDM.vertragDM;
												cLen = aktver.length;
												for (i = 0; i < cLen; i++) {
														let id = parseFloat(aktver[i].id);
														if (newid <= id) {
																newid = id + 1
														}
												}
												let jetzt = new Date();
												let ye = jetzt.getFullYear();

												let aktrech = MainDM.rechnungenDM;
												for (let i = 0; i < aktrech.length; i++) {
														let id = parseFloat(aktrech[i].id);
														if (newidrec <= id) {
																newidrec = id + 1
														}
												}
												aktrech = MainDM.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH);
												for (let i = 0; i < aktrech.length; i++) {
														let text = aktrech[i].rechnr;
														let nr1 = 0, nr2 = 0;
														let recjahr = 0, recnr = 0;

														nr1 = parseInt(text.substring(0, text.indexOf("-")));
														nr2 = parseInt(text.substring(text.lastIndexOf("-") + 1, text.length));
														switch(PRGRECHNR){
																case 1:
																		recjahr = nr1;
																		recnr = nr2;
																		break;
																case 2: 
																		recjahr = nr2;
																		recnr = nr1;
																		break;
																case 3: 
																		recjahr = nr1+2000;
																		recnr = nr2;
																		break;
																default:
																		recjahr = nr1;
																		recnr = nr2;

														}


														if (typeof recjahr != 'undefined' && recjahr == parseInt(ye)) {
																if (recnr > recmax) {
																		recmax = recnr;
																}
														}
												}
												recmax++;
												if(PRGMANDANT == 2){
														if (parseInt(recmax) < 100) {
																recmax = ("0" + recmax).slice(-3);
														}
												}else
												{
														if (parseInt(recmax) < 1000) {
																recmax = ("00" + recmax).slice(-3);
														}
												}

												// -------------------
												// Vertrag
												item.name = "Vertrag" + newid;
												item.status = input.status;
												item.id = newid;

												item.typ = CONSTANTS.VERTRAGALLG;

												if (typeof input.kunde != 'undefined') {
														item.kunde = input.kunde;
														kun = MainDM.kundenDM.find(el => el.id == parseInt(item.kunde));
														if (typeof kun !== 'undefined') {
																item.anrede = kun.anrede;
																item.name = kun.name;
																item.vorname = kun.vorname;
														}
												}
												item.betrag = 0;
												if (input.bezug == 'true') {
														item.objekt = input.objekt;
														item.projekt = input.projekt;
														projekt = prjDM.find(el => el.id == parseInt(item.projekt));
														objekt = objDM.find(el => el.id == parseInt(item.objekt));
												} else {
														item.objekt = 0;
														item.projekt = 0;
												}
												item.vernr = "VER-" + recmax + "-" + ye;
												MainDM.vertragDM.push(item);

												// Wurde der Käufer des Objekts geändert?
												if (typeof input.kunObjid != 'undefined' && input.kunObjid != 0) {
														if (parseInt(objekt.kunde) != parseInt(input.kunObjid)) {
																objekt.kunde = input.kunObjid;
																if (SQL) {

																}
																if (USEFILES) {
																		datenmodell.setIdOnly('Objekte', objekt, objekt.id);
																}
														}

												}
												// -----------------------------------
												// Rechnung
												rechNew = new Object();
												rechNew.id = newidrec;
												//rechNew.rechnr = recmax + "-" + ye;
												switch(PRGRECHNR){
														case 1:
																rechNew.rechnr = ye + "-" + recmax;
																break;
														case 2: 
																rechNew.rechnr = recmax + "-" + ye;
																break;
														case 3:
																rechNew.rechnr = parseInt(ye)-2000 + "-" + recmax;
																break;
														default:
																rechNew.rechnr = ye + "-" + recmax;
												}
												betreff = input.betreff;
												rechNew.vertrag = newid;
												rechNew.anrede = input.anrede;
												if (typeof kun !== 'undefined') {
														rechNew.name = kun.name;
														rechNew.vorname = kun.vorname;
														rechNew.strasse = kun.strasse;
														rechNew.plz = kun.plz;
														rechNew.ort = kun.ort;
												}
												rechNew.status = CONSTANTS.ENTWURF;
												rechNew.betrag = 0;
												rechNew.rechdat = input.rechdat != 0 ? input.rechdat : getDatum(0);
												rechNew.faelligdat = input.faelligdat != 0 ? input.faelligdat : getDatum(10);
												rechNew.leistungszeitraum = input.leistungszeitraum;
												rechNew.m1dat = input.m1dat;
												rechNew.m2dat = input.m2dat;
												rechNew.m3dat = input.m3dat;
												rechNew.mahnstufe = input.mahnstufe;
												rechNew.rechtyp = input.rechtyp;
												rechNew.objektnr = input.objektnr;
												rechNew.txtprj = input.txtprj;
												rechNew.txtbeschreibung = input.txtbeschreibung;
												rechNew.txtobj = input.txtobj;
												rechNew.txtkunde = input.txtkunde;
												rechNew.kostenstelle = input.kostenstelle;
												rechNew.bezug = input.bezug;
												MainDM.rechnungenDM.push(rechNew);
												// -----------------------------------
												if (template != 0) {
														let akttemplate = MainDM.zahltemplates.find(el => el.id == parseInt(template));
														item.rechtyp = akttemplate.rechtyp;
														addPositionen(newid, input.betrag, akttemplate.name, newidrec);
												}
												// -----------------------------------
												calcData(MainDM);
												if (SQL) {
														datenablage.insertDataSet("vertrag", item,);
														datenablage.insertDataSet("rechnung", rechNew);
												}
												if (USEFILES) {
														datenmodell.setIdNew('Rechnungen', rechNew);
														datenmodell.setIdNew('Vertraege', item);
												}
												MainDM = dm2msg();
												MainDM.success = "Rechnung angelegt";
												mainWindow.webContents.send('reload', MainDM);
												//anzeige.show = input.bezug == 'true' ? "RNBOPL" : "rechnung";
												anzeige.show = "rechnung";
												anzeige.rechnungid = newidrec;
												mainWindow.webContents.send('display:set', anzeige);
												break;
										case "Rechnung":
												let rechid = newRechnung(input);
												MainDM = dm2msg();
												mainWindow.webContents.send('reload', MainDM);
												anzeige.rechnungid = rechid;
												anzeige.show = "rechnungedit"
												mainWindow.webContents.send('display:set', anzeige);
												break;
										case "Erstattung Reservierung":
												for (let i = 0; i < rechDM.length; i++) {
														let id = parseFloat(rechDM[i].id);
														if (newid <= id) {
																newid = id + 1
														}
												}
												item.id = newid;
												item.betreff = input.betreff;
												item.vertrag = input.vertrag;
												item.name = input.name;
												item.vorname = input.vorname;
												item.strasse = input.strasse;
												item.plz = input.plz;
												item.ort = input.ort;
												item.status = input.status;
												item.betrag = input.betrag;
												item.rechnr = input.rechnr;
												item.rechdat = input.rechdat;
												item.faelligdat = input.faelligdat;
												//console.log("Set Rechnung");
												datenmodell.setIdNew('Rechnungen', item);
												posid = selectRechnungsPositionen(input.vertrag, newid, CONSTANTS.ERSTATTUNG);
												genRechnungZE(newid, posid);

												MainDM = dm2msg();
												calcData(MainDM);
												mainWindow.webContents.send('reload', MainDM);

												anzeige.rechnungid = newid;
												anzeige.show = "vertrag"
												mainWindow.webContents.send('display:set', anzeige);
												break;
										case "Reservierungsgebühr":
												for (let i = 0; i < rechDM.length; i++) {
														let id = parseFloat(rechDM[i].id);
														if (newid <= id) {
																newid = id + 1
														}
												}
												item.id = newid;
												item.betreff = input.betreff;
												item.vertrag = input.vertrag;
												item.name = input.name;
												item.vorname = input.vorname;
												item.strasse = input.strasse;
												item.plz = input.plz;
												item.ort = input.ort;
												if (typeof input.kunde != 'undefined') {
														item.kunde = input.kunde;
														kun = MainDM.kundenDM.find(el => el.id == parseInt(item.kunde));
														if (typeof kun !== 'undefined') {
																item.anrede = kun.anrede;
																item.name = kun.name;
																item.vorname = kun.vorname;
														}
												}
												item.status = input.status;
												item.betrag = input.betrag;
												item.rechnr = input.rechnr;
												item.rechdat = input.rechdat;
												item.faelligdat = input.faelligdat;
												datenmodell.setIdNew('Rechnungen', item);
												posid = selectRechnungsPositionen(input.vertrag, newid, CONSTANTS.GEBUEHR);
												genRechnungZE(newid, posid);

												MainDM = dm2msg();
												calcData(MainDM);
												mainWindow.webContents.send('reload', MainDM);

												anzeige.rechnungid = newid;
												anzeige.show = "vertrag"
												mainWindow.webContents.send('display:set', anzeige);
												break;
										case "Kunde":
												let kundeid = newKunde(input);
												MainDM = dm2msg();
												MainDM.success = "Erfolgreich angelegt.";
												mainWindow.webContents.send('reload', MainDM);
												anzeige.kundeid = kundeid;
												mainWindow.webContents.send('display:set', anzeige);


												break;
										case "Buchung":
												item.id = nextRefNr();
												item.bdat = input.bdat;
												item.vdat = input.vdat;
												item.text = input.text;
												item.pn = input.pn;
												item.konto = parseInt(input.konto);
												item.gegenkonto = input.gegenkonto;
												item.betrag = input.betrag;
												item.quelle = input.quelle;
												datenmodell.setIdNew('Buchungen', item);
												MainDM = dm2msg();
												calcData(MainDM);
												mainWindow.webContents.send('reload', MainDM);

												break;
										case "Projekt":
												let prjid = newProject(input);
												MainDM = dm2msg();
												calcData(MainDM);
												mainWindow.webContents.send('reload', MainDM);
												anzeige.projektid = prjid;
												anzeige.show = "projekt"
												mainWindow.webContents.send('display:set', anzeige);
												break;
										default:
								}
						}
						datenmodell.releaseLock(MainDM.user);
						setTitle();
				}
				if (SQL) {
						if (typeof input !== 'undefined') {
								switch (input.type) {
										case "Kunde":
												let kundeid = newKunde(input);
												convdm2msg().then(msg => {
														MainDM = msg;
														MainDM.success = "Erfolgreich angelegt.";
														mainWindow.webContents.send('reload', MainDM);
														anzeige.kundeid = kundeid;
														mainWindow.webContents.send('display:set', anzeige);
												});
												break;
										default:
								}
						}
						setTitle();

				}
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);
		}
}
function newProject(input) {
		let newid = 0;
		for (let i = 0; i < MainDM.projekteDM.length; i++) {
				let id = parseFloat(MainDM.projekteDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		input.status = CONSTANTS.AKTIV;
		input.id = newid;
		datenmodell.setIdNew('Projekte', input);

		return newid;

}
function newObject(item) {
		let newid = 0;
		if (typeof MainDM.objekteDM !== 'undefined' && MainDM.objekteDM !== 'undefined') {
				let cLen = MainDM.objekteDM.length;
				for (i = 0; i < cLen; i++) {
						let id = parseFloat(MainDM.objekteDM[i].id);
						if (newid <= id) {
								newid = id + 1
						}
				}
				item.id = newid;
		} else {
				// Allererster Aufruf
				item.id = 1;
				MainDM.objekteDM = new Array();
		}
		MainDM.objekteDM.push(item);
		datenmodell.setIdNew('Objekte', item);

		return newid;

}
function changeZE(item) {
		if (item.id == '-1') {
				newZahlungseingang(item);
				datenmodell.reload();
				MainDM = dm2msg();
				calcData(MainDM);
		} else {
				ze = MainDM.zeDM.find(el => el.id == item.id);
				changeProperty(ze, item, 'rechnung', 'Zahlungseingang');
				changeProperty(ze, item, 'text', 'Zahlungseingang');
				changeProperty(ze, item, 'datum', 'Zahlungseingang');
				changeProperty(ze, item, 'betrag', 'Zahlungseingang');
				calcData(MainDM);
				if (SQL) {
						datenablage.setIdOnly('zahlungseingang', ze);
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Zahlungseingaenge', ze, item.id);
				}
		}
}
function changeVertrag(item) {
		let vertrag = MainDM.vertragDM.find(ver => ver.id == item.id);
		changeProperty(vertrag, item, 'anrede', 'Vertrag');
		changeProperty(vertrag, item, 'name', 'Vertrag');
		changeProperty(vertrag, item, 'vorname', 'Vertrag');
		let neuerBetrag = changeProperty(vertrag, item, 'betrag', 'Vertrag');
		changeProperty(vertrag, item, 'urnr', 'Vertrag');
		changeProperty(vertrag, item, 'gegenstand', 'Vertrag');
		changeProperty(vertrag, item, 'mwst', 'Vertrag');
		changeProperty(vertrag, item, 'urdat', 'Vertrag');
		changeProperty(vertrag, item, 'notariat', 'Vertrag');
		changeProperty(vertrag, item, 'fdat1', 'Vertrag');
		changeProperty(vertrag, item, 'fdat2', 'Vertrag');
		changeProperty(vertrag, item, 'abdat', 'Vertrag');
		changeProperty(vertrag, item, 'udat', 'Vertrag');
		changeProperty(vertrag, item, 'sdat', 'Vertrag');
		let neuerKunde = changeProperty(vertrag, item, 'kunde', 'Vertrag');
		changeProperty(vertrag, item, 'firma', 'Vertrag');
		changeProperty(vertrag, item, 'vernr', 'Vertrag');
		changeProperty(vertrag, item, 'objekt', 'Vertrag');
		changeProperty(vertrag, item, 'projekt', 'Vertrag');
		changeProperty(vertrag, item, 'status', 'Vertrag');
		changeProperty(vertrag, item, 'typ', 'Vertrag');

		if (vertrag.typ == CONSTANTS.RESERVIERUNG && neuerKunde != 0) {
				eintragenKunde(item);
		}
		let objDM = MainDM.objekteDM;
		obj = objDM.find(el => el.id == vertrag.objekt);
		if (typeof obj != 'undefined' && vertrag.typ != CONSTANTS.VERTRAGALLG) {
				changeProperty(obj, item, 'kunde', 'Objekt');
				item.kunde = item.kaeufer;
				changeProperty(obj, item, 'kunde', 'Objekt');
		}
		if (vertrag.typ == CONSTANTS.KAUFVERTRAG) {
				changeProperty(obj, item, 'betrag', 'Objekt');
				if (neuerBetrag != 0) {
						recalcPositions(item.id, item.betrag);
				}
		}
		calcData(MainDM);
		if (SQL) {
				datenablage.setIdOnly('vertrag', vertrag);

				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');

				if (vertrag.typ != CONSTANTS.VERTRAGALLG) {
						anzeige.vertragid = item.id;
						anzeige.show = "vertrag";

				} else {
						anzeige.show = "rechnung";
				}
				mainWindow.webContents.send('display:set', anzeige);
		}
		if (USEFILES) {
				datenmodell.setIdOnly('Objekte', obj, vertrag.objekt);
				datenmodell.setIdOnly('Vertraege', vertrag, vertrag.id);
		}

}
function changeObjekt(item) {
		let obj = MainDM.objekteDM.find(el => el.id == item.id);
		changeProperty(obj, item, 'objnummer', 'Objekt');
		changeProperty(obj, item, 'text', 'Objekt');
		changeProperty(obj, item, 'projekt', 'Objekt');
		changeProperty(obj, item, 'betrag', 'Objekt');
		changeProperty(obj, item, 'status', 'Objekt');
		datenmodell.setIdOnly('Objekte', obj, obj.id);
}
function changeKunde(item) {
		let kunden = MainDM.kundenDM;
		let kunde = kunden.find(el => el.id == item.id);
		changeProperty(kunde, item, 'name', 'Kunde');
		changeProperty(kunde, item, 'vorname', 'Kunde');
		changeProperty(kunde, item, 'strasse', 'Kunde');
		changeProperty(kunde, item, 'plz', 'Kunde');
		changeProperty(kunde, item, 'ort', 'Kunde');
		changeProperty(kunde, item, 'telefon', 'Kunde');
		changeProperty(kunde, item, 'telefon2', 'Kunde');
		changeProperty(kunde, item, 'email', 'Kunde');
		changeProperty(kunde, item, 'name2', 'Kunde');
		changeProperty(kunde, item, 'vorname2', 'Kunde');
		changeProperty(kunde, item, 'strasse2', 'Kunde');
		changeProperty(kunde, item, 'plz2', 'Kunde');
		changeProperty(kunde, item, 'ort2', 'Kunde');
		changeProperty(kunde, item, 'telefon3', 'Kunde');
		changeProperty(kunde, item, 'telefon4', 'Kunde');
		changeProperty(kunde, item, 'email2', 'Kunde');
		changeProperty(kunde, item, 'anrede', 'Kunde');
		changeProperty(kunde, item, 'anrede2', 'Kunde');
		changeProperty(kunde, item, 'status', 'Kunde');
		changeProperty(kunde, item, 'kundennummer', 'Kunde');
		changeProperty(kunde, item, 'kunde', 'Kunde');
		changeProperty(kunde, item, 'lieferant', 'Kunde');
		changeProperty(kunde, item, 'notar', 'Kunde');
		changeProperty(kunde, item, 'mitarbeiter', 'Kunde');
		if (SQL) {
				datenablage.setIdOnly('kunde', kunde);
		}

		if (USEFILES) {
				datenmodell.setIdOnly('Kunden', kunde, item.id);
		}
}
function newKunde(input) {
		let newid = 0;
		let item = new Object();
		for (let i = 0; i < MainDM.kundenDM.length; i++) {
				let id = parseFloat(MainDM.kundenDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		item.kundennummer = input.kundennummer;
		item.name = input.name;
		item.vorname = input.vorname;
		item.strasse = input.strasse;
		item.plz = input.plz;
		item.ort = input.ort;
		item.name2 = input.name2;
		item.vorname2 = input.vorname2;
		item.strasse2 = input.strasse2;
		item.plz2 = input.plz2;
		item.ort2 = input.ort2;
		item.telefon = input.telefon;
		item.telefon2 = input.telefon2;
		item.telefon3 = input.telefon3;
		item.telefon4 = input.telefon4;
		item.email = input.email;
		item.email2 = input.email2;
		item.kunde = input.kunde;
		item.lieferant = input.lieferant;
		item.notar = input.notar;
		item.mitarbeiter = input.mitarbeiter;
		item.anrede = input.anrede;
		item.anrede2 = input.anrede2;
		item.status = CONSTANTS.AKTIV;
		item.datchange = logdatum();
		item.user = user;
		if (SQL) {
				datenablage.getmaxID('kunde').then(msg => {
						item.id = msg;
						datenablage.insertDataSet('kunde', item);
				});

		}
		if (USEFILES) {
				item.id = newid;
				datenmodell.setIdNew('Kunden', item);
		}
		return newid;

}
function newVertrag(input) {
		let newid = 0;
		let item = new Object();

		cLen = MainDM.vertragDM.length;
		for (i = 0; i < cLen; i++) {
				let id = parseFloat(MainDM.vertragDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		item.name = input.name;
		item.status = input.status;
		item.id = newid;
		item.objekt = input.objekt;
		item.projekt = input.projekt;
		item.typ = input.typ;
		if (typeof input.kunde != 'undefined') {
				item.kunde = input.kunde;
				// kun = MainDM.kundenDM.find(el => el.id == parseInt(item.kunde));
				// if (typeof kun !== 'undefined') {
				// 	item.anrede = kun.anrede;
				// 	item.name = kun.name;
				// 	item.vorname = kun.vorname;
				// }
		}
		item.betrag = input.betrag;
		item.vernr = input.vernr;
		switch (item.typ) {
				case CONSTANTS.DARLEHEN:
						item.darbet = input.darbet;
						item.zins = input.zins;
						item.text = input.text;
						item.sdat = input.sdat;
						break;
				default:
		}

		datenmodell.setIdNew('Vertraege', item);

		return newid;

}
function newZahlungseingang(item) {
		let newid = 0;
		if (typeof MainDM.zeDM !== 'undefined' && MainDM.zeDM !== 'undefined') {
				let cLen = MainDM.zeDM.length;
				for (i = 0; i < cLen; i++) {
						let id = parseFloat(MainDM.zeDM[i].id);
						if (newid <= id) {
								newid = id + 1
						}
				}
				item.id = newid;
		} else {
				// Allererster Aufruf
				item.id = 1;
				MainDM.zeDM = new Array();
		}
		if (SQL) {
				datenablage.insertDataSet('zahlungseingang', item);
		}
		if (USEFILES) {
				datenmodell.setIdNew('Zahlungseingaenge', item);
		}

}
function newPosition(item) {
		let newid = 0;
		let cLen = MainDM.positionenDM.length;
		for (i = 0; i < cLen; i++) {
				let id = parseFloat(MainDM.positionenDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		item.id = newid;

		if (SQL) {
				datenablage.insertDataSet('positionenver', item);
		}
		if (USEFILES) {
				datenmodell.setIdNew('Positionen', item);
		}

}
function newRechnung(input) {
		let newid = 0;
		let item = new Object();
		for (let i = 0; i < MainDM.rechnungenDM.length; i++) {
				let id = parseFloat(MainDM.rechnungenDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		item.id = newid;
		item.betreff = input.betreff;
		item.vertrag = input.vertrag;
		item.anrede = input.anrede;
		item.name = input.name;
		item.vorname = input.vorname;
		item.strasse = input.strasse;
		item.plz = input.plz;
		item.ort = input.ort;
		item.status = input.status;
		item.betrag = input.betrag;
		item.leistungszeitraum = input.leistungszeitraum;
		item.kostenstelle = input.kostenstelle;
		item.txtprj = input.txtprj;
		item.txtbeschreibung = input.txtbeschreibung;
		item.txtobj = input.txtobj;
		item.txtkunde = input.txtkunde;
		item.rechnr = input.rechnr;
		item.rechdat = input.rechdat;
		item.faelligdat = input.faelligdat;
		datenmodell.setIdNew('Rechnungen', item);
		return newid;

}
function changeProperty(oldobject, newobject, property, category) {
		let status = 0;
		if (typeof newobject[property] !== 'undefined') {
				if (typeof oldobject[property] == 'undefined' | oldobject[property] != newobject[property]) {

						//max = changelog.get("max");
						//max++;
						//changelog.set("max", max);
						//changelog.set(max, logdatum() + ' ' + user + ' ' + category + ' ' + oldobject.id + ' ' + property + ' ' + oldobject[property] + '>>' + newobject[property]);
						oldobject[property] = newobject[property];
						oldobject.datchange = logdatum();
						oldobject.user = user;
						status = 1;
				}

		}
		return status;
}
function logdatum() {
		let jetzt = new Date();
		let str = "";
		let da = jetzt.getDate();
		let mo = jetzt.getMonth() + 1;
		let ye = jetzt.getFullYear();
		let ho = jetzt.getHours();
		let mi = jetzt.getMinutes();
		let se = jetzt.getSeconds();
		if (se < 10) { se = "0" + se; }
		if (mi < 10) { mi = "0" + mi; }
		if (ho < 10) { ho = "0" + ho; }
		if (da < 10) { da = "0" + da; }
		if (mo < 10) { mo = "0" + mo; }
		str = da + "." + mo + "." + ye + " " + ho + ":" + mi + ":" + se;
		return str;
}
function initDatenmodell() {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let projekte = datenmodell.get('Projekte');
				let zahltemplates = datenmodell.get('Zahltemplate');
				let userDM = datenmodell.get('Benutzer');
				let zeDM = datenmodell.get('Zahlungseingaenge')
				let anzeige = store.get('anzeige');
				let anrede = datenmodell.get('Anrede');
				let vertragstyp = datenmodell.get('Vertragstyp');
				let vertragstypkrz = datenmodell.get('VertragstypKz');
				let rechnrtyp = datenmodell.get('RechNrTyp');
				let buchungen = datenmodell.get('Buchungen');
				let conf = store.get('conf');
				if (typeof zahltemplates == 'undefined' || zahltemplates == 'undefined') {
						let zahltemplates = new Array();
						let zahltemplate = new Object();
						let positionen = new Array();
						zahltemplate.id = 1;
						zahltemplate.name = CONSTANTS.POSMABV;
						zahltemplate.bezeichnung = "Neuer Kaufvertrag";
						positionen[0] = {
								"nr": "10",
								"text": "Sicherheitseinbehalt",
								"anteil": -5
						};
						positionen[1] = {
								"nr": "20",
								"text": "Nach Beginn der Erdarbeiten",
								"anteil": 30
						};
						positionen[2] = {
								"nr": "30",
								"text": "Nach Rohbaufertigstellung einschließlich Zimmererarbeiten",
								"anteil": 28
						};
						positionen[3] = {
								"nr": "40",
								"text": "Für die Herstellung der Dachflächen und Dachrinnen",
								"anteil": 5.6
						};
						positionen[4] = {
								"nr": "50",
								"text": "Für die Rohinstallation der Heizungsanlagen",
								"anteil": 2.1
						};
						positionen[5] = {
								"nr": "60",
								"text": "Für die Rohinstallation der Sanitäranlagen",
								"anteil": 2.1
						};
						positionen[6] = {
								"nr": "70",
								"text": "Für die Rohinstallation der Elektroanlagen",
								"anteil": 2.1
						};
						positionen[7] = {
								"nr": "80",
								"text": "Für den Fensterbau, einschließlich der Verglasung",
								"anteil": 7
						};
						positionen[8] = {
								"nr": "90",
								"text": "Für den Innenputz, ausgenommen Beiputzarbeiten",
								"anteil": 4.2
						};
						positionen[9] = {
								"nr": "100",
								"text": "Für den Estrich",
								"anteil": 2.1
						};
						positionen[10] = {
								"nr": "110",
								"text": "Für die Fliesenarbeiten im Sanitärbereich",
								"anteil": 2.8
						};
						positionen[11] = {
								"nr": "120",
								"text": "Nach Bezugsfertigkeit und Zug um Zug gegen Besitzübergabe",
								"anteil": 8.4
						};
						positionen[12] = {
								"nr": "130",
								"text": "Für die Fassadenarbeiten",
								"anteil": 2.1
						};
						positionen[13] = {
								"nr": "140",
								"text": "Nach vollständiger Fertigstellung",
								"anteil": 3.5
						};
						positionen[14] = {
								"nr": "150",
								"text": "Erstattung Sicherheitseinbehalt",
								"anteil": 5
						};

						zahltemplate.positionen = positionen;
						zahltemplates.push(zahltemplate);

						let zahltemplate2 = new Object();
						let positionen2 = new Array();
						zahltemplate2.id = 2;
						zahltemplate2.name = CONSTANTS.POSRESERVIERUNG;
						positionen2[0] = {
								"nr": "1",
								"text": "Reservierungsgebühr",
								"anteil": 0,
								"betrag": 5000
						};
						positionen2[1] = {
								"nr": "2",
								"text": "Erstattung der Reservierungsgebühr",
								"anteil": 0,
								"betrag": -5000
						};
						zahltemplate2.positionen = positionen2;
						zahltemplates.push(zahltemplate2);
						mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);
						datenmodell.set('Zahltemplate', zahltemplates);


				}
				zahltemplates = datenmodell.get('Zahltemplate');
				let templateVertragTyp3 = zahltemplates.find(el => el.name == CONSTANTS.POSTYP3);
				if (typeof templateVertragTyp3 == 'undefined') {
						let zahltemplate = new Object();
						let positionen = new Array();
						zahltemplate.id = 3;
						zahltemplate.name = CONSTANTS.POSTYP3;
						positionen[0] = {
								"nr": "10",
								"text": "Brandschutz",
								"anteil": 0,
								"betrag": 10000
						};
						positionen[1] = {
								"nr": "20",
								"text": "Pos2",
								"anteil": 0,
								"betrag": 10000
						};
						positionen[2] = {
								"nr": "30",
								"text": "Position I - IV",
								"anteil": 0,
								"betrag": 10000
						};
						positionen[3] = {
								"nr": "40",
								"text": "Position V - VIII",
								"anteil": 0,
								"betrag": 10000
						};
						zahltemplate.positionen = positionen;
						zahltemplates.push(zahltemplate);

						mainWindow.webContents.send('updateDM:zahltemplates', zahltemplates);

						let promiseSetZahltemplate2 = new Promise(function (myResolve, myReject) {
								let status = 0;
								datenmodell.set('Zahltemplate', zahltemplates);
								if (status == 0) {
										myResolve("OK");
								} else {
										myReject("Error");
								}
						});
				}
				if (typeof userDM !== 'undefined') {
						userDM.forEach(user => {
								if (typeof user.name == 'undefined') {
										user.name = "ERROR";
										user.status = CONSTANTS.TRASH;
										datenmodell.setIdOnly('Benutzer', user, user.id)
								}
						});
				}
				// if (typeof userDM == 'undefined' ||
				// 	userDM.filter(el => el.status != CONSTANTS.TRASH).length == 0 ||
				// 	typeof userDM.filter(el => el.status != CONSTANTS.TRASH) == 'undefined' ||
				// 	userDM.filter(el => el.status != CONSTANTS.TRASH) == 'undefined') {
				// 	let users = new Array();
				// 	let firstuser = new Object();
				// 	firstuser.id = 1;
				// 	firstuser.name = user;
				// 	firstuser.admin = true;
				// 	firstuser.password = bcrypt.hashSync(user, 10);
				// 	//firstuser.password = user;
				// 	users.push(firstuser);
				// 	datenmodell.set('Benutzer', users);
				// }
				let dmversion = datenmodell.get('DMVersion');
				if (typeof dmversion == 'undefined') {
						datenmodell.set('DMVersion', 1000);
				}
				if (typeof zeDM == 'undefined' || zeDM == 'undefined') {
						let zeDM = new Array();
						datenmodell.set('Zahlungseingaenge', zeDM);
				}
				if (typeof anzeige == 'undefined' || anzeige == 'undefined') {
						store.set('anzeige', { rechnungid: 0, vertragid: 0, objektid: 0, kundeid: 0, projektid: 0, edit: false, show: "" });
				}
				if (typeof conf == 'undefined') {
						store.set('conf', { conf: "Default" });
				}
				if (typeof buchungen == 'undefined') {
						buchung = [{ id: 190, bdat: 0, vdat: 0, text: "", konto: 0, gegenkonto: 0, betrag: 0, quelle: "" }];
						datenmodell.set('Buchungen', buchung);
				}
				let projekt = projekte.find(el => el.id == 0);
				if (typeof projekt == 'undefined') {
						projekt = { id: 0, name: "Allgemeine Verträge", prjnummer: "INT", "status": CONSTANTS.AKTIV };
						datenmodell.setIdNew('Projekte', projekt);
				}
				anrede = ['geehrter Herr', 'geehrte Frau', 'geehrte Eheleute', 'geehrte Frau Dr.', 'geehrter Herr Dr.', 'geehrte Dres.', 'geehrte Familie', 'geehrte Frau Professor', 'geehrter Herr Professor'];
				vertragstyp = ['Kaufvertrag', 'Mehrkosten', 'Reservierung', 'Gutschrift', 'Erstattung', 'Gebühr', 'Architektenvertrag', 'Darlehen'];
				mwst = ['keine', '7%', '19%'];
				vertragstypkrz = ['KV', 'MK', 'RE', 'GU', 'ER', 'GB', 'AC', 'DA'];
				rechnrtyp = { 'KV': 'KPR', 'MK': 'MK', 'RE': 'RES', 'GU': 'GU', 'ER': 'ERST', 'GB': 'GEB', 'AC': 'AR', 'DA': 'DR' };
				saveme(rechnrtyp, anrede, vertragstyp, vertragstypkrz, mwst);
				checkTrash();
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
}
function saveme(rechnrtyp, anrede, vertragstyp, vertragstypkrz) {
		return new Promise((resolve, reject) => {
				datenmodell.set('RechNrTyp', rechnrtyp);
				datenmodell.set('Anrede', anrede);
				datenmodell.set('Vertragstyp', vertragstyp);
				datenmodell.set('VertragstypKz', vertragstypkrz);
				datenmodell.set('MwSt', mwst);
				const error = false;
				if (!error) {
						resolve();
				}
				else {
						reject('Error: Something went wrong!')
				}
		})
}
function setDatenbank() {

		//datenablage.close();
		getSQLData();

		global.datenablage = new Ablage({
				sql: sqlData,
				defaults: {}
		});
		checksql(showAlert = true).then(el => {
				datenablage.checktables();
				convdm2msg().then(msg => {
						MainDM = msg;
						mainWindow.reload();
				});

		}).catch(err => {
				SQL = false;
				let message = new Object();
				message.name = "Fehler der Datenbank: " + err.code
				message.detail = err;
				if (SQLprop) {
						errorDisplay(message);
				}

		});



}
async function checksql(showAlert) {
		try {
				await datenablage.check().then(rows => {
						someRows = rows;
				});
		}
		catch (error) {
				throw error;
		}

}
function getSQLData() {

		let statusct = 0;
		while (statusct < 10) {
				sqlData = openSQLParamFile();
				if (typeof sqlData.host != 'undefined' &&
						typeof sqlData.user != 'undefined' &&
						typeof sqlData.password != 'undefined' &&
						typeof sqlData.database != 'undefined' &&
						typeof sqlData.port != 'undefined'
				) {
						store.set('sql', sqlData);
						statusct = 0;
						break;
				}
				statusct += 1;
				let message = new Object();
				message.name = "In dieser Datei ist kein vollständiger Datensatz für eine Datenbankverbindung enthalten.";
				message.detail = "";
				errorDisplay(message);
		}
		if (statusct > 0) {
				app.quit();
		}
};

function loadDMFile() {
		let options = {
				title: 'Öffnen einer Datendatei',
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
		if (typeof selectedPaths !== 'undefined' && typeof selectedPaths[0] !== 'undefined') {
				let sp = selectedPaths[0];
				let ext = path.extname(sp);
				let pos = sp.lastIndexOf(ext);
				let splog = path.dirname(sp) + "\\Logfile.json";

				let storesp = sp;

				//				log(storesp);
				//if (typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
				//let relativename = path.relative(process.env.PORTABLE_EXECUTABLE_FILE, path.dirname(sp));
				//			let relativename = path.relative(path.dirname(process.env.PORTABLE_EXECUTABLE_FILE),path.dirname(storesp)); 
				//			log(process.env.PORTABLE_EXECUTABLE_FILE);
				//			log(relativename);
				//			storesp = path.join(relativename, path.basename(sp));
				//
				//				}
				//				log(storesp);

				anzeige = { rechnungid: 0, vertragid: 0, kundeid: 0, projektid: 0, edit: false, show: "" };
				store.set("anzeige", anzeige);
				store.set("datamodell", storesp);
				store.set("logfilename", splog);
				global.datenmodell = new Store({
						filename: sp,
						defaults: {
						}
				});


				msg = dm2msg();
				MainDM = msg;

				//changelog.loadnew(splog);
				mainWindow.webContents.send('reload', MainDM);

				// max = changelog.get("max");
				// if (typeof max == 'undefined' || max == 'undefined') {
				// 	changelog = new Store({
				// 		filename: splog,
				// 		defaults: { max: 0 }
				// 	});
				// }
				return 1;
		}
		return 0;
}
function import2MainDM(mode) {
		let dirname = "D:";
		let options = {
				title: "Auswählen",
				defaultPath: dirname,
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let newDM = new Object();
		let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
		if (typeof selectedPaths !== 'undefined') {
				let sp = selectedPaths[0];
				newDM = JSON.parse(fs.readFileSync(sp));
		}

		switch (mode) {
				case "Zahltemplates":
						MainDM.zahltemplates = newDM;

						if (SQL) {
								datenablage.set('ztemplate', newDM);
						}
						if (USEFILES) {
								datenmodell.set('Zahltemplate', newDM);
						}
						break;
				case "Anredeliste":
						MainDM.anredeliste = newDM;
						if (SQL) {

								setkonfigliste("anrede", newDM);

						}
						if (USEFILES) {
								datenmodell.set('Anrede', newDM);
						}
						break;
						break;
				default:
						return;
		}

		return;

}
function exportALL() {
		let options = {
				title: 'Exportieren nach....',
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let sp = dialog.showSaveDialogSync(mainWindow, options);
		if (typeof (sp) != 'undefined') {
				let ext = path.extname(sp);
				let data = {
						"Zahltemplate": DM.zahltemplates,
						"Zahlungseingaenge": DM.zeDM,
						"Rechnungen": DM.rechnungenDM,
						"Vertraege": DM.vertragDM,
						"Projekte": DM.projekteDM,
						"Kunden": DM.kundenDM,
						"Kommentare": DM.commentDM,
						"Objekte": DM.objekteDM,
						"Positionen": DM.positionenDM,
						"Benutzer": DM.benutzerDM,
						"DMVersion": DM.DMVersion,
						"Buchungen": DM.buchungen,
						"RechNrTyp": DM.rechnrtyp,
						"Anrede": DM.anredeliste,
						"Vertragstyp": DM.vertragstypen,
						"VertragstypKz": DM.vertragstypkz,

				}
				fs.writeFileSync(sp, JSON.stringify(data));

		}
		return 0;
}
function exportMainDM(mode) {
		let data = new Object();
		switch (mode) {
				case "Zahltemplates":
						data = JSON.stringify(MainDM.zahltemplates);
						break;
				case "Anredeliste":
						data = JSON.stringify(MainDM.anredeliste);
						break;
				case "Adressen":
						data = JSON.stringify(MainDM.kundenDM);
						break;
				default:
						return;
		}
		let options = {
				title: 'Exportieren nach....',
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let sp = dialog.showSaveDialogSync(mainWindow, options);

		if (typeof (sp) != 'undefined') {
				let ext = path.extname(sp);
				fs.writeFileSync(sp, data);

		}
		return 0;
}


async function updateDMZahltemplate() {
		let ztemplate = await datenablage.get('ztemplate');
		let vorlagen = await datenablage.get('vorlagen');
		for (i = 0; i < ztemplate.length; i++) {
				let vor = vorlagen.filter(el => el.ztemplate == ztemplate[i].id);
				if (typeof vor != 'undefined') {
						ztemplate[i].vorlagen = vor;
				}
		}
		let positionen = await datenablage.get('positionen');
		for (i = 0; i < ztemplate.length; i++) {
				let pos = positionen.filter(el => el.ztemplate == ztemplate[i].id);
				if (typeof pos != 'undefined') {
						ztemplate[i].positionen = pos;
				}
		}
		return ztemplate;
}
async function convdm2msg() {
		let msg = new Object();
		msg.rechnungenDM = await datenablage.get('rechnung');
		let vorlagen = await datenablage.get('vorlagenrech');
		for (i = 0; i < msg.rechnungenDM.length; i++) {
				let vor = vorlagen.filter(el => el.rechnung == msg.rechnungenDM[i].id);
				if (typeof vor != 'undefined') {
						msg.rechnungenDM[i].vorlagenrech = vor;
				}
		}
		msg.vertragDM = await datenablage.get('vertrag');
		msg.zahltemplates = await datenablage.get('ztemplate');
		vorlagen = await datenablage.get('vorlagen');
		for (i = 0; i < msg.zahltemplates.length; i++) {
				let vor = vorlagen.filter(el => el.ztemplate == msg.zahltemplates[i].id);
				if (typeof vor != 'undefined') {
						msg.zahltemplates[i].vorlagen = vor;
				}
		}
		let positionen = await datenablage.get('positionen');
		for (i = 0; i < msg.zahltemplates.length; i++) {
				let pos = positionen.filter(el => el.ztemplate == msg.zahltemplates[i].id);
				if (typeof pos != 'undefined') {
						msg.zahltemplates[i].positionen = pos;
				}
		}
		msg.zeDM = await datenablage.get('zahlungseingang');
		msg.positionenDM = await datenablage.get('positionenver');
		msg.projekteDM = await datenablage.get('projekt');
		msg.kundenDM = await datenablage.get('kunde');
		msg.objekteDM = await datenablage.get('objekt');
		msg.commentDM = await datenablage.get('kommentar');
		msg.benutzerDM = await datenablage.get('benutzer');
		msg.buchungen = await datenablage.get('buchung');
		msg.provisionDM = typeof datenablage.get('provision')

		let konf = await datenablage.get('konfig');

		let konftyp = konf.filter(el => el.typ == "Anrede");
		let typlist = new Array();
		konftyp.forEach(element => {
				typlist[element.obj] = element.value;
		});
		msg.anredeliste = typlist;

		konftyp = konf.filter(el => el.typ == "Vertragstyp");
		typlist = new Array();
		konftyp.forEach(element => {
				typlist[element.obj] = element.value;
		});
		msg.vertragstypen = typlist;

		konftyp = konf.filter(el => el.typ == "VertragstypKz");
		typlist = new Array();
		konftyp.forEach(element => {
				typlist[element.obj] = element.value;
		});
		msg.vertragstypkz = typlist;

		konftyp = konf.filter(el => el.typ == "RechNrTyp");
		let typobj = new Object();
		konftyp.forEach(element => {
				typobj[element.obj] = element.value;
		});
		msg.rechnrtyp = typobj;

		konftyp = konf.find(el => el.typ == "DMVersion");
		msg.DMVersion = konftyp.value;

		// ------------------

		//	msg.projekteDM = await datenmodell.get('Projekte');
		//	msg.kundenDM = await datenmodell.get('Kunden');
		//	msg.commentDM = await datenmodell.get('Kommentare');
		//	msg.objekteDM = await datenmodell.get('Objekte');
		//  msg.benutzerDM = datenmodell.get('Benutzer');

		// msg.anredeliste = await datenmodell.get('Anrede');
		// msg.vertragstypkz = await datenmodell.get('VertragstypKz');
		// msg.vertragstypen = await datenmodell.get('Vertragstyp');
		// msg.rechnrtyp = await datenmodell.get('RechNrTyp');
		// msg.DMVersion = await datenmodell.get('DMVersion');

		msg.conf = await store.get('conf');

		// let buchungenALL = await datenmodell.get('Buchungen');
		// if (typeof buchungenALL != 'undefined') {

		// 	let buchungenDM = buchungenALL.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
		// 	msg.buchungen = buchungenDM;
		// }
		// else {
		// 	msg.buchungen = new Array();
		// }

		calcData(msg);
		rechte(msg);
		return msg;
}

function dm2msg() {
		let msg = new Object();
		msg.zeDM = datenmodell.get('Zahlungseingaenge');
		msg.rechnungenDM = datenmodell.get('Rechnungen');
		msg.vertragDM = datenmodell.get('Vertraege');
		msg.projekteDM = datenmodell.get('Projekte');
		msg.kundenDM = datenmodell.get('Kunden');
		msg.commentDM = datenmodell.get('Kommentare');
		msg.zahltemplates = datenmodell.get('Zahltemplate');
		msg.positionenDM = datenmodell.get('Positionen');
		msg.objekteDM = datenmodell.get('Objekte');
		msg.anredeliste = datenmodell.get('Anrede');
		msg.vertragstypkz = datenmodell.get('VertragstypKz');
		msg.vertragstypen = datenmodell.get('Vertragstyp');
		msg.rechnrtyp = datenmodell.get('RechNrTyp');
		msg.benutzerDM = datenmodell.get('Benutzer');
		msg.DMVersion = datenmodell.get('DMVersion');
		msg.conf = store.get('conf');
		if (typeof msg.conf == 'undefined') {
				store.set('conf', { conf: "Default" });
				msg.conf = store.get('conf');
		}
		msg.provisionDM = typeof datenmodell.get('provisionen') != 'undefined' ? datenmodell.get('provisionen') : new Array();

		let buchungenALL = datenmodell.get('Buchungen');
		if (typeof buchungenALL != 'undefined') {
				let buchungenDM = buchungenALL.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
				msg.buchungen = buchungenDM;
		}
		else {
				msg.buchungen = new Array();
		}
		calcData(msg);
		rechte(msg);

		return msg;

}
function rechte(msg) {
		let benutzer;
		if (typeof msg.benutzerDM != 'undefined') {
				benutzer = msg.benutzerDM.find(el => el.name.toLowerCase() == user.toLowerCase());
		}
		if (typeof benutzer !== 'undefined') {
				msg.username = benutzer.name;
				if (typeof benutzer.admin != 'undefined' && benutzer.admin == true) {
						msg.roleadmin = true;
				} else {
						msg.roleadmin = false;
				}
				if (typeof benutzer.edit != 'undefined' && benutzer.edit == true) {
						msg.roleedit = true;
				} else {
						msg.roleedit = false;
				}
				if (typeof benutzer.leiter != 'undefined' && benutzer.leiter == true) {
						msg.roleleiter = true;
				} else {
						msg.roleleiter = false;
				}
				if (typeof benutzer.comment != 'undefined' && benutzer.comment == true) {
						msg.rolecomment = true;
				} else {
						msg.rolecomment = false;
				}

		} else {
				// hier kann es sein, dass der eingeloggte Benutzer ein DB öffnet in der er kein Benutzer ist!!!
				let message = new Object();
				message.name = "Benutzer in dieser Datenbank nicht eingetragen.";
				message.detail = "";
				errorDisplay(message);
		}
}
function onStartCheckDatenbank() {
		try {
				let options = {
						buttons: ["Neue Datenbank erstellen", "Existierende Datenbank laden"],
						message: "Datenbank",
						title: "Ich finde die Datenbank nicht...",
						detail: "Datei " + store.get('datamodell') + " gibt es nicht!\n\n",
						icon: path.join(__dirname, 'assets/icons/png/bandage.png')
				};

				let response = dialog.showMessageBoxSync(mainWindow, options);
				let status = false;
				switch (response) {
						case 0:
								status = newDMFile();
								setTitle();
								//mainWindow.setTitle(PRGNAME + "            " + user + "           " + store.get('datamodell'));

								break;
						case 1:
								status = loadDMFile();
								setTitle();
								mainWindow.setTitle(PRGNAME + "            " + user + "           " + store.get('datamodell'));

								break;
						default:
				}
				return status;
		} catch (err) {
				//console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}

}
function setDefaults() {
		let vertraege = [{ id: 1, name: "Der erste Vertrag" }];
		datenmodell.set("Vertraege", vertraege);
		let rechnungen = [{ id: 1, name: "Die erste Rechnung", "rechnr": "01", vorname: "", betrag: 0, status: CONSTANTS.TRASH }];
		datenmodell.set("Rechnungen", rechnungen);
		let kunden = [{ id: 1, name: "Nachname", vorname: "Vorname", "kundennummer": 1 }];
		datenmodell.set("Kunden", kunden);
		let projekte = [{ id: 1, name: "Erstes Projekt", prjnummer: "P1", "prjnummer": "1", "status": CONSTANTS.AKTIV }];
		datenmodell.set("Projekte", projekte);
		let kommentare = [{ id: 1 }];
		datenmodell.set("Kommentare", kommentare);
		let objekte = [{ id: 1, text: "Wohnung", objnummer: "01", projekt: 1 }];
		datenmodell.set("Objekte", objekte);
		let positionen = [{ id: 1 }];
		datenmodell.set("Positionen", positionen);
		let benutzer = [{ id: 1, name: user, admin: true, password: bcrypt.hashSync(user, 10) }];
		datenmodell.set("Benutzer", benutzer);

}
function newDMFile() {
		let options = {
				title: 'Neue Datendatei',
				buttonLabel: 'Datei anlegen',
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let sp = dialog.showSaveDialogSync(mainWindow, options);
		if (typeof (sp) != 'undefined') {
				let ext = path.extname(sp);
				let pos = sp.lastIndexOf(ext);
				//let splog = sp.substring(0, pos) + ".log";
				let splog = path.dirname(sp) + "\\Logfile.json";


				global.datenmodell = new Store({
						filename: sp,
						defaults: {

						}
				});
				// changelog = new Store({
				// 	filename: splog,
				// 	defaults: { max: 0 }
				// });
				setDefaults();
				initDatenmodell();
				calcData(MainDM);
				anzeige = { rechnungid: 0, vertragid: 0, kundeid: 0, projektid: 0, edit: false, show: "" };
				store.set("anzeige", anzeige);
				store.set("datamodell", sp);
				store.set("logfilename", splog);
				//	changelog.set("max", 0);
				datenmodell.loadnew(sp);
				//	changelog.loadnew(splog);
				MainDM = dm2msg();
				calcData(MainDM);
				mainWindow.webContents.send('reload', MainDM);
				return 1;
		}
		return 0;
}
function selectTemplateRec(pfad, titel, rechid, insert) {
		// 
		// Bei einer neuen Verlage steht im Titel text: "Neue Vorlage"
		// 
		//
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let rechnung = MainDM.rechnungenDM.find(el => parseInt(el.id) == parseInt(rechid));
				let pfadVorlage = "";
				let dirname = pfad == "" ? "D:" : path.dirname(pfad);
				let options = {
						title: titel,
						defaultPath: dirname,
						properties: ['openFile'],
						filters: [
								{ name: 'docx', extensions: ['docx'] },
								{ name: 'All Files', extensions: ['*'] }
						]
				};
				let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
				let filename = "", fdirname = "";
				if (typeof selectedPaths !== 'undefined') {
						let sp = selectedPaths[0];

						filename = path.basename(sp);
						let ext = path.extname(filename);
						let pos = filename.lastIndexOf(ext);
						filename = filename.substring(0, pos);

						fdirname = path.dirname(sp);

						pfadVorlage = sp;
				}
				if (typeof rechnung.vorlagenrech == 'undefined' | (typeof rechnung.vorlagenrech !== 'undefined' && rechnung.vorlagenrech.length == 0)) {
						insert = true;
				}

				if (typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
						let relativename = path.relative(process.env.PORTABLE_EXECUTABLE_FILE, path.dirname(pfadVorlage));
						pfadVorlage = path.join(relativename, path.basename(pfadVorlage));
				}


				if (insert != true) {
						vorlage = rechnung.vorlagenrech.find(el => el.text == titel);
						vorlage.pfad = pfadVorlage;
				}
				else {
						// Neue Vorlage hinzufügen falls eine Vorlage ausgewählt wurde
						if (pfadVorlage != "") {
								let vorneu = new Object();
								vorneu.pfad = pfadVorlage;
								vorneu.text = "Neue Vorlage";
								vorneu.text = path.basename(pfadVorlage, '.docx');
								if (typeof rechnung.vorlagenrech == 'undefined') {
										let vor = new Array();
										rechnung.vorlagenrech = vor;
								}
								rechnung.vorlagenrech.push(vorneu);
						}
						insert = true;
				}


				if (SQL) {
						if (insert == true) {
								let item = {
										"rechnung": rechid,
										"pfad": pfadVorlage,
										"text": filename
								};
								datenablage.insertDataSet("vorlagenrech", item);

						} else {
								let filter = {
										"text": titel,
										"rechnung": rechid
								};
								let fields = {
										"pfad": pfadVorlage,
										"text": filename
								};
								datenablage.updateDataFields("vorlagenrech", fields, filter);

						}
				}
				if (USEFILES) {
						datenmodell.setIdOnly('Rechnungen', rechnung, rechnung.id);
				}
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				let anzeige = store.get('anzeige');
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
}
function selectTemplate(titel, templatename, prjid) {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();

				let projekteDM = datenmodell.get('Projekte');
				let anzeige = store.get('anzeige');

				let templatepath = store.get(templatename);
				if (typeof templatepath == 'undefined') { templatepath = ""; }

				let dirname = path.dirname(templatepath);
				let options = {
						title: titel,
						defaultPath: dirname,
						properties: ['openFile'],
						filters: [
								{ name: 'docx', extensions: ['docx'] },
								{ name: 'All Files', extensions: ['*'] }
						]
				};
				let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
				if (typeof selectedPaths !== 'undefined') {
						let sp = selectedPaths[0];
						if (typeof process.env.PORTABLE_EXECUTABLE_FILE != 'undefined') {
								let relativename = path.relative(process.env.PORTABLE_EXECUTABLE_FILE, path.dirname(sp));
								sp = path.join(relativename, path.basename(sp));
						}

						if (typeof prjid == 'undefined') {
								store.set(templatename, sp);
						} else {
								let projekt = projekteDM.find(el => parseInt(el.id) == parseInt(prjid));
								let item = new Object();
								item[templatename] = sp;
								changeProperty(projekt, item, templatename, 'Projekt');
								datenmodell.setIdOnly('Projekte', projekt, prjid);
						}
				}
				MainDM = dm2msg();
				mainWindow.webContents.send('reload', MainDM);
				mainWindow.webContents.send('display:set', anzeige);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
}
function calcData(data) {
		let zahlungseingangDM = data.zeDM;
		let projekteDM = data.projekteDM;
		let positionenDM = data.positionenDM;
		let rechnungenDM = data.rechnungenDM;
		let rechnungen = data.rechnungenDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
		let vertragDM = data.vertragDM;
		let vertraege = data.vertragDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
		let objekteDM = data.objekteDM;
		let objekte = data.objekteDM.filter(el => el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);

		// ------------------
		let summe_zahlungseingang = 0;
		if (typeof (rechnungen) != 'undefined' && rechnungen != 'undefined' && rechnungen.length > 0) {
				let len = rechnungen.length
				for (re = 0; re < len; re++) {
						summe_zahlungseingang = 0;
						if (typeof (zahlungseingangDM) != 'undefined' && zahlungseingangDM != 'undefined') {
								let zahlungseingang = zahlungseingangDM.filter(el => parseInt(el.rechnung) == parseInt(rechnungen[re].id));
								if (typeof (zahlungseingang) !== 'undefined' && zahlungseingang.length > 0) {
										for (let i = 0; i < zahlungseingang.length; i++) {
												summe_zahlungseingang += zahlungseingang[i].betrag;
										}
								}
								rechnungen[re].zahlungseingang = summe_zahlungseingang;
						}
						let rechnungbetrag = 0;
						let rechnungnetto = 0;
						let positionen = positionenDM.filter(el => el.rechnung == rechnungen[re].id);
						for (po = 0; po < positionen.length; po++) {
								rechnungbetrag += positionen[po].betrag;
								rechnungnetto += positionen[po].netto;
						}
						rechnungen[re].betrag = rechnungbetrag;
						rechnungen[re].netto = rechnungnetto;
				}
		}
		if (vertraege.length > 0) {
				for (ve = 0; ve < vertraege.length; ve++) {
						let projekt = projekteDM.find(el => parseInt(el.id) == parseInt(vertraege[ve].projekt));
						let offenerbetrag = 0, vertragbetrag = 0, totaloffen = 0, bezahlt = 0, totalbezahlt = 0, totalverschickt = 0;
						let positionen = positionenDM.filter(re => re.vertrag == vertraege[ve].id && re.status !== CONSTANTS.TRASH && re.status !== CONSTANTS.ARCHIV);
						positionen.sort((a, b) => {
								let numA = a.rechnung;
								let numB = b.rechnung;
								return numA - numB;
						});
						let aktrecnr = 0, recNew = false, aktbezahlt = 0;
						for (po = 0; po < positionen.length; po++) {
								if (vertraege[ve].typ == CONSTANTS.RESERVIERUNG) {
										// Bei einer Reservierung werden nur
										// Positive BEträge gezählt
										if (positionen[po].betrag > 0) {
												vertragbetrag += positionen[po].betrag;
										}
								} else {
										vertragbetrag += positionen[po].betrag;
								}

								if (aktrecnr == positionen[po].rechnung) {
										recNew = false;
								} else {
										recNew = true;
										aktrecnr = positionen[po].rechnung;
								}
								bezahlt = 0;
								offenerbetrag = positionen[po].betrag;
								verschickt = 0;
								let rechzupos = rechnungenDM.find(el => el.id == positionen[po].rechnung);
								if (typeof rechzupos != 'undefined') {
										switch (vertraege[ve].typ) {
												case CONSTANTS.GUTSCHRIFT:
														// Gutschriften
														if (recNew == true) {

																if (typeof rechzupos.zahlungseingang !== 'undefined') {
																		aktbezahlt = rechzupos.zahlungseingang;
																} else {
																		aktbezahlt = 0;
																}
														}
														if (offenerbetrag > aktbezahlt) {
																bezahlt = aktbezahlt;
																offenerbetrag = positionen[po].betrag - aktbezahlt;
																verschickt = offenerbetrag;
																aktbezahlt = 0;
														} else {
																bezahlt = offenerbetrag;
																verschickt = 0;
																aktbezahlt = aktbezahlt - offenerbetrag;
																offenerbetrag = 0;
														}
														break;
												case CONSTANTS.RESERVIERUNG:
														if (typeof rechzupos.zahlungseingang !== 'undefined') {
																if (rechzupos.zahlungseingang > 0) {
																		bezahlt += rechzupos.zahlungseingang;
																} else {
																		offenerbetrag += rechzupos.zahlungseingang;
																}
														}

														break;
												default:
														// Alles andere
														if (recNew == true) {
																if (typeof rechzupos.zahlungseingang !== 'undefined') {
																		aktbezahlt = rechzupos.zahlungseingang;
																} else {
																		aktbezahlt = 0;
																}
														}

														if (aktbezahlt != 0) {
																if (offenerbetrag >= 0) {
																		if (offenerbetrag > aktbezahlt) {
																				bezahlt = aktbezahlt;
																				offenerbetrag = positionen[po].betrag - aktbezahlt;
																				verschickt = offenerbetrag;
																				aktbezahlt = 0;
																		} else {
																				bezahlt = offenerbetrag;
																				verschickt = 0;
																				aktbezahlt = aktbezahlt - offenerbetrag;
																				offenerbetrag = 0;
																		}
																} else {
																		// Wenn der offene Betrag negativ ist handelt es sich um
																		// einen Abzug. Dann wird das auf den aktuellen Betrag 
																		// angerechnet
																		bezahlt = offenerbetrag;
																		aktbezahlt = aktbezahlt - offenerbetrag;
																		offenerbetrag = 0;

																}
														} else {
																if (rechzupos.status == CONSTANTS.VERSCHICKT) {
																		verschickt = offenerbetrag;
																}
														}
										}
								}

								totalbezahlt += bezahlt;
								totaloffen += offenerbetrag;
								totalverschickt += verschickt;

								positionen[po].offenerbetrag = offenerbetrag;
								positionen[po].bezahlt = bezahlt;
								positionen[po].verschickt = verschickt;
								//	if (typeof projekt == 'undefined' || projekt.bearbeiter == user) {
								//datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
								//	}
						}

						vertraege[ve].betrag = vertragbetrag;
						vertraege[ve].offenerbetrag = totaloffen;
						vertraege[ve].bezahlt = totalbezahlt;
						vertraege[ve].verschickt = totalverschickt;
				}
		}
		// --------------------
		if (objekte.length > 0) {
				for (let i = 0; i < objekte.length; i++) {
						let projekt = projekteDM.find(el => parseInt(el.id) == parseInt(objekte[i].projekt));
						let kaufpreis = 0, whghaben = 0, rechoffen = 0, whgsoll = 0, mehrsoll = 0, mehrhaben = 0, gutschriften = 0, gutschriftenhaben = 0, reservierungbezahlt = 0,
								reservierungerstattet = 0, habengesamt = 0;
						vertraege = vertragDM.filter(el => parseInt(el.objekt) == parseInt(objekte[i].id) && el.status != CONSTANTS.TRASH && el.status != CONSTANTS.ARCHIV);
						if (typeof vertraege !== "undefined" && vertraege.length > 0) {
								for (let j = 0; j < vertraege.length; j++) {
										switch (parseInt(vertraege[j].typ)) {
												case CONSTANTS.KAUFVERTRAG:
														// Kaufvertrag
														kaufpreis += vertraege[j].betrag;
														whgsoll += vertraege[j].offenerbetrag;
														whghaben += vertraege[j].bezahlt;
														rechoffen += vertraege[j].verschickt;

														break;
												case CONSTANTS.MEHRKOSTEN:
														// Mehrkosten
														mehrsoll += vertraege[j].offenerbetrag;
														mehrhaben += vertraege[j].bezahlt;
														//rechoffen += vertraege[j].verschickt;
														break;
												case CONSTANTS.GUTSCHRIFT:
														// Mehrkosten
														gutschriften += vertraege[j].offenerbetrag;
														gutschriftenhaben += vertraege[j].bezahlt;
														break;
												case CONSTANTS.RESERVIERUNG:

														reservierungbezahlt += vertraege[j].bezahlt;
														reservierungerstattet += vertraege[j].offenerbetrag;
														break;
												default:
										}
								}
						} else {
								objekte[i].kunde = "";
						}
						if (kaufpreis == 0) {
								kaufpreis = objekte[i].betrag;
								whgsoll = objekte[i].betrag;
						}

						habengesamt = whghaben + mehrhaben - gutschriftenhaben;
						objekte[i].kaufpreis = kaufpreis;
						objekte[i].whgsoll = whgsoll;
						objekte[i].whghaben = whghaben;
						objekte[i].rechoffen = rechoffen;
						objekte[i].mehrsoll = mehrsoll;
						objekte[i].mehrhaben = mehrhaben;
						objekte[i].gutschriften = parseInt(Math.round(gutschriften * 100)) / 100;
						objekte[i].gutschriftenhaben = parseInt(Math.round(gutschriftenhaben * 100)) / 100;
						objekte[i].reservierungbezahlt = reservierungbezahlt;
						objekte[i].reservierungerstattet = reservierungerstattet;
						objekte[i].habengesamt = habengesamt;
						//datenmodell.setIdOnly('Objekte', objekte[i], objekte[i].id);

				}


		}
		// --------------------
		if (projekteDM.length > 0) {
				for (pr = 0; pr < projekteDM.length; pr++) {
						let kaufpreis = 0, whgsoll = 0, mehrsoll = 0, whghaben = 0, mehrhaben = 0, gutschriften = 0, gutschriftenhaben = 0, habengesamt = 0, rechoffen = 0;
						let objekte = objekteDM.filter(el => parseInt(el.projekt) == parseInt(projekteDM[pr].id));
						if (typeof objekte !== "undefined" && objekte.length > 0) {
								for (let i = 0; i < objekte.length; i++) {
										if (objekte[i].status !== CONSTANTS.TRASH && objekte[i].status !== CONSTANTS.ARCHIV) {
												kaufpreis += objekte[i].kaufpreis;
												whgsoll += objekte[i].whgsoll;
												whghaben += objekte[i].whghaben;
												rechoffen += objekte[i].rechoffen;
												mehrsoll += objekte[i].mehrsoll;
												mehrhaben += objekte[i].mehrhaben;
												gutschriften += objekte[i].gutschriften;
												gutschriftenhaben += objekte[i].gutschriftenhaben;
												habengesamt += objekte[i].habengesamt;
										}
								}
						}
						projekteDM[pr].kaufpreis = kaufpreis;
						projekteDM[pr].whgsoll = whgsoll;
						projekteDM[pr].whghaben = whghaben;
						projekteDM[pr].mehrsoll = mehrsoll;
						projekteDM[pr].mehrhaben = mehrhaben;
						projekteDM[pr].habengesamt = habengesamt;
						projekteDM[pr].rechoffen = rechoffen;
						projekteDM[pr].gutschriften = gutschriften;
						projekteDM[pr].gutschriftenhaben = gutschriftenhaben;
						//datenmodell.setIdOnly('Projekte', projekteDM[pr], projekteDM[pr].id);
				}
		}
}
function addPositionen(verID, betrag, typ, recID) {

		let vertrag = MainDM.vertragDM.find(el => el.id == verID);
		let zahltemplate = MainDM.zahltemplates.find(el => el.name == typ);
		let rechnung = MainDM.rechnungenDM.find(el => parseInt(el.id) == recID);
		if (typeof rechnung != 'undefined') {
				rechnung.vorlagenrech = zahltemplate.vorlagen;
		}

		if (typeof betrag == 'undefined' || isNaN(betrag)) {
				betrag = 0;
		}
		let newid = 0;
		for (let i = 0; i < MainDM.positionenDM.length; i++) {
				let id = parseFloat(MainDM.positionenDM[i].id);
				if (newid <= id) {
						newid = id + 1
				}
		}
		let positionen = zahltemplate.positionen;
		positionen.sort((a, b) => {
				let numA = parseInt(a.nr);
				let numB = parseInt(b.nr);
				return numA - numB;
		});
		for (i = 0; i < positionen.length; i++) {
				let position = new Object();
				position.vertrag = verID;
				position.id = newid;
				if (typeof recID != 'undefined') {
						position.rechnung = recID;
						position.rechpos = i + 1;
				}
				position.text = positionen[i].text.split("\\n").join("\n");
				let mwst = 0;
				if (typeof positionen[i].mwst != 'undefined') {
						position.mwst = positionen[i].mwst;
				}
				if (typeof positionen[i].faktor != 'undefined') {
						position.faktor = positionen[i].faktor;
				} else {
						position.faktor = 1;
				}

				if (positionen[i].anteil != "" && positionen[i].anteil != 0) {
						position.anteil = positionen[i].anteil;
						position.netto = position.anteil * betrag / 100;
						position.bmgBetrag = betrag;
						position.betrag = position.netto * (1 + mwst / 100)
				} else {
						position.netto = positionen[i].betrag;
						position.betrag = position.netto * (1 + mwst / 100)

						if (typ == CONSTANTS.POSRESERVIERUNG && position.betrag > 0) {
								vertrag.betrag = position.betrag;
								//console.log(vertrag);
								if (SQL) {
										datenablage.setIdOnly('vertrag', vertrag);
								}
								if (USEFILES) {
										//console.log(verID);

										datenmodell.setIdOnly('Vertraege', vertrag, verID);
								}
						}
				}
				if (SQL) {

				}
				if (USEFILES) {
						datenmodell.setIdNew('Positionen', position);
				}
				newid += 1;
		}

}
function recalcPositions(verID, betrag) {
		datenmodell.reload();
		MainDM = dm2msg();
		let positionenDM = datenmodell.get('Positionen');
		let positionen = positionenDM.filter(re => re.vertrag == verID);

		for (i = 0; i < positionen.length; i++) {
				positionen[i].betrag = positionen[i].anteil * betrag / 100;
				positionen[i].bmgBetrag = betrag;
				positionen[i].netto = positionen[i].betrag * (1 + positionen[i].mwst);
				datenmodell.setIdOnly('Positionen', positionen[i], positionen[i].id);
		}
		datenmodell.releaseLock(MainDM.user);
		setTitle();
}
function selectRechnungsPositionen(verID, recID, select) {
		let posDM = MainDM.positionenDM;
		let verDM = MainDM.vertragDM;
		let positionen = posDM.filter(re => re.vertrag == verID);
		let posid = 0;
		for (let pos = 0; pos < positionen.length; pos++) {
				switch (select) {
						case CONSTANTS.ERSTATTUNG:
								if (positionen[pos].betrag < 0) {
										positionen[pos].rechnung = recID;
										posid = positionen[pos].id;
								}
								break;
						case CONSTANTS.GEBUEHR:
								if (positionen[pos].betrag > 0) {
										positionen[pos].rechnung = recID;
										posid = positionen[pos].id;
								}
								break;
						default:

				}
				datenmodell.setIdOnly('Positionen', positionen[pos], positionen[pos].id);
		}
		MainDM = dm2msg();
		mainWindow.webContents.send('reload', MainDM);
		setTitle();
		return posid;

}
function genRechnungZE(recID, posid) {

		let posDM = MainDM.positionenDM;
		let position = posDM.find(re => re.id == posid);
		let recDM = MainDM.rechnungenDM;
		let rechnung = recDM.find(re => re.id == recID);
		let zeDM = MainDM.zeDM;
		let newid = 0;
		let item = new Object();
		if (typeof zeDM !== 'undefined' && zeDM !== 'undefined') {
				let cLen = zeDM.length;
				for (i = 0; i < cLen; i++) {
						let id = parseFloat(zeDM[i].id);
						if (newid <= id) {
								newid = id + 1
						}
				}
				item.id = newid;
		} else {
				// Allererster Aufruf
				item.id = 1;
				zeDM = new Array();

		}
		item.rechnung = recID;
		item.betrag = position.betrag;
		item.datum = getDatum(0);
		item.text = "Autom. erzeugt"
		rechnung.betrag = item.betrag;
		datenmodell.setIdNew('Zahlungseingaenge', item);
		//zeDM.push(item);
		mainWindow.webContents.send('updateDM:zeDM', zeDM);
		datenmodell.setIdOnly('Rechnungen', rechnung, recID);
		if (SQL) {
				datenablage.setIdOnly('rechnung', rechnung);
		}
		MainDM = dm2msg();
		calcData(MainDM);
		mainWindow.webContents.send('reload', MainDM);
}
function genReservierung(verID) {
		// ist das eine Reservierung....
		let verDM = datenmodell.get('Vertraege');
		let kunDM = datenmodell.get('Kunden');
		let ver = verDM.find(el => el.id == verID);
		let item = new Object();
		item.type = "Reservierungsgebühr";
		item.vertrag = ver.id;
		item.betreff = "Reservierungsgebühr";
		item.rechdat = getDatum(0);
		item.faelligdat = getDatum(0);
		item.status = CONSTANTS.BEZAHLT;
		item.rechnr = "Reservierungsgebühr";
		if (typeof ver.kunde != 'undefined') {
				let kunde = kunDM.find(el => el.id == ver.kunde);
				item.name = kunde.name;
				item.vorname = kunde.vorname;
				item.strasse = kunde.strasse;
				item.plz = kunde.plz;
				item.ort = kunde.ort;
		}

		editNew(item);

}
function eintragenKunde(vertrag) {
		let kunDM = datenmodell.get('Kunden');
		let kunde = kunDM.find(el => el.id == parseInt(vertrag.kunde));
		let rechDM = datenmodell.get('Rechnungen');
		let rechnungen = rechDM.filter(el => el.vertrag == vertrag.id);
		if (typeof rechnungen != 'undefined') {
				for (let re = 0; re < rechnungen.length; re++) {
						rechnungen[re].name = kunde.name;
						rechnungen[re].vorname = kunde.vorname;
						rechnungen[re].strasse = kunde.strasse;
						rechnungen[re].plz = kunde.plz;
						rechnungen[re].ort = kunde.ort;
						datenmodell.setIdOnly('Rechnungen', rechnungen[re], rechnungen[re].id);
						rechDM = datenmodell.get('Rechnungen');
						mainWindow.webContents.send('updateDM:rechnungenDM', rechDM);
				}
		}

}
function checkTrash() {
		let vertragDM = datenmodell.get('Vertraege');
		let recDM = datenmodell.get('Rechnungen');
		let zeDM = datenmodell.get('Zahlungseingaenge');
		let posDM = datenmodell.get('Positionen');
		let vertraege = vertragDM.filter(el => el.status == CONSTANTS.TRASH);

		for (let ve = 0; ve < vertraege.length; ve++) {
				let rechnungen = recDM.filter(el => el.vertrag == vertraege[ve].id && el.status != CONSTANTS.TRASH);
				for (let re = 0; re < rechnungen.length; re++) {
						rechnungen[re].status = CONSTANTS.TRASH;
						datenmodell.setIdOnly('Rechnungen', rechnungen[re], rechnungen[re].id);
						let ze = zeDM.filter(el => el.rechnung == rechnungen[re].id && el.status != CONSTANTS.TRASH);
						if (typeof ze != 'undefined') {
								for (let i = 0; i < ze.length; i++) {
										ze[i].status = CONSTANTS.TRASH;
										datenmodell.setIdOnly('Zahlungseingaenge', ze[i], ze[i].id);
								}
						}
				}
				let positionen = posDM.filter(el => el.vertrag == vertraege[ve].id && el.status != CONSTANTS.TRASH);
				for (let po = 0; po < positionen.length; po++) {
						positionen[po].status = CONSTANTS.TRASH;
						datenmodell.setIdOnly('Positionen', positionen[po], positionen[po].id);
				}
		}
}
function emptyTrash() {
		try {
				waitforlock();
				datenmodell.reload();
				MainDM = dm2msg();
				let rechnungenDM = MainDM.rechnungenDM;
				for (let re = rechnungenDM.length - 1; re >= 0; re -= 1) {
						if (rechnungenDM[re].status == CONSTANTS.TRASH) {
								rechnungenDM.splice(re, 1);
						}
				}
				datenmodell.set('Rechnungen', rechnungenDM);

				let verDM = MainDM.vertragDM;
				for (let ve = verDM.length - 1; ve >= 0; ve -= 1) {
						if (verDM[ve].status == CONSTANTS.TRASH) {
								verDM.splice(ve, 1);
						}
				}
				datenmodell.set('Vertraege', verDM);

				let objDM = MainDM.objekteDM;
				for (let ob = objDM.length - 1; ob >= 0; ob -= 1) {
						if (objDM[ob].status == CONSTANTS.TRASH) {
								objDM.splice(ob, 1);
						}
				}
				datenmodell.set('Objekte', objDM);

				let zeDM = MainDM.zeDM;
				for (let ze = zeDM.length - 1; ze >= 0; ze -= 1) {
						if (zeDM[ze].status == CONSTANTS.TRASH) {
								zeDM.splice(ze, 1);
						}
				}
				datenmodell.set('Zahlungseingaenge', zeDM);

				let posDM = MainDM.positionenDM;
				for (let po = posDM.length - 1; po >= 0; po -= 1) {
						if (posDM[po].status == CONSTANTS.TRASH) {
								posDM.splice(po, 1);
						}
				}
				datenmodell.set('Positionen', posDM);

				let kundenDM = MainDM.kundenDM;
				for (let ku = kundenDM.length - 1; ku >= 0; ku -= 1) {
						if (kundenDM[ku].status == CONSTANTS.TRASH) {
								kundenDM.splice(ku, 1);
						}
				}
				datenmodell.set('Kunden', kundenDM);

				let projekteDM = MainDM.projekteDM;
				for (let pr = projekteDM.length - 1; pr >= 0; pr -= 1) {
						if (projekteDM[pr].status == CONSTANTS.TRASH) {
								projekteDM.splice(pr, 1);
						}
				}
				datenmodell.set('Projekte', projekteDM);
				MainDM = dm2msg();
				MainDM.success = "Gelöscht";
				mainWindow.webContents.send('reload', MainDM);
				datenmodell.releaseLock(MainDM.user);
				setTitle();
		} catch (err) {
				console.error(err);
				fs.writeFileSync("ERROR.log", err.message + "\r\n" + err.stack);

		}
}
function nextRefNr() {
		let buchungen = datenmodell.get('Buchungen');
		let recmax = 0;
		for (i = 0; i < buchungen.length; i++) {
				if (buchungen[i].id > recmax) {
						recmax = buchungen[i].id;
				}
		}
		let jetzt = new Date();
		let ye = jetzt.getFullYear();
		let offset = (ye - 2000) * 10000;
		if (recmax > offset) {
				return recmax + 1;
		} else {
				return offset + 1;
		}
}
function exportprj(prjid) {
		let data = new Object();

		data.projekt = MainDM.projekteDM.find(el => el.id == prjid);
		data.objekte = MainDM.objekteDM.filter(el => el.projekt == prjid);
		data.vertraege = MainDM.vertragDM.filter(el => el.projekt == prjid);
		data.rechnungen = MainDM.rechnungenDM.filter(el => {
				let found = false;
				for (let i = 0; i < data.vertraege.length; i++) {
						if (data.vertraege[i].id === el.vertrag) {
								found = true;
								break;
						}
				}
				return found;
		});
		data.positionen = MainDM.positionenDM.filter(el => {
				let found = false;
				for (let i = 0; i < data.vertraege.length; i++) {
						if (data.vertraege[i].id === el.vertrag) {
								found = true;
								break;
						}
				}
				return found;
		});
		data.ze = MainDM.zeDM.filter(el => {
				let found = false;
				for (let i = 0; i < data.rechnungen.length; i++) {
						if (data.rechnungen[i].id === el.rechnung) {
								found = true;
								break;
						}
				}
				return found;
		});

		data.kunden = MainDM.kundenDM.filter(el => {
				let found = false;
				for (let i = 0; i < data.vertraege.length; i++) {
						if (parseInt(data.vertraege[i].kunde) === parseInt(el.id)) {
								found = true;
								break;
						}
				}
				for (let i = 0; i < data.objekte.length; i++) {
						if (parseInt(data.objekte[i].kunde) === parseInt(el.id)) {
								found = true;
								break;
						}
				}
				return found;
		});

		data = JSON.stringify(data);

		let options = {
				title: 'Dateiname wählen',
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let sp = dialog.showSaveDialogSync(mainWindow, options);

		if (typeof (sp) != 'undefined') {
				let ext = path.extname(sp);
				fs.writeFileSync(sp, data);

		}
		return 0;

}

function importprj() {
		let dirname = "D:";
		let options = {
				title: "Auswählen",
				defaultPath: dirname,
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		};
		let data = new Object();
		let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
		if (typeof selectedPaths !== 'undefined') {
				let sp = selectedPaths[0];
				data = JSON.parse(fs.readFileSync(sp));
		}

		prjid = newProject(data.projekt)
		//console.log(prjid);
		let kunmap = new Object();
		let vermap = new Object();
		let rechmap = new Object();

		data.objekte.forEach(obj => {
				let objidOLD = obj.id;
				obj.projekt = prjid;
				let objidNEW = newObject(obj)
				let ov = data.vertraege.filter(el => el.objekt == objidOLD);
				if (typeof obj.kunde != "undefined" && obj.kunde != "") {
						kunmap[obj.kunde] = 0;
				}
				ov.forEach(ver => {
						let veridOLD = ver.id;
						ver.projekt = prjid;
						ver.objekt = objidNEW;
						let veridNew = newVertrag(ver);
						MainDM = dm2msg();
						let or = data.rechnungen.filter(el => el.vertrag == veridOLD);
						vermap[veridOLD] = veridNew;
						or.forEach(rech => {
								let rechidOLD = rech.id;
								rech.vertrag = veridNew;
								let rechidNew = newRechnung(rech);
								rechmap[rechidOLD] = rechidNew;
								MainDM = dm2msg();
						});
				});
		});
		data.positionen.forEach(pos => {
				let veroldid = pos.vertrag;
				let recholdid = pos.rechnung;
				pos.vertrag = vermap[veroldid];
				if (typeof recholdid != 'undefined') {
						pos.rechnung = rechmap[recholdid];
				}
				newPosition(pos);
		});
		data.ze.forEach(ze => {
				let recholdid = ze.rechnung;
				ze.rechnung = rechmap[recholdid];
				newZahlungseingang(ze);
		});
		data.kunden.forEach(kunde => {
				console.log("-------------");

				console.log("kunde.id ", kunde.id, " kundennr ", kunde.kundennummer, "Change " + kunde.datchange);
				let aktkun = MainDM.kundenDM.find(el => el.kundennummer == kunde.kundennummer && el.kundennummer != "");
				if (typeof aktkun != 'undefined') {
						console.log("akt ", aktkun.id, " kundennr ", aktkun.kundennummer, "Change " + aktkun.datchange);
						if (kunde.datchange > aktkun.datchange) {
								console.log("import neuer");
								kunmap[kunde.id] = aktkun.id;
								kunde.id = aktkun.id;
								changeKunde(kunde);
						} else {
								console.log("db neuer");
								kunmap[kunde.id] = aktkun.id;
								// Dann müssen wir die Kundenids umbiegen

						}
				} else {
						// Es gibt kein Kunde mit dieser Kundennummer
						console.log("Es gibt kein Kunde mit dieser Kundennummer");
						kunmap[kunde.id] = newKunde(kunde);
						MainDM = dm2msg();
				}
				console.log("-------------");

		});


		// Kundennummern in Verträgen und Objekten anpassen
		Object.keys(vermap).forEach(vid => {
				//console.log("vid ", vid);
				let ov = MainDM.vertragDM.filter(el => el.id == vermap[vid]);
				ov.forEach(ver => {
						if (typeof kunmap[ver.kunde] != 'undefined') {
								//console.log(ver);
								//console.log("kunde aendern: verid " + ver.id + " Kunde " + parseInt(ver.kunde) + " -> " + parseInt(kunmap[ver.kunde]));
								ver.kunde = parseInt(kunmap[ver.kunde]);
						} else {
								//console.log("Wird nicht geändert");
						}
						//changeVertrag(ver);
				});
		});

		MainDM = dm2msg();
		calcData(MainDM);
		mainWindow.webContents.send('reload', MainDM);
		mainWindow.webContents.send('display:set', store.get('anzeige'));

}
// ---------------- MENU --------------
const mainMenuTemplate = [
		{
				label: 'Datei',
				submenu: [
						//{
						//			label: 'Aktuelle Datenbank',
						//			click() {
						//	
						//								buttons: ["Ok"],
						//							message: "Datenbank",
						//							title: "Info Datenbank",
						//							detail: "Datei " + store.get('datamodell') + "\n\n Version " + datenmodell.get('DMVersion') + "\n\n",
						//							icon: path.join(__dirname, 'assets/icons/png/Folders-Folders-icon.png')
						//
						//											// Capital Suite Icons 
						//								};
						//
						//									dialog.showMessageBoxSync(mainWindow, options);
						//						}
						//			},
						//		{
						//			label: 'Neue Datendatei',
						//		click() {
						//				newDMFile();
						//				setTitle();
						//				//mainWindow.setTitle(PRGNAME + "            " + user + "           " + store.get('datamodell') + " ");
						//
						//							}
						//				},
						{
								label: 'Datendatei öffnen',
								click() {
										loadDMFile();
										setTitle();
										//mainWindow.setTitle(PRGNAME + "            " + user + "           " + store.get('datamodell'));
								}
						},
						{
								label: 'Vorlagen',
								click() {
										openWindowTemplateMABV();
								}
						},
						{
								label: 'Benutzer',
								click() {
										openWindowBenutzer();
								}
						},
						{
								label: 'Logout',
								accelerator: process.patform == 'darwin' ? 'Command+L' : 'Ctrl+L',
								click() {
										mainWindow.loadURL(url.format({
												pathname: path.join(__dirname, 'login.html'),
												protocol: 'file:',
												slashes: true
										}));
										mainWindow.setTitle(PRGNAME + " Login              Windowsuser: " + osuser);
										mainWindow.setBounds({ x: 0, y: 0, width: 1000, height: 650 });
										mainWindow.center();

								}
						},
						{
								label: 'Beenden',
								accelerator: process.patform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
								click() {
										app.quit();
								}
						}
				]

		},
		{
				label: 'Bearbeiten',
				submenu: [
						{
								label: 'Notiz',
								accelerator: process.patform == 'darwin' ? 'Command+k' : 'Ctrl+k',
								click() {
										mainWindow.webContents.send('comment:add', "");
								}
						},
						{ type: "separator" },
						{ label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
						{ label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
						{ label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" }
				]
		},
		{
				label: 'Hilfe',
				submenu: [
						{
								label: 'Version',
								click() {
										let options = {
												buttons: ["Ok"],
												message: "Invoice Tool",
												title: "Versioninfo",
												detail: "Version " + appVersion + "\n\n" + String.fromCharCode(169) + " 2021 Sibylle",
												icon: path.join(__dirname, 'assets/icons/png/invoice-icon.png')
										};
										dialog.showMessageBoxSync(mainWindow, options);
								}
						},
						{
								label: 'Releasenotes',
								click() {
										mainWindow.webContents.send('releasenotes:show', "");
								}
						},
						{
								label: 'Liste der Datenfelder',
								click() {
										execPath = path.dirname(app.getPath('exe'));
										//Geht nur in der gepackten Version
										let filename = path.join(process.resourcesPath, "../extraResources/Datenfelder.docx");
										openFileWithDefaultApp(filename);

								}
						},
						{
								label: 'Zahltemplates exportieren',
								click() {
										exportMainDM("Zahltemplates");
								}
						},
						{
								label: 'Zahltemplates importieren',
								click() {
										import2MainDM("Zahltemplates");
								}
						},
						//------------------------------------------------------
						// TEST Menüpunkte für Archivierung / Export
						// {
						// 	label: 'DEV: PRJEXP',
						// 	click() {
						// 		exportprj(3);
						// 	}
						// },
						// ,
						// {
						// 	label: 'DEV: PRJIMP',
						// 	click() {
						// 		importprj();
						// 	}
						// },
						//------------------------------------------------------
						// {
						//	 	label: 'Beispiel Tabelle Vertrag',
						//	 	click() {
						//	 		execPath = path.dirname(app.getPath('exe'));
						//	 		//Geht nur in der gepackten Version
						//	 		let filename = path.join(process.resourcesPath, "../extraResources/Vorlage_TabelleKV.docx");
						//	 		openFileWithDefaultApp(filename);
						//
						//			 	}
						//			 },
						// {
						// 	label: 'Recalc',
						// 	click() {
						// 		calcData();
						// 		let msg = new Object();
						// 		dm2msg(msg);
						// 		mainWindow.webContents.send('updateDM:all', msg);
						// 	}
						// },

				]

		}
];
if (SQL) {
		mainMenuTemplate.push({
				label: 'Datenbank',
				submenu: [
						// 		{
						// 			label: 'Datenbank anbinden',
						// 			click() {
						// 				setDatenbank();
						// 			}
						// 		},
						// 		{ type: "separator" },
						// 		{
						// 			label: 'Zahltemplates exportieren',
						// 			click() {
						// 				exportMainDM("Zahltemplates");
						// 			}
						// 		},
						// 		{
						// 			label: 'Zahltemplates importieren',
						// 			click() {
						// 				import2MainDM("Zahltemplates");
						// 			}
						// 		},
						// 		{ type: "separator" },
						// 		{
						// 			label: 'Anredeliste exportieren',
						// 			click() {
						// 				exportMainDM("Anredeliste");
						// 			}
						// 		},
						// 		{
						// 			label: 'Anredeliste importieren',
						// 			click() {
						// 				import2MainDM("Anredeliste");
						// 			}
						// 		},
						// 		{ type: "separator" },
						// 		{
						// 			label: 'DEV: Daten exportieren',
						// 			click() {
						// 				exportALL();
						// 			}
						// 		},
						{
								label: 'DEV: Daten übernehmen',
								click() {
										datenablage.set('rechnung', datenmodell.get('Rechnungen'));
										datenablage.set('vertrag', datenmodell.get('Vertraege'));
										datenablage.set('projekt', datenmodell.get('Projekte'));
										datenablage.set('objekt', datenmodell.get('Objekte'));
										datenablage.set('kunde', datenmodell.get('Kunden'));
										datenablage.set('provision', datenmodell.get('provisionen'));
										datenablage.set('buchung', datenmodell.get('Buchungen'));
										datenablage.set('benutzer', datenmodell.get('Benutzer'));
										datenablage.set('kommentar', datenmodell.get('Kommentare'));
										datenablage.set('ztemplate', datenmodell.get('Zahltemplate'));
										datenablage.set('zahlungseingang', datenmodell.get('Zahlungseingaenge'));
										datenablage.set('positionenver', datenmodell.get('Positionen'));

										let konfig = new Array();
										let id = 0;
										let werte = datenmodell.get('RechNrTyp')
										Object.keys(werte).forEach(key => {
												id++;
												konfig.push({ "id": id, "typ": "RechNrTyp", "obj": key, "value": werte[key] });

										});

										werte = datenmodell.get('Vertragstyp')
										Object.keys(werte).forEach(key => {
												id++;
												konfig.push({ "id": id, "typ": "Vertragstyp", "obj": key, "value": werte[key] });

										});

										werte = datenmodell.get('VertragstypKz')
										Object.keys(werte).forEach(key => {
												id++;
												konfig.push({ "id": id, "typ": "VertragstypKz", "obj": key, "value": werte[key] });

										});

										werte = datenmodell.get('Anrede')
										Object.keys(werte).forEach(key => {
												id++;
												konfig.push({ "id": id, "typ": "Anrede", "obj": key, "value": werte[key] });

										});

										werte = datenmodell.get('DMVersion')
										id++;
										konfig.push({ "id": id, "typ": "DMVersion", "obj": "Version", "value": werte });

										datenablage.set('konfig', konfig);


								}
						},
						{
								label: 'DEV: DM',
								click() {
										console.log('DM');
								}
						}
				]

		});

}
if (HELPMENU) {
		//	console.log(mainMenuTemplate[2].submenu);
		mainMenuTemplate[2].submenu.push(
				{
						label: 'Kontakt',
						click() {
								let text = "mailto:support@sig11.atlassian.net?subject=Support&body=Hallo Support,%0D%0A %0D%0A MfG%0D%0A %0D%0A";
								shell.openExternal(text);
						}
				}

		);
		//console.log(mainMenuTemplate[2].submenu);

}
if (process.platform == 'darwin') {
		mainMenuTemplate.unshift({});
}
if (process.env.NODE_ENV !== 'production') {
		mainMenuTemplate.push({
				label: 'Dev',
				submenu: [
						{
								label: 'Toggle DevTools',
								accelerator: process.patform == 'darwin' ? 'Command+I' : 'Ctrl+I',
								click(item, focusedWindow) {
										focusedWindow.toggleDevTools();
								}
						},
						{
								label: 'Daten reload',
								click() {
										MainDM = dm2msg();
										mainWindow.webContents.send('reload', MainDM);
								}
						},
						{
								role: 'reload'
						}
				]
		});
}

const workerMenuTemplate = [
		{
				label: 'Dev',
				submenu: [
						{
								label: 'Toggle DevTools',
								accelerator: process.patform == 'darwin' ? 'Command+I' : 'Ctrl+I',
								click(item, focusedWindow) {
										focusedWindow.toggleDevTools();
								}
						},
						{
								role: 'reload'
						}
				]
		}
];
const oplWinMenuTemplate = [
		{
				label: 'Datei',
				submenu: [
						{
								label: 'Beenden',
								accelerator: process.patform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
								click() {
										app.quit();
								}
						}
				]


		}
];
if (process.env.NODE_ENV !== 'production') {
		oplWinMenuTemplate.push({
				label: 'Dev',
				submenu: [
						{
								label: 'Toggle DevTools',
								accelerator: process.patform == 'darwin' ? 'Command+I' : 'Ctrl+I',
								click(item, focusedWindow) {
										focusedWindow.toggleDevTools();
								}
						},
						{
								role: 'reload'
						}
				]
		});
}
const BenutzerWindowMenuTemplate = [
		{
				label: 'Datei',
				submenu: [
						{
								label: 'Close Window',
								accelerator: process.patform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
								click() {
										BenutzerWindow.close();
										//app.quit();
								}
						}
				]


		}
];
if (process.env.NODE_ENV !== 'production') {
		BenutzerWindowMenuTemplate.push({
				label: 'Dev',
				submenu: [
						{
								label: 'Toggle DevTools',
								accelerator: process.patform == 'darwin' ? 'Command+I' : 'Ctrl+I',
								click(item, focusedWindow) {
										focusedWindow.toggleDevTools();
								}
						},
						{
								role: 'reload'
						}
				]
		});
}
const TemplateMABVWinMenuTemplate = [
		{
				label: 'Datei',
				submenu: [
						{
								label: 'Beenden',
								accelerator: process.patform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
								click() {
										app.quit();
								}
						}
				]


		}
];
if (process.env.NODE_ENV !== 'production') {
		TemplateMABVWinMenuTemplate.push({
				label: 'Dev',
				submenu: [
						{
								label: 'Toggle DevTools',
								accelerator: process.patform == 'darwin' ? 'Command+I' : 'Ctrl+I',
								click(item, focusedWindow) {
										focusedWindow.toggleDevTools();
								}
						},
						{
								role: 'reload'
						}
				]
		});
}

function openSQLParamFile() {
		let options = {
				title: 'Datenbankparameter einlesen',
				properties: ['openFile'],
				filters: [
						{ name: 'json', extensions: ['json'] },
						{ name: 'All Files', extensions: ['*'] }
				]
		}; let selectedPaths = dialog.showOpenDialogSync(mainWindow, options);
		if (typeof selectedPaths !== 'undefined' && typeof selectedPaths[0] !== 'undefined') {
				let sp = selectedPaths[0];
				return JSON.parse(fs.readFileSync(sp));
		}
		return;
}
function sendDM2mainWindow(msg) {
		//console.log("start wenn alles da ist ", msg.projekteDM);
		msg.user = user;
		rechte(msg);
		mainWindow.webContents.send('mainWindow:start', msg);
		// Version merken
		let lastVersion = store.get('lastVersion');
		let anzeige = store.get('anzeige');

		if (typeof anzeige == 'undefined' || anzeige == 'undefined') {
				store.set('anzeige', { rechnungid: 0, vertragid: 0, objektid: 0, kundeid: 0, projektid: 0, edit: false, show: "" });
				anzeige = store.get('anzeige');
		}

		if (typeof lastVersion == 'undefined' || parseFloat(lastVersion) < parseFloat(appVersion)) {
				store.set('lastVersion', appVersion)
				anzeige.neuesRelease = true;
		} else {
				anzeige.neuesRelease = false;
		}
		anzeige.neuesRelease = false; // wird nie angezeigt

		anzeige.suche = "";
		mainWindow.webContents.send('display:set', anzeige);
}
var wait = (ms) => {
		const start = Date.now();
		let now = start;
		while (now - start < ms) {
				now = Date.now();
		}
}
function waitforlock() {
		let waitforlock = true;
		let response = -1;
		let quit = false;
		let ct = 1;

		while (waitforlock) {

				if (response == 1) {
						datenmodell.releaseLock();
				}
				let message = datenmodell.getLock(MainDM.username);
				//	console.log(message);
				if (message == "ok") {
						MainDM.danger = "";
						MainDM.success = "";
						waitforlock = false;
				} else {
						MainDM.danger = "Sperrre wurde gesetzt durch " + message + ". Versuch " + ct;
						MainDM.success = "";
						mainWindow.webContents.send('reload', MainDM);
						wait(1000);
						ct += 1;
						if (ct > 8) {
								let options = {
										buttons: ["Exit", "Sperre entfernen"],
										message: "Datenbank",
										title: "Datenbank gesperrt",
										detail: "Die Datenbank ist gesperrt.\nSie können das Programm beenden oder die Sperre entfernen.\n",
										icon: path.join(__dirname, 'assets/icons/png/bandage.png')
								};
								response = dialog.showMessageBoxSync(mainWindow, options);
								if (response == 0) {
										waitforlock = false;
										quit = true;
								}
						}
				}

		}
		if (quit == true) {
				mainWindow.close();
				app.quit();
		}
}
function setTitle() {
		let ver = typeof MainDM.DMVersion != 'undefined' ? MainDM.DMVersion : datenmodell.get('DMVersion');
		if (USEFILES) {
				mainWindow.setTitle(PRGNAME + "            " + user + "          Datenfile: " + store.get('datamodell') + "     Version: " + ver + "     Apppath: " + process.env.PORTABLE_EXECUTABLE_FILE);
		}
		if (SQL) {
				let sqlData = store.get('sql');

				mainWindow.setTitle(PRGNAME + "            " + user + "          DB-Server: " + sqlData.host + "     Version: " + ver + "     Apppath: " + process.env.PORTABLE_EXECUTABLE_FILE);

		}
}
function debug(test){
		if (process.env.NODE_ENV !== 'production') {
				console.log(test);
		}
		//    fs.appendFileSync("D:\\debug.log", JSON.stringify(test)+'\n');
}
// EOF


/*
https://emea01.safelinks.protection.outlook.com/?url=https%3A%2F%2Faurelionimmobilien.sharepoint.com%2F%3Af%3A%2Fs%2FAurelionDateiablage%2FEly0ISz6CyNPnIXPAQr9Yy0Bto_8_c8OxYtpsfaOiYHzew%3Fe%3D5%253aNF2CkO%26at%3D9&data=04%7C01%7C%7C60ed80e4105a4585d08808d9c55fd73b%7C84df9e7fe9f640afb435aaaaaaaaaaaa%7C1%7C0%7C637757837686144348%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C3000&sdata=5WzrIWe0NzdEhyZTnFAJZuAAL5QS4eD43chWEZWQ7lA%3D&reserved=0

*/
