const electron = require('electron');
const { ById } = require('./modules/base');
const { ipcRenderer } = electron;
//const bcrypt = require('bcrypt');
const bcrypt = require('bcryptjs');

document.addEventListener('DOMContentLoaded', function (event) {
    ipcRenderer.send('WindowBenutzer:loaded', "0");
}, false);

document.addEventListener("click", function(){
    ById('notificationSuccess').classList.add('is-hidden');
    ById('notificationDanger').classList.add('is-hidden');

});

  
ipcRenderer.on('WindowBenutzer:start', function (e, msg) {
    global.benutzerDM = msg.benutzerDM;
    global.user = { id: msg.id, name: msg.user, osuser: msg.osuser, roleedit: msg.roleedit,roleleiter: msg.roleleiter, roleadmin: msg.roleadmin, rolecomment: msg.rolecomment };
    if (user.roleadmin) {
        ById('navBenutzer').classList.remove('is-hidden');
    } else {
        ById('navBenutzer').classList.add('is-hidden');
    }
});

ipcRenderer.on('updateDM:benutzerDM', function (e, msg) {
    //console.log(msg);
    global.benutzerDM = msg.benutzerDM;
    user.name = msg.user;
    user.osuser = msg.osuser;
    user.roleedit = msg.roleedit;
    user.roleadmin = msg.roleadmin;
    user.roleleiter = msg.roleleiter;
    user.rolecomment = msg.rolecomment;
    console.log(msg);
    if (msg.show == 'benutzer') {
        showTblBenutzer();
        ById('notificationSuccess').innerHTML = "Daten aktualisiert."
        ById('notificationSuccess').classList.remove('is-hidden');
    }
    if (msg.show == 'daten') {
        showmeineDaten();
        ById('notificationSuccess').innerHTML = "Daten aktualisiert."
        ById('notificationSuccess').classList.remove('is-hidden');
    }
    if (user.roleadmin) {
        ById('navBenutzer').classList.remove('is-hidden');
    } else {
        ById('navBenutzer').classList.add('is-hidden');
    }

});



ById('nav').addEventListener('click', function (event) {
    if (event.target.classList.contains('panel-block')) {
        switch (event.target.id) {
            case "navDaten":
                showmeineDaten()
                break;
            case "navBenutzer":
                if (user.roleadmin) {
                    showTblBenutzer()
                }
                break;
            default:

        }
    }
});

ById('btnBenutzerOK').addEventListener('click', function (event) {
    getDatafromTblBenutzer();
});

function getDatafromTblBenutzer() {
    let benutzer = benutzerDM.find(el => el.name.toLowerCase() == user.name.toLowerCase());

    let msg = new Object();

    let daten = new Array();
    msg.daten = daten;
    let trs = ById('tblBenutzerBody').childNodes;
    trs.forEach(tr => {
        let datensatz = new Object();
        let tds = tr.childNodes;
        tds.forEach(td => {
            if (td.classList.contains('ben-id')) {
                datensatz.id = td.innerHTML;
            }
            if (td.classList.contains('ben-name')) {
                datensatz.name = td.innerHTML;
            }
            if (td.classList.contains('ben-email')) {
                datensatz.email = td.innerHTML;
            }
            if (td.classList.contains('ben-password')) {
                datensatz.password = td.innerHTML;
            }
            if (td.classList.contains('ben-passwordreset')) {
                td.childNodes.forEach(ch => {
                    if (ch.nodeName == "INPUT") {
                        datensatz.password = bcrypt.hashSync(datensatz.name, 10);
                        //datensatz.password = datensatz.name;
                    }
                })
            }
            if (td.classList.contains('ben-roleedit')) {
                td.childNodes.forEach(ch => {
                    if (ch.nodeName == "INPUT") {
                        datensatz.edit = ch.checked;
                    }
                })
            }
            if (td.classList.contains('ben-roleadmin')) {
                td.childNodes.forEach(ch => {
                    if (ch.nodeName == "INPUT") {
                        datensatz.admin = ch.checked;
                    }
                })
            }
            if (td.classList.contains('ben-roleleiter')) {
                td.childNodes.forEach(ch => {
                    if (ch.nodeName == "INPUT") {
                        datensatz.leiter = ch.checked;
                    }
                })
            }
            if (td.classList.contains('ben-rolecomment')) {
                td.childNodes.forEach(ch => {
                    if (ch.nodeName == "INPUT") {
                        datensatz.comment = ch.checked;
                    }
                })
            }

        });

        if (datensatz.name.toLowerCase() == user.name.toLowerCase()) {
            if (datensatz.admin != benutzer.admin) {
                datensatz.admin = benutzer.admin;
                ById('notificationDanger').innerHTML = "Recht 'Admin' kann beim eigenen Benutzer nicht geändert werden!"
                ById('notificationDanger').classList.add('is-warning');
                ById('notificationDanger').classList.remove('is-hidden');
            } else {
                ById('notificationDanger').classList.add('is-hidden');
            }

        }
        if (datensatz.nr != "") {
            daten.push(datensatz);
        }
        //console.log(tr);
    });


    //console.log(msg);
    // ById('notificationAllg').classList.add('is-hidden');
    ipcRenderer.send('WindowBenutzer:update', msg);
}
ById('btnBenutzerCancel').addEventListener('click', function (event) {
    ById('notificationSuccess').classList.add('is-hidden');
    ById('notificationDanger').classList.add('is-hidden');
    ById('pnBenutzer').classList.add('is-hidden');
});
ById('btnBenutzerAdd').addEventListener('click', function () {
    let msg = new Object();
    ipcRenderer.send('WindowBenutzer:addLine', msg);
});

ById('btnDatenCancel').addEventListener('click', function (event) {
    ById('notificationSuccess').classList.add('is-hidden');
    ById('notificationDanger').classList.add('is-hidden');
    ById('pnDaten').classList.add('is-hidden');
});
ById('btnDatenOk').addEventListener('click', function (event) {
    ById('notification').classList.add('is-hidden');

    let benutzer = benutzerDM.find(el => el.name.toLowerCase() == user.name.toLowerCase());
    let change = 0;
    if (ById('inpDatenName').value != benutzer.name) {
        benutzer.name = ById('inpDatenName').value;
        change++;
    }
    if (ById('inpDatenEmail').value != benutzer.email) {
        benutzer.email = ById('inpDatenEmail').value;
        change++;
    }

    pass1 = ById('inpDatenPass1').value;
    pass2 = ById('inpDatenPass2').value;
    if (pass1 != "") {
        if (pass1 != pass2) {
            ById('notification').innerHTML = "Die Passworte stimmen nicht überein."
            ById('notification').classList.add('is-danger');
            ById('notification').classList.remove('is-hidden');
        } else {
            benutzer.password = bcrypt.hashSync(pass1, 10);
            //benutzer.password = pass1;
            change++;
        }
    }

    if (change) {
        ipcRenderer.send('WindowBenutzer:updateSingle', benutzer);
    }

});
ById('inpDatenName').addEventListener('focus', function (event) {
    ById('notification').innerHTML = "Mit diesem Usernamen kann man sich einloggen. <br><br> Verwendet man den Windowslogin-Namen startet das Programm ohne den Logindialog. <br> <br> Der aktuelle Loginnamen unter Windows lautet: " + user.osuser;
    ById('notification').classList.remove('is-danger');
    ById('notification').classList.remove('is-warning');
    ById('notification').classList.remove('is-hidden');
});

ById('inpDatenEmail').addEventListener('focus', function (event) {
    ById('notification').innerHTML = " ";
    ById('notification').classList.remove('is-danger');
    ById('notification').classList.remove('is-warning');
    ById('notification').classList.add('is-hidden');
});

function showmeineDaten() {
    ById('pnBenutzer').classList.add('is-hidden');
    ById('pnDaten').classList.remove('is-hidden');
    ById('notification').classList.add('is-hidden');

    let benutzer = benutzerDM.find(el => el.name.toLowerCase() == user.name.toLowerCase());
    let val = "";

    ById('inpDatenUserId').value = benutzer.id;
    val = typeof benutzer.name != 'undefined' ? benutzer.name : "";
    ById('inpDatenName').value = val;
    val = typeof benutzer.email != 'undefined' ? benutzer.email : "";
    ById('inpDatenEmail').value = val;
    ById('inpDatenPass1').value = "";
    ById('inpDatenPass2').value = "";
}

function showTblBenutzer() {
    ById('pnDaten').classList.add('is-hidden');
    ById('pnBenutzer').classList.remove('is-hidden');
    ById('tblBenutzerBody').innerHTML = "";
    benutzerDM.sort((a, b) => {
        let textA = a.name.toUpperCase();
        let textB = b.name.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;

    });

    for (i = 0; i < benutzerDM.length; i++) {
        let tr = document.createElement('tr');
        let str = "";
        str += "<td class='ben-id uneditable is-hidden'>" + benutzerDM[i].id + "</td>";
        str += "<td class='ben-name'>" + benutzerDM[i].name + "</td>";
        val = typeof benutzerDM[i].email != 'undefined' ? benutzerDM[i].email : "";
        str += "<td class='ben-email'>" + val + "</td>";
        checked = benutzerDM[i].edit ? "checked" : "";
        str += "<td class='ben-roleedit uneditable'> <input type='checkbox'" + checked + "></td>";
        checked = benutzerDM[i].leiter ? "checked" : "";
        str += "<td class='ben-roleleiter uneditable'> <input type='checkbox'" + checked + "></td>";
        checked = benutzerDM[i].admin ? "checked" : "";
        str += "<td class='ben-roleadmin uneditable'> <input type='checkbox'" + checked + "></td>";
        checked = benutzerDM[i].comment ? "checked" : "";
        str += "<td class='ben-rolecomment uneditable'> <input type='checkbox'" + checked + "></td>";

        if (typeof benutzerDM[i].password == 'undefined' || benutzerDM[i].password == 'undefined') {
            benutzerDM[i].password = bcrypt.hashSync(benutzerDM[i].name, 10);
            //benutzerDM[i].password = benutzerDM[i].name;
        }
        str += "<td class='ben-password uneditable is-hidden'>" + benutzerDM[i].password + "</td>";

        str += "<td class='ben-passwordreset uneditable'> <input type='checkbox'></td>";

        tr.innerHTML = str;
        ById('tblBenutzerBody').appendChild(tr);
    }
    $('#tblBenutzer').editableTableWidget().numericInputExample().find('td:first').focus();
    $('#tblBenutzer td.uneditable').on('change', function (evt, newValue) {
        return false;
    });
}
